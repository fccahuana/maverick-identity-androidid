package pe.interbank.maverick.identity.esb.api.model.leads;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LeadsResponseDto {
	private List<Lead> leads;
}
