package pe.interbank.maverick.identity.esb.api.databind;

import java.io.IOException;

import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonGenerator;

import pe.interbank.maverick.commons.esb.api.databind.EsbApiSerializer;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.Answer;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestions;

/**
 * @author everis
 *
 */
public class ValidationQuestionsBodySerializer extends EsbApiSerializer<ValidationQuestions> {
	private static final long serialVersionUID = -2811631616860012473L;

	private EsbApiProperties esbApiProperties;

	public ValidationQuestionsBodySerializer(EsbApiProperties esbApiProperties) {
		this.esbApiProperties = esbApiProperties;
	}

	@Override
	public void serialize(ValidationQuestions  request, JsonGenerator gen) throws IOException {
		gen.writeStartObject();
		serializeFindById(request, gen);
		gen.writeEndObject();		
	}
	
	private void serializeFindById(ValidationQuestions request, JsonGenerator gen) throws IOException {
		gen.writeStringField("operationId", request.getOperationId());
		gen.writeStringField("model", this.esbApiProperties.getValidationQuestions().getModel());
		if (!CollectionUtils.isEmpty(request.getQuestion())) {
			gen.writeArrayFieldStart("question");
			for(Answer answer: request.getQuestion()) {
				gen.writeStartObject();
				gen.writeStringField("category", answer.getCategory());
				gen.writeStringField("optionId", answer.getOptionId());
				gen.writeStringField("id", answer.getId());
				gen.writeEndObject();
			}
			gen.writeEndArray();
		}
	}
}
