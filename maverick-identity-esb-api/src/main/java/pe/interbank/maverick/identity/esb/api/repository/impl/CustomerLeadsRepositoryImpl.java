package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.CustomerLeadsBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.CustomerLeadsBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.CustomerLeadsRepository;



/**
 * 
 * @author everis
 *
 */

@Repository
public class CustomerLeadsRepositoryImpl implements CustomerLeadsRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public CustomerLeadsRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new CustomerLeadsBodySerializer(esbApiProperties))
				.deserializer(new CustomerLeadsBodyDeserializer())
				.endpoint(esbApiProperties.getCustomerLeads().getEndpoint())
				.methodType(esbApiProperties.getCustomerLeads().getMethodType())
				.thirdGeneration(esbApiProperties.getCustomerLeads().getThirdGeneration())
				.typeCertificate(esbApiProperties.getCustomerLeads().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String getLeads(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
