package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.GetQuestionsBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.GetQuestionsBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.GetQuestionsRepository;


/**
 * 
 * @author everis
 *
 */

@Repository
public class GetQuestionsRepositoryImpl implements GetQuestionsRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public GetQuestionsRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new GetQuestionsBodySerializer(esbApiProperties))
				.deserializer(new GetQuestionsBodyDeserializer())
				.endpoint(esbApiProperties.getGetQuestions().getEndpoint())
				.methodType(esbApiProperties.getGetQuestions().getMethodType())
				.thirdGeneration(esbApiProperties.getGetQuestions().getThirdGeneration())
				.typeCertificate(esbApiProperties.getGetQuestions().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String getQuestions(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
