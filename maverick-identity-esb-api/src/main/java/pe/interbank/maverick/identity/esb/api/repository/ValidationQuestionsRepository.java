package pe.interbank.maverick.identity.esb.api.repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestions;

/**
 * 
 * @author everis
 *
 */
public interface ValidationQuestionsRepository {
	
	String validationAnswers(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, ValidationQuestions validationQuestions);
}
