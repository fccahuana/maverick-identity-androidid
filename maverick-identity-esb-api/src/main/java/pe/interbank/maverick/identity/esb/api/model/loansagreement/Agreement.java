package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Agreement  {

	private String id;
	private String name;
	private String owner;
	private String customerFullName;
	private String currencyName;
	private String periodicity;
	private String disbursementDate;
	private String tea;
	private String tcea;
	private String itf;
	private TotalAmount totalAmount;
	private List<InstallmentPayment> installment;
	
	

}
