package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerResponse {

	private String id;
	private String firstName;
	private String secondName;
	private String lastName;
	private String secondLastName;
}
