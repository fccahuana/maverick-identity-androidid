package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoansAgreementResponseDto {

	private CustomerResponse customer;
	private List<CreditsResponse> credits;
}
