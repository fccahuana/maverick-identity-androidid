package pe.interbank.maverick.identity.esb.api.model.fraudlist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailPhoneResponse {

	private String reason;
	private String comment;
	private StatusDetailResponse status;
}
