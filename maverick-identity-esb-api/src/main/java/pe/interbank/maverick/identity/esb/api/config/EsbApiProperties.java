package pe.interbank.maverick.identity.esb.api.config;

import java.io.Serializable;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.esb.api.config.AbstractEsbApiProperties;

/**
 * @author acacerec
 */
@Getter
@Setter
@ConfigurationProperties("identity.esb.api")
public class EsbApiProperties extends AbstractEsbApiProperties implements Serializable {
	private static final long serialVersionUID = -5981613695555612208L;

	private GetInformationPersonService getinformationperson;

	private LoansAgreementService loansAgreement;
	
	private CustomerLeads customerLeads;
	
	private Lead lead;
	
	private GetQuestions getQuestions;
	
	private ValidationQuestions validationQuestions;

	private FraudList fraudList;

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public static class GetInformationPersonService extends Service {
		private static final long serialVersionUID = 7099246928423935204L;
	}

	public static class LoansAgreementService extends Service {
		private static final long serialVersionUID = 7099246928423935204L;

	}
	
	public static class CustomerLeads extends Service {
		private static final long serialVersionUID = 7099246928423935205L;
	}
	
	public static class Lead extends Service {
		private static final long serialVersionUID = 7099246928423935205L;
	}
	
	public static class GetQuestions extends Service {
		private static final long serialVersionUID = 7099246928423935205L;
	}
	
	public static class ValidationQuestions extends Service {
		private static final long serialVersionUID = 7099246928423935205L;
		private String model;
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
	}
	public static class FraudList extends Service {
		private static final long serialVersionUID = 7099246928423935205L;
	}

}
