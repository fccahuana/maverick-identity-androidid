package pe.interbank.maverick.identity.esb.api.model.validationquestions;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationQuestionResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3619848542416354935L;
	private String model;
	private Result result;
}
