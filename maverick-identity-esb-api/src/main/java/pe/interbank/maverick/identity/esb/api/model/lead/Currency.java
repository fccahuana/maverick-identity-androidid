package pe.interbank.maverick.identity.esb.api.model.lead;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Currency {

	private String id;
	private String name;
	private String symbol;
}
