package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InstallmentPayment implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2605961362176925817L;
	private String number;
	private String statusId;
	private String dueDate;
	private String amount;
	private String capitalBalance;
	private String pendingPayment;
	private String amortization;
	private String commission;
	private String compensatory;
	private String insuranceDesgravamen;
}
