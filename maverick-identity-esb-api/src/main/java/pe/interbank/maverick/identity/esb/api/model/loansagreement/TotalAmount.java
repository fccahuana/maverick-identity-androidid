package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TotalAmount implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5844052711668635532L;
	private String disbursement;
	private String amortization;
	private String insuranceDesgravamen;
	private String installment;
	private String pendingPayment;
	private String commission;
	private String compensatory;
	
}
