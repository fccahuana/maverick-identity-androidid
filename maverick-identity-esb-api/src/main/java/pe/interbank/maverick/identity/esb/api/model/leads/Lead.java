package pe.interbank.maverick.identity.esb.api.model.leads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Lead {

	private String id;
	private String amount;
	private String channelPriority;
	private Campaign campaign;
	private Offer offer;
	private Treatment treatment;
}
