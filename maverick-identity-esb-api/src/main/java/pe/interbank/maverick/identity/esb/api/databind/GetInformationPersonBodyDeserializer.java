package pe.interbank.maverick.identity.esb.api.databind;

import com.fasterxml.jackson.databind.JsonNode;

import pe.interbank.maverick.commons.esb.api.databind.EsbApiObjectDeserializer;
import pe.interbank.maverick.identity.esb.api.exception.IdentityEsbException;

/**
 * 
 * @author everis
 *
 */
public class GetInformationPersonBodyDeserializer extends EsbApiObjectDeserializer<String> {
	private static final long serialVersionUID = -2629542383935766593L;

	public GetInformationPersonBodyDeserializer() {
		super(String.class);
	}


	@Override
	protected String deserialize(JsonNode jsonNode) {
		if(jsonNode.isMissingNode())
			throw new IdentityEsbException();
		return  jsonNode.toString();
	}
}
