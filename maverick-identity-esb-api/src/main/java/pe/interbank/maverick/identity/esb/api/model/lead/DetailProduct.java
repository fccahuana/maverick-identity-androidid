package pe.interbank.maverick.identity.esb.api.model.lead;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.identity.esb.api.model.leads.Product;

@Getter
@Setter
public class DetailProduct extends Product{

}
