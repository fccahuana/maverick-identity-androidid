package pe.interbank.maverick.identity.esb.api.model.leads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Treatment {
	private String id;
	private String number;
	private String name;	
}
