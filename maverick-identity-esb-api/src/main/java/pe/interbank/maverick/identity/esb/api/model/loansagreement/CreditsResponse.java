package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditsResponse {

	private String id;
	private int companyId;
	private String agreementName;
	private DisbursementResponse disbursement;
	private InstallmentResponse installment;
	
}
