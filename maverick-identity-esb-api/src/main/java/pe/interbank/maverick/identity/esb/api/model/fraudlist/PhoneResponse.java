package pe.interbank.maverick.identity.esb.api.model.fraudlist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneResponse {

	private String type;
	private String description;
	private String prefix;
	private String number;
}
