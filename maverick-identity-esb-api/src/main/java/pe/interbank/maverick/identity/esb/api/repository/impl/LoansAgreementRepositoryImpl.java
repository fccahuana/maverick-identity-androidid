package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.LoansAgreementBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.LoansAgreementBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.LoansAgreementRepository;


/**
 * 
 * @author everis
 *
 */

@Repository
public class LoansAgreementRepositoryImpl implements LoansAgreementRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public LoansAgreementRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new LoansAgreementBodySerializer(esbApiProperties))
				.deserializer(new LoansAgreementBodyDeserializer())
				.endpoint(esbApiProperties.getLoansAgreement().getEndpoint())
				.methodType(esbApiProperties.getLoansAgreement().getMethodType())
				.thirdGeneration(esbApiProperties.getLoansAgreement().getThirdGeneration())
				.typeCertificate(esbApiProperties.getLoansAgreement().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String getLoansAgreement(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
