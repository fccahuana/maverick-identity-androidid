package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.LeadBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.LeadBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.LeadRepository;



/**
 * 
 * @author everis
 *
 */

@Repository
public class LeadRepositoryImpl implements LeadRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public LeadRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new LeadBodySerializer(esbApiProperties))
				.deserializer(new LeadBodyDeserializer())
				.endpoint(esbApiProperties.getLead().getEndpoint())
				.methodType(esbApiProperties.getLead().getMethodType())
				.thirdGeneration(esbApiProperties.getLead().getThirdGeneration())
				.typeCertificate(esbApiProperties.getLead().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String getDetailLead(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
