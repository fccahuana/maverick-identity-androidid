package pe.interbank.maverick.identity.esb.api.repository.impl;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.ValidationQuestionsBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.ValidationQuestionsBodySerializer;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestions;
import pe.interbank.maverick.identity.esb.api.repository.ValidationQuestionsRepository;


/**
 * 
 * @author everis
 *
 */

@Repository
public class ValidationQuestionsRepositoryImpl implements ValidationQuestionsRepository {
	private EsbApiClient<ValidationQuestions, String> esbApiClient;
	
	public ValidationQuestionsRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(ValidationQuestions.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new ValidationQuestionsBodySerializer(esbApiProperties))
				.deserializer(new ValidationQuestionsBodyDeserializer())
				.endpoint(esbApiProperties.getValidationQuestions().getEndpoint())
				.methodType(esbApiProperties.getValidationQuestions().getMethodType())
				.thirdGeneration(esbApiProperties.getValidationQuestions().getThirdGeneration())
				.typeCertificate(esbApiProperties.getValidationQuestions().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String validationAnswers(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, ValidationQuestions validationQuestions) {
		return esbApiClient.execute(consumeApiRequestDto, session, validationQuestions, null);
	}

}
