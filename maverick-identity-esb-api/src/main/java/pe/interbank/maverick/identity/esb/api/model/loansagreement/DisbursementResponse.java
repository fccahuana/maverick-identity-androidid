package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DisbursementResponse {

	private String currencyName;
	private String insuranceDesgravamen;
	private String tea;
	private String tcea;
	private String date;
	private String amount;
	private String capitalBalance;
	private String paidCapitalAmount;
}
