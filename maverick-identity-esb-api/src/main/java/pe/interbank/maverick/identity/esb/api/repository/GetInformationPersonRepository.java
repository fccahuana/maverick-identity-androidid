package pe.interbank.maverick.identity.esb.api.repository;

import java.util.Map;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;

/**
 * 
 * @author guillermo.nasi
 *
 */
public interface GetInformationPersonRepository {
	
	String getInformationPerson(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean);
}
