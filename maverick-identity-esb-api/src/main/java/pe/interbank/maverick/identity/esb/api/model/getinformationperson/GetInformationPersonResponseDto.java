package pe.interbank.maverick.identity.esb.api.model.getinformationperson;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetInformationPersonResponseDto {

	private String customerId;
	private String firstName;
	private String secondName;
	private String lastName;
	private String secondLastName;
	private List<PhoneResponse> telephones;
	private List<EmailResponse> emails;
}
