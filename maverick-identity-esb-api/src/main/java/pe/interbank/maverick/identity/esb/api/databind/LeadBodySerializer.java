package pe.interbank.maverick.identity.esb.api.databind;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;

import pe.interbank.maverick.commons.esb.api.databind.EsbApiSerializer;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;

/**
 * @author everis
 *
 */
@SuppressWarnings("rawtypes")
public class LeadBodySerializer extends EsbApiSerializer<Map> {
	private static final long serialVersionUID = -2811631616860012473L;

	@SuppressWarnings("unused")
	private EsbApiProperties esbApiProperties;

	public LeadBodySerializer(EsbApiProperties esbApiProperties) {
		this.esbApiProperties = esbApiProperties;
	}

	@Override
	public void serialize(Map  request, JsonGenerator gen) throws IOException {
		gen.writeStartObject();		
		gen.writeEndObject();		
	}
}
