package pe.interbank.maverick.identity.esb.api.model.fraudlist;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhonesResponse {

	private List<PhoneResponse> phone;
	private List<DetailPhoneResponse> detail;
}
