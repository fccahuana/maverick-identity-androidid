package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InstallmentResponse {
	private String paid;
	private String pending;
	private String total;
	private String amount;
	private String dueDate;
	private String nextDueDate;
}
