package pe.interbank.maverick.identity.esb.api.model.validationquestions;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Result implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6503263379563282623L;
	private String description;
	private String reason;
	private String reject;
	private String disapproved;
	private String unanswered;
}
