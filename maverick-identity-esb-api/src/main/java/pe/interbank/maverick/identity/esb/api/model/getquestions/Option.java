package pe.interbank.maverick.identity.esb.api.model.getquestions;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Option implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2717421103102612789L;
	private String id;
	private String description;
}
