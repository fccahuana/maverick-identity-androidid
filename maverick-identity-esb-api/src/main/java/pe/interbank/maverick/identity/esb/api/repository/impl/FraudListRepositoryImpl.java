package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.FraudListBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.FraudListBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.FraudListRepository;


/**
 * 
 * @author everis
 *
 */

@Repository
public class FraudListRepositoryImpl implements FraudListRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public FraudListRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new FraudListBodySerializer(esbApiProperties))
				.deserializer(new FraudListBodyDeserializer())
				.endpoint(esbApiProperties.getFraudList().getEndpoint())
				.methodType(esbApiProperties.getFraudList().getMethodType())
				.thirdGeneration(esbApiProperties.getFraudList().getThirdGeneration())
				.typeCertificate(esbApiProperties.getFraudList().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String validatePhoneInFraudList(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
