package pe.interbank.maverick.identity.esb.api.repository.impl;

import java.util.Map;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.EsbApiClient;
import pe.interbank.maverick.commons.esb.api.EsbApiClientBuilder;
import pe.interbank.maverick.identity.esb.api.config.EsbApiProperties;
import pe.interbank.maverick.identity.esb.api.databind.GetInformationPersonBodyDeserializer;
import pe.interbank.maverick.identity.esb.api.databind.GetInformationPersonBodySerializer;
import pe.interbank.maverick.identity.esb.api.repository.GetInformationPersonRepository;


/**
 * 
 * @author everis
 *
 */

@Repository
public class GetInformationPersonRepositoryImpl implements GetInformationPersonRepository {
	@SuppressWarnings("rawtypes")
	private EsbApiClient<Map, String> esbApiClient;
	
	public GetInformationPersonRepositoryImpl(EsbApiProperties esbApiProperties, RestTemplateBuilder restTemplateBuilder) {
		esbApiClient = new EsbApiClientBuilder<>(Map.class, String.class)
				.properties(esbApiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new GetInformationPersonBodySerializer(esbApiProperties))
				.deserializer(new GetInformationPersonBodyDeserializer())
				.endpoint(esbApiProperties.getGetinformationperson().getEndpoint())
				.methodType(esbApiProperties.getGetinformationperson().getMethodType())
				.thirdGeneration(esbApiProperties.getGetinformationperson().getThirdGeneration())
				.typeCertificate(esbApiProperties.getGetinformationperson().getTypeCertificate())
				.buildForObjectOutput();
	}

	@Override
	public String getInformationPerson(ConsumeApiRequestDto consumeApiRequestDto, CurrentSession session, Map<String, String> bean) {
		return esbApiClient.execute(consumeApiRequestDto, session, null, bean);
	}

}
