package pe.interbank.maverick.identity.esb.api.model.lead;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DetailLeadsResponseDto {
	private List<DetailLead> leads;
}
