package pe.interbank.maverick.identity.esb.api.model.getquestions;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Question implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5598936353189656229L;
	private String description;
	private String category;
	private String id;
	private List<Option> option;
}
