package pe.interbank.maverick.identity.esb.api.model.loansagreement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentScheduleAPIResponseDto {

	private String pageIndicator ;
	private Agreement agreement;
	
}
