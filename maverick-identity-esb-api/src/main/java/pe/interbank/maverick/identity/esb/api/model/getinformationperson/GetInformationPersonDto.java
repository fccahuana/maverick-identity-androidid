package pe.interbank.maverick.identity.esb.api.model.getinformationperson;


import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetInformationPersonDto {

	Map<String , String> parameter;
	
}
