package pe.interbank.maverick.identity.esb.api.model.getquestions;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionsResponseDto {

	private String operationId;
	private String reject;
	private String unanswered;
	private String disapproved;
	private List<Question> questions;
}
