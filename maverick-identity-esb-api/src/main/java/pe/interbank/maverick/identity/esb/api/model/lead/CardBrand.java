package pe.interbank.maverick.identity.esb.api.model.lead;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CardBrand {

	private String id;
	private String name;	
}
