package pe.interbank.maverick.identity.esb.api.exception;

import org.springframework.http.HttpStatus;

import pe.interbank.maverick.commons.esb.api.exception.EsbException;

/**
 * @author everis
 */
public class IdentityEsbException extends EsbException {

    private static final long serialVersionUID = 1805024473996646394L;

    public IdentityEsbException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, "03.01.12", "Unexpected error");
    }
}
