package pe.interbank.maverick.identity.esb.api.model.validationquestions;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Answer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1600879677174235010L;
	private String category;
	private String optionId;
	private String id;	

}
