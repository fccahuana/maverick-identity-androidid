package pe.interbank.maverick.identity.esb.api.model.lead;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Additional {

	private String item1;
	private String item2;
	private String item3;
	private String item4;
	private String item5;
	private String item6;
	private String item7;
	private String item8;
	private String item9;
	private String item10;
	private String item11;
	private String item12;
	private String item13;
	private String item14;
	private String item15;

}
