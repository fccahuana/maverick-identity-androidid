package pe.interbank.maverick.identity.esb.api.model.fraudlist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusDetailResponse {

	private String id;
}
