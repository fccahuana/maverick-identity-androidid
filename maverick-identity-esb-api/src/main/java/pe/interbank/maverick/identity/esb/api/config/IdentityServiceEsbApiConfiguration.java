/**
 *
 */
package pe.interbank.maverick.identity.esb.api.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author acacerec
 */
@Configuration
@EnableConfigurationProperties({ EsbApiProperties.class})
public class IdentityServiceEsbApiConfiguration { 


}
