package pe.interbank.maverick.identity.esb.api.model.lead;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.identity.esb.api.model.leads.Campaign;
import pe.interbank.maverick.identity.esb.api.model.leads.Offer;
import pe.interbank.maverick.identity.esb.api.model.leads.Treatment;


@Getter
@Setter
public class DetailLead {

	private String id;
	private String customerType;
	private String type;
	private CardBrand cardBrand;
	private CardType cardType;
	private String cardId;
	private String accountHost;
	private String accountAmount;
	private Currency currency;
	private String rent;
	private String amount;
	private String amount1;
	private String amount2;
	private String rate1;
	private String rate2;
	
	@SerializedName("CEM")
	private String cem;
	
	private String incomeFlow;
	private String deadline;
	private Quota quota;
	private String channelPriority;
	private String priority;
	private String merchandisingId;
	private String channelId;
	private List<Additional> additionals;
	private List<Campaign> campaigns;
	private List<Offer> offers;
	private List<Treatment> treatments;
	
	
	
}
