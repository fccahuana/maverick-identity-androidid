package pe.interbank.maverick.identity.esb.api.model.getinformationperson;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailResponse {

	private String subtype;
	private String email;
}
