package pe.interbank.maverick.identity.esb.api.model.leads;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Campaign {

	private String id;
	private String number;
	private String name;
	private String startDate;
	private String endDate;
	private String objective;
	private String priority;
	private Product product;
	
}
