package pe.interbank.maverick.identity.esb.api.model.fraudlist;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FraudListResponseDto {

	private List<PhonesResponse> phones;

}
