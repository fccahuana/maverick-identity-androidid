#### README ####
## MAVERICK-IDENTITY ##
Este componente cuenta con las siguientes funcionalidades:

   * Login.
   * Recuperar/Reestablecer contraseña.
   * Mi Perfil.
   * Registro.
   * Validación de Identidad.
   * Logout.
   
#### Comenzando 🚀 ####
Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo.
#### Pre-requisitos 📋 ####
1.- Instalar JDK 8

2.- Instalar la última versión del IDE STS o Eclipse.

3.- Instalar sonarqube-7.8 o +

4.- Agregar lombok al IDE STS. 

5.- Clonar la versíon "master" del proyecto commons que se encuentra en bitbucket: git clone https://tuusuario@bitbucket.org/ibkteam/maverick-commons.git

6.- Compilar el proyecto commons usando el comando: mvn clean install -U

#### Instalación 🔧

1.- Compilar el proyecto maverick-identity usando el comando: mvn clean install -U

2.-   Ingresar a Run -> Run Configurations -> click derecho sobre "Java Application" -> New Configuration e ingresar los siguientes valores:

   * Name      :  identity
   * Proyect   : Seleccionar maverick-identity-app
   * Main type : click en "Search" y seleccionar: "pe.interbank.maverick.identity.identityApplication"
   * En la pestaña **Environment**, agregar los valores indicados en la siguiente **tabla**:

|**Variable**|**Value**|
| --------- |:---|:---------|
|PORT|Ingresar puerto con el que se levantará el proyecto|
|KEYVAULT_CLIENT_ID|1e4d7d72-7d35-49d7-abe0-02b4dbb80b11 |
|PROFILE|dev  [ --> ver README.MD - proyecto maverick-development](https://bitbucket.org/ibkteam/maverick-development/src/master/README:MD)|
|URI_CONFIG_SERVER| http://localhost:8090/ [--> ver README.MD - proyecto maverick-configserver](https://bitbucket.org/ibkteam/maverick-configserver/src/master/readme.md)|
|URI_DISCOVERY_SERVER|http://localhost:8091/eureka/ [ -->  ver README.MD - proyecto maverick -discoveryserver](https://bitbucket.org/ibkteam/maverick-discoveryserver/src/master/rEADME:MD)|
|KEYVAULT_CLIENT_KEY| RRxAc+x808fwKg3kKpyaQwY/K1Yl1zlH3XlbIq+CEBA=|
|KEYVAULT_KEYIDENTIFIER_HSM|https://akveu2c003mvrkdev01.vault.azure.net/keys/maverick-key-dev/38a535386c03489baa56b63d6e92d024 |
|KEYVAULT_URI|https://akveu2c003mvrkdev01.vault.azure.net/ |
|RDSC_HOST_SERVER|rdseeu2c003mvrkdev01.redis.cache.windows.net|
|RSA_PRIVATE_KEY|MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDKgNhsmvENIBwnRMAodNvgzf9ZfomZxlL9XFoSLJChz8Kgibcf/ITdEVZvK7xQjsJ5yBWYDdU4lN3efgFj+yPbppD+4Ii3WB27/cQU0luJP2EDDsSb4Wl5I/VVWX3lsOFfOG0xVdDoVO6kjH7mcljgZqObNbNBaNhWCO1bIDSAEBIpPg+6BDv3HAMddFVj7yxYrFRkXLWx5mFkejf80E+KtB9JygvZek2qNjOTO2YHr9bLPI06i2RyOy8MF8AhrrvcjYt2BqeyDQMLggbm6It5Suwg/96pp6pdI/HXAipXtjkZ5RMlLDjmmWUf7L9czVLqI2CWjXpUmAzAsxnXkStzAgMBAAECggEBAIasc8KetMs23KJc/bKqLQTv2u9o0Rg8pjqrbZeCndmPM2/Sxr/2D2J6QlKC8qVcnAejwoCySmhr0LZEdKZKgkl1GB75stLEPlok5X+3enrndJw0+Rp2KdAP0vwlHS5hmTDbdPsHiHzK5z711PtrdqIegJYbe5B3qtBk9X3SCDhVx7S58iglweIaevLPulBjciv5Wxk2iYv2gAbZpKmwQk7zz27m0QmVGTNgTYJABbeajRpvYF7PNejOzGI55JqAI4Qd5NO6/JKDTEbqtSCYJF6UQQMrDB29m41zvH3LujCqLtwuXmUav9ff5qwLD1D56oKom+79tmBhnODNDtfum0ECgYEA+Vug4zykXQZiTelaQhf0Rk6QTeScEH7P+ZTF+VzduunDTz2X3OtwXLbLQXuMaeixNoxV2/JTfbG5+k9pObvyCzCUQeMC5MDoZ8KL9GtRjHE1CdaNS181evis4+2Addj8Tjoi1UpE7G0PzGY0C3ZPzJg1AIVjY2SuOyuifyHYV5MCgYEAz+W3HjpniSPPysyI8D+bGOJ1r2b5WasSn3wmAcpI6RVpv1hJa2hifJkq0A7ILUR770TZJ7FAj4j6Q+cI1RDahZ3u+sbKmRh8KM7C78Gi9f5odQcqVCIpKDzyJzZfd9GSJncbmtHAanqtr8APh56FHcpZwKkRq8RrZ+0HKWWUiKECgYEAj2p4zVkqe2pZjdABnnPihbFX48TxUbdt7HD5sGMOEBu0vlWBi4lKFbj4jBAAAUXY7rOmChuEk41tlp6nuZH0MyVcj8x99I1S3aKSFiDHRUwzIhZpK4l12+U8otcNhDopWuCq+ht+KrDLXlcF5fXFDboY+dS+BgN5BgUhr5cKs8sCgYBV3L8Laf224yZ/0IfCCdMJrQYD4libkw8G9WRRGvWP5nsdPt5b0OnUIgJPmbtNDtrM0DeLsCZE4cLS5ol8ThCiXL8FrbKhFj65L+PM18FxLZiAtOvjgZRT/Q/Z6Bf3yYM+BbVN9NEuaOpxCrafQXYi/p6g9nLI6T+e1YM+sqhs4QKBgGpb2HK/2jE/Ha77vF42jbuz7V/yMJ51Zw5D7PzfzjhD9qYhz19e5K5rR/D54lhS/08FECk2s16b2JW3XhOxE3QPAOLyf03QXmqWoqZsNotT05rNmbZpHtFVUQsJetehEmZgXnz1yZP/0qRd5CLnfOfT90+cmttJ+Rf1mjWnIqn2|
|RSA_PUBLIC_KEY|MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyoDYbJrxDSAcJ0TAKHTb4M3/WX6JmcZS/VxaEiyQoc/CoIm3H/yE3RFWbyu8UI7CecgVmA3VOJTd3n4BY/sj26aQ/uCIt1gdu/3EFNJbiT9hAw7Em+FpeSP1VVl95bDhXzhtMVXQ6FTupIx+5nJY4GajmzWzQWjYVgjtWyA0gBASKT4PugQ79xwDHXRVY+8sWKxUZFy1seZhZHo3/NBPirQfScoL2XpNqjYzkztmB6/WyzyNOotkcjsvDBfAIa673I2Ldgansg0DC4IG5uiLeUrsIP/eqaeqXSPx1wIqV7Y5GeUTJSw45pllH+y/XM1S6iNglo16VJgMwLMZ15ErcwIDAQAB|  

#### Despliegue 📦 

Ejecutar los siguientes comandos:

1. Ingresar a la ruta donde se encuentra el archivo: Dockerfile
2. Ejecutar el comando: docker build -t="acreu2c003mvrkdev01.azurecr.io/maverick-identity-app:0.0.x-dev" --build-arg artifact_id=maverick-identity-app --build-arg artifact_version=0.0.x --build-arg profile=dev .
3. Ejecutar el comando: docker push acreu2c003mvrkdev01.azurecr.io/maverick-identity-app:0.0.x-dev
donde x = número de versión del componente maverick-identity-app
#### Construido con 🛠
* Spring boot - Framework usado.
* Maven - Manejador de dependencias.

#### Deprecado ![](https://a.slack-edge.com/production-standard-emoji-assets/10.2/google-large/1f4cc.png)
El presente componente ha sido deprecado y en su lugar se han creado los siguientes componentes:
1. Maverick-user:  Con las funcionalidades de login, logout, mi perfil y reestablecer contraseña [ --> ver README.MD - proyecto maverick-user](https://bitbucket.org/ibkteam/maverick-user/src/desacoplamiento_identity/README:MD).
2. Maverick-customer: Con las funcionalidades de registro y validacion de identidad [ --> ver README.MD - proyecto maverick-customer](https://bitbucket.org/ibkteam/maverick-customer/src/desacoplamiento_identity/README:MD).