package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;


@Component
public class ApiDetail extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			Parameter param = new Parameter("productId", "1-1UWL6P");
			///ibk/uat/api/lead/v1/{leadId}
			//?channelId=ACC&productId=1-1UWL6P
			esbMock("/ibk/uat/api/lead/v1/1-CBRP-306", "apis/detail/ok/07883984/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/lead/v1/1-CDSO-6", "apis/detail/ok/40225641/HappyPathOutput.json", "GET",
					param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
