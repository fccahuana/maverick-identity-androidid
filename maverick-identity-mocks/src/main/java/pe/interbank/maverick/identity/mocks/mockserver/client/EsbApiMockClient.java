package pe.interbank.maverick.identity.mocks.mockserver.client;


import static org.mockserver.model.Header.header;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.JsonBody.json;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.mockserver.matchers.MatchType;
import org.mockserver.model.HttpStatusCode;
import org.mockserver.model.Parameter;

/**
 * @author Abner Ballardo
 *
 */
public abstract class EsbApiMockClient extends GenericMockClient {
	


	protected final void esbMock(String path, String inputJson, String outputJson, String method) throws IOException {
		getClientAndServer()
				.when(request()
						.withMethod(method)
						.withPath(path)
						.withBody(json(getTemplate(inputJson), MatchType.ONLY_MATCHING_FIELDS)))
				.respond(response()
						.withStatusCode(HttpStatusCode.OK_200.code())
						.withHeader(
								header("Content-Type","application/json"))
						.withBody(json(getTemplate(outputJson))));		
	}
	
	protected final void esbMock(String path, String outputJson, String method, Parameter parameter) throws IOException {
		getClientAndServer()
				.when(request()
						.withMethod(method)
						.withPath(path))			
				.respond(response()
						.withStatusCode(HttpStatusCode.OK_200.code())
						.withHeader(
								header("Content-Type","application/json"))
						.withBody(json(getTemplate(outputJson))));		
	}

	protected final void esbMockTimeout(String path, String inputJson) throws IOException {
		getClientAndServer()
				.when(request()
						.withMethod("POST")
						.withPath(path)
						.withBody(json(getTemplate(inputJson), MatchType.ONLY_MATCHING_FIELDS)))
				.respond(response()
						.withDelay(TimeUnit.SECONDS, 60)
						.withStatusCode(HttpStatusCode.BAD_REQUEST_400.code()));		
	}

}
