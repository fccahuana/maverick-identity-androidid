package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;


@Component
public class ApiFraudList extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			Parameter param = new Parameter("type", "2");
			esbMock("/ibk/uat/api/customer/v1/phoneNumber/987654321/fraud-list", "apis/fraude/ok/987654321/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/customer/v1/phoneNumber/987805547/fraud-list", "apis/fraude/ok/987805547/HappyPathOutput.json", "GET",
					param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
