package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;


@Component
public class ApiBanner extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			Parameter param = new Parameter("documentType", "01");
			///ibk/uat/api/customer/v2/document/{documentId}/leads
			//?documentType=01&productId=1-1UWL6P&channelId=ACC
			esbMock("/ibk/uat/api/customer/v2/document/07883984/leads", "apis/banner/ok/07883984/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/customer/v2/document/40225641/leads", "apis/banner/ok/40225641/HappyPathOutput.json", "GET",
					param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
