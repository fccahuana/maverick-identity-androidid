package pe.interbank.maverick.identity.mocks.mockserver.client;

import static org.mockserver.model.Header.header;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.JsonBody.json;

import java.io.IOException;

import org.mockserver.integration.ClientAndServer;
import org.mockserver.matchers.MatchType;
import org.mockserver.model.HttpStatusCode;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Abner Ballardo
 *
 */
public abstract class GenericMockClient implements MockClient {

	private ClientAndServer clientAndServer;

	public void setClientAndServer(ClientAndServer clientAndServer) {
		this.clientAndServer = clientAndServer;
	}

	public ClientAndServer getClientAndServer() {
		return clientAndServer;
	}

	protected final String getTemplate(String filename) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		Object jsonObject = mapper.readValue(new ClassPathResource(filename).getInputStream(), Object.class);
		return mapper.writeValueAsString(jsonObject);
	}

	protected final void jsonMock(String method, String path, String inputJson, String outputJson,
			HttpStatusCode responseStatus) throws IOException {
		this.clientAndServer
				.when(request().withMethod(method).withPath(path)
						.withBody(json(getTemplate(inputJson), MatchType.ONLY_MATCHING_FIELDS)))
				.respond(response().withStatusCode(responseStatus.code())
						.withHeader(header("Content-Type", "application/json"))
						.withBody(json(getTemplate(outputJson))));
	}
	
	protected final void jsonMock(String method, String path, String outputJson,
			HttpStatusCode responseStatus) throws IOException {
		this.clientAndServer
				.when(request().withMethod(method).withPath(path))
				.respond(response().withStatusCode(responseStatus.code())
						.withHeader(header("Content-Type", "application/json"))
						.withBody(json(getTemplate(outputJson))));
	}

}
