package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;

@Component
public class ApiPersonEquifax extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			Parameter param = new Parameter("documentType", "1");
			esbMock("/ibk/uat/api/person/v1/07883984", "apis/person/ok/07883984/HappyPathOutput.json", "GET", param);
			esbMock("/ibk/uat/api/person/v1/40225641", "apis/person/ok/40225641/HappyPathOutput.json", "GET", param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
