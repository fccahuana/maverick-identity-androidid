package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;

@Component
public class ApiLoans extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			Parameter param = new Parameter("documentType", "01");
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/07883984", "apis/loans/ok/07883984/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/40225641", "apis/loans/ok/40225641/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/50171739", "apis/loans/ok/50171739/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/77777976", "apis/loans/ok/77777976/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/77778057", "apis/loans/ok/77778057/HappyPathOutput.json", "GET",
					param);
			esbMock("/ibk/uat/api/loan/v1/agreements/documentId/77778187", "apis/loans/ok/77778187/HappyPathOutput.json", "GET",
					param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
