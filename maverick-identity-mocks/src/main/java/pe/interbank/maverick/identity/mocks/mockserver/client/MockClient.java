package pe.interbank.maverick.identity.mocks.mockserver.client;

import org.mockserver.integration.ClientAndServer;

/**
 * @author Abner Ballardo
 *
 */
public interface MockClient {

	void loadMocks();

	void setClientAndServer(ClientAndServer clientAndServer);

}
