package pe.interbank.maverick.identity.mocks.mockclient;

import org.mockserver.model.Parameter;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;

@Component
public class ApiEquifaxQuestions extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			//https://cap-sg-prd-2.integration.ibmcloud.com:17864/ibk/uat/api/equifax/v1/questions/documentId/{documentId}?documentType=1&validation=E&model=02563921
			Parameter param = new Parameter("documentType", "1");
			esbMock("/ibk/uat/api/equifax/v1/questions/documentId/07883984", "apis/equifax/questions/ok/07883984/HappyPathOutput.json", "GET", param);
			esbMock("/ibk/uat/api/equifax/v1/questions/documentId/40225641", "apis/equifax/questions/ok/40225641/HappyPathOutput.json", "GET", param);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
