package pe.interbank.maverick.identity.mocks.mockserver.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MockRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String sensitiveField;

}
