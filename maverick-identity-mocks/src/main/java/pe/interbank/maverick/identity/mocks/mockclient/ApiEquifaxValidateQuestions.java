package pe.interbank.maverick.identity.mocks.mockclient;

import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.EsbApiMockClient;

@Component
public class ApiEquifaxValidateQuestions extends EsbApiMockClient {
	@Override
	public void loadMocks() {
		try {
			//https://cap-sg-prd-2.integration.ibmcloud.com:17864/ibk/uat/api/equifax/v1/questions-validation
			esbMock("/ibk/uat/api/equifax/v1/questions-validation"
					, "apis/equifax/validation/ok/07883984/HappyPathInput.json"
					, "apis/equifax/validation/ok/07883984/HappyPathOutput.json",
					"POST");
			esbMock("/ibk/uat/api/equifax/v1/questions-validation"
					, "apis/equifax/validation/ok/40225641/HappyPathInput.json"
					, "apis/equifax/validation/ok/40225641/HappyPathOutput.json",
					"POST");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}
}
