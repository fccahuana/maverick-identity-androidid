package pe.interbank.maverick.identity.mocks.mockserver.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MockResponseDto {

	private String ecryptField;
	private String hashField;
	private String decryptedField;
}
