package pe.interbank.maverick.identity.mocks.mockserver.server;

import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import java.util.List;
import org.mockserver.integration.ClientAndServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.identity.mocks.mockserver.client.MockClient;


/**
 * @author Abner Ballardo
 *
 *
 */
@Component
public class MockServer implements CommandLineRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(MockServer.class);
	
	@Autowired(required=false)
	private List<MockClient> mocks;

	@Value("${maverick.identity.mockserver.port}")
	private Integer mockServerPort;

	@Override
	public void run(String... args) throws Exception {
		if (mocks == null) {
			LOGGER.info("No mocks found in classpath");
			return;
		}
		
		LOGGER.info("Loading {} mocks", mocks);
		ClientAndServer clientAndServer = startClientAndServer(this.mockServerPort);

		for (MockClient mock : mocks) {
			mock.setClientAndServer(clientAndServer);
			mock.loadMocks();
		}
	}

}
