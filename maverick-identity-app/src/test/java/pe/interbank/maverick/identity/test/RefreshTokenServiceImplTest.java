package pe.interbank.maverick.identity.test;

import java.util.Optional;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import pe.interbank.maverick.identity.core.model.login.AccessToken;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.oauth.service.impl.TokenServiceImpl;
import pe.interbank.maverick.identity.refreshtoken.service.impl.RefreshTokenServiceImpl;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@RunWith(PowerMockRunner.class)
public class RefreshTokenServiceImplTest {

	@InjectMocks
	RefreshTokenServiceImpl refreshTokenServiceImpl;

	@Mock
	TokenServiceImpl tokenService;

	@Mock
	UserRepository userRepository;

	AccessToken accessToken = null;

	User user;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		accessToken = easyRandom.nextObject(AccessToken.class);

	}

	@Test
	public void refreshToken() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(tokenService.getRefreshToken(Mockito.anyString(), Mockito.any())).thenReturn(accessToken);
		refreshTokenServiceImpl.refreshToken(Mockito.anyString());
		Assert.assertNotNull(tokenService);
	}

	@Test
	public void refreshTokenUserNull() {
		Mockito.when(tokenService.getRefreshToken(Mockito.anyString(), Mockito.any())).thenReturn(accessToken);
		refreshTokenServiceImpl.refreshToken(Mockito.anyString());
		Assert.assertNotNull(tokenService);
	}
}
