package pe.interbank.maverick.identity.test;

import java.util.Optional;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.microsoft.azure.servicebus.ITopicClient;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.exception.profile.UserProfileCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.profile.dto.UserProfileRequestDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileResponseDto;
import pe.interbank.maverick.identity.profile.service.impl.UserProfileServiceImpl;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CarrierType.class)
public class UserProfileServiceImplTest {

	@InjectMocks
	UserProfileServiceImpl userProfileServiceImpl;
	
	@Mock
	UserRepository userRepository;

	@Mock
	SensitiveData sensitiveData;

	@Mock
	PasswordHistoryServiceImpl passwordHistoryServiceImpl;

	@Mock
	IdentityUtil identityUtil;
	
	@Mock
	ITopicClient topicUpdateClient;
	
	User user;
	UserProfileResponseDto userProfileResponseDto;
	UserProfileRequestDto userProfileRequestDto;
	String tokenId = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJkYTQ1ZmU4ZGMwMTVlYWI3OWQ1OThhZDI4ZmNkNjUxNjdjODc0NGM1NjM0MGEzNGZlMDc1MTg5YWQ0ZTk1NmU2Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6ImRhNDVmZThkYzAxNWVhYjc5ZDU5OGFkMjhmY2Q2NTE2N2M4NzQ0YzU2MzQwYTM0ZmUwNzUxODlhZDRlOTU2ZTZLZW1TIiwiY3VzdG9tZXJJZCI6IjAwNjAzMTIzMDciLCJleHAiOjE1ODIxMzUxNTIsImZhbWlseSI6IiIsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJqdGkiOiI5ZTM1MGNkYS0yZDAyLTQ5OTItOTlmZC0yZDY4YzFlMGMxMGQiLCJjbGllbnRfaWQiOiIxZTRkN2Q3Mi03ZDM1LTQ5ZDctYWJlMC0wMmI0ZGJiODBiMTEifQ.pviWvuzG3tiQrodrjzBLqDljDRepOabH3Oz9JIS5TDA";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		userProfileResponseDto =  easyRandom.nextObject(UserProfileResponseDto.class);
		userProfileRequestDto = easyRandom.nextObject(UserProfileRequestDto.class);
	}
	
	@Test
	public void getUserProfileTest() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Assert.assertNotNull(userProfileServiceImpl.getUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void getUserProfileFingerPrintIdTest() {
		user.setFingerPrintId(null);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Assert.assertNotNull(userProfileServiceImpl.getUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void getUserProfileUserNullTest() {
		User userNull = null;
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(userNull));
		Assert.assertNotNull(userProfileServiceImpl.getUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void getUserProfileFingerPrintOkTest() {
		userProfileRequestDto.setFcmtoken("1223334444555556666667777777");
		userProfileRequestDto.setFingerPrint("463D1A9A64B641CFAD95409AC1260592D1617A474C5470AE8D442DA5C3772E0C");
		userProfileRequestDto.setUserId("524a5987733a2137af8fecad0a8a3974a2417050338bf2422dcaff2f00fca5cb");
		user.setFingerPrintId("c1ba6dbfc64e04089ec01f98addbe448bfd6e7d2ed41e4d238bff74336e38ef7");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Assert.assertNotNull(userProfileServiceImpl.getUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void getUserProfileNullNamesTest() {
		user.setFirstName(null);
		user.setSecondName(null);
		user.setLastName(null);
		user.setSecondLastName(null);
		user.setEmail(null);
		user.setPhoneNumber(null);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Assert.assertNotNull(userProfileServiceImpl.getUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileTest() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567A");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileTestDataProtectionNull() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567A");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		userProfileRequestDto.setDataProtection(null);
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test (expected = UserProfileCustomException.class)
	public void updateUserProfileIncorrectPassTest() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567C");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileNullValuesTest() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword(null);
		userProfileRequestDto.setNewPassword(null);
		userProfileRequestDto.setNewEmail(null);
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileEmptyValuesTest() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("");
		userProfileRequestDto.setNewPassword("");
		userProfileRequestDto.setNewEmail("");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileUserNullTest() {
		User userNull = null;
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(userNull));
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileFingerPrintNullTest() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567A");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		userProfileRequestDto.setEnableFingerPrint(null);
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileFingerPrintTrueTest() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567A");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		userProfileRequestDto.setFcmtoken("1223334444555556666667777777");
		userProfileRequestDto.setFingerPrint("463D1A9A64B641CFAD95409AC1260592D1617A474C5470AE8D442DA5C3772E0C");
		userProfileRequestDto.setUserId("524a5987733a2137af8fecad0a8a3974a2417050338bf2422dcaff2f00fca5cb");
		userProfileRequestDto.setEnableFingerPrint(true);
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
	@Test
	public void updateUserProfileFingerPrintFalseTest() {
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678oscarcastillo@gmail.com");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		userProfileRequestDto.setOldPassword("1234567A");
		userProfileRequestDto.setNewPassword("1234567B");
		userProfileRequestDto.setNewEmail("estoesunanuevaprueba@test.com");
		userProfileRequestDto.setEnableFingerPrint(false);
		user.setPassword("3d66f31308c7159b94a83d1a81f1844e457b797292fa6b7918099813b9ef41e1");
		user.setEmail("estoesunaprueba@test.com");
		Assert.assertNotNull(userProfileServiceImpl.updateUserProfile(userProfileRequestDto));
	}
	
}