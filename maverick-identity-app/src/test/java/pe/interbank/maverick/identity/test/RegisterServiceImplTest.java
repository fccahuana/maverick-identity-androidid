package pe.interbank.maverick.identity.test;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.user.DeviceDetails;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiFraudList;
import pe.interbank.maverick.identity.api.ConsumeEndpointNotificationEmail;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.dto.email.EmailNotificationResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CreditsResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.LoansAgreementResponseDto;
import pe.interbank.maverick.identity.exception.register.RegisterCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.register.config.RegisterConfiguration;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.service.impl.RegisterServiceImpl;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CarrierType.class)
public class RegisterServiceImplTest {

	@Mock
	UserRepository userRepository;

	@Mock
	SensitiveData sensitiveData;

	@Mock
	PasswordHistoryServiceImpl passwordHistoryServiceImpl;

	@Mock
	IdentityUtil identityUtil;
	
	@Mock
	StringRedisTemplate thirteenRedisDb;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationThirteenRedisDb;
	
	@Mock
	ConsumeApiFraudList consumeApiFraudList;
	
	@Mock
	RegisterConfiguration registerConfiguration;
	
	@Mock
	GenericRepository genericRepository;
	
	@Mock
	ConsumeEndpointNotificationEmail consumeEndpointNotificationEmail;

	@InjectMocks
	RegisterServiceImpl registerServiceImpl;

	RegisterRequestDto registerRequestDto = null;

	String dni = "09984285";

	String userId = "289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb";

	User user = null;

	LoansAgreementResponseDto loansAgreementResponseDto;
	
	ConsumeApiRequestDto consumeApiRequestDto;
	
	GetInformationPersonResponseDto informationPersonResponseDto;
	
	EmailNotificationResponseDto emailNotificationResponseDto;
	
	Generic generic;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		registerRequestDto = easyRandom.nextObject(RegisterRequestDto.class);
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
		loansAgreementResponseDto = easyRandom.nextObject(LoansAgreementResponseDto.class);
		informationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		emailNotificationResponseDto = easyRandom.nextObject(EmailNotificationResponseDto.class);
		generic = easyRandom.nextObject(Generic.class);

	}
	
	@Test
	public void validationDniCustomerHasCredit() {
		registerRequestDto.setDni(dni);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(),Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniCustomerBuc() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("123456");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		informationPersonResponseDto.setCustomerId("123456");
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniNotInterbankCustomer() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto = new GetInformationPersonResponseDto();
		informationPersonResponseDto.setCustomerId(null);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test(expected = RegisterCustomException.class)
	public void validationDniErrorFormatDni() {
		registerRequestDto.setDni("123ase");
		registerServiceImpl.validationDni(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void validationDniUserExist() {
		registerRequestDto.setDni(dni);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		registerServiceImpl.validationDni(registerRequestDto);
	}
	
	@Test
	public void validationDniSaveDataInRedisCustomerIdEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("");
		informationPersonResponseDto.setFirstName("nombre");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniSaveDataInRedisNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName(null);
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDniSaveDataInRedisNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniSaveDataInRedisSecondNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName(null);
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDniSaveDataInRedisSecondNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName("");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniSaveDataInRedisLastNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName(null);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDniSaveDataInRedisLastNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("materno");
		informationPersonResponseDto.setLastName("");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDniSaveDataInRedisSecondLastNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName(null);
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDniSaveDataInRedisSecondLastNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto.setCustomerId("12345678");
		informationPersonResponseDto.setFirstName("primer nombre");
		informationPersonResponseDto.setSecondName("segundo nombre");
		informationPersonResponseDto.setSecondLastName("");
		informationPersonResponseDto.setLastName("paterno");
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisCustomerIdNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId(null);
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisCustomerIdEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName(null);
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisSecondNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName(null);
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisSecondNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisLastNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName(null);
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisLastNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisSecondLastNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName(null);
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisSecondLastNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("nombre del convenio");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisInstitutionNameNull() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName(null);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	@Test
	public void validationDnisaveCustomerDataApiLoansInRedisInstitutionNameEmpty() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCustomer().setId("12345678");
		loansAgreementResponseDto.getCustomer().setFirstName("primer nombre");
		loansAgreementResponseDto.getCustomer().setSecondName("segundo nombre");
		loansAgreementResponseDto.getCustomer().setLastName("apellido paterno");
		loansAgreementResponseDto.getCustomer().setSecondLastName("apellido materno");
		loansAgreementResponseDto.getCredits().get(0).setCompanyId(12345678);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("");
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(true);
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Assert.assertNotNull(registerServiceImpl.validationDni(registerRequestDto));
	}
	
	@Test
	public void savePhone() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("921390641");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(consumeApiFraudList.consumeApiFraudList(Mockito.any(), Mockito.anyString())).thenReturn(false);
		registerServiceImpl.savePhone(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void incorrectFormatCellPhone() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("92139064a");
		registerServiceImpl.savePhone(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void errorInStepOneForKeyDoesnotExist() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("921390641");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP2", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.savePhone(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void errorInStepOneForKeyDifferent() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("921390641");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64ff");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.savePhone(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void savePhoneErrorForCellPhoneRepeated() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("921390641");
		List<User> users = new ArrayList<>();
		users.add(user);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findByPhoneNumberId(Mockito.anyString())).thenReturn(users);
		registerServiceImpl.savePhone(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void savePhoneErrorForCellPhoneInFraudList() {
		registerRequestDto.setDni("12345678");
		Mockito.when(registerConfiguration.getDestinationUsersBlocks()).thenReturn("usuario@gmail.com");
		registerRequestDto.setPhoneNumber("921390641");	
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(consumeApiFraudList.consumeApiFraudList(Mockito.any(), Mockito.anyString())).thenReturn(true);
		Mockito.when(consumeEndpointNotificationEmail.consumeEndpointNotificationEmail(registerRequestDto)).thenReturn(emailNotificationResponseDto);
		registerServiceImpl.savePhone(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test
	public void createUserInterbankCustomerAcceptTermsConditionsNoNames() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		List<CreditsResponse> credits = new ArrayList<>();
		loansAgreementResponseDto.setCredits(credits);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(), Mockito.any())).thenReturn(informationPersonResponseDto);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test
	public void createUserNotInterbankCustomer() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		List<CreditsResponse> credits = new ArrayList<>();
		loansAgreementResponseDto.setCredits(credits);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		informationPersonResponseDto.setCustomerId(null);
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(), Mockito.any())).thenReturn(informationPersonResponseDto);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test
	public void createUserInterbankCustomer() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer(null);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "NOT_BUC");
        values.put("IS_CUSTOMER", "true");
        when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		List<CreditsResponse> credits = new ArrayList<>();
		loansAgreementResponseDto.setCredits(credits);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(), Mockito.any())).thenReturn(informationPersonResponseDto);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test
	public void createUserInterbankCustomerAcceptTermsConditionsNoNamesNoBUC() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "NO_BUC");
        when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(identityUtil.getResponseApiLoansAgreement(Mockito.anyString(), Mockito.any())).thenReturn(loansAgreementResponseDto);
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(), Mockito.any())).thenReturn(informationPersonResponseDto);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test
	public void createUserInterbankCustomerAcceptTermsConditions() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
        values.put("COMPANY_ID", "COMPANY_ID");
        values.put("INSTITUTION_NAME", "INSTITUTION_NAME");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test
	public void createUserInterbankNotCustomerAcceptTermsConditions() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer(null);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
        values.put("COMPANY_ID", "COMPANY_ID");
        values.put("INSTITUTION_NAME", "INSTITUTION_NAME");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test (expected = RegisterCustomException.class)
	public void createUserInterbankNotCustomerAcceptTermsConditionsNoValidStep2() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("false");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
        values.put("COMPANY_ID", "COMPANY_ID");
        values.put("INSTITUTION_NAME", "INSTITUTION_NAME");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	@Test 
	public void createUserInterbankNotCustomerAcceptTermsConditionsValidStep2() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("false");
		registerRequestDto.setPhoneNumber("977404117");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
        values.put("CUSTOMER_TYPE", "BUC");
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
        values.put("COMPANY_ID", "COMPANY_ID");
        values.put("INSTITUTION_NAME", "INSTITUTION_NAME");
        values.put("PHONE_NUMBER", "515f8ca2b4ec005da09683367afbc0e5db55872f1458e2a3c22c239fb6ea8464");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Assert.assertNotNull(registerServiceImpl.createUser(registerRequestDto));
	}
	
	
	@Test(expected = RegisterCustomException.class)
	public void createUserInterbankCustomerNotAcceptTermsConditions() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(false);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "628ddf09f57088c6498939506f12fee7df8d736a4f0e56667d6546e9f33607e3");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.createUser(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void createUserInterbankCustomerNotAcceptTermsConditionsValidStep1() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(false);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.createUser(registerRequestDto);
	}
	
	@Test
	public void createBucNoClientUser() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		registerRequestDto.setPhoneNumber("977404117");
		DeviceDetails deviceDetails = new DeviceDetails();
		deviceDetails.setDeviceId("device123");
		deviceDetails.setManufacturer("manufactures");
		deviceDetails.setModel("model");
		registerRequestDto.setDeviceDetails(deviceDetails);
		when(sensitiveData.encryptHsm(Mockito.anyString())).thenReturn("xxxyyyzzz");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.createBucNoClientUser(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test
	public void createBucNoClientUserNoNames() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		registerRequestDto.setPhoneNumber("977404117");
		DeviceDetails deviceDetails = new DeviceDetails();
		deviceDetails.setDeviceId("device123");
		deviceDetails.setManufacturer("manufactures");
		deviceDetails.setModel("model");
		registerRequestDto.setDeviceDetails(deviceDetails);
		when(sensitiveData.encryptHsm(Mockito.anyString())).thenReturn("xxxyyyzzz");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("FIRST_NAME", "");
        values.put("SECOND_NAME", "");
        values.put("LAST_NAME", "");
        values.put("SECOND_LAST_NAME", "");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.createBucNoClientUser(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test
	public void createBucNoClientUserPhoneEmpty() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(true);
		registerRequestDto.setIsCustomer("true");
		registerRequestDto.setPhoneNumber("");
		DeviceDetails deviceDetails = new DeviceDetails();
		deviceDetails.setDeviceId("device123");
		deviceDetails.setManufacturer("manufactures");
		deviceDetails.setModel("model");
		registerRequestDto.setDeviceDetails(deviceDetails);
		when(sensitiveData.encryptHsm(Mockito.anyString())).thenReturn("xxxyyyzzz");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("FIRST_NAME", "Oscar");
        values.put("SECOND_NAME", "Alexander");
        values.put("LAST_NAME", "Castillo");
        values.put("SECOND_LAST_NAME", "Pampas");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		registerServiceImpl.createBucNoClientUser(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void createBucNoClientNoAcceptTermsConditions() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setTermsConditions(false);
		registerServiceImpl.createBucNoClientUser(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void validationDniNotInterbankCustomerUserBlockForFraudList() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto = new GetInformationPersonResponseDto();
		informationPersonResponseDto.setCustomerId(null);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		registerServiceImpl.validationDni(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void validationDniErrorSaveDataInRedis() {
		registerRequestDto.setDni(dni);
		registerRequestDto.setMessageId("12345678");
		registerRequestDto.setSessionId("12345678");
		informationPersonResponseDto = new GetInformationPersonResponseDto();
		informationPersonResponseDto.setCustomerId(null);
		Mockito.when(identityUtil.customerHasCredit(Mockito.any())).thenReturn(false);
		Mockito.doNothing().when(identityUtil).validateCustomerBuc(Mockito.anyString(), Mockito.any());
		Mockito.when(identityUtil.getResponseApiPerson(Mockito.anyString(),Mockito.any())).thenReturn(informationPersonResponseDto);	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		Mockito.doNothing().when(identityUtil).validateBucNoClientsAttempts(Mockito.anyString());
		NumberFormatException exception = new NumberFormatException();
		Mockito.doThrow(exception).when(thirteenRedisDb).expire(Mockito.anyString(),  Mockito.anyLong(), Mockito.any());
		registerServiceImpl.validationDni(registerRequestDto);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void savePhoneErrorSendEmail() {
		registerRequestDto.setDni("12345678");
		registerRequestDto.setPhoneNumber("921390641");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(registerConfiguration.getDestinationUsersBlocks()).thenReturn("usuario@gmail.com");
		Mockito.when(consumeApiFraudList.consumeApiFraudList(Mockito.any(), Mockito.anyString())).thenReturn(true);
		NumberFormatException exception = new NumberFormatException();
		Mockito.doThrow(exception).when(consumeEndpointNotificationEmail).consumeEndpointNotificationEmail( Mockito.any());
		registerServiceImpl.savePhone(registerRequestDto);
	}
}