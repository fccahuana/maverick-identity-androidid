package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import pe.interbank.maverick.identity.api.ConsumeApiGenerateKey;
import pe.interbank.maverick.identity.config.GenerateKeyDtoBase;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyDto;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.exception.api.ConsumeApiGenerateKeyCustomException;


@RunWith(PowerMockRunner.class)
public class ConsumeApiGenerateKeyTest {
	
	@Mock
	GenerateKeyDtoBase generateKeyDtoBase;
	
	@Mock
	RestTemplate restTemplate;
	
	@InjectMocks
	ConsumeApiGenerateKey consumeApiGenerateKey;
	
	User user;
	
	GenerateKeyDto generateKeyDto;
	
	String hashToken = "{\"token\":\"RFTnIH8hyPvA3h8ymlU66w==\",\"send_key\":true,\"message\":null}";
	
	String hashTokenError = "{{\"token\":\"RFTnIH8hyPvA3h8ymlU66w==\",\"send_key\":true,\"message\":null}}";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		generateKeyDto = easyRandom.nextObject(GenerateKeyDto.class);
		
		
	}
	
	@Test
	public void consumeServiceGenerateKeyOK() {
		ReflectionTestUtils.setField(consumeApiGenerateKey, "uri", "uri", String.class);
		generateKeyDto.setCarrier("CARRIER");
		user.setCarrier("CLARO");
		Mockito.when(generateKeyDtoBase.initGenerateKeyDtoBase()).thenReturn(generateKeyDto);
		ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"token\": \"3Izcnfc4+AF2zwomDTCRAg==\",\"sendKey\": true, \"message\": \"\",\"validToken\": \"65210054\"}", HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		
		consumeApiGenerateKey.consumeServiceGenerateKey(user);
		assertNotNull(generateKeyDto);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceGenerateKeyResponseEmpty() {
		ReflectionTestUtils.setField(consumeApiGenerateKey, "uri", "uri", String.class);
		generateKeyDto.setCarrier("CARRIER");
		user.setCarrier("CLARO");
		Mockito.when(generateKeyDtoBase.initGenerateKeyDtoBase()).thenReturn(generateKeyDto);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		consumeApiGenerateKey.consumeServiceGenerateKey(user);

	}
	
	@Test(expected = ConsumeApiGenerateKeyCustomException.class)
	public void consumeServiceGenerateKeyParserResponseGenerateKeyError() {
		generateKeyDto.setCarrier("CARRIER");
		user.setCarrier("CLARO");
		ResponseEntity<String> responseEntity = new ResponseEntity<>(null,HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		Mockito.when(generateKeyDtoBase.initGenerateKeyDtoBase()).thenReturn(generateKeyDto);
		consumeApiGenerateKey.consumeServiceGenerateKey(user);

	}

}
