package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import java.util.HashMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.util.ReflectionTestUtils;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;
import pe.interbank.maverick.identity.recoverypassword.service.impl.SecurityValidationRedis;

@RunWith(PowerMockRunner.class)
public class SecurityValidationRedisTest {
	
	@Mock
	StringRedisTemplate redisTemplateAttempts;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationAttemptsRedisDb;
	
	@Mock
	StringRedisTemplate redisStepTemplate;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationSteptsRedisDb;
	
	@InjectMocks
	SecurityValidationRedis securityValidationRedis;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(securityValidationRedis, "allowedAttempts", 2, int.class);
		ReflectionTestUtils.setField(securityValidationRedis, "forgotAttempsExpirationTime", 86400, long.class);
	}
	
	@Test()
	public void validateAttempsRecoveryPassworOK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("12345678", 0);
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateAttempsRecoveryPassword("12345678");
		assertNotNull(hashOperationAttemptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateAttempsRecoveryPassworErrorMaxAttempts() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("12345678", 3);
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateAttempsRecoveryPassword("12345678");
	}
	
	@Test
	public void validateAttempsRecoveryPassworOKEmpty() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateAttempsRecoveryPassword("12345678");
		assertNotNull(hashOperationAttemptsRedisDb);
	}
	
	@Test
	public void updateAttemptsInRedisOK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("12345678", 1);
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.updateAttemptsInRedis("12345678");
		assertNotNull(hashOperationAttemptsRedisDb);
	}
	
	@Test
	public void updateAttemptsInRedisEmpty() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.updateAttemptsInRedis("12345678");
		assertNotNull(hashOperationAttemptsRedisDb);
	}
	
	@Test
	public void updateAttemptsInRedisMaxAttempts() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("12345678", 4);
		when(redisTemplateAttempts.opsForHash()).thenReturn(hashOperationAttemptsRedisDb);
		when(hashOperationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.updateAttemptsInRedis("12345678");
		assertNotNull(hashOperationAttemptsRedisDb);
	}
	
	@Test
	public void generateHashStep1OK() {
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		securityValidationRedis.generateHashStep1("12345678", "921654123");
		assertNotNull(hashOperationSteptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void generateHashStep1Error() {
		securityValidationRedis.generateHashStep1("12345678", "921654123");
	}
	
	@Test
	public void validateHashStep1OK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateHashStep1("09984285");
		assertNotNull(hashOperationSteptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateHashStep1Null() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateHashStep1("09984285");
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateHashStep1DiferentsUserIds() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("STEP1", "289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4fec");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateHashStep1("09984285");
	}
	
	@Test
	public void generateHashStep2OK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.generateHashStep2("12345678", "CLARO");
		assertNotNull(securityValidationRedis);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void generateHashStep2Null() {
		securityValidationRedis.generateHashStep2("12345678", "CLARO");
	}
	
	@Test
	public void generateHashStep3OK() {
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		securityValidationRedis.generateHashStep3("09984285", "45698547");
		assertNotNull(hashOperationSteptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void generateHashStep3Error() {
		securityValidationRedis.generateHashStep3("09984285", "45698547");
	}
	
	@Test
	public void validateHashStep3OK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("STEP3", "9f6500adaab61a05ec55e174d8f14e12c3045674d83495045080faa9f6a248bf");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateHashStep3("09984285", "45698547");
		assertNotNull(hashOperationSteptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateHashStep3Null() {
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		securityValidationRedis.validateHashStep3("09984285", "45698547");
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateHashStep3DiferentsHash() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("STEP3", "9f6500adaab61a05ec55e174d8f14e12c3045674d83495045080faa9f6a248bg");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.validateHashStep3("09984285", "45698547");
	}
	
	@Test
	public void deleteStepsOfRedisOK() {	
		securityValidationRedis.deleteStepsOfRedis("09984285");
		assertNotNull(securityValidationRedis);
	}
	
	@Test
	public void getPhoneNumberOfRedisOK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("PHONE", "921365214");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.getPhoneNumberOfRedis("12345678");
		assertNotNull(hashOperationSteptsRedisDb);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void getPhoneNumberOfRedisNull() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationSteptsRedisDb);
		when(hashOperationSteptsRedisDb.entries(Mockito.anyString())).thenReturn(values);
		securityValidationRedis.getPhoneNumberOfRedis("12345678");
	}

}
