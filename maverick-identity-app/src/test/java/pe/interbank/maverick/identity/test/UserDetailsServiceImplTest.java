package pe.interbank.maverick.identity.test;

import static org.mockito.Mockito.when;
import java.util.Optional;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.oauth.service.impl.UserDetailsServiceImpl;

import pe.interbank.maverick.identity.repository.register.UserRepository;


@RunWith(PowerMockRunner.class)
@PrepareForTest(CarrierType.class)
public class UserDetailsServiceImplTest {
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@InjectMocks
	UserDetailsServiceImpl userDetailsServiceImpl;
	
	String dni = "09984285";
	
	String userId = "289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb";
	
	User user = null;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		
	}
	
	@Test(expected = UsernameNotFoundException.class)
	public void userNotFound() {
		userDetailsServiceImpl.loadUserByUsername(userId);
	}
	
	@Test
	public void loadUserByUsername() {
		when(userRepository.findById(userId)).thenReturn(Optional.of(user));
		when(bCryptPasswordEncoder.encode(Mockito.anyString())).thenReturn("");
		userDetailsServiceImpl.loadUserByUsername(userId);
		Assert.assertNotNull(user.getDni());
	}
	

}
