package pe.interbank.maverick.identity.test;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.interbank.maverick.identity.config.TokenRequestBase;
import pe.interbank.maverick.identity.core.model.login.AccessToken;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.oauth.dto.TokenRequestDto;
import pe.interbank.maverick.identity.oauth.exception.TokenCustomException;
import pe.interbank.maverick.identity.oauth.service.impl.TokenServiceImpl;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Gson.class)
public class TokenServiceImplTest {

	@InjectMocks
	TokenServiceImpl tokenServiceImpl;
	
	@Mock
	TokenRequestBase tokenRequestBase;

	@Mock
	StringRedisTemplate stringRedisTemplateLogin;
	
	@Mock
	RestTemplate restTemplate;
	
	@Mock
	HashOperations<String,Object,Object> hashOperations;	
	
	@Mock
	UserRepository userRepository;
	
	HttpServletRequest request;
	String token;
	
	AccessToken accessToken=null;
	
	TokenRequestDto tokenRequestDto = null;
	
	User user;
	LoginRequestDto loginRequestDto;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		user = easyRandom.nextObject(User.class);
		loginRequestDto = easyRandom.nextObject(LoginRequestDto.class);
		accessToken = easyRandom.nextObject(AccessToken.class);
		tokenRequestDto = new TokenRequestDto();
		tokenRequestDto.setUriAuth("uri");
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("grant_type", "password");
		body.add("client_id", "clientId");
		body.add("username", "userName");
		body.add("password", "password");
		
		String plainCreds = "clientId" + ":" + "clientSecret";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("Accept", "application/x-www-form-urlencoded, text/plain, */*");
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		tokenRequestDto.setBody(body);
		tokenRequestDto.setHttpHeaders(headers);
		
	}
	
	@Test(expected = TokenCustomException.class)
	public void refreshTokenErrorInRedis() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\"}",HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		tokenServiceImpl.getRefreshToken(Mockito.anyString(),Mockito.any());
	}
	
	@Test
	public void refreshTokenInRedisOK() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\"}",HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("REFRESH_TOKEN", "dff06e96e5c414021fa2456ff806790d844f55ff2855e41d95d3d0e4d1cec481");
		when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		when(hashOperations.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		tokenServiceImpl.getRefreshToken(Mockito.anyString(),Mockito.any());
		Assert.assertNotNull(tokenRequestBase);	
	}
	
	@Test
	public void refreshTokenTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\"}",HttpStatus.OK);
		PowerMockito.when(restTemplate.exchange(
				ArgumentMatchers.anyString(), 
				ArgumentMatchers.any(HttpMethod.class),
				ArgumentMatchers.<HttpEntity<?>> any(), 
				ArgumentMatchers.<Class<String>> any()
                )).thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);	
		AccessToken tokenResponseDto = tokenServiceImpl.getAccessToken(loginRequestDto,user);
		Assert.assertNotNull(tokenResponseDto);	
	}
	
	@Test
	public void updateTokenRedis() {
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("REFRESH_TOKEN", "dff06e96e5c414021fa2456ff806790d844f55ff2855e41d95d3d0e4d1cec481");
		when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		when(hashOperations.entries(Mockito.anyString())).thenReturn(values);
		
		tokenServiceImpl.updateTokenRedis("", "", "");
		Assert.assertNotNull(hashOperations);	
	}
	
}
