package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.repository.GetInformationPersonRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiInformationPersonCustomException;


@RunWith(PowerMockRunner.class)
public class ConsumeApiInformationPersonTest {
	
	@Mock
	GetInformationPersonRepository getInformationPersonRepository;
	
	@InjectMocks
	ConsumeApiInformationPerson consumeApiInformationPerson;
	
	GetInformationPersonResponseDto getInformationPersonResponseDto;
	
	String jsonMock = "{\"isCustomer\": \"S\",\"customerId\": \"00000060115772\",\"lastName\": \"ACEVEDO\",\"secondLastName\": \"JHONG\",\"firstName\": \"DANIEL\",\"secondName\": \"NELSON\",\"birthdate\": \"1991-09-09\",\"genderType\": \"M\",\"maritalstatusId\": \"M\",\"telephones\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"C001994325466\",\"type\": \"C\",\"number\": \"994325466\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2019-02-19\",\"registerHour\": \"09:48:45\"},{\"orderNumber\": \"2\",\"statusId\": \"A\",\"id\": \"R0012872425\",\"type\": \"R\",\"number\": \"2872425\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:47\"}],\"emails\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"003EMAPER\",\"subtype\": \"EMAPER\",\"email\": \"SFABIANJ@EVERIS.COM\",\"registerDate\": \"2019-02-26\",\"registerHour\": \"09:47:50\"}],\"addres\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"subType\": \"DOM\",\"useCode\": \"PRINCI\",\"secuenceId\": \"000\",\"isStandard\": \"S\",\"standardAddress\": {\"roadType\": \"AG\",\"roadName\": \"AV LAS LOMAS                            \",\"roadNumber\": \"4    \",\"block\": \"5    \",\"apartment\": \"6    \",\"inside\": \"2    \",\"location\": \"4                                       \",\"reference\": \"AV EL SOL\"},\"roadTypeName\": \"Agrupacion\",\"country\": \"PERU\",\"department\": \"LIMA\",\"province\": \"LIMA\",\"district\": \"LA MOLINA\",\"ubigeoCode\": \"150114\",\"zipCode\": \"L12\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:46\"}]}";
	
	String jsonMockError = "{{\"isCustomer\": \"S\",\"customerId\": \"00000060115772\",\"lastName\": \"ACEVEDO\",\"secondLastName\": \"JHONG\",\"firstName\": \"DANIEL\",\"secondName\": \"NELSON\",\"birthdate\": \"1991-09-09\",\"genderType\": \"M\",\"maritalstatusId\": \"M\",\"telephones\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"C001994325466\",\"type\": \"C\",\"number\": \"994325466\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2019-02-19\",\"registerHour\": \"09:48:45\"},{\"orderNumber\": \"2\",\"statusId\": \"A\",\"id\": \"R0012872425\",\"type\": \"R\",\"number\": \"2872425\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:47\"}],\"emails\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"003EMAPER\",\"subtype\": \"EMAPER\",\"email\": \"SFABIANJ@EVERIS.COM\",\"registerDate\": \"2019-02-26\",\"registerHour\": \"09:47:50\"}],\"addres\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"subType\": \"DOM\",\"useCode\": \"PRINCI\",\"secuenceId\": \"000\",\"isStandard\": \"S\",\"standardAddress\": {\"roadType\": \"AG\",\"roadName\": \"AV LAS LOMAS                            \",\"roadNumber\": \"4    \",\"block\": \"5    \",\"apartment\": \"6    \",\"inside\": \"2    \",\"location\": \"4                                       \",\"reference\": \"AV EL SOL\"},\"roadTypeName\": \"Agrupacion\",\"country\": \"PERU\",\"department\": \"LIMA\",\"province\": \"LIMA\",\"district\": \"LA MOLINA\",\"ubigeoCode\": \"150114\",\"zipCode\": \"L12\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:46\"}]}}";
	
	String jsonMockNotPhoneNumber = "{\"isCustomer\": \"S\",\"customerId\": \"00000060115772\",\"lastName\": \"ACEVEDO\",\"secondLastName\": \"JHONG\",\"firstName\": \"DANIEL\",\"secondName\": \"NELSON\",\"birthdate\": \"1991-09-09\",\"genderType\": \"M\",\"maritalstatusId\": \"M\",\"telephones\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"C001994325466\",\"type\": \"P\",\"number\": \"994325466\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2019-02-19\",\"registerHour\": \"09:48:45\"},{\"orderNumber\": \"2\",\"statusId\": \"A\",\"id\": \"R0012872425\",\"type\": \"R\",\"number\": \"2872425\",\"codeArea\": \"001\",\"external\": \"\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:47\"}],\"emails\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"id\": \"003EMAPER\",\"subtype\": \"EMAPER\",\"email\": \"SFABIANJ@EVERIS.COM\",\"registerDate\": \"2019-02-26\",\"registerHour\": \"09:47:50\"}],\"addres\": [{\"orderNumber\": \"1\",\"statusId\": \"A\",\"subType\": \"DOM\",\"useCode\": \"PRINCI\",\"secuenceId\": \"000\",\"isStandard\": \"S\",\"standardAddress\": {\"roadType\": \"AG\",\"roadName\": \"AV LAS LOMAS                            \",\"roadNumber\": \"4    \",\"block\": \"5    \",\"apartment\": \"6    \",\"inside\": \"2    \",\"location\": \"4                                       \",\"reference\": \"AV EL SOL\"},\"roadTypeName\": \"Agrupacion\",\"country\": \"PERU\",\"department\": \"LIMA\",\"province\": \"LIMA\",\"district\": \"LA MOLINA\",\"ubigeoCode\": \"150114\",\"zipCode\": \"L12\",\"registerDate\": \"2018-09-28\",\"registerHour\": \"09:33:46\"}]}";
	
	String dni = "43256895";
	
	ConsumeApiRequestDto consumeApiRequestDto;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);	
		EasyRandom easyRandom = new EasyRandom();
		getInformationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
	}
	
	@Test
	public void consumeServiceGenerateKeyOK() {
		Mockito.when(getInformationPersonRepository.getInformationPerson(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonMock);		
		consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,dni);
		assertNotNull(consumeApiInformationPerson);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceGenerateKeyEmpty() {
		Mockito.when(getInformationPersonRepository.getInformationPerson(Mockito.any(), Mockito.any() ,Mockito.any())).thenReturn("");
		consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,dni);
		
	}
	
	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void consumeServiceGenerateKeyParserInformationPersonError() {
		Mockito.when(getInformationPersonRepository.getInformationPerson(Mockito.any(), Mockito.any() ,Mockito.any())).thenReturn(jsonMockError);
		consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,dni);
	}
	
	@Test
	public void getTelephonesOK() {
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921365214");
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		consumeApiInformationPerson.getPhoneNumber(getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto);
	}
	
	@Test
	public void getTelephonesClientNotPhoneNumberRegistered() {
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921365214");
		getInformationPersonResponseDto.getTelephones().get(0).setType("P");
		consumeApiInformationPerson.getPhoneNumber(getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto);
	}
	
	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void consumeServiceGenerateKeyGenericError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		Mockito.when(getInformationPersonRepository.getInformationPerson(Mockito.any(), Mockito.any() ,Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,dni);
	}
	

}
