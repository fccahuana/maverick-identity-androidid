package pe.interbank.maverick.identity.test;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.config.TokenRequestBase;
import pe.interbank.maverick.identity.core.model.androididaudit.AndroidIdAudit;
import pe.interbank.maverick.identity.core.model.androididwhite.AndroidId;
import pe.interbank.maverick.identity.core.model.androididwhite.AndroidIdWhite;
import pe.interbank.maverick.identity.core.model.login.AccessToken;
import pe.interbank.maverick.identity.exception.login.LoginCustomException;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.login.dto.LoginResponseDto;
import pe.interbank.maverick.identity.login.service.impl.LoginServiceImpl;
import pe.interbank.maverick.identity.oauth.dto.TokenRequestDto;
import pe.interbank.maverick.identity.oauth.service.impl.TokenServiceImpl;
import pe.interbank.maverick.identity.repository.androididaudit.AndroidIdAuditRepository;
import pe.interbank.maverick.identity.repository.androididwhite.AndroidIdWhiteRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Gson.class)
public class LoginServiceImplTest {

	@InjectMocks
	LoginServiceImpl loginServiceImpl;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	AndroidIdWhiteRepository androidIdWhiteRepository;
	
	@Mock
	AndroidIdAuditRepository androidIdAuditRepository;
	
	@Mock
	TokenServiceImpl tokenService;
	
	@Mock
	StringRedisTemplate stringRedisTemplateLogin;
	
	@Mock
	StringRedisTemplate stringRedisTemplateAndroidId;
	
	@Mock
	RestTemplate restTemplate;
	
	@Mock
	HashOperations<String, Object, Object> hashOperations;

	@Mock
	ValueOperations<String, String> valueOperations;

	@Mock
	TokenRequestBase tokenRequestBase;
	
	@Mock
	IdentityUtil identityUtil;

	LoginRequestDto loginRequestDto;
	HttpServletRequest request;
	
	User correctUser = null;
	User userExist = null;
	User userById = null;
	AndroidId androidIdRedis = null;
	AndroidIdWhite androidIdWhite = null;
	AndroidIdAudit androidIdAudit = null;
	AccessToken accessToken = null;
	TokenRequestDto tokenRequestDto = null;

	String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k";
	String fcmToken = "cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf";
	String jsonBlocked = "{\"users\":[{\"userId\":\"e2390c614008439fe90729e21ae3a989e2641f26718005932fa46d8497ebdcb9\",\"lastLoginTimeDate\":\"22/07/2021 17:36:26\"}],\"blocked\":true}";
	String jsonUnblocked = "{\"users\":[{\"userId\":\"e2390c614008439fe90729e21ae3a989e2641f26718005932fa46d8497ebdcb9\",\"lastLoginTimeDate\":\"22/07/2021 17:36:26\"}],\"blocked\":false}";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		loginRequestDto = easyRandom.nextObject(LoginRequestDto.class);
		userById = easyRandom.nextObject(User.class);
		correctUser = easyRandom.nextObject(User.class);
		accessToken = easyRandom.nextObject(AccessToken.class);
		androidIdRedis = easyRandom.nextObject(AndroidId.class);
		androidIdWhite = easyRandom.nextObject(AndroidIdWhite.class);
		androidIdAudit = easyRandom.nextObject(AndroidIdAudit.class);
		tokenRequestDto = new TokenRequestDto();
		tokenRequestDto.setUriAuth("uri");
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("grant_type", "password");
		body.add("client_id", "clientId");
		body.add("username", "userName");
		body.add("password", "password");

		String plainCreds = "clientId" + ":" + "clientSecret";
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("Accept", "application/x-www-form-urlencoded, text/plain, */*");
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		tokenRequestDto.setBody(body);
		tokenRequestDto.setHttpHeaders(headers);
	}

	@Test
	public void loginAndAccesTokenCorrectTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		userById.setType(null);
		userById.setFcmToken("cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",  \"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		List<User> users = new ArrayList<>();
		users.add(userById);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		Mockito.when(userRepository.findByFcmToken(Mockito.anyString())).thenReturn(users);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}
	
	@Test
	public void loginAndAccesTokenCorrectNotBuc() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		correctUser.setCustomerId("123456");
		userById.setType("BUC");
		userById.setFcmToken("cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",\"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		List<User> users = new ArrayList<>();
		users.add(userById);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		Mockito.when(userRepository.findByFcmToken(Mockito.anyString())).thenReturn(users);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}

	@Test(expected = LoginCustomException.class)
	public void loginAndAccesTokenUserNotPassedValidationOtp() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('R');
		userById.setCustomerId("123456");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(), loginRequestDto.getPassword()))
				.thenReturn(correctUser);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}

	@Test(expected = NullPointerException.class)
	public void validateRegisteredUserUsernameEmpty() {
		loginRequestDto.setUsername("");
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(valueOperations.get(Mockito.anyString())).thenReturn(jsonUnblocked);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(jsonUnblocked, AndroidId.class)).thenReturn(androidIdRedis);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}

	@Test(expected = NullPointerException.class)
	public void validateAndroidIdNull() {
		loginRequestDto.setAndroidId(null);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}
	
	@Test(expected = NullPointerException.class)
	public void validateRegisteredUserUsernameNull() {
		loginRequestDto.setUsername(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(valueOperations.get(Mockito.anyString())).thenReturn(jsonUnblocked);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(jsonUnblocked, AndroidId.class)).thenReturn(androidIdRedis);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}

	@Test(expected = LoginCustomException.class)
	public void validateRegisteredUserDoesnotExistInMongo() {
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(valueOperations.get(Mockito.anyString())).thenReturn(jsonUnblocked);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(jsonUnblocked, AndroidId.class)).thenReturn(androidIdRedis);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}	
	
	@Test
	public void loginCorrectTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		correctUser.setUserId("12345678");
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",\"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(androidIdWhiteRepository.findByAndroidId(loginRequestDto.getAndroidId())).thenReturn(androidIdWhite);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}

	@Test
	public void loginCorrectDoesnotEventTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		correctUser.setUserId("12345678");
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",\"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}

	@Test(expected = LoginCustomException.class)
	public void loginInCorrectValidateCorrectUserTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(), loginRequestDto.getPassword()))
				.thenReturn(null);
		PowerMockito.when(
				userRepository.findByUserId(Mockito.anyString()))
				.thenReturn(userById);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}
	
	@Test(expected = LoginCustomException.class)
	public void loginInCorrectValidateCorrectUserNullTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(), loginRequestDto.getPassword()))
				.thenReturn(null);
		PowerMockito.when(
				userRepository.findByUserId(Mockito.anyString()))
				.thenReturn(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);

	}

	@Test(expected = LoginCustomException.class)
	public void loginCorrectValidateBlockedUserTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(3);
		userById.setStatus('F');
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(), loginRequestDto.getPassword()))
				.thenReturn(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}

	@Test(expected = LoginCustomException.class)
	public void loginCorrectWithoutValidationTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('R');
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(), loginRequestDto.getPassword()))
				.thenReturn(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);

	}
	
	@Test
	public void logoutTest () {
		String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k";
		LoginResponseDto loginResponseDto = loginServiceImpl.logout(token);
		Assert.assertNotNull(loginResponseDto);
	}
	
	@Test
	public void loginFingerPrintCorrectTest() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		userById.setFingerPrintId("c1ba6dbfc64e04089ec01f98addbe448bfd6e7d2ed41e4d238bff74336e38ef7");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(loginRequestDto.getUsername())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserId(Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",\"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		loginRequestDto.setAndroidId("abaf27ea12ced5b29b325e92a7511661073562d566e91ca5961a44fff56816c");
		loginRequestDto.setFcmtoken("1223334444555556666667777777");
		loginRequestDto.setFingerPrint("463D1A9A64B641CFAD95409AC1260592D1617A474C5470AE8D442DA5C3772E0C");
		loginRequestDto.setUsername("524a5987733a2137af8fecad0a8a3974a2417050338bf2422dcaff2f00fca5cb");
		loginRequestDto.setPassword(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}
	
	@Test (expected = LoginCustomException.class)
	public void loginFingerPrintUserNullTest() {
		loginRequestDto.setAndroidId("abaf27ea12ced5b29b325e92a7511661073562d566e91ca5961a44fff56816c");
		loginRequestDto.setFcmtoken("1223334444555556666667777777");
		loginRequestDto.setFingerPrint("463D1A9A64B641CFAD95409AC1260592D1617A474C5470AE8D442DA5C3772E0C");
		loginRequestDto.setUsername("524a5987733a2137af8fecad0a8a3974a2417050338bf2422dcaff2f00fca5cb");
		loginRequestDto.setPassword(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(userRepository.findByUserId(Mockito.anyString())).thenReturn(null);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}
	
	@Test (expected = LoginCustomException.class)
	public void loginFingerPrintNullTest() {
		loginRequestDto.setAndroidId("abaf27ea12ced5b29b325e92a7511661073562d566e91ca5961a44fff56816c");
		loginRequestDto.setFcmtoken("1223334444555556666667777777");
		loginRequestDto.setFingerPrint("463D1A9A64B641CFAD95409AC1260592D1617A474C5470AE8D442DA5C3772E0C");
		loginRequestDto.setUsername("524a5987733a2137af8fecad0a8a3974a2417050338bf2422dcaff2f00fca5cb");
		loginRequestDto.setPassword(null);
		userById.setFingerPrintId(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(userRepository.findByUserId(Mockito.anyString())).thenReturn(userById);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}
	
	@Test (expected = LoginCustomException.class)
	public void androidIdIsBlockedInRedis() {
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(valueOperations.get(Mockito.anyString())).thenReturn(jsonBlocked);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(jsonUnblocked, AndroidId.class)).thenReturn(androidIdRedis);
		loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
	}
	
	@Test
	public void loginCorrectAndroidIdRegisteredInWhiteList() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		userById.setType(null);
		userById.setFcmToken("cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf");
		androidIdWhite.setAndroidId("abaf27ea12ced5b29b325e92a7511661073562d566e91ca5961a44fff56816c");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		PowerMockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",  \"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		List<User> users = new ArrayList<>();
		users.add(userById);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(androidIdWhiteRepository.findByAndroidId(Mockito.anyString())).thenReturn(androidIdWhite);
		Mockito.when(userRepository.findByFcmToken(Mockito.anyString())).thenReturn(users);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}
	
	@Test (expected = LoginCustomException.class)
	public void loginCorrectAndroidIdBlockAndSaveInAudit() {
		PowerMockito.when(tokenRequestBase.initTokenRequestDto(Mockito.anyString(), Mockito.any())).thenReturn(tokenRequestDto);
		userById.setAttempts(0);
		userById.setStatus('F');
		userById.setCustomerId("123456");
		userById.setFamilyName("Educación");
		userById.setType(null);
		userById.setFcmToken("cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf");
		ReflectionTestUtils.setField(loginServiceImpl, "attempts", 3, int.class);
		ReflectionTestUtils.setField(loginServiceImpl, "maxUsers", 1, int.class);
		
		PowerMockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(userById));
		PowerMockito.when(
				userRepository.findByUserIdAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(userById);
		ResponseEntity<String> responseEntity = new ResponseEntity<>(
				"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k\",  \"customerType\": \"NOT_BUC\"}",
				HttpStatus.OK);
		PowerMockito
				.when(restTemplate.exchange(ArgumentMatchers.anyString(), ArgumentMatchers.any(HttpMethod.class),
						ArgumentMatchers.<HttpEntity<?>>any(), ArgumentMatchers.<Class<String>>any()))
				.thenReturn(responseEntity);
		Gson gsonMock = PowerMockito.mock(Gson.class);
		PowerMockito.when(gsonMock.fromJson(responseEntity.getBody(), AccessToken.class)).thenReturn(accessToken);
		PowerMockito.when(stringRedisTemplateLogin.opsForHash()).thenReturn(hashOperations);
		List<User> users = new ArrayList<>();
		users.add(userById);
		Mockito.when(tokenService.getAccessToken(Mockito.any(),Mockito.any())).thenReturn(accessToken);
		PowerMockito.when(androidIdWhiteRepository.findByAndroidId(Mockito.anyString())).thenReturn(null);
		PowerMockito.when(stringRedisTemplateAndroidId.opsForValue()).thenReturn(valueOperations);
		PowerMockito.when(valueOperations.get(Mockito.anyString())).thenReturn(jsonUnblocked);
		PowerMockito.when(androidIdAuditRepository.save(androidIdAudit)).thenReturn(null);
		Mockito.when(userRepository.findByFcmToken(Mockito.anyString())).thenReturn(users);
		LoginResponseDto loginResponseDto = loginServiceImpl.loginAndAccesToken(loginRequestDto, fcmToken);
		Assert.assertNotNull(loginResponseDto);
	}
}
