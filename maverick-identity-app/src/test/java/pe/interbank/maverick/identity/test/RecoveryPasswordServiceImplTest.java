package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Optional;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiGenerateKey;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.config.GenerateKeyDtoBase;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSRequestDto;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RecoveryPasswordServiceImpl;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RequiredValidations;
import pe.interbank.maverick.identity.recoverypassword.service.impl.SecurityValidationRedis;
import pe.interbank.maverick.identity.recoverypassword.service.impl.ValidationDni;
import pe.interbank.maverick.identity.recoverypassword.service.impl.ValidationNewPassword;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CarrierType.class)
public class RecoveryPasswordServiceImplTest {

	@Mock
	GenerateKeyDtoBase generateKeyDtoBase;

	@Mock
	IdentityUtil identityUtil;

	@Mock
	ValidationDni dniValidation;

	@Mock
	SensitiveData sensitiveData;

	@Mock
	ValidationNewPassword validationNewPassword;

	@Mock
	ConsumeApiGenerateKey consumeApiGenerateKey;

	@Mock
	ConsumeApiInformationPerson consumeApiInformationPerson;

	@Mock
	RequiredValidations requiredValidations;

	@Mock
	SecurityValidationRedis securityValidationRedis;

	@Mock
	PasswordHistoryServiceImpl passwordHistoryService;

	@Mock
	StringRedisTemplate redisStepTemplate;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationredisStepTemplate;

	@Mock
	UserRepository userRepository;

	@InjectMocks
	RecoveryPasswordServiceImpl recoveryPasswordServiceImpl;

	User user = null;

	GetInformationPersonResponseDto getInformationPersonResponseDto;

	ConfirmCarrierRequestDto confirmCarrierRequestDto;

	GenerateKeyResponseDto generateKeyResponseDto;

	ValidateTokenSMSRequestDto validateTokenSMSRequestDto;
	
	RecoveryPasswordRequestDto recoveryPasswordRequestDto;

	ValidatePasswordRequestDto validatePasswordRequestDto;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		recoveryPasswordRequestDto = easyRandom.nextObject(RecoveryPasswordRequestDto.class);
		getInformationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		confirmCarrierRequestDto = easyRandom.nextObject(ConfirmCarrierRequestDto.class);
		generateKeyResponseDto = easyRandom.nextObject(GenerateKeyResponseDto.class);
		validateTokenSMSRequestDto = easyRandom.nextObject(ValidateTokenSMSRequestDto.class);
		validatePasswordRequestDto = easyRandom.nextObject(ValidatePasswordRequestDto.class);
		user = easyRandom.nextObject(User.class);

	}

	@Test
	public void validateScreenDniOK() {
		
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921360541");
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(Mockito.any(),Mockito.anyString()))
				.thenReturn(getInformationPersonResponseDto);
		Mockito.when(identityUtil.getPhoneNumberOfApiPerson(Mockito.any(GetInformationPersonResponseDto.class)))
				.thenReturn("921390256");
		recoveryPasswordRequestDto.setDni("12345678");
		getInformationPersonResponseDto.setCustomerId("12345678");
		Mockito.when(identityUtil.getPhoneNumber(getInformationPersonResponseDto,"ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f")).thenReturn("921547862");
		recoveryPasswordServiceImpl.validateScreenDni(recoveryPasswordRequestDto);
		assertNotNull(recoveryPasswordRequestDto);
	}

	@Test
	public void confirmScreenCarrierOK() {
		confirmCarrierRequestDto.setDni("12345678");
		confirmCarrierRequestDto.setCarrier("CLARO");
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921360541");
		Mockito.when(securityValidationRedis.getPhoneNumberOfRedis(Mockito.anyString())).thenReturn("921390256");
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class)))
				.thenReturn(generateKeyResponseDto);
		Mockito.when(identityUtil.getUser("ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f")).thenReturn(user);
		recoveryPasswordServiceImpl.confirmScreenCarrier(confirmCarrierRequestDto);
		
		assertNotNull(confirmCarrierRequestDto);
	}

	@Test
	public void validateScreenValidationCodeOK() {
		validateTokenSMSRequestDto.setDni("12345678");
		recoveryPasswordServiceImpl.validateScreenValidationCode(validateTokenSMSRequestDto);
		assertNotNull(validateTokenSMSRequestDto);

	}

	@Test
	public void validateScreenNewPasswordNoMailOK() {
		validatePasswordRequestDto.setDni("12345678");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("PHONE", "921365412");
        values.put("CARRIER", "CLARO");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationredisStepTemplate);
		when(hashOperationredisStepTemplate.entries(Mockito.anyString())).thenReturn(values);		
		recoveryPasswordServiceImpl.validateScreenNewPassword(validatePasswordRequestDto);
		assertNotNull(validatePasswordRequestDto);
	}
	
	@Test
	public void validateScreenNewPasswordOK() {
		validatePasswordRequestDto.setDni("12345678");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("PHONE", "921365412");
        values.put("CARRIER", "CLARO");
        values.put("EMAIL", "oscar.alexander@gmail.com");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationredisStepTemplate);
		when(hashOperationredisStepTemplate.entries(Mockito.anyString())).thenReturn(values);		
		recoveryPasswordServiceImpl.validateScreenNewPassword(validatePasswordRequestDto);
		assertNotNull(validatePasswordRequestDto);
	}
	
	@Test
	public void validateScreenNewPasswordUserNullOK() {
		validatePasswordRequestDto.setDni("12345678");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("PHONE", "921365412");
        values.put("CARRIER", "CLARO");
		when(redisStepTemplate.opsForHash()).thenReturn(hashOperationredisStepTemplate);
		when(hashOperationredisStepTemplate.entries(Mockito.anyString())).thenReturn(values);		
		recoveryPasswordServiceImpl.validateScreenNewPassword(validatePasswordRequestDto);
		assertNotNull(validatePasswordRequestDto);
	}

}
