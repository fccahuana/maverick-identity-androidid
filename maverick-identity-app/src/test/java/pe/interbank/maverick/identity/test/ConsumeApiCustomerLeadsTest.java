package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiCustomerLeads;
import pe.interbank.maverick.identity.esb.api.repository.impl.CustomerLeadsRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiCustomerLeadCustomException;

@RunWith(PowerMockRunner.class)
public class ConsumeApiCustomerLeadsTest {

	@Mock
	CustomerLeadsRepositoryImpl customerLeadsRepositoryImpl;
	
	@InjectMocks
	ConsumeApiCustomerLeads consumeApiCustomerLeads;
	
	ConsumeApiRequestDto consumeApiRequestDto;
	
	String jsonApiCustomerLeads = "{\"leads\":[{\"id\": \"1-C0X1-2\",\"amount\": \"60920.26\",\"channelPriority\": \"9\",\"campaign\":{\"id\": \"1-4ABOED\",\"number\": \"1-259205701\",\"name\": \"Convenio Ampliación de Campaña 15.04.19\",\"startDate\": \"04/16/2019 00:02:10\",\"endDate\": \"06/30/2021 14:05:32\",\"objective\": \"\",\"priority\": \"2 - Alta\",\"product\":{\"id\": \"1-1UWL6P\",\"number\": \"P00038\",\"name\": \"CREDITO POR CONVENIO\",\"core\": \"ADQ/LIC\"}},\"offer\":{\"id\": \"1-4ABOF9\",\"number\": \"1-259205733\",\"name\": \"OFERTA CONVENIO AMPLIACIÓN DE CAMPAÑA 15.04.19\",\"type\": \"Adquisición\",\"description\": \"\"},\"treatment\":{\"id\": \"1-4ABOFF\",\"number\": \"1-259205739\",\"name\": \"TRATAMIENTO Convenio ampliación de campaña 15.04.19\"}}]}";
	String jsonApiCustomerLeadsError = "{{\"leads\":[{\"id\": \"1-C0X1-2\",\"amount\": \"60920.26\",\"channelPriority\": \"9\",\"campaign\":{\"id\": \"1-4ABOED\",\"number\": \"1-259205701\",\"name\": \"Convenio Ampliación de Campaña 15.04.19\",\"startDate\": \"04/16/2019 00:02:10\",\"endDate\": \"06/30/2021 14:05:32\",\"objective\": \"\",\"priority\": \"2 - Alta\",\"product\":{\"id\": \"1-1UWL6P\",\"number\": \"P00038\",\"name\": \"CREDITO POR CONVENIO\",\"core\": \"ADQ/LIC\"}},\"offer\":{\"id\": \"1-4ABOF9\",\"number\": \"1-259205733\",\"name\": \"OFERTA CONVENIO AMPLIACIÓN DE CAMPAÑA 15.04.19\",\"type\": \"Adquisición\",\"description\": \"\"},\"treatment\":{\"id\": \"1-4ABOFF\",\"number\": \"1-259205739\",\"name\": \"TRATAMIENTO Convenio ampliación de campaña 15.04.19\"}}]}";
	
	@Before
	public void init() {
		EasyRandom easyRandom = new EasyRandom();
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
	}
	
	@Test
	public void consumeApiCustomerLeads() {
		Mockito.when(customerLeadsRepositoryImpl.getLeads(Mockito.any(), Mockito.any(),Mockito.any())).thenReturn(jsonApiCustomerLeads);
		assertNotNull(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678", consumeApiRequestDto));
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeApiCustomerLeadsResponseEmpty() {
		Mockito.when(customerLeadsRepositoryImpl.getLeads(Mockito.any(), Mockito.any(),Mockito.any())).thenReturn("");
		consumeApiCustomerLeads.consumeApiCustomerLeads("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiCustomerLeadCustomException.class)
	public void consumeApiCustomerLeadsError() {
		Mockito.when(customerLeadsRepositoryImpl.getLeads(Mockito.any(), Mockito.any(),Mockito.any())).thenReturn(jsonApiCustomerLeadsError);
		consumeApiCustomerLeads.consumeApiCustomerLeads("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiCustomerLeadCustomException.class)
	public void consumeApiCustomerLeadsGenericError() {
		Mockito.when(customerLeadsRepositoryImpl.getLeads(Mockito.any(), Mockito.any(),Mockito.any())).thenThrow(ConsumeApiCustomerLeadCustomException.class);
		consumeApiCustomerLeads.consumeApiCustomerLeads("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiCustomerLeadCustomException.class)
	public void consumeApiCustomerLeadsServiceError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		Mockito.when(customerLeadsRepositoryImpl.getLeads(Mockito.any(), Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiCustomerLeads.consumeApiCustomerLeads("12345678", consumeApiRequestDto);
	}
}
