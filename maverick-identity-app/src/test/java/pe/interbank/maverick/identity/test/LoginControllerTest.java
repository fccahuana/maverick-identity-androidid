package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pe.interbank.maverick.identity.login.controller.web.LoginController;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.login.dto.LoginResponseDto;
import pe.interbank.maverick.identity.login.service.impl.LoginServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class LoginControllerTest {

	@InjectMocks
	LoginController loginController;
	
	@Mock
	LoginServiceImpl loginServiceImpl;
	
	String token = "";
	String fcmToken = "cToh-vS9HS4:APA91bHluE_aE0VpkAojer7jUfHB634M_fdZSvbWLPfBRrOy1uXPpiU7UWGfJ-O8d2ktu5IQGyxA_kuqAbESY9FvVAyxqV3aD96Wt4GuXp5tz1LMwdgfufGOyfXhAXlAIbjB7-UjcWtf";
	
	LoginRequestDto loginRequestDto;
	LoginResponseDto loginResponseDto;
	
	
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		loginRequestDto =  easyRandom.nextObject(LoginRequestDto.class);
		loginResponseDto =  easyRandom.nextObject(LoginResponseDto.class);
		token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k";
	}
	
	@Test
	public void loginTest() {		
		loginController.login(fcmToken,"fingerPrint", loginRequestDto,null);
		ResponseEntity<LoginRequestDto> responseEntity = new ResponseEntity<>(loginRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
		
	}
	
	@Test
	public void logoutTest() {		
		try {
			loginController.logout(token);
			ResponseEntity<LoginResponseDto> responseEntity = new ResponseEntity<>(loginResponseDto, HttpStatus.OK);
			Assert.assertNotNull(responseEntity);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
