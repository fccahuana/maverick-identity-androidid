package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotEquals;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RequiredValidations;
import pe.interbank.maverick.identity.recoverypassword.service.impl.ValidationDni;

@RunWith(PowerMockRunner.class)
public class ValidationDniTest {

	@Mock
	RequiredValidations requiredValidations;
	
	@InjectMocks
	ValidationDni validationDni;
	
	RecoveryPasswordRequestDto recoveryPasswordRequestDto;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		recoveryPasswordRequestDto = easyRandom.nextObject(RecoveryPasswordRequestDto.class);
	}
	
	@Test
	public void validateDni() {
		recoveryPasswordRequestDto.setDni("43258754");
		validationDni.validateDni(recoveryPasswordRequestDto);
		assertNotEquals("12345678", "");
	}
}
