package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotEquals;

import java.util.Optional;
import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.RegexType;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RequiredValidations;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@RunWith(PowerMockRunner.class)
public class RequiredValidationsTest {

	@Mock
	UserRepository userRepository;

	@InjectMocks
	RequiredValidations requiredValidations;

	User user;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
        user = easyRandom.nextObject(User.class);
	}

	@Test
	public void validationFieldNullOK() {
		requiredValidations.validationFieldNull("12345678");
		assertNotEquals("12345678", "");
	}

	@Test(expected = NullPointerException.class)
	public void validationFieldNullIsEmpty() {
		requiredValidations.validationFieldNull("");
	}

	@Test
	public void validationFormatOK() {
		requiredValidations.validationFormat("12345678", RegexType.REGEX_WRONG_DNI_FORMAT.getRegex(),
				ErrorType.ERROR_WRONG_DNI_FORMAT);
		assertNotEquals("12345678", "");
	}

	@Test(expected = RecoveryPasswordCustomException.class)
	public void validationFormatError() {
		requiredValidations.validationFormat("123456789", RegexType.REGEX_WRONG_DNI_FORMAT.getRegex(),
				ErrorType.ERROR_WRONG_DNI_FORMAT);
	}

	@Test
	public void validateRegisteredUserOK() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		requiredValidations.validateRegisteredUser("12345678");
		assertNotEquals("12345678", "");
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void validateRegisteredUserError() {
		requiredValidations.validateRegisteredUser("12345678");
	}

}
