package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Optional;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiGenerateKey;
import pe.interbank.maverick.identity.api.ConsumeApiGetQuestions;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.api.ConsumeApiValidationQuestions;
import pe.interbank.maverick.identity.config.GenerateKeyDtoBase;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getquestions.QuestionsResponseDto;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestionResponse;
import pe.interbank.maverick.identity.exception.validation.IdentityValidationCustomException;
import pe.interbank.maverick.identity.register.service.impl.RegisterServiceImpl;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationCodeRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationPhoneRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionResponseDto;
import pe.interbank.maverick.identity.validation.service.impl.IdentityValidationServiceImpl;


@RunWith(PowerMockRunner.class)
@PrepareForTest(CarrierType.class)
public class IdentityValidationServiceImplTest {

	@InjectMocks
	IdentityValidationServiceImpl identityValidationServiceImpl;
	
	@Mock
	GenerateKeyDtoBase generateKeyDtoBase;
	
	@Mock
	ConsumeApiInformationPerson consumeApiInformationPerson;
	
	@Mock
	ConsumeApiGenerateKey consumeApiGenerateKey;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	GenericRepository genericRepository;
	
	@Mock
	IdentityUtil identityUtil;
	
	@Mock
	StringRedisTemplate secureIdentityValidationRedisDb;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationSecureIdentityValidationRedisDb;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationIdentityValidationAttemptsRedisDb;
		
	@Mock
	StringRedisTemplate identityValidationAttemptsRedisDb;
	
	@Mock
	StringRedisTemplate threeRedisDb;
	
	@Mock
	StringRedisTemplate thirteenRedisDb;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationIdentityValidationThreeRedisDb;
	
	@Mock
	SensitiveData sensitiveData;
	
	@Mock
	ConsumeApiGetQuestions consumeApiGetQuestions;
	
	@Mock
	ConsumeApiValidationQuestions consumeApiValidationQuestions;
	
	@Mock
	RegisterServiceImpl registerService;
	
	ValidationPhoneRequestDto validationPhoneRequestDto;
	
	GetInformationPersonResponseDto getInformationPersonResponseDto;
	
	ConfirmCarrierRequestDto confirmCarrierRequestDto;
	
	GenerateKeyResponseDto generateKeyResponseDto;
	
	ValidationCodeRequestDto validationCodeRequestDto;
	
	QuestionsResponseDto questionsResponseDto ;
	
	GetQuestionRequestDto getQuestionRequestDto;
	
	ValidationQuestionResponse validationQuestionResponse;
	
	ValidationQuestionResponseDto validationQuestionResponseDto;
	ValidationQuestionRequestDto validationQuestionRequestDto;
	
	Generic generic;
	
	User user;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		ReflectionTestUtils.setField(identityValidationServiceImpl, "allowedAttempts", 2, int.class);
		ReflectionTestUtils.setField(identityValidationServiceImpl, "identityValidationAttempsExpirationTime", 86400, long.class);
		ReflectionTestUtils.setField(identityValidationServiceImpl, "identityValidationStepExpirationTime", 86400, long.class);
		ReflectionTestUtils.setField(identityValidationServiceImpl, "approved", "APROBADA", String.class);
		validationPhoneRequestDto = easyRandom.nextObject(ValidationPhoneRequestDto.class);
		getInformationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		user = easyRandom.nextObject(User.class);
		confirmCarrierRequestDto = easyRandom.nextObject(ConfirmCarrierRequestDto.class);
		generateKeyResponseDto = easyRandom.nextObject(GenerateKeyResponseDto.class);
		validationCodeRequestDto = easyRandom.nextObject(ValidationCodeRequestDto.class);
		questionsResponseDto = easyRandom.nextObject(QuestionsResponseDto.class);
		generic = easyRandom.nextObject(Generic.class);
		getQuestionRequestDto = easyRandom.nextObject(GetQuestionRequestDto.class);
		validationQuestionResponse = easyRandom.nextObject(ValidationQuestionResponse.class);
		validationQuestionResponseDto = easyRandom.nextObject(ValidationQuestionResponseDto.class);
		validationQuestionRequestDto = easyRandom.nextObject(ValidationQuestionRequestDto.class);
	}
	
	@Test
	public void getPhoneOK() {
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921360541");
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(Mockito.any(), Mockito.anyString())).thenReturn(getInformationPersonResponseDto);
		Mockito.when(identityUtil.getPhoneNumberOfApiPerson(Mockito.any(GetInformationPersonResponseDto.class))).thenReturn("921390256");
		Mockito.when(identityUtil.getEmailApiPerson(Mockito.any(GetInformationPersonResponseDto.class))).thenReturn("oscar.alexander@gmail.co,");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		identityValidationServiceImpl.getPhone(validationPhoneRequestDto);
		assertNotNull(validationPhoneRequestDto);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void getPhoneGenerateHashStep1Error() {
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("921360541");
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(Mockito.any(),Mockito.anyString())).thenReturn(getInformationPersonResponseDto);
		Mockito.when(identityUtil.getPhoneNumberOfApiPerson(Mockito.any(GetInformationPersonResponseDto.class))).thenReturn("921390256");
		identityValidationServiceImpl.getPhone(validationPhoneRequestDto);
	}
	
	@Test
	public void confirmCarrierOK() {
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
        values.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "0");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		Mockito.when(identityUtil.getUser("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b")).thenReturn(user);
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
		assertNotNull(confirmCarrierRequestDto);
	}
	
	@Test (expected = NullPointerException.class)
	public void confirmCarrierException() {
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
        values.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "0");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		Mockito.when(identityUtil.getUser("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b")).thenReturn(user);
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		generateKeyResponseDto.setToken("");
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
		assertNotNull(confirmCarrierRequestDto);
	}
	
	@Test
	public void confirmCarrierValidateAttemptsOK() {
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
		valuesAttempts.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "0");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		Mockito.when(identityUtil.getUser("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b")).thenReturn(user);
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
		
		assertNotNull(confirmCarrierRequestDto);
	}
	
	@Test
	public void confirmCarrierValidateAttempts1_OK() {
		
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
        Mockito.when(identityUtil.getUser("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b")).thenReturn(user);
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
		valuesAttempts.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "1");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
		
		assertNotNull(confirmCarrierRequestDto);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void confirmCarrierValidateAttemptsMax() {
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
		valuesAttempts.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "4");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
	}
	
	@Test(expected = NullPointerException.class)
	public void confirmCarrierGenerateHashStep2TokenEmpty() {
		confirmCarrierRequestDto.setDni("01161253");
		confirmCarrierRequestDto.setCarrier("CLARO");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("PHONE", "921365236");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		
		HashMap<Object, Object> valuesAttempts = new HashMap<Object, Object>();
        values.put("23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b", "0");
		when(identityValidationAttemptsRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationAttemptsRedisDb);
		when(hashOperationIdentityValidationAttemptsRedisDb.entries(Mockito.anyString())).thenReturn(valuesAttempts);
		
		Mockito.when(consumeApiGenerateKey.consumeServiceGenerateKey(Mockito.any(User.class))).thenReturn(generateKeyResponseDto);
		generateKeyResponseDto.setToken("");
		identityValidationServiceImpl.confirmCarrier(confirmCarrierRequestDto);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodeValidatePassStep1Null() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodePassStep1Diferent() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682c");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test (expected = IdentityValidationCustomException.class)
	public void validateCodeCarrierRedisNull() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "5c1ec05ed6170951c2e7ef610ef2c9c33117f6b0d5a43a94db9c941e3738cf6b");
        when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		when(identityUtil.generateIdToken(Mockito.anyString(),Mockito.anyString())).thenReturn("12345678");
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		assertNotNull(validationCodeRequestDto);
	}
	
	
	@Test (expected = IdentityValidationCustomException.class)
	public void validateCodePhoneNumberRedisNull() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "5c1ec05ed6170951c2e7ef610ef2c9c33117f6b0d5a43a94db9c941e3738cf6b");
        values.put("CARRIER", "CLARO");
        when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		when(identityUtil.generateIdToken(Mockito.anyString(),Mockito.anyString())).thenReturn("12345678");
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		assertNotNull(validationCodeRequestDto);
	}
	
	@Test
	public void validateCodeNotUpdateRegisteredUser() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "5c1ec05ed6170951c2e7ef610ef2c9c33117f6b0d5a43a94db9c941e3738cf6b");
        values.put("CARRIER", "CLARO");
        values.put("PHONE", "921365213");
        values.put("EMAIL", "oscar.alexander@gmail.com");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		when(identityUtil.generateIdToken(Mockito.anyString(),Mockito.anyString())).thenReturn("12345678");
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		assertNotNull(validationCodeRequestDto);
	}
	
	@Test
	public void validateCodeUpdateRegisteredUser() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "5c1ec05ed6170951c2e7ef610ef2c9c33117f6b0d5a43a94db9c941e3738cf6b");
        values.put("CARRIER", "CLARO");
        values.put("PHONE", "921365213");
        values.put("EMAIL", "oscar.alexander@gmail.com");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		when(identityUtil.generateIdToken(Mockito.anyString(),Mockito.anyString())).thenReturn("12345678");
		when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		when(sensitiveData.encryptHsm(Mockito.anyString())).thenReturn("xxxxxyyyyggghhhss");
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		assertNotNull(validationCodeRequestDto);
	}
	
	@Test
	public void validateCodeUpdateRegisteredUserNoEmail() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "5c1ec05ed6170951c2e7ef610ef2c9c33117f6b0d5a43a94db9c941e3738cf6b");
        values.put("CARRIER", "CLARO");
        values.put("PHONE", "921365213");
       	when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		when(identityUtil.generateIdToken(Mockito.anyString(),Mockito.anyString())).thenReturn("12345678");
		when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		when(sensitiveData.encryptHsm(Mockito.anyString())).thenReturn("xxxxxyyyyggghhhss");
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		assertNotNull(validationCodeRequestDto);
	}
	
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodeValidatePassStep2Null() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("CARRIER", "CLARO");
        values.put("PHONE", "921365213");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodeValidatePassStep2Diferent() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		validationCodeRequestDto.setHashToken("12345678");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "92b5b5b62e7184182dd57eec55e79dbdfc032afb91e0a7bb5dbca5028a17abcg");
        values.put("CARRIER", "CLARO");
        values.put("PHONE", "921365213");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodeGetCarrierFromRedisNull() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "92b5b5b62e7184182dd57eec55e79dbdfc032afb91e0a7bb5dbca5028a17abcf");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateCodeGetPhoneNumberOfRedisNull() {
		validationCodeRequestDto.setDni("01161253");
		validationCodeRequestDto.setVerificationCode("12345678");
		
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        values.put("STEP2", "92b5b5b62e7184182dd57eec55e79dbdfc032afb91e0a7bb5dbca5028a17abcf");
        values.put("CARRIER", "CLARO");
		when(secureIdentityValidationRedisDb.opsForHash()).thenReturn(hashOperationSecureIdentityValidationRedisDb);
		when(hashOperationSecureIdentityValidationRedisDb.entries(Mockito.anyString())).thenReturn(values);
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityValidationServiceImpl.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
	}
	
	@Test
	public void getQuestions() {
		generic.getValidationAttempts().setAttempts("1");
		Mockito.when(consumeApiGetQuestions.consumeServiceGetQuestions(Mockito.anyString(), Mockito.any())).thenReturn(questionsResponseDto);
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
		GetQuestionResponseDto  getQuestionResponseDto  = identityValidationServiceImpl.getquestions(getQuestionRequestDto);
		assertNotNull(getQuestionResponseDto);
	}
	
	@Test
	public void getQuestionsGenericNull() {
		Mockito.when(consumeApiGetQuestions.consumeServiceGetQuestions(Mockito.anyString(), Mockito.any())).thenReturn(questionsResponseDto);
		Generic genericNull = null;
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(genericNull));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
		GetQuestionResponseDto  getQuestionResponseDto  = identityValidationServiceImpl.getquestions(getQuestionRequestDto);
		assertNotNull(getQuestionResponseDto);
	}

	@Test(expected = NullPointerException.class)
	public void getQuestionsDniEmpty() {
		Mockito.when(consumeApiGetQuestions.consumeServiceGetQuestions(Mockito.anyString(), Mockito.any())).thenReturn(questionsResponseDto);
		Generic genericNull = null;
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(genericNull));
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "23088f2d8ba9af79aeef4eb13d3fb8145b1074bf41d2c787a6cb71aedbf9682b");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
        getQuestionRequestDto.setDni("");
		identityValidationServiceImpl.getquestions(getQuestionRequestDto);
	}
	
	@Test
	public void validationQuestionsAprobada() {
		Mockito.when(consumeApiValidationQuestions.consumeServiceValidationQuestions(Mockito.any(), Mockito.any())).thenReturn(validationQuestionResponse);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "33a7d3da476a32ac237b3f603a1be62fad00299e0d4b5a8db8d913104edec629");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
        Mockito.doNothing().when(identityUtil).validationIsUser(Mockito.anyString());
        Mockito.doNothing().when(identityUtil).validationUserByPhoneNumber(Mockito.anyString(), Mockito.any());
        Mockito.doNothing().when(identityUtil).validateBucNoClients(Mockito.anyString());
        Mockito.doNothing().when(registerService).createBucNoClientUser(Mockito.any());
        validationQuestionRequestDto.setDni("22222222");
        validationQuestionResponse.getResult().setDescription("APROBADA");
        validationQuestionResponseDto = identityValidationServiceImpl.validationQuestions(validationQuestionRequestDto);
        assertNotNull(validationQuestionResponseDto);
	}
	
	@Test
	public void validationQuestionsDesaprobada() {
		Mockito.when(consumeApiValidationQuestions.consumeServiceValidationQuestions(Mockito.any(), Mockito.any())).thenReturn(validationQuestionResponse);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "33a7d3da476a32ac237b3f603a1be62fad00299e0d4b5a8db8d913104edec629");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
        Mockito.doNothing().when(identityUtil).validationIsUser(Mockito.anyString());
        Mockito.doNothing().when(identityUtil).validationUserByPhoneNumber(Mockito.anyString(), Mockito.any());
        Mockito.doNothing().when(identityUtil).validateBucNoClients(Mockito.anyString());
        Mockito.doNothing().when(registerService).createBucNoClientUser(Mockito.any());
        validationQuestionRequestDto.setDni("22222222");
        validationQuestionResponse.getResult().setDescription("DESAPROBADA");
        validationQuestionResponseDto = identityValidationServiceImpl.validationQuestions(validationQuestionRequestDto);
        assertNotNull(validationQuestionResponseDto);
	}
	
	@Test
	public void validationQuestionsHashStep1() {
		Mockito.when(consumeApiValidationQuestions.consumeServiceValidationQuestions(Mockito.any(), Mockito.any())).thenReturn(validationQuestionResponse);
		HashMap<Object, Object> values = new HashMap<Object, Object>();
        values.put("STEP1", "33a7d3da476a32ac237b3f603a1be62fad00299e0d4b5a8db8d913104edec629xxx");
        when(threeRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationThreeRedisDb);
        when(hashOperationIdentityValidationThreeRedisDb.entries(Mockito.anyString())).thenReturn(values);
        Mockito.doNothing().when(identityUtil).validateBucNoClients(Mockito.anyString());
        validationQuestionRequestDto.setDni("22222222");
        validationQuestionResponse.getResult().setDescription("DESAPROBADA");
        validationQuestionResponseDto = identityValidationServiceImpl.validationQuestions(validationQuestionRequestDto);
        assertNotNull(validationQuestionResponseDto);
	}
}
