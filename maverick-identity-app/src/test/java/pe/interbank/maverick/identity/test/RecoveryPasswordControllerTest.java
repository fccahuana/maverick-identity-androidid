package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pe.interbank.maverick.identity.recoverypassword.controller.web.RecoveryPasswordController;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSRequestDto;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RecoveryPasswordServiceImpl;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;


@RunWith(MockitoJUnitRunner.class)
public class RecoveryPasswordControllerTest {

	@InjectMocks
	RecoveryPasswordController recoveryPasswordController;
	@Mock
	RecoveryPasswordServiceImpl recoveryPasswordServiceImpl;
	
	RecoveryPasswordRequestDto recoveryPasswordRequestDto;
	ValidateTokenSMSRequestDto validateTokenSMSRequestDto;
	ValidatePasswordRequestDto validatePasswordRequestDto;
	ConfirmCarrierRequestDto confirmCarrierRequestDto;
	
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		recoveryPasswordRequestDto =  easyRandom.nextObject(RecoveryPasswordRequestDto.class);
		validateTokenSMSRequestDto =  easyRandom.nextObject(ValidateTokenSMSRequestDto.class);
		validatePasswordRequestDto =  easyRandom.nextObject(ValidatePasswordRequestDto.class);
		confirmCarrierRequestDto = easyRandom.nextObject(ConfirmCarrierRequestDto.class);
	}
	
	@Test
	public void validationDniTest() {		
		recoveryPasswordController.validateScreenDni("messageIdxxxyyy", "sessionIdxxxyyy", recoveryPasswordRequestDto);
		ResponseEntity<RecoveryPasswordRequestDto> responseEntity = new ResponseEntity<>(recoveryPasswordRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void confirmCarierTest() {		
		recoveryPasswordController.confirmScreenCarrier("messageIdxxxyyy", "sessionIdxxxyyy", confirmCarrierRequestDto);
		ResponseEntity<RecoveryPasswordRequestDto> responseEntity = new ResponseEntity<>(recoveryPasswordRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void validationTokenSmsTest() {		
		recoveryPasswordController.validationTokenSms(validateTokenSMSRequestDto);
		ResponseEntity<ValidateTokenSMSRequestDto> responseEntity = new ResponseEntity<>(validateTokenSMSRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void validationNewPasswordTest() {		
		recoveryPasswordController.validationNewPassword(validatePasswordRequestDto);
		ResponseEntity<ValidatePasswordRequestDto> responseEntity = new ResponseEntity<>(validatePasswordRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	
}
