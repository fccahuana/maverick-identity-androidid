package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pe.interbank.maverick.identity.refreshtoken.controller.web.RefreshTokenController;
import pe.interbank.maverick.identity.refreshtoken.dto.RefreshTokenResponseDto;
import pe.interbank.maverick.identity.refreshtoken.service.impl.RefreshTokenServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class RefreshTokenControllerTest {

	@InjectMocks
	RefreshTokenController refreshTokenController;
	
	@Mock
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	
	String token = "";
	
	RefreshTokenResponseDto refreshTokenResponseDto;
	
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		refreshTokenResponseDto =  easyRandom.nextObject(RefreshTokenResponseDto.class);
		token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIzMWQxNjM5YjRkZWY1MmYzNDM1MTkzZTVjYzYyODUyNTIxZTRkM2I0NTNlZTkyY2UwMDkyY2VmNWYyNjBjZmViIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6IjMxZDE2MzliNGRlZjUyZjM0MzUxOTNlNWNjNjI4NTI1MjFlNGQzYjQ1M2VlOTJjZTAwOTJjZWY1ZjI2MGNmZWJtQWFzIiwiZXhwIjoxNTU2MzExODc4LCJhdXRob3JpdGllcyI6WyJST0xFX1VTRVIiXSwianRpIjoiMzkzNWJjNjAtMWJlOC00NDcyLTlmZDgtOTMwOGQ5YTYyYjY2IiwiY2xpZW50X2lkIjoiMWU0ZDdkNzItN2QzNS00OWQ3LWFiZTAtMDJiNGRiYjgwYjExIn0._KMVfVrCOmQk6s-7Yy6WcXzTZb_cJDCtKw1cZAdax3k";
	}
	
	@Test
	public void refreshTokenTest() {		
		refreshTokenController.refresh(token);
		ResponseEntity<RefreshTokenResponseDto> responseEntity = new ResponseEntity<>(HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
		
	}
}
