package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import com.microsoft.azure.servicebus.ITopicClient;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.core.model.catalogs.Catalogs;
import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.model.validation.ValidationAttempts;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.commons.crypto.service.CryptoAesService;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.api.ConsumeApiLoansAgreement;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponse;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponseDto;
import pe.interbank.maverick.identity.campaign.service.impl.CampaignServiceImpl;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.EmailResponse;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.PhoneResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CreditsResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CustomerResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.LoansAgreementResponseDto;
import pe.interbank.maverick.identity.exception.api.ConsumeApiCustomerLeadCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiDetailLeadCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiInformationPersonCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiLoansAgreementCustomException;
import pe.interbank.maverick.identity.exception.register.RegisterCustomException;
import pe.interbank.maverick.identity.exception.validation.IdentityValidationCustomException;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@RunWith(PowerMockRunner.class)
public class IdentityUtilTest {

	@Mock
	UserRepository userRepository;

	@Mock
	SensitiveData sensitiveData;

	@Mock
	ConsumeApiInformationPerson consumeApiInformationPerson;

	@Mock
	ITopicClient topicNotificationClient;

	@Mock
	ConsumeApiLoansAgreement consumeApiLoansAgreement;

	@InjectMocks
	IdentityUtil identityUtil;

	@Mock
	StringRedisTemplate identityValidationTokenOtpRedisDb;

	@Mock
	HashOperations<String, Object, Object> hashOperationIdentityValidationTokenOtpRedisDb;

	@Mock
	GenericRepository genericRepository;

	@Mock
	CampaignServiceImpl campaignServiceImpl;
	
	@Mock
	StringRedisTemplate thirteenRedisDb;

	@Mock
	HashOperations<String, Object, Object> hashOperationThirteenRedisDb;
	
	@Mock
	CryptoAesService cryptoAesService;

	GetInformationPersonResponseDto informationPersonResponseDto;

	GenerateKeyResponseDto generateKeyResponseDto;

	User user;

	Generic generic;
	Catalogs catalogs;

	LoansAgreementResponseDto loansAgreementResponseDto;

	ConsumeApiRequestDto consumeApiRequestDto;

	CampaignResponseDto campaignResponseDto;

	GetInformationPersonResponseDto getInformationPersonResponseDto;
	
	public static final String DNI = "12345678";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		informationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		user = easyRandom.nextObject(User.class);
		generic = easyRandom.nextObject(Generic.class);
		catalogs = easyRandom.nextObject(Catalogs.class);
		generateKeyResponseDto = easyRandom.nextObject(GenerateKeyResponseDto.class);
		ReflectionTestUtils.setField(identityUtil, "identityValidationTokenExpirationTime", 300, long.class);
		ReflectionTestUtils.setField(identityUtil, "sms",
				"INTERBANK \\nSu codigo de\\n verificacion es:\\n $otpcode$ \\n($date$ $time$)", String.class);
		loansAgreementResponseDto = easyRandom.nextObject(LoansAgreementResponseDto.class);
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
		campaignResponseDto = easyRandom.nextObject(CampaignResponseDto.class);
		getInformationPersonResponseDto = easyRandom.nextObject(GetInformationPersonResponseDto.class);
		ReflectionTestUtils.setField(identityUtil, "allowedBucNoClientsAttempts", 2, int.class);
	}


	@Test
	public void getPhoneNumberOK() {
		informationPersonResponseDto.getTelephones().get(0).setNumber("9213654874");
		informationPersonResponseDto.getTelephones().get(0).setType("C");
		identityUtil.getPhoneNumberOfApiPerson(informationPersonResponseDto);
		assertNotNull(informationPersonResponseDto);

	}

	@Test
	public void getPhoneNumberNotCellPhoneRegistered() {
		informationPersonResponseDto.getTelephones().get(0).setNumber("9213654874");
		informationPersonResponseDto.getTelephones().get(0).setType("P");
		identityUtil.getPhoneNumberOfApiPerson(informationPersonResponseDto);
		assertNotNull(informationPersonResponseDto);

	}

	@Test
	public void validateClientHaveCellPhoneOK() {
		informationPersonResponseDto.getTelephones().get(0).setNumber("9213654874");
		informationPersonResponseDto.getTelephones().get(0).setType("C");
		identityUtil.validateClientHaveCellPhone(informationPersonResponseDto,
				ErrorType.ERROR_SERVICE_CLIENT_DOES_HAVE_CELL_PHONE);
		assertNotNull(informationPersonResponseDto);
	}

	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void validateClientHaveCellPhoneNotAnyPhone() {
		List<PhoneResponse> phones = new ArrayList<>();
		informationPersonResponseDto.setTelephones(phones);
		identityUtil.validateClientHaveCellPhone(informationPersonResponseDto,
				ErrorType.ERROR_SERVICE_CLIENT_DOES_HAVE_CELL_PHONE);
		assertNotNull(informationPersonResponseDto);
	}

	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void validateClientHaveCellPhoneNotCellPhone() {
		informationPersonResponseDto.getTelephones().get(0).setNumber("9213654874");
		informationPersonResponseDto.getTelephones().get(0).setType("P");
		identityUtil.validateClientHaveCellPhone(informationPersonResponseDto,
				ErrorType.ERROR_SERVICE_CLIENT_DOES_HAVE_CELL_PHONE);
		assertNotNull(informationPersonResponseDto);
	}

	@Test
	public void getJsonSMS() {
		user.setDni(DNI);
		user.setPhoneNumber("977404117");
		user.setCarrier("Movistar");
		user.setCustomerId("123456789");
		identityUtil.getJsonSMS(DNI, user, "123456789", "sessionIdxxxyyy");
		assertNotNull(informationPersonResponseDto);
	}

	@Test(expected = IdentityValidationCustomException.class)
	public void getJsonSMSBlank() {
		user.setDni(DNI);
		user.setPhoneNumber("977404117");
		user.setCarrier("Movistar");
		user.setCustomerId("123456789");
		identityUtil.getJsonSMS("", user, "123456789", "sessionIdxxxyyy");
	}

	@Test
	public void sendMessage() {
		Mockito.when(cryptoAesService.encryptAes(Mockito.anyString())).thenReturn("messaje");
		Mockito.when(topicNotificationClient.sendAsync(Mockito.any())).thenReturn(new CompletableFuture<Void>());
		identityUtil.sendMessage("smsBody");
		assertNotNull(informationPersonResponseDto);
	}

	@Test
	public void sendMessageBlank() {
		Mockito.when(cryptoAesService.encryptAes(Mockito.anyString())).thenReturn("");
		Mockito.when(topicNotificationClient.sendAsync(Mockito.any())).thenReturn(new CompletableFuture<Void>());
		identityUtil.sendMessage("");
		assertNotNull(informationPersonResponseDto);
	}

	@Test
	public void saveTokenInRedisOK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("TOKEN", "");
		when(identityValidationTokenOtpRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationTokenOtpRedisDb);
		when(hashOperationIdentityValidationTokenOtpRedisDb.entries(Mockito.anyString())).thenReturn(values);
		generateKeyResponseDto.setValidToken(DNI);
		identityUtil.saveTokenInRedis("userId", generateKeyResponseDto);
		assertNotNull(generateKeyResponseDto);
	}

	@Test(expected = NullPointerException.class)
	public void saveTokenInRedisNullResponse() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("TOKEN", "");
		when(identityValidationTokenOtpRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationTokenOtpRedisDb);
		when(hashOperationIdentityValidationTokenOtpRedisDb.entries(Mockito.anyString())).thenReturn(values);
		generateKeyResponseDto.setValidToken(DNI);
		generateKeyResponseDto = null;
		identityUtil.saveTokenInRedis("userId", generateKeyResponseDto);
	}

	@Test
	public void validateTokenInRedisOK() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("TOKEN", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f");
		when(identityValidationTokenOtpRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationTokenOtpRedisDb);
		when(hashOperationIdentityValidationTokenOtpRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.validateTokenInRedis("userId", DNI, ErrorType.ERROR_GENERIC);
		assertNotNull(values);
	}

	@Test(expected = IdentityValidationCustomException.class)
	public void validateTokenInRedisDifferents() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("TOKEN", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64g");
		when(identityValidationTokenOtpRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationTokenOtpRedisDb);
		when(hashOperationIdentityValidationTokenOtpRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.validateTokenInRedis("userId", DNI, ErrorType.ERROR_GENERIC);
	}

	@Test(expected = IdentityValidationCustomException.class)
	public void validateTokenInRedisNull() {
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(identityValidationTokenOtpRedisDb.opsForHash()).thenReturn(hashOperationIdentityValidationTokenOtpRedisDb);
		when(hashOperationIdentityValidationTokenOtpRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.validateTokenInRedis("userId", DNI, ErrorType.ERROR_GENERIC);
	}

	public void validateIsClientOneClient() {
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(Mockito.any(), Mockito.any()))
				.thenReturn(loansAgreementResponseDto);
		CreditsResponse credit = loansAgreementResponseDto.getCredits().get(0);
		credit.getDisbursement().setAmount("50000");
		credit.getDisbursement().setCurrencyName("SOLES");
		List<CreditsResponse> lstCredits = new ArrayList<>();
		lstCredits.add(credit);
		loansAgreementResponseDto.setCredits(lstCredits);
		assertNotNull(loansAgreementResponseDto);
	}

	@Test
	public void validateIsClientMoreClients() {
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(Mockito.any(), Mockito.any()))
				.thenReturn(loansAgreementResponseDto);
		CreditsResponse credit = loansAgreementResponseDto.getCredits().get(0);
		credit.getDisbursement().setAmount("50000");
		credit.getDisbursement().setCurrencyName("SOLES");
		CreditsResponse credit1 = loansAgreementResponseDto.getCredits().get(1);
		credit1.getDisbursement().setAmount("60000");
		credit1.getDisbursement().setCurrencyName("SOLES");
		List<CreditsResponse> lstCredits = new ArrayList<>();
		lstCredits.add(credit);
		lstCredits.add(credit1);
		loansAgreementResponseDto.setCredits(lstCredits);
		assertNotNull(loansAgreementResponseDto);
	}


	@Test
	public void generateIdToken() {
		identityUtil.generateIdToken("", "");
		assertNotNull(loansAgreementResponseDto);
	}

	@Test
	public void getUserOK() {
		Mockito.when(userRepository.findById(DNI)).thenReturn(Optional.of(user));
		User user = identityUtil.getUser(DNI);
		assertNotNull(user);
	}

	@Test
	public void getUserNull() {
		User user = identityUtil.getUser(DNI);
		assertNull(user);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void validateCustomerBuc() {
		List<CampaignResponse> leads = new ArrayList<>();
		campaignResponseDto.setLeads(leads);
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenReturn(campaignResponseDto);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test
	public void customerNotBuc() {
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenReturn(campaignResponseDto);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void customerNotDoesNotBucCampaign() {
		ConsumeApiDetailLeadCustomException consumeApiDetailLeadCustomException = new ConsumeApiDetailLeadCustomException(ErrorType.NO_EXTENSION_CAMPAIGN);
		consumeApiDetailLeadCustomException.setError(new ErrorDto());
		consumeApiDetailLeadCustomException.getError().setCode(ErrorType.NO_EXTENSION_CAMPAIGN.getCode());
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenThrow(consumeApiDetailLeadCustomException);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void customerNotDoesNotBucCampaignGenericError() {
		ConsumeApiDetailLeadCustomException consumeApiDetailLeadCustomException = new ConsumeApiDetailLeadCustomException(ErrorType.ERROR_GENERIC);
		consumeApiDetailLeadCustomException.setError(new ErrorDto());
		consumeApiDetailLeadCustomException.getError().setCode(ErrorType.ERROR_GENERIC.getCode());
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenThrow(consumeApiDetailLeadCustomException);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test
	public void customerHasCredit() {
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(DNI,consumeApiRequestDto)).thenReturn(loansAgreementResponseDto);
		assertEquals(true, identityUtil.customerHasCredit(loansAgreementResponseDto));
	}
	
	@Test
	public void customerDoesNotHasCredit() {
		List<CreditsResponse> credits = new ArrayList<>();
		loansAgreementResponseDto.setCredits(credits);
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(DNI,consumeApiRequestDto)).thenReturn(loansAgreementResponseDto);
		assertEquals(false, identityUtil.customerHasCredit(loansAgreementResponseDto));
	}
	
	@Test
	public void getEmail() {
		List<EmailResponse> emails = new ArrayList<>();
		EmailResponse emailResponse = new EmailResponse();
		emailResponse.setEmail("usuario@gmail.com");
		emailResponse.setSubtype("EMAPER");
		emails.add(emailResponse);
		getInformationPersonResponseDto.setEmails(emails);
		assertEquals("usuario@gmail.com", identityUtil.getEmailApiPerson(getInformationPersonResponseDto));
	}
	@Test
	public void customerNotHasEmailEmaper() {
		List<EmailResponse> emails = new ArrayList<>();
		EmailResponse emailResponse = new EmailResponse();
		emailResponse.setEmail("usuario@gmail.com");
		emailResponse.setSubtype("EMAPER1");
		emails.add(emailResponse);
		getInformationPersonResponseDto.setEmails(emails);
		assertEquals("", identityUtil.getEmailApiPerson(getInformationPersonResponseDto));
	}
	@Test
	public void customerNotHasEmail() {
		List<EmailResponse> emails = new ArrayList<>();
		getInformationPersonResponseDto.setEmails(emails);
		assertEquals("", identityUtil.getEmailApiPerson(getInformationPersonResponseDto));
	}
	
	@Test
	public void getResponseApiPerson() {
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,DNI)).thenReturn(getInformationPersonResponseDto);
		assertNotNull(identityUtil.getResponseApiPerson(DNI, consumeApiRequestDto));
	}
	
	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void getResponseApiPersonGenericError() {
		ConsumeApiInformationPersonCustomException consumeApiInformationPersonCustomException = new ConsumeApiInformationPersonCustomException(ErrorType.ERROR_ONLY_CLIENTS);
		consumeApiInformationPersonCustomException.setError(new ErrorDto());
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(Mockito.any(), Mockito.any())).thenThrow(consumeApiInformationPersonCustomException);
		identityUtil.getResponseApiPerson(DNI, consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiInformationPersonCustomException.class)
	public void getResponseApiPersonError() {
		ConsumeApiInformationPersonCustomException consumeApiInformationPersonCustomException = new ConsumeApiInformationPersonCustomException(ErrorType.ERROR_SERVICE_GET_PHONE_RM);
		consumeApiInformationPersonCustomException.setError(new ErrorDto());
		Mockito.when(consumeApiInformationPerson.consumeServiceApiInformationPerson(Mockito.any(), Mockito.any())).thenThrow(consumeApiInformationPersonCustomException);
		identityUtil.getResponseApiPerson(DNI, consumeApiRequestDto);
	}
	
	@Test
	public void getLoansAgreementResponse() {
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(DNI,consumeApiRequestDto)).thenReturn(loansAgreementResponseDto);
		assertNotNull(identityUtil.getResponseApiLoansAgreement(DNI, consumeApiRequestDto));
	}
	
	@Test
	public void getLoansAgreementError() {
		ConsumeApiLoansAgreementCustomException consumeApiLoansAgreementCustomException = new ConsumeApiLoansAgreementCustomException(ErrorType.ERROR_ONLY_CLIENTS);
		consumeApiLoansAgreementCustomException.setError(new ErrorDto());
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(DNI, consumeApiRequestDto)).thenThrow(consumeApiLoansAgreementCustomException);
		assertNotNull(identityUtil.getResponseApiLoansAgreement(DNI, consumeApiRequestDto));
	}
	
	@Test(expected = ConsumeApiLoansAgreementCustomException.class)
	public void getLoansAgreementGenericError() {
		ConsumeApiLoansAgreementCustomException consumeApiLoansAgreementCustomException = new ConsumeApiLoansAgreementCustomException(ErrorType.ERROR_GENERIC);
		consumeApiLoansAgreementCustomException.setError(new ErrorDto());
		Mockito.when(consumeApiLoansAgreement.consumeServiceLoansAgreement(DNI, consumeApiRequestDto)).thenThrow(consumeApiLoansAgreementCustomException);
		identityUtil.getResponseApiLoansAgreement(DNI, consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void validateCustomerBucError() {
		List<CampaignResponse> leads = new ArrayList<>();
		campaignResponseDto.setLeads(leads);
		ConsumeApiCustomerLeadCustomException consumeApiCustomerLeadCustomException = new ConsumeApiCustomerLeadCustomException(ErrorType.NO_EXTENSION_CAMPAIGN);
		consumeApiCustomerLeadCustomException.setError(new ErrorDto());
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenThrow(consumeApiCustomerLeadCustomException);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiCustomerLeadCustomException.class)
	public void validateCustomerBucGenericError() {
		List<CampaignResponse> leads = new ArrayList<>();
		campaignResponseDto.setLeads(leads);
		ConsumeApiCustomerLeadCustomException consumeApiCustomerLeadCustomException = new ConsumeApiCustomerLeadCustomException(ErrorType.ERROR_GENERIC);
		consumeApiCustomerLeadCustomException.setError(new ErrorDto());
		Mockito.when(campaignServiceImpl.getBucLead(DNI, consumeApiRequestDto)).thenThrow(consumeApiCustomerLeadCustomException);
		identityUtil.validateCustomerBuc(DNI, consumeApiRequestDto);
	}
	
	@Test
	public void getPhoneNumberInterbankCustomer() {
		Mockito.when(userRepository.findById(DNI)).thenReturn(Optional.of(user));
		getInformationPersonResponseDto.setCustomerId(DNI);
		getInformationPersonResponseDto.getTelephones().get(0).setNumber("9213654874");
		getInformationPersonResponseDto.getTelephones().get(0).setType("C");
		assertNotNull(identityUtil.getPhoneNumber(getInformationPersonResponseDto, DNI));
	}
	
	@Test
	public void getPhoneNumberNotInterbankCustomer() {
		user.setPhoneNumber("1921390641");
		getInformationPersonResponseDto.setCustomerId(null);
		Mockito.when(userRepository.findById(DNI)).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("123456789");
		assertNotNull(identityUtil.getPhoneNumber(getInformationPersonResponseDto, DNI));
	}
	
	@Test
	public void validateBucNoClients() {
		identityUtil.validateBucNoClients(DNI);
		assertNotNull(user);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateBucNoClientsAvailableAttempts() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("1");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClients(DNI);
	} 
	
	@Test (expected = IdentityValidationCustomException.class)
	public void validateBucNoClientsAvailableAttempts_1() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("1");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClientsAttempts(DNI);
	}
	
	@Test
	public void validateBucNoClientsAvailableAttempts_2() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("2");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClientsAttempts(DNI);
	}
	
	@Test
	public void validateBucNoClientsAllAttempts_1() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("1");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClientsAllAttempts(DNI);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateBucNoClientsAllAttempts_2() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("2");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClientsAllAttempts(DNI);
	}
	
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateBucNoClientsLimitAttempts() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("2");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClients(DNI);
	}
	
	@Test(expected = IdentityValidationCustomException.class)
	public void validateBucNoClientsMaxAttempts() {
		generic.setValidationAttempts(new ValidationAttempts());
		generic.getValidationAttempts().setAttempts("5");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		identityUtil.validateBucNoClients(DNI);
	}
	
	@Test
	public void validationUserByPhoneNumber() {
		identityUtil.validationUserByPhoneNumber("921390641",ErrorType.ERROR_EXISTING_USER);		
		assertNotNull(DNI);
		
	}
	
	@Test(expected = RegisterCustomException.class)
	public void cellPhoneExistInMongo() {
		List<User> users = new ArrayList<>();
		users.add(user);
		Mockito.when(userRepository.findByPhoneNumberId(Mockito.anyString())).thenReturn(users);
		identityUtil.validationUserByPhoneNumber("921390641",ErrorType.ERROR_EXISTING_USER);
	}
	
	@Test
	public void cellPhoneExistInMongoUserNull() {
		List<User> users = null;
		Mockito.when(userRepository.findByPhoneNumberId(Mockito.anyString())).thenReturn(users);
		identityUtil.validationUserByPhoneNumber("921390641",ErrorType.ERROR_EXISTING_USER);
		assertNotNull(DNI);
	}
	
	@Test
	public void cellPhoneExistInMongoUserEmpty() {
		List<User> users = new ArrayList<User>();
		Mockito.when(userRepository.findByPhoneNumberId(Mockito.anyString())).thenReturn(users);
		identityUtil.validationUserByPhoneNumber("921390641",ErrorType.ERROR_EXISTING_USER);
		assertNotNull(users);
	}
	
	@Test
	public void validationIsUserOk() {
		identityUtil.validationIsUser(DNI);
		assertNotNull(DNI);
	}
	
	@Test(expected = RegisterCustomException.class)
	public void validationIsUserError() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		identityUtil.validationIsUser(DNI);
		assertNotNull(DNI);
	}
	
	@Test
	public void getFamilyNameForCompanyId() {
		assertNotNull(identityUtil.getFamilyNameForCompanyId(50));
	}
	
	@Test
	public void getFamilyNameOfGeneric() {
		generic.getCatalogs().setFamily("Huancayo");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		assertNotNull(identityUtil.getFamilyNameForCompanyId(50));
	}
	
	@Test
	public void getFamilyNameOfGenericOthers() {
		generic.getCatalogs().setFamily("Otros");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		assertNotNull(identityUtil.getFamilyNameForCompanyId(50));
	}
	
	@Test
	public void getPlaceNameForCompanyId() {
		assertNotNull(identityUtil.getPlaceNameForCompanyId(50));
	}
	
	@Test
	public void getPlaceNameForCompanyIdOfGeneric() {
		generic.getCatalogs().setPlace("Plaza");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		assertNotNull(identityUtil.getPlaceNameForCompanyId(50));
	}
	
	@Test
	public void getPlaceNameForCompanyIdOfGenericPlaceNull() {
		generic.getCatalogs().setPlace("NULL");
		Mockito.when(genericRepository.findById(Mockito.anyString())).thenReturn(Optional.of(generic));
		assertNotNull(identityUtil.getPlaceNameForCompanyId(50));
	}
	
	@Test
	public void setUserOfLoansAgreementNameEmpty() {
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
		customerOfLoans.setFirstName("primer nombre");
		customerOfLoans.setSecondName("segundo nombre");
		customerOfLoans.setLastName("paterno");
		customerOfLoans.setSecondLastName("materno");
		loansAgreementResponseDto.setCustomer(customerOfLoans);
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		creditsResponse.setAgreementName("");
		creditsResponse.setCompanyId(20);
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("");
		identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		assertNotNull(loansAgreementResponseDto.getCredits().get(0));
	}
	@Test
	public void setUserOfLoansAgreementNameNull() {
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
		customerOfLoans.setFirstName("primer nombre");
		customerOfLoans.setSecondName("segundo nombre");
		customerOfLoans.setLastName("paterno");
		customerOfLoans.setSecondLastName("materno");
		loansAgreementResponseDto.setCustomer(customerOfLoans);
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		creditsResponse.setAgreementName("");
		creditsResponse.setCompanyId(20);
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName(null);
		identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		assertNotNull(loansAgreementResponseDto.getCredits().get(0));
	}
	
	@Test
	public void setUserOfLoansAgreementName() {
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
		customerOfLoans.setFirstName("primer nombre");
		customerOfLoans.setSecondName("segundo nombre");
		customerOfLoans.setLastName("paterno");
		customerOfLoans.setSecondLastName("materno");
		loansAgreementResponseDto.setCustomer(customerOfLoans);
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		creditsResponse.setAgreementName("");
		creditsResponse.setCompanyId(20);
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("Municipalidad");
		identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		assertNotNull(loansAgreementResponseDto.getCredits().get(0));
	}
	
	@Test
	public void setUserOfLoansFamilyName() {
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
		customerOfLoans.setFirstName("primer nombre");
		customerOfLoans.setSecondName("segundo nombre");
		customerOfLoans.setLastName("paterno");
		customerOfLoans.setSecondLastName("materno");
		loansAgreementResponseDto.setCustomer(customerOfLoans);
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		creditsResponse.setAgreementName("Minicipalidad");
		creditsResponse.setCompanyId(20);
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("Municipalidad");		
		generic.getCatalogs().setFamily("familia");
		Mockito.when(genericRepository.findById("a1fa14a2a5c82c0dfee11be090e5f77dbbe4f1fcff116710378eda0afa54aba6")).thenReturn(Optional.of(generic));
		identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		assertNotNull(loansAgreementResponseDto.getCredits().get(0));
	}
	
	@Test
	public void setUserOfLoansFamilyNameOthers() {
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
		customerOfLoans.setFirstName("primer nombre");
		customerOfLoans.setSecondName("segundo nombre");
		customerOfLoans.setLastName("paterno");
		customerOfLoans.setSecondLastName("materno");
		loansAgreementResponseDto.setCustomer(customerOfLoans);
		List<CreditsResponse> credits = new ArrayList<>();
		CreditsResponse creditsResponse = new CreditsResponse();
		creditsResponse.setAgreementName("Minicipalidad");
		creditsResponse.setCompanyId(20);
		credits.add(creditsResponse);
		loansAgreementResponseDto.setCredits(credits);
		loansAgreementResponseDto.getCredits().get(0).setAgreementName("Municipalidad");
		Catalogs catalogs = new Catalogs();
		catalogs.setFamily("Otros");
		generic.setCatalogs(catalogs);
		Mockito.when(genericRepository.findById("a1fa14a2a5c82c0dfee11be090e5f77dbbe4f1fcff116710378eda0afa54aba6")).thenReturn(Optional.of(generic));
		identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		assertNotNull(loansAgreementResponseDto.getCredits().get(0));
	}
	
	@Test
	public void setUserWithApiPersonName() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto.getFirstName());
	}
	
	@Test
	public void setUserWithApiPersonFirstNameEmpty() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("");
		getInformationPersonResponseDto.setSecondName("segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertEquals("",getInformationPersonResponseDto.getFirstName());
	}
	
	@Test
	public void setUserWithApiPersonSecondName() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto.getSecondLastName());
	}
	
	@Test
	public void setUserWithApiPersonSecondNameEmpty() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertEquals("",getInformationPersonResponseDto.getSecondName());
	}
	
	@Test
	public void setUserWithApiPersonLastName() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("Segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto.getFirstName());
	}
	
	@Test
	public void setUserWithApiPersonLastNameEmpty() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("Segundo nombre");
		getInformationPersonResponseDto.setLastName("");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertEquals("",getInformationPersonResponseDto.getLastName());
	}
	
	@Test
	public void setUserWithApiPersonSecondLastName() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("Segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("materno");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertNotNull(getInformationPersonResponseDto.getLastName());
	}
	
	@Test
	public void setUserWithApiPersonSecondLastNameEmpty() {
		getInformationPersonResponseDto.setCustomerId("25");
		getInformationPersonResponseDto.setFirstName("primer nombre");
		getInformationPersonResponseDto.setSecondName("Segundo nombre");
		getInformationPersonResponseDto.setLastName("paterno");
		getInformationPersonResponseDto.setSecondLastName("");
		identityUtil.setUserWithApiPerson(user, getInformationPersonResponseDto);
		assertEquals("",getInformationPersonResponseDto.getSecondLastName());
	}
	
		
	@Test
	public void setUserEncryptedNamesOK() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		assertNotNull(identityUtil.setUserEncryptedNames(getInformationPersonResponseDto, "12345678"));
	}
	
	@Test
	public void setUserEncryptedNames() {
		assertNull(identityUtil.setUserEncryptedNames(getInformationPersonResponseDto, "12345678"));
	}
	
	@Test
	public void deleteKeyOfRedis() {
		identityUtil.deleteKeyOfRedis("key", identityValidationTokenOtpRedisDb);
		assertNotNull(identityValidationTokenOtpRedisDb);
	}
	
	@Test
	public void updateNamesUserNull() {
		User userNull = null;
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.ofNullable(userNull));
		identityUtil.updateNames(getInformationPersonResponseDto, "userId");
		assertNotNull(getInformationPersonResponseDto);
	}
	
	@Test
	public void updateNamesUser() {
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("abc123");
		identityUtil.updateNames(getInformationPersonResponseDto, "userId");
		assertNotNull(getInformationPersonResponseDto);
	}
	
	@Test
	public void updateNamesNullUser() {
		getInformationPersonResponseDto.setFirstName("");
		getInformationPersonResponseDto.setSecondName("");
		getInformationPersonResponseDto.setLastName("");
		getInformationPersonResponseDto.setSecondLastName("");
		Mockito.when(userRepository.findById(Mockito.anyString())).thenReturn(Optional.of(user));
		Mockito.when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("abc123");
		identityUtil.updateNames(getInformationPersonResponseDto, "userId");
		assertNotNull(getInformationPersonResponseDto);
	}
	
	@Test
	public void setNamesUserOfRedis() {
		User user = new User();
		user.setUserId("12345678");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("FIRST_NAME", "FIRST NAME");
		values.put("SECOND_NAME", "SECOND NAME");
		values.put("LAST_NAME", "LAST NAME");
		values.put("SECOND_LAST_NAME", "SECOND LAST NAME");	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.setNamesUserOfRedis(user);
		assertNotNull(user);
	}
	
	@Test
	public void errorSetNamesUserOfRedis() {
		User user = new User();
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.setNamesUserOfRedis(user);
		assertNotNull(user);
	}
	
	@Test
	public void setNameEmptyUserOfRedis() {
		User user = new User();
		user.setUserId("12345678");
		HashMap<Object, Object> values = new HashMap<Object, Object>();
		values.put("FIRST_NAME", "");
		values.put("SECOND_NAME", "SECOND NAME");
		values.put("LAST_NAME", "LAST NAME");
		values.put("SECOND_LAST_NAME", "SECOND LAST NAME");	
		when(thirteenRedisDb.opsForHash()).thenReturn(hashOperationThirteenRedisDb);
		when(hashOperationThirteenRedisDb.entries(Mockito.anyString())).thenReturn(values);
		identityUtil.setNamesUserOfRedis(user);
		assertNotNull(user);
	}
}
