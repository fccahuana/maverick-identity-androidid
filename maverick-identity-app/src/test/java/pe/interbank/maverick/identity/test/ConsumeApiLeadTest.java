package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiLead;
import pe.interbank.maverick.identity.esb.api.repository.impl.LeadRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiDetailLeadCustomException;

@RunWith(PowerMockRunner.class)
public class ConsumeApiLeadTest {

	@Mock
	LeadRepositoryImpl leadRepositoryImpl;

	@InjectMocks
	ConsumeApiLead consumeApiLead;

	ConsumeApiRequestDto consumeApiRequestDto;

	String jsonApiLeads = "{\"leads\": [{\"id\": \"1-BBGY-47\",\"customerType\": \"0\",\"type\": \"14\",\"cardBrand\": {\"id\": \"1\",\"name\": \"\"},\"cardType\": {\"id\": \"1\",\"name\": \"\"},\"cardId\": \"\",\"accountHost\": \"\",\"accountAmount\": \"\",\"currency\": {\"id\": \"1\",\"name\": \"\",\"symbol\": \"\"},\"rent\": \"4200.6\",\"amount\": \"76000\",\"amount1\": \"0\",\"amount2\": \"76000\",\"rate1\": \"1.1\",\"rate2\": \"0\",\"CEM\": \"1659.77\",\"incomeFlow\": \"75\",\"deadline\": \"72\",\"quota\":{\"number\": \"\",\"amount\": \"\"},\"channelPriority\": \"9\",\"priority\": \"1\",\"merchandisingId\": \"190705002001\",\"channelId\": \"ACC\",\"additionals\": [{\"item1\": \"\",\"item2\": \"\",\"item3\": \"\",\"item4\": \"\",\"item5\": \"\",\"item6\": \"\",\"item7\": \"\",\"item8\": \"\",\"item9\": \"\",\"item10\": \"\",\"item11\": \"544\",\"item12\": \"31\",\"item13\": \"0\",\"item14\": \"0\",\"item15\": \"AA\"}],\"campaigns\":[{\"id\": \"1-4ABOED\",\"number\": \"1-259205701\",\"name\": \"Convenio Ampliación de Campaña 15.04.19\",\"startDate\": \"04/16/2019 00:02:10\",\"endDate\": \"06/30/2021 14:05:32\",\"objective\": \"\",\"priority\": \"2 - Alta\",\"products\": [{\"id\": \"1-1UWL6P\",\"code\": \"P00038\",\"name\": \"CREDITO POR CONVENIO\",\"core\": \"ADQ/LIC\"}]}],\"offers\": [{\"id\": \"1-4ABOF9\",\"number\": \"1-259205733\",\"name\": \"OFERTA CONVENIO AMPLIACIÓN DE CAMPAÑA 15.04.19\",\"type\": \"Adquisición\",\"description\": \"\"}],\"treatments\": [{\"id\": \"1-4ABOFF\",\"number\": \"1-259205739\",\"name\": \"TRATAMIENTO Convenio ampliación de campaña 15.04.19\"}]}]}";

	String jsonApiLeadsError = "{{\"leads\": [{\"id\": \"1-BBGY-47\",\"customerType\": \"0\",\"type\": \"14\",\"cardBrand\": {\"id\": \"1\",\"name\": \"\"},\"cardType\": {\"id\": \"1\",\"name\": \"\"},\"cardId\": \"\",\"accountHost\": \"\",\"accountAmount\": \"\",\"currency\": {\"id\": \"1\",\"name\": \"\",\"symbol\": \"\"},\"rent\": \"4200.6\",\"amount\": \"76000\",\"amount1\": \"0\",\"amount2\": \"76000\",\"rate1\": \"1.1\",\"rate2\": \"0\",\"CEM\": \"1659.77\",\"incomeFlow\": \"75\",\"deadline\": \"72\",\"quota\":{\"number\": \"\",\"amount\": \"\"},\"channelPriority\": \"9\",\"priority\": \"1\",\"merchandisingId\": \"190705002001\",\"channelId\": \"ACC\",\"additionals\": [{\"item1\": \"\",\"item2\": \"\",\"item3\": \"\",\"item4\": \"\",\"item5\": \"\",\"item6\": \"\",\"item7\": \"\",\"item8\": \"\",\"item9\": \"\",\"item10\": \"\",\"item11\": \"544\",\"item12\": \"31\",\"item13\": \"0\",\"item14\": \"0\",\"item15\": \"AA\"}],\"campaigns\":[{\"id\": \"1-4ABOED\",\"number\": \"1-259205701\",\"name\": \"Convenio Ampliación de Campaña 15.04.19\",\"startDate\": \"04/16/2019 00:02:10\",\"endDate\": \"06/30/2021 14:05:32\",\"objective\": \"\",\"priority\": \"2 - Alta\",\"products\": [{\"id\": \"1-1UWL6P\",\"code\": \"P00038\",\"name\": \"CREDITO POR CONVENIO\",\"core\": \"ADQ/LIC\"}]}],\"offers\": [{\"id\": \"1-4ABOF9\",\"number\": \"1-259205733\",\"name\": \"OFERTA CONVENIO AMPLIACIÓN DE CAMPAÑA 15.04.19\",\"type\": \"Adquisición\",\"description\": \"\"}],\"treatments\": [{\"id\": \"1-4ABOFF\",\"number\": \"1-259205739\",\"name\": \"TRATAMIENTO Convenio ampliación de campaña 15.04.19\"}]}]}";

	@Before
	public void init() {
		EasyRandom easyRandom = new EasyRandom();
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
	}

	@Test
	public void consumeApiDetailLead() {
		Mockito.when(leadRepositoryImpl.getDetailLead(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonApiLeads);
		assertNotNull(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto));
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeApiDetailLeadEmpty() {
		Mockito.when(leadRepositoryImpl.getDetailLead(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn("");
		consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void consumeApiDetailLeadError() {
		Mockito.when(leadRepositoryImpl.getDetailLead(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonApiLeadsError);
		consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto);
	}
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void consumeApiDetailLeadGenericError() {
		Mockito.when(leadRepositoryImpl.getDetailLead(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(ConsumeApiDetailLeadCustomException.class);
		consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiDetailLeadCustomException.class)
	public void consumeApiDetailLeadServiceError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		Mockito.when(leadRepositoryImpl.getDetailLead(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto);
	}
}
