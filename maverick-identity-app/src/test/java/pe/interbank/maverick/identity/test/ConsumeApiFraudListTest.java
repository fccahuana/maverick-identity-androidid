package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiFraudList;
import pe.interbank.maverick.identity.esb.api.repository.impl.FraudListRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiFraudListCustomException;

@RunWith(PowerMockRunner.class)
public class ConsumeApiFraudListTest {

	@Mock
	FraudListRepositoryImpl fraudListRepositoryImpl;
	
	@InjectMocks
	ConsumeApiFraudList consumeApiFraudList;
	
	ConsumeApiRequestDto consumeApiRequestDto = null;
	
	@Before
	public void init() {
		consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId("12345678");
		consumeApiRequestDto.setSessionId("12345678");
		consumeApiRequestDto.setUserId("1234578");
		consumeApiRequestDto.setToken(null);
	}
	@Test
	public void consumeApiFraudList() {
		
		Mockito.when(fraudListRepositoryImpl.validatePhoneInFraudList(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn("true");
		assertEquals(true, consumeApiFraudList.consumeApiFraudList(consumeApiRequestDto,"12345678"));
	}
	
	@Test
	public void consumeApiFraudListCellphoneDoesnotBlackList() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.FORBIDDEN);
		Mockito.when(fraudListRepositoryImpl.validatePhoneInFraudList(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		assertEquals(false, consumeApiFraudList.consumeApiFraudList(consumeApiRequestDto,"12345678"));
	}
	
	@Test(expected = ConsumeApiFraudListCustomException.class)
	public void consumeApiFraudListCellphoneInBlackList() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		Mockito.when(fraudListRepositoryImpl.validatePhoneInFraudList(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiFraudList.consumeApiFraudList(consumeApiRequestDto,"12345678");
	}
}
