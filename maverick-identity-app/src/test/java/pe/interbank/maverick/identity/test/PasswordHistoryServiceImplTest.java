package pe.interbank.maverick.identity.test;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;

import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.core.model.passwordhistory.PasswordHistory;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.repository.passwordhistory.PasswordHistoryRepository;


@RunWith(PowerMockRunner.class)
public class PasswordHistoryServiceImplTest {
	
	@Mock
	PasswordHistoryRepository passwordHistoryRepository;
	
	@Mock
	SensitiveData sensitiveData;
	
	@Mock
	StringRedisTemplate redisStepTemplateDb;
	
	@Mock
	HashOperations<String,Object,Object> hashOperationSteptsRedisDb;
	
	@InjectMocks
	PasswordHistoryServiceImpl passwordHistoryServiceImpl;		
	
	RegisterRequestDto registerRequestDto = null;
	
	ValidatePasswordRequestDto validatePasswordRequestDto = null;
	
	String dni = "09984285";
	
	String userId = "289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb";
	
	List<PasswordHistory> passwordsHistory = new ArrayList<PasswordHistory>();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		registerRequestDto = easyRandom.nextObject(RegisterRequestDto.class);
		validatePasswordRequestDto = easyRandom.nextObject(ValidatePasswordRequestDto.class); 
		PasswordHistory passwordHistory = easyRandom.nextObject(PasswordHistory.class);
		PasswordHistory passwordHistory1 = easyRandom.nextObject(PasswordHistory.class);
		passwordHistory.setPassword("BzQ22TmPLL0dCSjRHZaV6hFrBmDuQxugVpkXkS+DAFEf/BCjX01a747ZOraC1Hx+ljBsv300VxB/SFl5n1v7hJbEFxgRy7+t0Qt1ji4bqSnpoQPBS4TVWYi0qyJz5Y/jcCAYN7vr+V3eNRz7EvYtSOOPAvSNmxT/q+Qv9oxyzXPqW/4lYVqL3PgknjYVE2va7tzl5uCBFfgQUN2KrjLqEBfNZcFkMVThcP3DoTV6swH3tIz464uO/ZANWhz8SlGmqaHnoR9fWdta15xX28kw1jXmIKuy21NBEnbjMO2exmPLrlwu41sW/YVFSAlHNSuV5ier30R+G/8q1XBoHSsDNA==");
		passwordHistory1.setPassword("jdDFS9Odxq0EOFCwHOHh8L6XcjjCslq/BH2XR8qJH2bkCQuw5UlrYyb1Imt48DUTRJRJi+8QUFeHHnP7bjanh1D8ajJRidhUR/8n4vQV0BS7Pj93K7cidv8mYjLzYvVrb2WB8JFDcu6/q8j4EsOl/CrO+dDYSv7OFuppS+AHdXNZZbWlskBMydd3WonhBirZjgd5Eiw+2lNgUcYSxipeRtlelJazUU1xupGuwFRS6aoph7zZQ6MfqXDPIcQ3swpu2vs1o/AROZAS9iFRo3mC/otLbHmAe2f0EhHXYiZY5fXmGxuqIRrU3vHaw+tyOs5uHrhikwd8+kVRa/JxQhCHzQ==");
		passwordsHistory.add(passwordHistory);
		passwordsHistory.add(passwordHistory1);
	}
	
	@Test(expected = DuplicateKeyException.class)
	public void morePasswordInPasswordHistoryTest() {
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setDni(dni);
		when(passwordHistoryRepository.countByUserId(userId)).thenReturn(Long.parseLong("1"));
		passwordHistoryServiceImpl.loadPasswordHistory(registerRequestDto);
	}
	
	@Test
	public void loadPasswordHistoryTest() {
		registerRequestDto.setSecret("12345678A");
		registerRequestDto.setDni(dni);
		when(passwordHistoryRepository.countByUserId(userId)).thenReturn(Long.parseLong("0"));
		passwordHistoryServiceImpl.loadPasswordHistory(registerRequestDto);
		Assert.assertNotNull(registerRequestDto);
	}
	
	@Test(expected = RecoveryPasswordCustomException.class)
	public void passwordExistInPasswordHistoryTest() {
		validatePasswordRequestDto.setDni(dni);
		validatePasswordRequestDto.setSecret("12345678a");
		when(passwordHistoryRepository.findFirst5ByUserId(Mockito.anyString(),Mockito.any())).thenReturn(passwordsHistory);
		when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678a");
		passwordHistoryServiceImpl.savePasswordHistoryByRecoveryPassword("12345678a","289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb");
	}
	
	@Test
	public void savePasswordHistoryByRecoveryPasswordTest() {
		validatePasswordRequestDto.setDni(dni);
		when(passwordHistoryRepository.findFirst5ByUserId(Mockito.anyString(),Mockito.any())).thenReturn(passwordsHistory);
		when(sensitiveData.decrypt(Mockito.anyString())).thenReturn("12345678a");
		passwordHistoryServiceImpl.savePasswordHistoryByRecoveryPassword("12345678b","289822106759e8c919d527f81976a98e2feaf9f4987815b42fd1596dc0da4feb");
		Assert.assertNotNull(validatePasswordRequestDto);
	}
	
	
}
