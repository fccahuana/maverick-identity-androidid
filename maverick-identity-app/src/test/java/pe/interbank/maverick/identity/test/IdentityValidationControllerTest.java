package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pe.interbank.maverick.identity.validation.controller.web.IdentityValidationController;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationCodeRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationPhoneRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionResponseDto;
import pe.interbank.maverick.identity.validation.service.impl.IdentityValidationServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class IdentityValidationControllerTest {

	@InjectMocks
	IdentityValidationController identityValidationController;
	@Mock
	IdentityValidationServiceImpl identityValidationServiceImpl;
	
	ValidationPhoneRequestDto validationPhoneRequestDto;
	ConfirmCarrierRequestDto confirmCarrierRequestDto;
	ValidationCodeRequestDto validationCodeRequestDto;
	GetQuestionRequestDto getQuestionRequestDto;
	GetQuestionResponseDto getQuestionResponseDto;
	ValidationQuestionRequestDto validationQuestionRequestDto;
	ValidationQuestionResponseDto validationQuestionResponseDto;
	
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		validationPhoneRequestDto =  easyRandom.nextObject(ValidationPhoneRequestDto.class);
		confirmCarrierRequestDto =  easyRandom.nextObject(ConfirmCarrierRequestDto.class);
		validationCodeRequestDto =  easyRandom.nextObject(ValidationCodeRequestDto.class);
		getQuestionRequestDto = easyRandom.nextObject(GetQuestionRequestDto.class);
		validationQuestionRequestDto = easyRandom.nextObject(ValidationQuestionRequestDto.class);
	}
	
	@Test
	public void getPhoneTest() {		
		identityValidationController.getPhone("messageIdxxxyyy","sessionIdxxxyyy",validationPhoneRequestDto);
		ResponseEntity<ValidationPhoneRequestDto> responseEntity = new ResponseEntity<>(validationPhoneRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void confirmCarrierTest() {		
		identityValidationController.confirmCarrier("messageIdxxxyyy","sessionIdxxxyyy",confirmCarrierRequestDto);
		ResponseEntity<ConfirmCarrierRequestDto> responseEntity = new ResponseEntity<>(confirmCarrierRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void validateCodeTest() {		
		identityValidationController.validateCode(validationCodeRequestDto, "messageIdxxxyyy", "sessionIdxxxyyy");
		ResponseEntity<ValidationCodeRequestDto> responseEntity = new ResponseEntity<>(validationCodeRequestDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void getQuestionsTest() {		
		identityValidationController.getQuestions("123","123",getQuestionRequestDto);
		ResponseEntity<GetQuestionResponseDto> responseEntity = new ResponseEntity<>(getQuestionResponseDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void validAnswersTest() {		
		identityValidationController.validAnswers("123","123",validationQuestionRequestDto);
		ResponseEntity<ValidationQuestionResponseDto> responseEntity = new ResponseEntity<>(validationQuestionResponseDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
}
