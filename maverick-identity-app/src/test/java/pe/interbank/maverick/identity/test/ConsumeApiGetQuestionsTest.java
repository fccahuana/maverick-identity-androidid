package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiGetQuestions;
import pe.interbank.maverick.identity.esb.api.repository.GetQuestionsRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiGetQuestionsCustomException;

@RunWith(MockitoJUnitRunner.class)
public class ConsumeApiGetQuestionsTest {

	@Mock
	GetQuestionsRepository getQuestionsRepository;
	
	@InjectMocks
	ConsumeApiGetQuestions consumeApiGetQuestions;
	
	ConsumeApiRequestDto consumeApiRequestDto = null;
	
	String jsonResponseOk = "{\"operationId\": \"202002060000030\",\"reject\": \"0\",\"unanswered\": \"1\",\"disapproved\": \"0\",\"questions\": [{\"description\": \"¿SU RUC ESTÁ ACTIVO?\",\"category\": \"05\",\"id\": \"52\",\"option\": [{\"id\": \"0\",\"description\": \"SI\"},{\"id\": \"1\",\"description\": \"NO\"}]},{\"description\": \"¿TIENE RUC?\",\"category\": \"05\",\"id\": \"51\",\"option\": [{\"id\": \"0\",\"description\": \"Si\"},{\"id\": \"1\",\"description\": \"No\"}]},{\"description\": \"¿SU EMPRESA HA HECHO EXPORTACIONES EN LOS ÚLTIMOS 12 MESES?\",\"category\": \"05\",\"id\": \"59\",\"option\": [{\"id\": \"0\",\"description\": \"Si\"},{\"id\": \"1\",\"description\": \"No\"}]}]}";
	
	String jsonParseResponse = "{{\"operationId\": \"202002060000030\",\"reject\": \"0\",\"unanswered\": \"1\",\"disapproved\": \"0\",\"questions\": [{\"description\": \"¿SU RUC ESTÁ ACTIVO?\",\"category\": \"05\",\"id\": \"52\",\"option\": [{\"id\": \"0\",\"description\": \"SI\"},{\"id\": \"1\",\"description\": \"NO\"}]},{\"description\": \"¿TIENE RUC?\",\"category\": \"05\",\"id\": \"51\",\"option\": [{\"id\": \"0\",\"description\": \"Si\"},{\"id\": \"1\",\"description\": \"No\"}]},{\"description\": \"¿SU EMPRESA HA HECHO EXPORTACIONES EN LOS ÚLTIMOS 12 MESES?\",\"category\": \"05\",\"id\": \"59\",\"option\": [{\"id\": \"0\",\"description\": \"Si\"},{\"id\": \"1\",\"description\": \"No\"}]}]}";
	
	@Before
	public void init() {
		consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId("12345678");
		consumeApiRequestDto.setSessionId("12345678");
		consumeApiRequestDto.setUserId("1234578");
		consumeApiRequestDto.setToken(null);
	}
	
	@Test
	public void consumeServiceGetQuestions() {
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonResponseOk);
		assertNotNull(consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto));
	}
	
	@Test(expected = ConsumeApiGetQuestionsCustomException.class)
	public void genericBusinessError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.FORBIDDEN);
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiGetQuestionsCustomException.class)
	public void genericError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiGetQuestionsCustomException.class)
	public void genericErrorSystemMessageOfBus() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		esbBackendCommunicationException.setEsbResponseMessage(null);
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiGetQuestionsCustomException.class)
	public void genericErrorSystemMessageOfErrorType() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		esbBackendCommunicationException.setEsbResponseMessage("Error");
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceGetQuestionsResponseEmpty() {
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn("");
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiGetQuestionsCustomException.class)
	public void parseError() {
		Mockito.when(getQuestionsRepository.getQuestions(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonParseResponse);
		consumeApiGetQuestions.consumeServiceGetQuestions("12345678", consumeApiRequestDto);
	}
	
	
}
