package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.commons.crypto.service.CryptoServiceImpl;
import pe.interbank.maverick.identity.api.ConsumeEndpointNotificationEmail;
import pe.interbank.maverick.identity.exception.api.ConsumeEndpointNotificationEmailException;
import pe.interbank.maverick.identity.register.config.RegisterConfiguration;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.util.IdentityUtil;


@RunWith(PowerMockRunner.class)
public class ConsumeEndpointNotificationEmailTest {

	@Mock
	RegisterConfiguration registerConfiguration;
	
	@Mock
	IdentityUtil identityUtil;
	
	@Mock
	SensitiveData sensitiveData;
	
	@Mock
	CryptoServiceImpl cryptoServiceImpl;
	
	@InjectMocks
	ConsumeEndpointNotificationEmail consumeEndpointNotificationEmail;
	
	RegisterRequestDto registerRequestDto;
	
	@Before
	public void init() {
		EasyRandom easyRandom = new EasyRandom();
		registerRequestDto = easyRandom.nextObject(RegisterRequestDto.class);
	}
	
	@Test(expected = ConsumeEndpointNotificationEmailException.class)
	public void consumeEndpointNotificationEmail() {
		Mockito.when(registerConfiguration.getMailTemplate()).thenReturn("template");
		Mockito.when(registerConfiguration.getPublickey()).thenReturn("11111111");
		Mockito.when(registerConfiguration.getEndpointSendEmail()).thenReturn("11111111");
		Mockito.when(cryptoServiceImpl.encryptRsa(Mockito.anyString(), Mockito.anyString())).thenReturn("111111");
		consumeEndpointNotificationEmail.consumeEndpointNotificationEmail(registerRequestDto);
	}
	
	@Test(expected = ConsumeEndpointNotificationEmailException.class)
	public void consumeEndpointNotificationEmailEmptyFirstName() {
		Mockito.when(registerConfiguration.getMailTemplate()).thenReturn("template");
		Mockito.when(registerConfiguration.getPublickey()).thenReturn("11111111");
		Mockito.when(registerConfiguration.getEndpointSendEmail()).thenReturn("11111111");
		Mockito.when(cryptoServiceImpl.encryptRsa(Mockito.anyString(), Mockito.anyString())).thenReturn("111111");
		consumeEndpointNotificationEmail.consumeEndpointNotificationEmail(registerRequestDto);
	}
}
