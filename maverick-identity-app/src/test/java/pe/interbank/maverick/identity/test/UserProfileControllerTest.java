package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import pe.interbank.maverick.identity.profile.controller.web.UserProfileController;
import pe.interbank.maverick.identity.profile.dto.UserProfileRequestDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileResponseDto;
import pe.interbank.maverick.identity.profile.service.impl.UserProfileServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class UserProfileControllerTest {

	@InjectMocks
	UserProfileController userProfileController;
	
	@Mock
	UserProfileServiceImpl userProfileService;
	
	UserProfileResponseDto userProfileResponseDto;
	UserProfileRequestDto userProfileRequestDto;
	String tokenId = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJkYTQ1ZmU4ZGMwMTVlYWI3OWQ1OThhZDI4ZmNkNjUxNjdjODc0NGM1NjM0MGEzNGZlMDc1MTg5YWQ0ZTk1NmU2Iiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm9yZ2FuaXphdGlvbiI6ImRhNDVmZThkYzAxNWVhYjc5ZDU5OGFkMjhmY2Q2NTE2N2M4NzQ0YzU2MzQwYTM0ZmUwNzUxODlhZDRlOTU2ZTZLZW1TIiwiY3VzdG9tZXJJZCI6IjAwNjAzMTIzMDciLCJleHAiOjE1ODIxMzUxNTIsImZhbWlseSI6IiIsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiJdLCJqdGkiOiI5ZTM1MGNkYS0yZDAyLTQ5OTItOTlmZC0yZDY4YzFlMGMxMGQiLCJjbGllbnRfaWQiOiIxZTRkN2Q3Mi03ZDM1LTQ5ZDctYWJlMC0wMmI0ZGJiODBiMTEifQ.pviWvuzG3tiQrodrjzBLqDljDRepOabH3Oz9JIS5TDA";
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		userProfileRequestDto =  easyRandom.nextObject(UserProfileRequestDto.class);
		userProfileResponseDto =  easyRandom.nextObject(UserProfileResponseDto.class);
	}
	
	@Test
	public void getUserProfileTest() {			
		ResponseEntity<UserProfileResponseDto> responseEntity = userProfileController.getUserProfile(tokenId, "fcmToken", "fingerPrint");
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void updateUserProfileTest() {			
		ResponseEntity<UserProfileResponseDto> responseEntity = userProfileController.updateUserProfile(tokenId, "fcmtoken", "fingerprint", userProfileRequestDto);
		Assert.assertNotNull(responseEntity);	
	}
	
	
}
