package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpStatus;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiValidationQuestions;
import pe.interbank.maverick.identity.esb.api.repository.ValidationQuestionsRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiValidationQuestionsCustomException;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;

@RunWith(PowerMockRunner.class)
public class ConsumeApiValidationQuestionsTest {
	
	@Mock
	ValidationQuestionsRepository validationQuestionsRepository;
	
	@InjectMocks
	ConsumeApiValidationQuestions consumeApiValidationQuestions;
	
	ConsumeApiRequestDto consumeApiRequestDto = null;
	
	ValidationQuestionRequestDto validationQuestionRequestDto = null;
	
	String responseOk = "{\"model\": \"02563912\",\"user\": \"WSIDVIBK1\",\"customer\": {\"fullName\": \"UTIA TORREJON MARLY MERCEDES\",\"identityDocument\": {\"type\": \"1\",\"id\": \"00042102295\"}},\"operation\": {\"date\": \"10/02/2020\",\"hour\": \"15:12\",\"id\": \"202002100000034\"},\"result\": {\"description\": \"DESAPROBADA\",\"reason\": \"SIN DESCRIPCION\",\"reject\": \"0\",\"disapproved\": \"1\",\"unanswered\": \"1\"}}";
	
	String responseParse = "{{\"model\": \"02563912\",\"user\": \"WSIDVIBK1\",\"customer\": {\"fullName\": \"UTIA TORREJON MARLY MERCEDES\",\"identityDocument\": {\"type\": \"1\",\"id\": \"00042102295\"}},\"operation\": {\"date\": \"10/02/2020\",\"hour\": \"15:12\",\"id\": \"202002100000034\"},\"result\": {\"description\": \"DESAPROBADA\",\"reason\": \"SIN DESCRIPCION\",\"reject\": \"0\",\"disapproved\": \"1\",\"unanswered\": \"1\"}}";
	
	@Before
	public void init() {
		consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId("12345678");
		consumeApiRequestDto.setSessionId("12345678");
		consumeApiRequestDto.setUserId("1234578");
		consumeApiRequestDto.setToken(null);
		EasyRandom easyRandom = new EasyRandom();
		validationQuestionRequestDto = easyRandom.nextObject(ValidationQuestionRequestDto.class);
	}
	
	@Test
	public void consumeServiceValidationQuestions() {
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(), Mockito.any())).thenReturn(responseOk);
		assertNotNull(consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto));
	}
	
	@Test(expected = ConsumeApiValidationQuestionsCustomException.class)
	public void genericBusinessError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.FORBIDDEN);
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}
	
	@Test(expected = ConsumeApiValidationQuestionsCustomException.class)
	public void genericError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}
	
	@Test(expected = ConsumeApiValidationQuestionsCustomException.class)
	public void genericErrorSystemMessageOfBus() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		esbBackendCommunicationException.setEsbResponseMessage(null);
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}
	
	@Test(expected = ConsumeApiValidationQuestionsCustomException.class)
	public void genericErrorSystemMessageOfErrorType() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		esbBackendCommunicationException.getError().setHttpStatus(HttpStatus.CONFLICT);
		esbBackendCommunicationException.setEsbResponseMessage("Error");
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceGetQuestionsResponseEmpty() {
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn("");
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}
	
	@Test(expected = ConsumeApiValidationQuestionsCustomException.class)
	public void parseError() {
		Mockito.when(validationQuestionsRepository.validationAnswers(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(responseParse);
		consumeApiValidationQuestions.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
	}

}
