package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiCustomerLeads;
import pe.interbank.maverick.identity.api.ConsumeApiLead;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponseDto;
import pe.interbank.maverick.identity.campaign.service.impl.CampaignServiceImpl;
import pe.interbank.maverick.identity.esb.api.model.lead.Additional;
import pe.interbank.maverick.identity.esb.api.model.lead.DetailLead;
import pe.interbank.maverick.identity.esb.api.model.lead.DetailLeadsResponseDto;
import pe.interbank.maverick.identity.esb.api.model.leads.Lead;
import pe.interbank.maverick.identity.esb.api.model.leads.LeadsResponseDto;
import pe.interbank.maverick.identity.repository.register.UserRepository;



@RunWith(PowerMockRunner.class)
public class CampaignServiceImplTest {

	@Mock
	ConsumeApiCustomerLeads consumeApiCustomerLeads;
	
	@Mock
	ConsumeApiLead consumeApiLead;
	
	@Mock
	UserRepository userRepository;
	
	@Mock
	SensitiveData sensitiveData;
	
	@InjectMocks
	CampaignServiceImpl campaignServiceImpl;
	
	CampaignResponseDto campaignResponseDto;
	
	ConsumeApiRequestDto consumeApiRequestDto;
	
	LeadsResponseDto leadsResponseDto;
	DetailLeadsResponseDto detailLeadsResponseDto;
	List<Additional> additionalOne = null;
	List<DetailLead> detailsleadOne = null;
	
	@Before
	public void init() {
		EasyRandom easyRandom = new EasyRandom();
		ReflectionTestUtils.setField(campaignServiceImpl, "campaignBuc", "BUC", String.class);
		campaignResponseDto = easyRandom.nextObject(CampaignResponseDto.class);
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
		leadsResponseDto = easyRandom.nextObject(LeadsResponseDto.class);
		detailLeadsResponseDto = easyRandom.nextObject(DetailLeadsResponseDto.class);
		additionalOne = easyRandom.objects(Additional.class, 1).collect(Collectors.toList());
		detailsleadOne = easyRandom.objects(DetailLead.class, 1).collect(Collectors.toList());
	}
	
	@Test
	public void getBucLead() {
		Lead lead = new Lead();
		lead.setId("12345678");
		List<Lead> leads = new ArrayList<>();
		leads.add(lead);
		leadsResponseDto.setLeads(leads);
		additionalOne.get(0).setItem7("0004");
		detailsleadOne.get(0).setAdditionals(additionalOne);
		detailsleadOne.get(0).getAdditionals().get(0).setItem15("BUC");
		detailsleadOne.get(0).setAmount("1500");
		detailsleadOne.get(0).getCurrency().setId("1");
		detailLeadsResponseDto.setLeads(detailsleadOne);
		Mockito.when(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678",consumeApiRequestDto)).thenReturn(leadsResponseDto);
		Mockito.when(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto)).thenReturn(detailLeadsResponseDto);
		assertNotNull(campaignServiceImpl.getBucLead("12345678", consumeApiRequestDto));
	}
	
	@Test
	public void getBucLeadAdditionalNull() {
		Lead lead = new Lead();
		lead.setId("12345678");
		List<Lead> leads = new ArrayList<>();
		leads.add(lead);
		leadsResponseDto.setLeads(leads);
		additionalOne.get(0).setItem7("0004");
		detailsleadOne.get(0).setAdditionals(additionalOne);
		detailsleadOne.get(0).getAdditionals().set(0, null);
		detailsleadOne.get(0).setAmount("1500");
		detailsleadOne.get(0).getCurrency().setId("1");
		detailLeadsResponseDto.setLeads(detailsleadOne);
		Mockito.when(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678",consumeApiRequestDto)).thenReturn(leadsResponseDto);
		Mockito.when(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto)).thenReturn(detailLeadsResponseDto);
		assertNotNull(campaignServiceImpl.getBucLead("12345678", consumeApiRequestDto));
	}
	
	@Test
	public void getBucLeadAdditionalAA() {
		Lead lead = new Lead();
		lead.setId("12345678");
		List<Lead> leads = new ArrayList<>();
		leads.add(lead);
		leadsResponseDto.setLeads(leads);
		additionalOne.get(0).setItem7("0004");
		detailsleadOne.get(0).setAdditionals(additionalOne);
		detailsleadOne.get(0).getAdditionals().get(0).setItem15("AA");
		detailsleadOne.get(0).setAmount("1500");
		detailsleadOne.get(0).getCurrency().setId("1");
		detailLeadsResponseDto.setLeads(detailsleadOne);
		Mockito.when(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678",consumeApiRequestDto)).thenReturn(leadsResponseDto);
		Mockito.when(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto)).thenReturn(detailLeadsResponseDto);
		assertNotNull(campaignServiceImpl.getBucLead("12345678", consumeApiRequestDto));
	}
	
	@Test
	public void getBucLeadAdditionalItem15() {
		Lead lead = new Lead();
		lead.setId("12345678");
		List<Lead> leads = new ArrayList<>();
		leads.add(lead);
		leadsResponseDto.setLeads(leads);
		additionalOne.get(0).setItem7("0004");
		detailsleadOne.get(0).setAdditionals(additionalOne);
		detailsleadOne.get(0).getAdditionals().get(0).setItem15(null);
		detailsleadOne.get(0).setAmount("1500");
		detailsleadOne.get(0).getCurrency().setId("1");
		detailLeadsResponseDto.setLeads(detailsleadOne);
		Mockito.when(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678",consumeApiRequestDto)).thenReturn(leadsResponseDto);
		Mockito.when(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto)).thenReturn(detailLeadsResponseDto);
		assertNotNull(campaignServiceImpl.getBucLead("12345678", consumeApiRequestDto));
	}
	
	
	@Test
	public void getCampaignNotBuc() {
		Lead lead = new Lead();
		lead.setId("12345678");
		List<Lead> leads = new ArrayList<>();
		leads.add(lead);
		leadsResponseDto.setLeads(leads);
		additionalOne.get(0).setItem7("0004");
		detailsleadOne.get(0).setAdditionals(additionalOne);
		detailsleadOne.get(0).getAdditionals().get(0).setItem15("BUC1");
		detailsleadOne.get(0).setAmount("1500");
		detailsleadOne.get(0).getCurrency().setId("1");
		detailLeadsResponseDto.setLeads(detailsleadOne);
		Mockito.when(consumeApiCustomerLeads.consumeApiCustomerLeads("12345678",consumeApiRequestDto)).thenReturn(leadsResponseDto);
		Mockito.when(consumeApiLead.consumeApiLeads("12345678", consumeApiRequestDto)).thenReturn(detailLeadsResponseDto);
		assertNotNull(campaignServiceImpl.getBucLead("12345678", consumeApiRequestDto));
	}
	

}
