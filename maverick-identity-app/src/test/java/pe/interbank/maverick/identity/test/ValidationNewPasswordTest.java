package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.modules.junit4.PowerMockRunner;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RequiredValidations;
import pe.interbank.maverick.identity.recoverypassword.service.impl.ValidationNewPassword;

@RunWith(PowerMockRunner.class)
public class ValidationNewPasswordTest {
	
	@Mock
	RequiredValidations requiredValidations;
	
	@InjectMocks
	ValidationNewPassword validationNewPassword;
	
	ValidatePasswordRequestDto validatePasswordRequestDto;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		EasyRandom easyRandom = new EasyRandom();
		validatePasswordRequestDto = easyRandom.nextObject(ValidatePasswordRequestDto.class);
	}
	
	@Test
	public void validationNewPasswordOK() {
		validatePasswordRequestDto.setSecret("12345678A");
		validationNewPassword.validationNewPassword(validatePasswordRequestDto);
		assertNotNull(validationNewPassword);
	}

}
