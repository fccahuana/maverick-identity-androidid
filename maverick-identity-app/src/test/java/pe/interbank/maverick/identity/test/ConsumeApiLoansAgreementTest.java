package pe.interbank.maverick.identity.test;

import static org.junit.Assert.assertNotNull;

import org.jeasy.random.EasyRandom;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.api.ConsumeApiLoansAgreement;
import pe.interbank.maverick.identity.esb.api.repository.LoansAgreementRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiLoansAgreementCustomException;


@RunWith(MockitoJUnitRunner.class)
public class ConsumeApiLoansAgreementTest {
	
	@Mock
	LoansAgreementRepository loansAgreementRepository;
	
	@InjectMocks
	ConsumeApiLoansAgreement consumeApiLoansAgreement;
	
	String jsonResponseLoansAgreement = "{\r\n" + 
			"   \"customer\":    {\r\n" + 
			"      \"id\": \"0060078514\",\r\n" + 
			"      \"firstName\": \"MARIO\",\r\n" + 
			"      \"secondName\": \"EDMUNDO\",\r\n" + 
			"      \"lastName\": \"ALVARADO\",\r\n" + 
			"      \"secondLastName\": \"GAMBOA\",\r\n" + 
			"      \"identityDocument\":       {\r\n" + 
			"         \"type\": \"1\",\r\n" + 
			"         \"id\": \"05644664\"\r\n" + 
			"      }\r\n" + 
			"   },\r\n" + 
			"   \"credits\":    [\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00056809\",\r\n" + 
			"         \"companyId\": \"0050002368\",\r\n" + 
			"         \"agreementName\": \"AUTOCRAFT\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"19.5618\",\r\n" + 
			"            \"tcea\": \"38.1165\",\r\n" + 
			"            \"date\": \"11/02/2019\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"1035.57\",\r\n" + 
			"            \"paidCapitalAmount\": \"0.00\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"0\",\r\n" + 
			"            \"pending\": \"30\",\r\n" + 
			"            \"total\": \"30\",\r\n" + 
			"            \"amount\": \"50.99\",\r\n" + 
			"            \"dueDate\": \"17/04/2022\",\r\n" + 
			"            \"nextDueDate\": \"17/04/2022\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00005573\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"23.9363\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"10000.00\",\r\n" + 
			"            \"capitalBalance\": \"21095.66\",\r\n" + 
			"            \"paidCapitalAmount\": \"21095.66\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"4\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"4\",\r\n" + 
			"            \"amount\": \"653.70\",\r\n" + 
			"            \"dueDate\": \"17/05/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"000056902\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"27.4523\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"10547.84\",\r\n" + 
			"            \"paidCapitalAmount\": \"10547.84\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"3\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"3\",\r\n" + 
			"            \"amount\": \"10527.65\",\r\n" + 
			"            \"dueDate\": \"17/04/2018\",\r\n" + 
			"            \"nextDueDate\": \"17/04/2022\"\r\n" + 
			"         }\r\n" + 
			"      }\r\n" + 
			"   ]\r\n" + 
			"}";
	
	String jsonResponseLoansAgreementCanceled = "{\r\n" + 
			"   \"customer\":    {\r\n" + 
			"      \"id\": \"0060078514\",\r\n" + 
			"      \"firstName\": \"MARIO\",\r\n" + 
			"      \"secondName\": \"EDMUNDO\",\r\n" + 
			"      \"lastName\": \"ALVARADO\",\r\n" + 
			"      \"secondLastName\": \"GAMBOA\",\r\n" + 
			"      \"identityDocument\":       {\r\n" + 
			"         \"type\": \"1\",\r\n" + 
			"         \"id\": \"05644664\"\r\n" + 
			"      }\r\n" + 
			"   },\r\n" + 
			"   \"credits\":    [\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00056809\",\r\n" + 
			"         \"companyId\": \"0050002368\",\r\n" + 
			"         \"agreementName\": \"AUTOCRAFT\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"19.5618\",\r\n" + 
			"            \"tcea\": \"38.1165\",\r\n" + 
			"            \"date\": \"11/02/2019\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"1035.57\",\r\n" + 
			"            \"paidCapitalAmount\": \"0.00\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"0\",\r\n" + 
			"            \"pending\": \"30\",\r\n" + 
			"            \"total\": \"30\",\r\n" + 
			"            \"amount\": \"50.99\",\r\n" + 
			"            \"dueDate\": \"17/04/2022\",\r\n" + 
			"            \"nextDueDate\": \"SinDesembolso\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00005573\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"23.9363\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"10000.00\",\r\n" + 
			"            \"capitalBalance\": \"21095.66\",\r\n" + 
			"            \"paidCapitalAmount\": \"21095.66\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"4\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"4\",\r\n" + 
			"            \"amount\": \"653.70\",\r\n" + 
			"            \"dueDate\": \"17/05/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"000056902\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"27.4523\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"10547.84\",\r\n" + 
			"            \"paidCapitalAmount\": \"10547.84\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"3\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"3\",\r\n" + 
			"            \"amount\": \"10527.65\",\r\n" + 
			"            \"dueDate\": \"17/04/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      }\r\n" + 
			"   ]\r\n" + 
			"}";
	
	String jsonResponseLoansAgreementEqualsAmount = "{\r\n" + 
			"   \"customer\":    {\r\n" + 
			"      \"id\": \"0060078514\",\r\n" + 
			"      \"firstName\": \"MARIO\",\r\n" + 
			"      \"secondName\": \"EDMUNDO\",\r\n" + 
			"      \"lastName\": \"ALVARADO\",\r\n" + 
			"      \"secondLastName\": \"GAMBOA\",\r\n" + 
			"      \"identityDocument\":       {\r\n" + 
			"         \"type\": \"1\",\r\n" + 
			"         \"id\": \"05644664\"\r\n" + 
			"      }\r\n" + 
			"   },\r\n" + 
			"   \"credits\":    [\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00056809\",\r\n" + 
			"         \"companyId\": \"0050002368\",\r\n" + 
			"         \"agreementName\": \"AUTOCRAFT\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"19.5618\",\r\n" + 
			"            \"tcea\": \"38.1165\",\r\n" + 
			"            \"date\": \"11/02/2019\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"1035.57\",\r\n" + 
			"            \"paidCapitalAmount\": \"0.00\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"0\",\r\n" + 
			"            \"pending\": \"30\",\r\n" + 
			"            \"total\": \"30\",\r\n" + 
			"            \"amount\": \"50.99\",\r\n" + 
			"            \"dueDate\": \"17/04/2022\",\r\n" + 
			"            \"nextDueDate\": \"17/04/2022\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00005573\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"23.9363\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"21095.66\",\r\n" + 
			"            \"paidCapitalAmount\": \"21095.66\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"4\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"4\",\r\n" + 
			"            \"amount\": \"653.70\",\r\n" + 
			"            \"dueDate\": \"17/05/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"000056902\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"27.4523\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"10547.84\",\r\n" + 
			"            \"paidCapitalAmount\": \"10547.84\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"3\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"3\",\r\n" + 
			"            \"amount\": \"10527.65\",\r\n" + 
			"            \"dueDate\": \"17/04/2018\",\r\n" + 
			"            \"nextDueDate\": \"17/04/2022\"\r\n" + 
			"         }\r\n" + 
			"      }\r\n" + 
			"   ]\r\n" + 
			"}";
	
	String jsonResponseLoansAgreementError = "{{\r\n" + 
			"   \"customer\":    {\r\n" + 
			"      \"id\": \"0060078514\",\r\n" + 
			"      \"firstName\": \"MARIO\",\r\n" + 
			"      \"secondName\": \"EDMUNDO\",\r\n" + 
			"      \"lastName\": \"ALVARADO\",\r\n" + 
			"      \"secondLastName\": \"GAMBOA\",\r\n" + 
			"      \"identityDocument\":       {\r\n" + 
			"         \"type\": \"1\",\r\n" + 
			"         \"id\": \"05644664\"\r\n" + 
			"      }\r\n" + 
			"   },\r\n" + 
			"   \"credits\":    [\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00056809\",\r\n" + 
			"         \"companyId\": \"0050002368\",\r\n" + 
			"         \"agreementName\": \"AUTOCRAFT\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"19.5618\",\r\n" + 
			"            \"tcea\": \"38.1165\",\r\n" + 
			"            \"date\": \"11/02/2019\",\r\n" + 
			"            \"amount\": \"1000.00\",\r\n" + 
			"            \"capitalBalance\": \"1035.57\",\r\n" + 
			"            \"paidCapitalAmount\": \"0.00\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"0\",\r\n" + 
			"            \"pending\": \"30\",\r\n" + 
			"            \"total\": \"30\",\r\n" + 
			"            \"amount\": \"50.99\",\r\n" + 
			"            \"dueDate\": \"17/04/2022\",\r\n" + 
			"            \"nextDueDate\": \"17/04/2022\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00005573\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"23.9363\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"20000.00\",\r\n" + 
			"            \"capitalBalance\": \"21095.66\",\r\n" + 
			"            \"paidCapitalAmount\": \"21095.66\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"4\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"4\",\r\n" + 
			"            \"amount\": \"653.70\",\r\n" + 
			"            \"dueDate\": \"17/05/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      },\r\n" + 
			"            {\r\n" + 
			"         \"id\": \"00005172\",\r\n" + 
			"         \"companyId\": \"0050002376\",\r\n" + 
			"         \"agreementName\": \"BEST FERRETERA\",\r\n" + 
			"         \"disbursement\":          {\r\n" + 
			"            \"currencyName\": \"SOLES\",\r\n" + 
			"            \"insuranceDesgravamen\": \"0.0750\",\r\n" + 
			"            \"tea\": \"29.8407\",\r\n" + 
			"            \"tcea\": \"27.4523\",\r\n" + 
			"            \"date\": \"07/11/2017\",\r\n" + 
			"            \"amount\": \"10000.00\",\r\n" + 
			"            \"capitalBalance\": \"10547.84\",\r\n" + 
			"            \"paidCapitalAmount\": \"10547.84\"\r\n" + 
			"         },\r\n" + 
			"         \"installment\":          {\r\n" + 
			"            \"paid\": \"3\",\r\n" + 
			"            \"pending\": \"0\",\r\n" + 
			"            \"total\": \"3\",\r\n" + 
			"            \"amount\": \"10527.65\",\r\n" + 
			"            \"dueDate\": \"17/04/2018\",\r\n" + 
			"            \"nextDueDate\": \"Cancelado\"\r\n" + 
			"         }\r\n" + 
			"      }\r\n" + 
			"   ]\r\n" + 
			"}}";
	
	ConsumeApiRequestDto consumeApiRequestDto;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);	
		EasyRandom easyRandom = new EasyRandom();
		consumeApiRequestDto = easyRandom.nextObject(ConsumeApiRequestDto.class);
	}
	
	@Test
	public void consumeServiceLoansAgreementOK() {
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonResponseLoansAgreement);
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);	
		assertNotNull(jsonResponseLoansAgreement);
	}
	
	@Test
	public void consumeServiceLoansAgreementCanceledOK() {
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonResponseLoansAgreementCanceled);
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);		
		assertNotNull(jsonResponseLoansAgreement);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceLoansAgreementResponseNull() {
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(null);
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);
	}
	
	@Test(expected = NullPointerException.class)
	public void consumeServiceLoansAgreementResponseEmpty() {
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn("");
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiLoansAgreementCustomException.class)
	public void consumeServiceLoansAgreementParseResponseError() {
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenReturn(jsonResponseLoansAgreementError);
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);
	}
	
	@Test(expected = ConsumeApiLoansAgreementCustomException.class)
	public void consumeServiceLoansAgreementGenericError() {
		EsbBackendCommunicationException esbBackendCommunicationException = new EsbBackendCommunicationException();
		esbBackendCommunicationException.setError(new ErrorDto());
		Mockito.when(loansAgreementRepository.getLoansAgreement(Mockito.any(),Mockito.any(),Mockito.any())).thenThrow(esbBackendCommunicationException);
		consumeApiLoansAgreement.consumeServiceLoansAgreement("12345678",consumeApiRequestDto);
	}
	

}
