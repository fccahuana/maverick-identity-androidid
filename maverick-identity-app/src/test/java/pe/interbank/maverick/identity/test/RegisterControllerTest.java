package pe.interbank.maverick.identity.test;

import org.jeasy.random.EasyRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import pe.interbank.maverick.identity.register.controller.web.RegisterController;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.dto.RegisterResponseDto;
import pe.interbank.maverick.identity.register.service.impl.RegisterServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class RegisterControllerTest {

	@InjectMocks
	RegisterController registerController;
	
	@Mock
	RegisterServiceImpl registerServiceImpl;
	
	RegisterRequestDto registerRequestDto;
	RegisterResponseDto registerResponseDto;
	
	@Before
	public void setUp() throws Exception {
		EasyRandom easyRandom = new EasyRandom();
		registerRequestDto =  easyRandom.nextObject(RegisterRequestDto.class);
		registerResponseDto =  easyRandom.nextObject(RegisterResponseDto.class);
	}
	
	@Test
	public void validationDniTest() {			
		ResponseEntity<RegisterResponseDto> responseEntity = registerController.validationDni("messageIdxxxyyy", "sessionIdxxxyyy", registerRequestDto);
		Assert.assertNotNull(responseEntity);	
	}
	
	@Test
	public void savePhone() {
		
		ResponseEntity<RegisterResponseDto> responseEntity = registerController.savePhone("12345678", "12345678", registerRequestDto);
		Assert.assertNotNull(responseEntity);	
		
	}
	
	
	@Test
	public void createUserTest() {		
		registerController.createUser("messageId", "sessionId", registerRequestDto);
		ResponseEntity<RegisterResponseDto> responseEntity = new ResponseEntity<>(registerResponseDto, HttpStatus.OK);
		Assert.assertNotNull(responseEntity);	
	}
	
	
}
