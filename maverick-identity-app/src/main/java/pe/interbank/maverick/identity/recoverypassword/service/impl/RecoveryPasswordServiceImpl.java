package pe.interbank.maverick.identity.recoverypassword.service.impl;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiGenerateKey;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.recoverypassword.dto.ForgotPasswordDto;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordResponseDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSRequestDto;
import pe.interbank.maverick.identity.recoverypassword.service.IRecoveryPasswordService;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;

@Service
public class RecoveryPasswordServiceImpl implements IRecoveryPasswordService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	ConsumeApiInformationPerson consumeApiInformationPerson;

	@Autowired
	IdentityUtil identityUtil;

	@Autowired
	ValidationDni dniValidation;

	@Autowired
	SensitiveData sensitiveData;

	@Autowired
	ValidationNewPassword validationNewPassword;

	@Autowired
	ConsumeApiGenerateKey consumeApiGenerateKey;

	@Autowired
	RequiredValidations requiredValidations;

	@Autowired
	SecurityValidationRedis securityValidationRedis;

	@Autowired
	PasswordHistoryServiceImpl passwordHistoryService;

	@Autowired
	@Qualifier("stringRedisTemplateStep")
	protected StringRedisTemplate redisStepTemplate;
	
	@Autowired
	@Qualifier("identityValidationTokenOtpRedisDb")
	StringRedisTemplate identityValidationTokenOtpRedisDb;

	@Override
	public RecoveryPasswordResponseDto validateScreenDni(RecoveryPasswordRequestDto recoveryPasswordRequestDto) {
		RecoveryPasswordResponseDto recoveryPasswordResponseDto = new RecoveryPasswordResponseDto();
		String dni = recoveryPasswordRequestDto.getDni();
		dniValidation.validateDni(recoveryPasswordRequestDto);
		String userId = DigestUtils.sha256Hex(dni);
		requiredValidations.validateRegisteredUser(userId);
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(recoveryPasswordRequestDto.getMessageId(),
				userId, recoveryPasswordRequestDto.getSessionId());
		GetInformationPersonResponseDto getInformationPersonResponseDto = consumeApiInformationPerson
				.consumeServiceApiInformationPerson(consumeApiRequestDto, dni);
		identityUtil.updateNames(getInformationPersonResponseDto, userId);
        String phoneNumber = identityUtil.getPhoneNumber(getInformationPersonResponseDto, userId);
		String obfuscated = Util.ofuscateFieldValidation(phoneNumber, '•', 2, 3);
		ForgotPasswordDto forgotPasswordDto = new ForgotPasswordDto();
		forgotPasswordDto.setPhone(obfuscated);
		recoveryPasswordResponseDto.setForgot(forgotPasswordDto);
		securityValidationRedis.generateHashStep1(userId, phoneNumber);
		return recoveryPasswordResponseDto;

	}

	@Override
	public RecoveryPasswordResponseDto confirmScreenCarrier(ConfirmCarrierRequestDto confirmCarrierRequestDto) {
		RecoveryPasswordResponseDto recoveryPasswordResponseDto = new RecoveryPasswordResponseDto();
		String dni = confirmCarrierRequestDto.getDni().trim();
		String userId = DigestUtils.sha256Hex(dni);
		String idTokenForRedis = identityUtil.generateIdToken(dni, Constants.FLOW_RECOVERY_PASS);
		String carrier = confirmCarrierRequestDto.getCarrier().toUpperCase();
		securityValidationRedis.validateHashStep1(dni);
		String phoneNumber = securityValidationRedis.getPhoneNumberOfRedis(userId);
		securityValidationRedis.validateAttempsRecoveryPassword(userId);
		User userDecrypted = getUserFoundDecrypted(userId, phoneNumber, dni, carrier);
		GenerateKeyResponseDto generateKeyResponseDto = consumeApiGenerateKey.consumeServiceGenerateKey(userDecrypted);
		String smsBody = identityUtil.getJsonSMS(generateKeyResponseDto.getValidToken(), userDecrypted,
				confirmCarrierRequestDto.getMessageId(), confirmCarrierRequestDto.getSessionId());
		CompletableFuture.runAsync(() -> identityUtil.sendMessage(smsBody));
		identityUtil.saveTokenInRedis(idTokenForRedis, generateKeyResponseDto);
		securityValidationRedis.updateAttemptsInRedis(userId);
		securityValidationRedis.generateHashStep2(dni, carrier);
		recoveryPasswordResponseDto.setForgot(getForgotPasswordDto(generateKeyResponseDto.getToken(), phoneNumber, carrier));
		return recoveryPasswordResponseDto;
	}

	private User getUserFoundDecrypted(String userId, String phoneNumber, String dni, String carrier) {
		User userFound = identityUtil.getUser(userId);
		userFound.setPhoneNumber(phoneNumber);
		userFound.setDni(dni);
		userFound.setCarrier(carrier.toUpperCase());
		return userFound;
	}

	private ForgotPasswordDto getForgotPasswordDto(String token, String phoneNumber, String carrier) {
		ForgotPasswordDto forgotPasswordDto = new ForgotPasswordDto();
		StringBuilder sb = new StringBuilder(Util.ofuscateFieldValidation(phoneNumber, '•', 2, 3));
		sb.append("(");
		sb.append(CarrierType.valueOf(carrier).getFormatName());
		sb.append(")");
		forgotPasswordDto.setPhone(sb.toString());
		forgotPasswordDto.setHashToken(token);
		return forgotPasswordDto;
	}

	@Override
	public void validateScreenValidationCode(ValidateTokenSMSRequestDto validateTokenSMSRequestDto) {
		String dni = validateTokenSMSRequestDto.getDni().trim();
		String idTokenForRedis = identityUtil.generateIdToken(dni, Constants.FLOW_RECOVERY_PASS);
		requiredValidations.validationFieldNull(dni);
		securityValidationRedis.validateHashStep1(dni);
		identityUtil.validateTokenInRedis(idTokenForRedis, validateTokenSMSRequestDto.getVerificationCode(),
				ErrorType.ERROR_VALIDATION_CODE);
		requiredValidations.validationFieldNull(dni);
		securityValidationRedis.generateHashStep3(dni, validateTokenSMSRequestDto.getVerificationCode().trim());
	}

	@Override
	public void validateScreenNewPassword(ValidatePasswordRequestDto validatePasswordRequestDto) {
		String dni = validatePasswordRequestDto.getDni().trim();
		String idTokenForRedis = identityUtil.generateIdToken(dni, Constants.FLOW_RECOVERY_PASS);
		String verificationCode = validatePasswordRequestDto.getVerificationCode();
		requiredValidations.validationFieldNull(dni);
		requiredValidations.validationFieldNull(verificationCode);
		String userId = DigestUtils.sha256Hex(dni);
		validationNewPassword.validationNewPassword(validatePasswordRequestDto);
		securityValidationRedis.validateHashStep1(dni);
		securityValidationRedis.validateHashStep3(dni, verificationCode);
		savePasswordHistoryByRecoveryPassword(validatePasswordRequestDto);
		updateUser(validatePasswordRequestDto,userId);
		securityValidationRedis.deleteStepsOfRedis(userId);
		identityUtil.deleteKeyOfRedis(idTokenForRedis, identityValidationTokenOtpRedisDb);
	}

	private void savePasswordHistoryByRecoveryPassword(ValidatePasswordRequestDto validatePasswordRequestDto) {
		String dniHash = DigestUtils.sha256Hex(validatePasswordRequestDto.getDni());
		String newPassword = validatePasswordRequestDto.getSecret().trim();
		passwordHistoryService.savePasswordHistoryByRecoveryPassword(newPassword, dniHash);
	}

	private void updateUser(ValidatePasswordRequestDto validatePasswordRequestDto, String userId) {
		Optional<User> userFound = userRepository.findById(userId);
		if (userFound.isPresent()) {
			User updateUser = userFound.get();
			String newPassword = validatePasswordRequestDto.getSecret();
			Map<Object, Object> map = redisStepTemplate.opsForHash().entries(userId);
			String phoneNumberRedis = (String) map.get(Constants.PHONE);
			String carrierRedis = (String) map.get(Constants.CARRIER);
			updateUser.setPhoneNumber(sensitiveData.encryptHsm("+51".concat(phoneNumberRedis)));
			updateUser.setPhoneNumberId(DigestUtils.sha256Hex("+51".concat(phoneNumberRedis)));
			updateUser.setCarrier(carrierRedis);
			updateUser.setPassword(DigestUtils.sha256Hex(newPassword));
			updateUser.setAttempts(0);
			userRepository.save(updateUser);
		}

	}

}
