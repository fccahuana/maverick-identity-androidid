package pe.interbank.maverick.identity.oauth.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessTokenDto {

	private String accessToken;
	
	public AccessTokenDto(String accesToeken) {
		this.accessToken = accesToeken;
	}
}
