package pe.interbank.maverick.identity.login.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.model.androididaudit.AndroidIdAudit;
import pe.interbank.maverick.identity.core.model.androididwhite.AndroidId;
import pe.interbank.maverick.identity.core.model.androididwhite.LoginUser;
import pe.interbank.maverick.identity.core.model.androididwhite.AndroidIdWhite;
import pe.interbank.maverick.identity.core.model.login.AccessToken;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.core.util.UtilToken;
import pe.interbank.maverick.identity.exception.login.LoginCustomException;
import pe.interbank.maverick.identity.login.dto.LoginDto;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.login.dto.LoginResponseDto;
import pe.interbank.maverick.identity.login.service.ILoginService;
import pe.interbank.maverick.identity.oauth.service.impl.TokenServiceImpl;
import pe.interbank.maverick.identity.repository.androididaudit.AndroidIdAuditRepository;
import pe.interbank.maverick.identity.repository.androididwhite.AndroidIdWhiteRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@Service
public class LoginServiceImpl implements ILoginService {
	
	static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AndroidIdWhiteRepository androidIdWhiteRepository;
	
	@Autowired
	AndroidIdAuditRepository androidIdAuditRepository;
	
	@Autowired
	@Qualifier("stringRedisTemplateLogin")
	StringRedisTemplate stringRedisTemplateLogin;

	@Autowired
	@Qualifier("stringRedisTemplateAndroidId")
	StringRedisTemplate stringRedisTemplateAndroidId;

	@Autowired
	IdentityUtil identityUtil;

	@Value("${identity.token.expiration}")
	long tokenExpirationTime;

	@Value("${identity.login.attempts}")
	int attempts;
	
	@Value("${identity.androidId.timePeriod}")
	int timePeriod;
	
	@Value("${identity.androidId.timeBlocked}")
	int timeBlocked;
	
	@Value("${identity.androidId.maxUsers}")
	int maxUsers;
	
    @Value("${identity.androidId.titleErrorBlocked}")
	String titleErrorBlocked;
	
	@Value("${identity.androidId.messageErrorBlocked}")
	String messageErrorBlocked;
	
	@Autowired
	TokenServiceImpl tokenService;

	private Gson gson = new Gson();
	private static final char FINISH = 'F';
	private static final int NEGATIVE_ONE = -1;
	
	@Override
	public LoginResponseDto loginAndAccesToken(LoginRequestDto loginRequestDto, String fcmToken) {

		LoginResponseDto loginResponseDto = null;
		User correctUser = null;
		
		synchronized (this) {
			validateBlockedAndroidIdRedis(loginRequestDto.getAndroidId());
			if(StringUtils.isEmpty(loginRequestDto.getPassword())) {
				correctUser = validateCorrectUserFingerPrint(loginRequestDto);
			} else {
				validateRegisteredUser(loginRequestDto, ErrorType.ERROR_UNREGISTERED_USER);
				validateUserBlocked(loginRequestDto, ErrorType.ERROR_BLOCKED_ACCOUNT);
				correctUser = validateCorrectUser(loginRequestDto, ErrorType.ERROR_INCORRECT_PASSWORD_OR_USER);
			}
		}

		if (FINISH == correctUser.getStatus()) {
			validateUniqueFcmToken(fcmToken);
			loginResponseDto = new LoginResponseDto();
			correctUser.setAttempts(0);
			correctUser.setFcmToken(fcmToken);
			userRepository.save(correctUser);
			
			if (isAndroidIdInWhitelist(loginRequestDto)) {
			} else {
				validateAndroidId(loginRequestDto);
			}
			
			Map<String, String> accesTokenCache = new HashMap<>();
			AccessToken token = tokenService.getAccessToken(loginRequestDto, correctUser);
			String accessToken = token.getAccesToken();
			accesTokenCache.put("ACCESS_TOKEN", accessToken);
			accesTokenCache.put("REFRESH_TOKEN", token.getRefreshToken());
			stringRedisTemplateLogin.opsForHash().putAll(correctUser.getUserId(), accesTokenCache);
			stringRedisTemplateLogin.expire(correctUser.getUserId(), tokenExpirationTime, TimeUnit.MINUTES);
			LoginDto loginDto = new LoginDto();
			loginDto.setAccessToken(accessToken);
			loginDto.setCustomerType(correctUser.getType() != null ? correctUser.getType() : "NOT_BUC");
			loginResponseDto.setLogin(loginDto);
		} else {
			throw new LoginCustomException(ErrorType.ERROR_NOT_ANSWER_QUESTIONS);
		}
		return loginResponseDto;
	}

	private void validateRegisteredUser(LoginRequestDto loginRequestDto, ErrorType errorType) {
		if (StringUtils.isEmpty(loginRequestDto.getUsername())) {
			throw new NullPointerException();
		}
		Optional<User> userById = userRepository.findById(loginRequestDto.getUsername());
		if (!userById.isPresent()) {
			throw new LoginCustomException(errorType);
		}
	}

	private User validateCorrectUser(LoginRequestDto loginRequestDto, ErrorType errorType) {
		User correctUser = userRepository.findByUserIdAndPassword(loginRequestDto.getUsername(),
				loginRequestDto.getPassword());
		if (null == correctUser) {
			User user = userRepository.findByUserId(loginRequestDto.getUsername());
			if(user != null) {
			user.setAttempts(user.getAttempts() + 1);
			userRepository.save(user);
			}
			throw new LoginCustomException(errorType);
		}
		return correctUser;
	}
	
	private User validateCorrectUserFingerPrint(LoginRequestDto loginRequestDto) {
		String fingerHash = Util.getFingerPrintHash(loginRequestDto.getFcmtoken(), loginRequestDto.getUsername(),
				loginRequestDto.getFingerPrint());
		User user = userRepository.findByUserId(loginRequestDto.getUsername());
		if(user == null || StringUtils.isEmpty(user.getFingerPrintId())
				|| !MessageDigest.isEqual(user.getFingerPrintId().getBytes(StandardCharsets.UTF_8), fingerHash.getBytes(StandardCharsets.UTF_8))) {
			throw new LoginCustomException(ErrorType.ERROR_INCORRECT_LOGIN_FINGERPRINT);	
		}
		return user;
	}

	private boolean isAndroidIdInWhitelist(LoginRequestDto loginRequestDto) {
		logger.info("VERIFICANDO SI EL ANDROID ID ESTA EN LA WHITE LIST");
		AndroidIdWhite whiteAndroidId = androidIdWhiteRepository.findByAndroidId(loginRequestDto.getAndroidId());
		if(null == whiteAndroidId || StringUtils.isEmpty(whiteAndroidId.getAndroidId())) {
			return false;
		} else {
			whiteAndroidId.setLastLoginTimeDate(Util.getCurrentTimeDate());
			androidIdWhiteRepository.save(whiteAndroidId);
		}
		return true;
	}
	
	private void validateBlockedAndroidIdRedis(String androidId) {
		logger.info("VERIFICANDO QUE EL ANDROID ID NO ESTE BLOQUEADO EN REDIS");
		String json = stringRedisTemplateAndroidId.opsForValue().get(androidId);
		if(null != json ) {
			AndroidId androidIdRedis = gson.fromJson(json, AndroidId.class);
			if(androidIdRedis.isBlocked()) {
				throw new LoginCustomException("01.06.08", titleErrorBlocked, messageErrorBlocked, "El android id del dispositivo esta bloqueado en redis.",HttpStatus.FORBIDDEN);
			}
		}
	}
	
	private void addAndroidIdToRedis(LoginRequestDto loginRequestDto) {
		logger.info("NO EXISTE EL ANDROID ID Y SE AGREGARA A REDIS");
		LoginUser loginUser = new LoginUser();
		AndroidId androidId = new AndroidId();

		loginUser.setUserId(loginRequestDto.getUsername());
		loginUser.setLastLoginTimeDate(Util.getCurrentTimeDate());
		androidId.getUsers().add(loginUser);
		androidId.setBlocked(false);
		String json = gson.toJson(androidId);
		stringRedisTemplateAndroidId.opsForValue().set(loginRequestDto.getAndroidId(), json, timePeriod, TimeUnit.HOURS);
	}
	
	private void updateLastLoginTimeDateUserInAndroidIdRedis(LoginRequestDto loginRequestDto, AndroidId androidId, int userIndex) {	
		logger.info("EL USUARIO ESTA REGISTRADO EN EL ANDROID ID, NO ESTA BLOQUEADO Y SE ACTUALIZA LA FECHA DE INICIO DE SESION");
		androidId.getUsers().get(userIndex).setLastLoginTimeDate(Util.getCurrentTimeDate());
		String json = gson.toJson(androidId);
		stringRedisTemplateAndroidId.opsForValue().set(
				loginRequestDto.getAndroidId(), 
				json, 
				stringRedisTemplateAndroidId.opsForValue().getOperations().getExpire(loginRequestDto.getAndroidId(), TimeUnit.MILLISECONDS), 
				TimeUnit.MILLISECONDS);
	}
	
	private void addUserToAndroidIdRedis(LoginRequestDto loginRequestDto, AndroidId androidId) {
		logger.info("EL ANDROID ID TIENE MENOS DE " + maxUsers + " USUSARIOS, SE AGREGA AL USUARIO A LA LISTA ");
		LoginUser loginUser = new LoginUser();
		loginUser.setUserId(loginRequestDto.getUsername());
		loginUser.setLastLoginTimeDate(Util.getCurrentTimeDate());
		androidId.getUsers().add(loginUser);
		String json = gson.toJson(androidId);
		stringRedisTemplateAndroidId.opsForValue().set(
				loginRequestDto.getAndroidId(), 
				json, 
				stringRedisTemplateAndroidId.opsForValue().getOperations().getExpire(loginRequestDto.getAndroidId(), TimeUnit.MILLISECONDS), 
				TimeUnit.MILLISECONDS);		
	}
	
	private void validateAndroidId(LoginRequestDto loginRequestDto) {
		String json = stringRedisTemplateAndroidId.opsForValue().get(loginRequestDto.getAndroidId());
		if(null == json) {
			addAndroidIdToRedis(loginRequestDto);
		} else {
			AndroidId androidId = gson.fromJson(json, AndroidId.class);
			int userIndex = findUserInAndroidId(androidId.getUsers(), loginRequestDto.getUsername());
		
			if(NEGATIVE_ONE != userIndex) {
				updateLastLoginTimeDateUserInAndroidIdRedis(loginRequestDto, androidId, userIndex);
			} else {
				if(androidId.getUsers().size() < maxUsers) {
					addUserToAndroidIdRedis(loginRequestDto, androidId);
				} else {
					blockAndroidIdRedis(loginRequestDto, androidId);
					addAndroidIdToAudit(loginRequestDto, androidId);
				}
			}
		}
	}
	
	private void blockAndroidIdRedis(LoginRequestDto loginRequestDto, AndroidId androidId) {
		logger.info("BLOQUEO EL ANDROID ID EN REDIS");
		LoginUser loginUser = new LoginUser();
		loginUser.setUserId(loginRequestDto.getUsername());
		loginUser.setLastLoginTimeDate(Util.getCurrentTimeDate());
		androidId.getUsers().add(loginUser);
				
		androidId.setBlocked(true);;
		String json = gson.toJson(androidId);
		stringRedisTemplateAndroidId.opsForValue().set(loginRequestDto.getAndroidId(), json, timeBlocked, TimeUnit.HOURS);
	}
	
	private void addAndroidIdToAudit(LoginRequestDto loginRequestDto, AndroidId androidId) {
		logger.info("AGREGO EL ANDROID A AUDITORIA");
		AndroidIdAudit androidIdAudit = new AndroidIdAudit();
		androidIdAudit.setUsers(androidId.getUsers());
		androidIdAudit.setAndroidId(loginRequestDto.getAndroidId());
		androidIdAudit.setBlockedTimeDate(Util.getCurrentTimeDate());
		androidIdAuditRepository.save(androidIdAudit);			
		throw new LoginCustomException("01.06.08", titleErrorBlocked, messageErrorBlocked, "El android id del dispositivo esta bloqueado en redis.",HttpStatus.FORBIDDEN);
	}
	
	private int findUserInAndroidId(List<LoginUser> users,String userId) {
		for (int i = 0; i < users.size(); i++) {
			if(userId.equals(users.get(i).getUserId()))
				return i;
		}
		return NEGATIVE_ONE;
	}
	
	private void validateUniqueFcmToken(String fcmToken) {
		List<User> users = userRepository.findByFcmToken(fcmToken);
		for (User user : users) {
			user.setFcmToken(null);
			userRepository.save(user);
		}
	}

	private void validateUserBlocked(LoginRequestDto loginRequestDto, ErrorType errorBlockedAccount) {
		Optional<User> userById = userRepository.findById(loginRequestDto.getUsername());
		if (userById.isPresent() && userById.get().getAttempts() >= attempts) {
			throw new LoginCustomException(errorBlockedAccount);
		}
	}

	@Override
	public LoginResponseDto logout(String token) {
		LoginResponseDto loginResponseDto = new LoginResponseDto();
		stringRedisTemplateLogin.delete(UtilToken.getUserIdFromJWT(token));
		return loginResponseDto;
	}

}
