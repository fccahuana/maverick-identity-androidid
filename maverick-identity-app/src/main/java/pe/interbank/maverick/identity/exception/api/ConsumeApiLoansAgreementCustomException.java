package pe.interbank.maverick.identity.exception.api;

import org.springframework.http.HttpStatus;
import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;

@Getter
@Setter
public class ConsumeApiLoansAgreementCustomException extends BaseException {

	private final ErrorDto error;

	private static final long serialVersionUID = 1L;

	public ConsumeApiLoansAgreementCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(),
				errorType.getHttpStatus());
		error = super.getError();
	}

	public ConsumeApiLoansAgreementCustomException(String code, String title, String userMessage, String systemMessage,
			HttpStatus httpStatus) {
		super(code, title, userMessage, systemMessage, httpStatus);
		error = super.getError();
	}

	public ConsumeApiLoansAgreementCustomException(EsbBackendCommunicationException e) {
		super("01.06.26", "Error", "Ups, ocurrio un error inesperado.", "Ocurrió un error en el servicio de leads",
				HttpStatus.INTERNAL_SERVER_ERROR);
		ErrorType genericError = ErrorType.ERROR_GENERIC;
		ConsumeApiLoansAgreementCustomException consumeApiLoansAgreementCustomException = new ConsumeApiLoansAgreementCustomException(
				genericError.getCode(), genericError.getTitle(), genericError.getUserMessage(),
				e.getEsbResponseMessage() != null ? e.getEsbResponseMessage() : e.getError().getSystemMessage(),
				genericError.getHttpStatus());

		if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN
				&& (Constants.PROVIDER_CODE_NOT_CREDITS.equals(e.getEsbResponseCode())
						|| Constants.PROVIDER_CODE_NOT_DETAILS_CREDIT.equals(e.getEsbResponseCode()))) {
			ErrorType errorOnlyClient = ErrorType.ERROR_ONLY_CLIENTS;
			consumeApiLoansAgreementCustomException = new ConsumeApiLoansAgreementCustomException(
					errorOnlyClient.getCode(), errorOnlyClient.getTitle(), errorOnlyClient.getUserMessage(),
					e.getEsbResponseMessage(), errorOnlyClient.getHttpStatus());
		}
		error = consumeApiLoansAgreementCustomException.getError();
	}

}
