package pe.interbank.maverick.identity.recoverypassword.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class ForgotPasswordDto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String phone;
    private String hashToken;
}
