package pe.interbank.maverick.identity.login.controller.web;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import pe.interbank.maverick.commons.core.controller.BaseController;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.login.dto.LoginResponseDto;
import pe.interbank.maverick.identity.login.service.ILoginService;

/**
 * Método para login
 * 
 * @author acacerec
 *
 */
@RestController
public class LoginController extends BaseController{

	@Autowired
	private ILoginService loginService;
	
	@Resource(name="tokenStore")
	TokenStore tokenStore;
	
	@Resource(name = "tokenServices")
    ConsumerTokenServices tokenServices;
	
	@PostMapping(value = "/public/login")
	public ResponseEntity<LoginResponseDto> login(@RequestHeader("Firebase-token") String fcmToken,
			@RequestHeader("Fingerprint-Subscription-Key") String fingerPrint, @RequestBody LoginRequestDto userRequestDto, BindingResult bindingResult) {
		userRequestDto.setFcmtoken(fcmToken);
		userRequestDto.setFingerPrint(fingerPrint);
		return new ResponseEntity<>(loginService.loginAndAccesToken(userRequestDto, fcmToken), HttpStatus.OK);	
	}
	
	
	@PostMapping(value = "/private/logout")
	public ResponseEntity<LoginResponseDto> logout(@RequestHeader("Authorization") String token)  {
		loginService.logout(token);
		return new ResponseEntity<>(new LoginResponseDto(),HttpStatus.OK);	
	}
	
	
	
	
}