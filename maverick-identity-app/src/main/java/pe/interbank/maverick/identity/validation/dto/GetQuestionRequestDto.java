package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetQuestionRequestDto implements Serializable {
	private static final long serialVersionUID = -7647894307373391565L;
	@NotNull
	private String dni;
	private String messageId;
	private String sessionId;
}
