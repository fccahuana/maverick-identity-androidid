package pe.interbank.maverick.identity.exception;

import javax.servlet.http.HttpServletRequest;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.GenericErrorDto;
import pe.interbank.maverick.identity.core.util.ErrorType;

public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(BaseException.class)
	@ResponseBody
	ResponseEntity<GenericErrorDto> handleControllerCustomException(HttpServletRequest request, Throwable throwable) {
		BaseException baseException = (BaseException)throwable;
		GenericErrorDto genericErrorDto = new GenericErrorDto();
		genericErrorDto.setError(baseException.getError());
		return new ResponseEntity<>(genericErrorDto, baseException.getError().getHttpStatus());
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	ResponseEntity<GenericErrorDto> handleControllerIllegalArgumentException(HttpServletRequest request) {
		CustomException customException = new CustomException(ErrorType.INVALID_REQUEST);
		GenericErrorDto genericErrorDto = new GenericErrorDto();
		genericErrorDto.setError(customException.getError());
		return new ResponseEntity<>(genericErrorDto, customException.getError().getHttpStatus());
	}
	
	@ExceptionHandler(NullPointerException.class)
	@ResponseBody
	ResponseEntity<GenericErrorDto> handleControllerNullPointerException(HttpServletRequest request, Throwable throwable) {
		CustomException customException = new CustomException(ErrorType.NULLPOINTER_REQUEST);
		GenericErrorDto genericErrorDto = new GenericErrorDto();
		genericErrorDto.setError(customException.getError());
		return new ResponseEntity<>(genericErrorDto, customException.getError().getHttpStatus());
	}
	
	@ExceptionHandler(DuplicateKeyException.class)
	@ResponseBody
	ResponseEntity<GenericErrorDto> handleControllerDuplicateKeyException(HttpServletRequest request, Throwable throwable) {
		CustomException customException = new CustomException(ErrorType.ERROR_DUPLICATE_REGISTER_MONGO);
		GenericErrorDto genericErrorDto = new GenericErrorDto();
		genericErrorDto.setError(customException.getError());
		return new ResponseEntity<>(genericErrorDto, customException.getError().getHttpStatus());
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	ResponseEntity<GenericErrorDto> handleUnexpectedException(HttpServletRequest request, Throwable throwable) {
		CustomException customException = new CustomException(ErrorType.ERROR_GENERIC);
		GenericErrorDto genericErrorDto = new GenericErrorDto();
		genericErrorDto.setError(customException.getError());
		genericErrorDto.getError().setSystemMessage(throwable.getMessage());
		return new ResponseEntity<>(genericErrorDto, customException.getError().getHttpStatus());
	}
		
}
