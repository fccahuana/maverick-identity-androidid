package pe.interbank.maverick.identity.campaign.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tcea {

	private String company;
	private String baseAgreement;
	private String tceaMaximum;
	private String agreement;
}
