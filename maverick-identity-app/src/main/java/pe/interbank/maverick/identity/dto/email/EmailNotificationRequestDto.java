package pe.interbank.maverick.identity.dto.email;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailNotificationRequestDto {

	private String from;
	private String destination;
	private String destinationCopy;
	private String subject;
	private String attachment;
	private TemplateDto template;
	private String messageId;
	private String userId;
	private String sessionId;

}
