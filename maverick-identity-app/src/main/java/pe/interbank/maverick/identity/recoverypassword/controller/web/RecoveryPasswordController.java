package pe.interbank.maverick.identity.recoverypassword.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordResponseDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordResponseDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSResponseDto;
import pe.interbank.maverick.identity.recoverypassword.service.impl.RecoveryPasswordServiceImpl;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;

@RequestMapping("/public")
@RefreshScope
@RestController
public class RecoveryPasswordController {

	@Autowired
	RecoveryPasswordServiceImpl recoveryPasswordService;
	


	@PostMapping(value = "/forgot/validation/dni")
	public ResponseEntity<RecoveryPasswordResponseDto> validateScreenDni(@RequestHeader("X-MESSAGE-ID") String messageId, 
			@RequestHeader("Session-Id") String sessionId, @RequestBody RecoveryPasswordRequestDto recoveryPasswordRequestDto) {
		recoveryPasswordRequestDto.setMessageId(messageId);
		recoveryPasswordRequestDto.setSessionId(sessionId);
		RecoveryPasswordResponseDto recoveryPasswordResponseDto = recoveryPasswordService.validateScreenDni(recoveryPasswordRequestDto);
		return  new ResponseEntity<>(recoveryPasswordResponseDto, HttpStatus.OK);	
	}
	
	@PostMapping(value = "/forgot/confirm/carrier")
	public ResponseEntity<RecoveryPasswordResponseDto> confirmScreenCarrier(@RequestHeader("X-MESSAGE-ID") String messageId, @RequestHeader("Session-Id") String sessionId, @RequestBody ConfirmCarrierRequestDto confirmCarrierRequestDto) {
		confirmCarrierRequestDto.setMessageId(messageId);
		confirmCarrierRequestDto.setSessionId(sessionId);
		RecoveryPasswordResponseDto recoveryPasswordResponseDto = recoveryPasswordService.confirmScreenCarrier(confirmCarrierRequestDto);
		return  new ResponseEntity<>(recoveryPasswordResponseDto, HttpStatus.OK);	
	}
	
	@PostMapping(value = "/forgot/validation/code")
	public ResponseEntity<ValidateTokenSMSResponseDto> validationTokenSms(@RequestBody ValidateTokenSMSRequestDto validateTokenSMSRequestDto) {		
		recoveryPasswordService.validateScreenValidationCode(validateTokenSMSRequestDto);
		return new ResponseEntity<>(new ValidateTokenSMSResponseDto(),HttpStatus.OK);	
	}
	
	@PostMapping(value = "/forgot/validation/password")
	public ResponseEntity<ValidatePasswordResponseDto> validationNewPassword(@RequestBody ValidatePasswordRequestDto validatePasswordRequestDto) {		
		recoveryPasswordService.validateScreenNewPassword(validatePasswordRequestDto);
		return new ResponseEntity<>(new ValidatePasswordResponseDto(),HttpStatus.OK);	
	}

}
