package pe.interbank.maverick.identity.exception.api;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;



@Getter
@Setter
public class ConsumeApiDetailLeadCustomException extends BaseException{

	private final ErrorDto error;
	
	private static final long serialVersionUID = 1L;
	
	public ConsumeApiDetailLeadCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
		error = super.getError();
	}
	
	public ConsumeApiDetailLeadCustomException(String code,String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		super(code, title, userMessage, systemMessage, httpStatus);
		error = super.getError();
	}
	
	public ConsumeApiDetailLeadCustomException(ErrorType errorType, String systemMessage) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), systemMessage, errorType.getHttpStatus());
		error = super.getError();
	}
	
	public ConsumeApiDetailLeadCustomException(EsbBackendCommunicationException e) {
		super("01.06.26", "Error", "Ups, ocurrio un error inesperado.", "Ocurrió un error en el servicio de leads",
				HttpStatus.INTERNAL_SERVER_ERROR);
		ConsumeApiDetailLeadCustomException consumeApiLeadsCustomException = new ConsumeApiDetailLeadCustomException(
				ErrorType.ERROR_SERVICE_CAMPAIGN, ErrorType.ERROR_SERVICE_CAMPAIGN.getSystemMessage());
		if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN) {
			String systemMessageService = e.getEsbResponseMessage() != null ? e.getEsbResponseMessage()
					: e.getError().getSystemMessage();
			consumeApiLeadsCustomException = new ConsumeApiDetailLeadCustomException(ErrorType.NO_EXTENSION_CAMPAIGN,
					systemMessageService);
		}
		error = consumeApiLeadsCustomException.getError();

	}

}
