package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.getquestions.QuestionsResponseDto;
import pe.interbank.maverick.identity.esb.api.repository.GetQuestionsRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiGetQuestionsCustomException;


@Component
public class ConsumeApiGetQuestions {

	@Autowired
	GetQuestionsRepository getQuestionsRepository;

	public QuestionsResponseDto consumeServiceGetQuestions(String dni, ConsumeApiRequestDto consumeApiRequestDto) {
		String jsonResponseQuestions = callServiceGetQuestions(dni, consumeApiRequestDto);
		return parserQuestionsResponse(jsonResponseQuestions);
	}

	private String callServiceGetQuestions(String dni, ConsumeApiRequestDto consumeApiRequestDto) {

		String jsonCustomerInformation = null;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("documentId", dni);
			jsonCustomerInformation = getQuestionsRepository.getQuestions(consumeApiRequestDto, new CurrentSession(), parameter);
		} catch (EsbBackendCommunicationException e) {
			ErrorType genericError = ErrorType.ERROR_GENERIC;
			if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN) {
				ErrorType errorGetQuestions = ErrorType.ERROR_GET_QUESTIONS;
				throw new ConsumeApiGetQuestionsCustomException(errorGetQuestions.getCode(), errorGetQuestions.getTitle(),
						errorGetQuestions.getUserMessage(), e.getEsbResponseMessage(), errorGetQuestions.getHttpStatus());
			} else {
				throw new ConsumeApiGetQuestionsCustomException(genericError.getCode(), genericError.getTitle(),
						genericError.getUserMessage(),
						e.getEsbResponseMessage() != null ? e.getEsbResponseMessage() : e.getError().getSystemMessage(),
						genericError.getHttpStatus());
			}

		}

		return jsonCustomerInformation;
	}

	private QuestionsResponseDto parserQuestionsResponse(String jsonResponseQuestions) {

		if (!StringUtils.isNotBlank(jsonResponseQuestions.trim())) {
			throw new NullPointerException();
		}
		QuestionsResponseDto questionsResponseDto;
		try {
			questionsResponseDto = new Gson().fromJson(jsonResponseQuestions, QuestionsResponseDto.class);
		} catch (Exception e) {
			throw new ConsumeApiGetQuestionsCustomException(ErrorType.ERROR_GENERIC);
		}
		return questionsResponseDto;
	}

}
