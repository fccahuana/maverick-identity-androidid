package pe.interbank.maverick.identity.profile.service;

import pe.interbank.maverick.identity.profile.dto.UserProfileRequestDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileResponseDto;

public interface UserProfileService {
	UserProfileResponseDto getUserProfile(UserProfileRequestDto userProfileRequestDto);
	UserProfileResponseDto updateUserProfile(UserProfileRequestDto userProfileRequestDto);
	
}
