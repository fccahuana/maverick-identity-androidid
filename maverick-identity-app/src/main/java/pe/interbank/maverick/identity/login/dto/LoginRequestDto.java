package pe.interbank.maverick.identity.login.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequestDto {
	private String username;
	private String password;
	private String fingerPrint;
	private String fcmtoken;
	private String androidId;
}
