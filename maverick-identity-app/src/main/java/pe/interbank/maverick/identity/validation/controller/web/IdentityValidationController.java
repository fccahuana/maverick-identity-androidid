package pe.interbank.maverick.identity.validation.controller.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationCodeRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationPhoneRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationResponseDto;
import pe.interbank.maverick.identity.validation.service.IdentityValidationService;

@RequestMapping("/public/register/validation")
@RefreshScope
@RestController

public class IdentityValidationController {

	@Autowired
	IdentityValidationService validationService;

	@PostMapping(value = "/getphone")
	public ResponseEntity<ValidationResponseDto> getPhone(@RequestHeader("X-MESSAGE-ID") String messageId,
			@RequestHeader("Session-Id") String sessionId,
			@Valid @RequestBody ValidationPhoneRequestDto validationPhoneRequestDto) {
		validationPhoneRequestDto.setMessageId(messageId);
		validationPhoneRequestDto.setSessionId(sessionId);
		ValidationResponseDto response = validationService.getPhone(validationPhoneRequestDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/carrier")
	public ResponseEntity<ValidationResponseDto> confirmCarrier(@RequestHeader("X-MESSAGE-ID") String messageId,
			@RequestHeader("Session-Id") String sessionId,
			@Valid @RequestBody ConfirmCarrierRequestDto confirmCarrierRequestDto) {
		confirmCarrierRequestDto.setMessageId(messageId);
		confirmCarrierRequestDto.setSessionId(sessionId);
		ValidationResponseDto response = validationService.confirmCarrier(confirmCarrierRequestDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/code")
	public ResponseEntity<ValidationResponseDto> validateCode(
			@Valid @RequestBody ValidationCodeRequestDto validationCodeRequestDto,
			@RequestHeader("Session-Id") String sessionId,
			@RequestHeader("X-MESSAGE-ID") String messageId) {
		ValidationResponseDto response = validationService.validateCode(validationCodeRequestDto, sessionId, messageId);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping(value = "/questions")
	public ResponseEntity<GetQuestionResponseDto> getQuestions(@RequestHeader("X-MESSAGE-ID") String messageId,
			@RequestHeader("Session-Id") String sessionId,
			@Valid @RequestBody GetQuestionRequestDto getQuestionRequestDto) {
		getQuestionRequestDto.setMessageId(messageId);
		getQuestionRequestDto.setSessionId(sessionId);
		GetQuestionResponseDto response = validationService.getquestions(getQuestionRequestDto);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping(value = "/answers")
	public ResponseEntity<ValidationQuestionResponseDto> validAnswers(@RequestHeader("X-MESSAGE-ID") String messageId,
			@RequestHeader("Session-Id") String sessionId,
			@Valid @RequestBody ValidationQuestionRequestDto validationQuestionRequestDto) {
		validationQuestionRequestDto.setMessageId(messageId);
		validationQuestionRequestDto.setSessionId(sessionId);
		ValidationQuestionResponseDto validationQuestionResponseDto = validationService.validationQuestions(validationQuestionRequestDto);
		return new ResponseEntity<>(validationQuestionResponseDto, HttpStatus.OK);
	}

}
