package pe.interbank.maverick.identity.api;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.api.assi.exception.ApiAssiException;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdReponse;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;
import pe.interbank.maverick.identity.api.assi.repository.impl.VisionNoCustomerRepositoryImpl;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.exception.api.ConsumeApiVisionCustomerException;

@Slf4j
@Component
public class ConsumeApiVisionNoCustomer {

	@Autowired
	private VisionNoCustomerRepositoryImpl visionNoCustomerRepositoryImpl;
	
	public RegisterLdpdReponse registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest) {
		String response = callRegisterLdpd(consumeApiRequestDto, registerLdpdRequest);
		return parseResponseApiVisionCliente(response);
	}
	
	private String callRegisterLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest) {
		String jsonRegisterLdpd = null;
		try {
			jsonRegisterLdpd = visionNoCustomerRepositoryImpl.registerLdpd(consumeApiRequestDto, registerLdpdRequest);
		} catch (ApiAssiException e) {
			log.error("Error in callRegisterLdpd: {}", e.getError().getSystemMessage());
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC,e.getError().getSystemMessage());
		}
		return jsonRegisterLdpd;
	}
	
	private RegisterLdpdReponse parseResponseApiVisionCliente(String jsonRegisterLdpd) {
		if (!StringUtils.isNotBlank(jsonRegisterLdpd.trim())) {
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC, "Response Blank");
		}
		RegisterLdpdReponse registerLdpdResponse = null;
		try {
			registerLdpdResponse = new Gson().fromJson(jsonRegisterLdpd, RegisterLdpdReponse.class);
		} catch (Exception e) {
			log.error("Error in parseResponseApiAccountCreation: {}", e.getMessage());
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC,e.getMessage());
		}
		return registerLdpdResponse;
	}
}
