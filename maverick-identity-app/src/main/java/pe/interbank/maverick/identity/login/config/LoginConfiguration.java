package pe.interbank.maverick.identity.login.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;


@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.login.config", redisTemplateRef = "stringRedisTemplateLogin")
public class LoginConfiguration {
	
	@Value("${identity.login.redis.db}")
	int loginDb;
	
	@Bean("stringRedisTemplateLogin")
	StringRedisTemplate stringRedisTemplateLogin(RedisConfiguration regisConfiguration) {
		return new StringRedisTemplate(regisConfiguration.jedisConnectionFactory(loginDb));
	}
}
