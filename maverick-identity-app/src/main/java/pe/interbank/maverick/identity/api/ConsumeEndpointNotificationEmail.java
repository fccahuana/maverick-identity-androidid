package pe.interbank.maverick.identity.api;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.commons.crypto.service.CryptoServiceImpl;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.dto.email.EmailNotificationRequestDto;
import pe.interbank.maverick.identity.dto.email.EmailNotificationResponseDto;
import pe.interbank.maverick.identity.dto.email.TemplateDto;
import pe.interbank.maverick.identity.exception.api.ConsumeEndpointNotificationEmailException;
import pe.interbank.maverick.identity.register.config.RegisterConfiguration;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.util.IdentityUtil;


@Component
public class ConsumeEndpointNotificationEmail {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumeEndpointNotificationEmail.class);

	@Autowired
	RegisterConfiguration registerConfiguration;

	@Autowired
	IdentityUtil identityUtil;

	@Autowired
	SensitiveData sensitiveData;
	
	@Autowired
	CryptoServiceImpl cryptoServiceImpl;
	
	public EmailNotificationResponseDto consumeEndpointNotificationEmail(RegisterRequestDto registerRequestDto) {
		EmailNotificationRequestDto emailNotificationRequestDto = setEmailNotificationRequestDtoFromRegisterRequestDto(registerRequestDto);
		return callEndpointNotificationEmail(emailNotificationRequestDto);
	}

	private EmailNotificationRequestDto setEmailNotificationRequestDtoFromRegisterRequestDto(RegisterRequestDto registerRequestDto) {
		EmailNotificationRequestDto emailNotificationRequestDto = new EmailNotificationRequestDto();
		emailNotificationRequestDto.setAttachment(registerRequestDto.getAttachment());
		TemplateDto templateDto = new TemplateDto();
		templateDto.setName(registerConfiguration.getMailTemplate());
		String dni =  registerRequestDto.getDni();
		Map<String, String> parameters = new HashMap<>();
		parameters.put("nombre", getNames(DigestUtils.sha256Hex(dni)));
		parameters.put("dni",cryptoServiceImpl.encryptRsa(dni, registerConfiguration.getPublickey()));
		parameters.put("celular", registerRequestDto.getPhoneNumber());
		templateDto.setParameters(parameters);
		emailNotificationRequestDto.setTemplate(templateDto);
		emailNotificationRequestDto.setDestination(registerRequestDto.getEmail());
		emailNotificationRequestDto.setFrom(registerConfiguration.getMailFrom());
		emailNotificationRequestDto.setDestinationCopy("");
		emailNotificationRequestDto.setSubject(registerRequestDto.getSubjectEmail());
		emailNotificationRequestDto.setSessionId(registerRequestDto.getSessionId());
		emailNotificationRequestDto.setMessageId(registerRequestDto.getMessageId());
		emailNotificationRequestDto.setUserId(DigestUtils.sha256Hex(registerRequestDto.getDni()));
		return emailNotificationRequestDto;
	}

	private EmailNotificationResponseDto callEndpointNotificationEmail(
			EmailNotificationRequestDto emailNotificationRequestDto) {
		LOGGER.info("Se va a ejecutar el metodo callEndpointNotificationEmail");
		EmailNotificationResponseDto emailNotificationResponseDto = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			headers.add("X-MESSAGE-ID", emailNotificationRequestDto.getMessageId());
			headers.add("Session-Id", emailNotificationRequestDto.getSessionId());
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonRequest = mapper.writeValueAsString(emailNotificationRequestDto);
			HttpEntity<?> httpEntity = new HttpEntity<>(jsonRequest, headers);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
			ResponseEntity<EmailNotificationResponseDto> responseEntity = restTemplate.exchange(registerConfiguration.getEndpointSendEmail(),
					HttpMethod.POST, httpEntity, EmailNotificationResponseDto.class);
			emailNotificationResponseDto = responseEntity.getBody();
		}  catch (Exception e) {
			LOGGER.error("Error al llamar al endpoint que envia notificaciones por email {}",
					e.getMessage());
			throw new ConsumeEndpointNotificationEmailException(ErrorType.ERROR_GENERIC);
		}
		return emailNotificationResponseDto;

	}
	
	private String getNames(String userId) {
		User user = new User();
		user.setUserId(userId);
		identityUtil.setNamesUserOfRedis(user);
		StringBuilder sb = new StringBuilder(decryptSensitiveDate(user.getFirstName()));
		sb.append(decryptSensitiveDate(user.getSecondName()));
		sb.append(decryptSensitiveDate(user.getLastName()));
		sb.append(decryptSensitiveDate(user.getSecondLastName()));
		return sb.toString().trim();
	}
	
	private String decryptSensitiveDate(String sensitiveDate) {
		return !StringUtils.isEmpty(sensitiveDate) ? sensitiveData.decrypt(sensitiveDate)+Constants.SPACE : "";
	}

}
