package pe.interbank.maverick.identity.refreshtoken.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import pe.interbank.maverick.commons.core.controller.BaseController;
import pe.interbank.maverick.identity.core.util.UtilToken;
import pe.interbank.maverick.identity.refreshtoken.dto.RefreshTokenResponseDto;
import pe.interbank.maverick.identity.refreshtoken.service.impl.RefreshTokenServiceImpl;

@RestController
public class RefreshTokenController extends BaseController{
	
	@Autowired
	RefreshTokenServiceImpl refreshTokenServiceImpl;
	
	@PostMapping(value = "/private/token/refresh")
	public ResponseEntity<RefreshTokenResponseDto> refresh(@RequestHeader("Authorization") String token) {
		return new ResponseEntity<>(refreshTokenServiceImpl.refreshToken(UtilToken.getUserIdFromJWT(token)), HttpStatus.OK);	
	}	
	
}