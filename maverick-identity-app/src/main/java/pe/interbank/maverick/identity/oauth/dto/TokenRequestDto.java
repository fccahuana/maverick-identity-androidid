package pe.interbank.maverick.identity.oauth.dto;

import java.io.Serializable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenRequestDto implements Serializable{

	private static final long serialVersionUID = 1L;

	private HttpHeaders httpHeaders;
	private MultiValueMap<String, String> body;
	private String uriAuth;
	
}
