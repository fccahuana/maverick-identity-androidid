package pe.interbank.maverick.identity.refreshtoken.service;

import pe.interbank.maverick.identity.refreshtoken.dto.RefreshTokenResponseDto;

public interface IRefreshTokenService {
	RefreshTokenResponseDto refreshToken(String userId);
}