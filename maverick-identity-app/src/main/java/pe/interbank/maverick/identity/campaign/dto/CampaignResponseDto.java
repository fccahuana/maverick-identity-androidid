package pe.interbank.maverick.identity.campaign.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.ErrorDto;

@Setter
@Getter
@JsonInclude(Include.NON_EMPTY)
public class CampaignResponseDto implements Serializable{

	private static final long serialVersionUID = 1L;	
	private String greaterAmount;
	private String agreement;
	private String tcea;
	private String cem;
	private String currencySymbol;
	private List<CampaignResponse> leads;
	private ErrorDto error;

}
