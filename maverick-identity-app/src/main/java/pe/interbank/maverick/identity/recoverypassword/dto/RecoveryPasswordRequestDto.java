package pe.interbank.maverick.identity.recoverypassword.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoveryPasswordRequestDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private String phoneNumber;
	private String dni;
	private String messageId;
	private String sessionId;
}
