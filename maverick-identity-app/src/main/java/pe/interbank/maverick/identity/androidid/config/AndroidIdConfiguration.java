package pe.interbank.maverick.identity.androidid.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;


@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.android.config", redisTemplateRef = "stringRedisTemplateLogin")
public class AndroidIdConfiguration {
	
	@Value("${identity.androidId.redis.db}")
	int androidIdDb;
	
	@Bean("stringRedisTemplateAndroidId")
	StringRedisTemplate stringRedisTemplateLogin(RedisConfiguration regisConfiguration) {
		return new StringRedisTemplate(regisConfiguration.jedisConnectionFactory(androidIdDb));
	}
}
