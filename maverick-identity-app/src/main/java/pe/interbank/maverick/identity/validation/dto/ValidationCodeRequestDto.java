package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationCodeRequestDto  implements Serializable {

	private static final long serialVersionUID = 1L;
	@NotNull
	private String dni;
	@NotNull
	private String hashToken;
	@NotNull
	private String verificationCode;
}
