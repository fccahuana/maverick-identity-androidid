package pe.interbank.maverick.identity.register.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.generic.UserBlockForBlackList;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiFraudList;
import pe.interbank.maverick.identity.api.ConsumeEndpointNotificationEmail;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.RegexType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CustomerResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.LoansAgreementResponseDto;
import pe.interbank.maverick.identity.exception.register.RegisterCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.register.config.RegisterConfiguration;
import pe.interbank.maverick.identity.register.dto.Register;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.dto.RegisterResponseDto;
import pe.interbank.maverick.identity.register.service.IRegisterService;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@Slf4j
@Service
public class RegisterServiceImpl implements IRegisterService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private SensitiveData sensitiveData;

	@Autowired
	PasswordHistoryServiceImpl passwordHistoryServiceImpl;

	@Autowired
	IdentityUtil identityUtil;

	@Autowired
	@Qualifier("thirteenRedisDb")
	StringRedisTemplate thirteenRedisDb;

	@Autowired
	ConsumeApiFraudList consumeApiFraudList;

	@Autowired
	GenericRepository genericRepository;
	
	@Autowired
	RegisterConfiguration registerConfiguration;
	
	@Autowired
	ConsumeEndpointNotificationEmail consumeEndpointNotificationEmail;

	public static final String USERBLOCKINBLACKLIST = "USERBLOCKINBLACKLIST";

	static final String CUSTOMER_TYPE = "CUSTOMER_TYPE";
	static final String COMPANY_ID = "COMPANY_ID";
	static final String INSTITUTION_NAME = "INSTITUTION_NAME";
	static final String FAMILY_NAME = "FAMILY_NAME";
	static final String PLACE_NAME = "PLACE_NAME";
	static final String FIRST_NAME = "FIRST_NAME";
	static final String SECOND_NAME = "SECOND_NAME";
	static final String LAST_NAME = "LAST_NAME";
	static final String SECOND_LAST_NAME = "SECOND_LAST_NAME";
	static final String CUSTOMER_ID = "CUSTOMER_ID";
	static final String PHONE_NUMBER = "PHONE_NUMBER";

	static final String BUC = "BUC";
	static final String NOT_BUC = "NOT_BUC";
	static final String FLOW_REGISTER = "_REGISTER";
	static final String COMPLETE_CERO = "%010d";
	static final String CODE_NUMBER_PHONE = "+51";
	static final String FALSE = "false";
	static final String IS_CUSTOMER = "IS_CUSTOMER";
	static final String TRUE = "true";

	@Override
	public RegisterResponseDto validationDni(RegisterRequestDto registerRequestDto) {
		RegisterResponseDto registerResponseDto = new RegisterResponseDto();
		Register register = new Register();
		register.setIsCustomer("true");
		String customerType = NOT_BUC;
		String dni = registerRequestDto.getDni().trim();
		String userId = DigestUtils.sha256Hex(dni);
		validationFormat(dni, RegexType.REGEX_WRONG_DNI_FORMAT.getRegex(), ErrorType.ERROR_WRONG_DNI_FORMAT);
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(registerRequestDto.getMessageId(), userId,
				registerRequestDto.getSessionId());
		validationIsUser(userId);
		LoansAgreementResponseDto loansAgreementResponseDto = identityUtil.getResponseApiLoansAgreement(dni,
				consumeApiRequestDto);
		GetInformationPersonResponseDto responseApiPerson = null;
		if (!identityUtil.customerHasCredit(loansAgreementResponseDto)) {
			identityUtil.validateCustomerBuc(dni, consumeApiRequestDto);
			responseApiPerson = identityUtil.getResponseApiPerson(dni,
					consumeApiRequestDto);
			customerType = BUC;
			if (null == responseApiPerson.getCustomerId()) {
				validateCustomerBlockForFraudList(userId);
				register.setIsCustomer(FALSE);
				identityUtil.validateBucNoClientsAllAttempts(dni);
			}
			saveCustomerDataApiPersonInRedis(userId, responseApiPerson);
		}
		register.setType(customerType);
		register.setCustomerName(getCustomerName(loansAgreementResponseDto,responseApiPerson));
		registerResponseDto.setRegister(register);
		generateHashStep1(userId, customerType, register.getIsCustomer());
		return registerResponseDto;

	}

	private void validationFormat(String dato, String regex, ErrorType errorType) {
		Pattern patterValidation = Pattern.compile(regex);
		Matcher matcherValidation = patterValidation.matcher(dato);
		if (!matcherValidation.matches())
			throw new RegisterCustomException(errorType);
	}

	private void validationIsUser(String userId) {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent())
			throw new RegisterCustomException(ErrorType.ERROR_EXISTING_USER);
	}
	
	private void validateCustomerBlockForFraudList(String userId) {
		StringBuilder sb = new StringBuilder(userId);
		sb.append(USERBLOCKINBLACKLIST);
		String identifier = DigestUtils.sha256Hex(sb.toString());
		Optional<Generic> genericFound = genericRepository.findById(identifier);
		if(genericFound.isPresent()) {
			throw new RegisterCustomException(ErrorType.ERROR_DNI_IN_FRAUD);
		}
	}
	

	private void saveCustomerDataApiPersonInRedis(String userId, GetInformationPersonResponseDto responseApiPerson) {
		try {
			Map<String, String> stepMap = new HashMap<>();

			setRedisSensitiveData(FIRST_NAME, responseApiPerson.getFirstName(), stepMap);
			setRedisSensitiveData(SECOND_NAME, responseApiPerson.getSecondName(), stepMap);
			setRedisSensitiveData(LAST_NAME, responseApiPerson.getLastName(), stepMap);
			setRedisSensitiveData(SECOND_LAST_NAME, responseApiPerson.getSecondLastName(), stepMap);

			thirteenRedisDb.opsForHash().putAll(userId.concat(FLOW_REGISTER), stepMap);
			thirteenRedisDb.expire(userId.concat(FLOW_REGISTER), registerConfiguration.getExpirationRedisRegister(), TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new RegisterCustomException(ErrorType.ERROR_SAVE_NAMES_IN_REDIS);
		}
	}

	private void setRedisSensitiveData(String key, String value, Map<String, String> stepMap) {
		stepMap.put(key, null != value && !value.isEmpty() ? sensitiveData.encryptHsm(value) : Constants.EMPTY);
	}
	
	private String getCustomerName(LoansAgreementResponseDto loansAgreementResponseDto, GetInformationPersonResponseDto responseApiPerson) {
		String firstName = null;
		String secondName = null;
		String lastName = null;
		String secondLastName = null;
		if(null!=loansAgreementResponseDto && !loansAgreementResponseDto.getCredits().isEmpty()) {
			CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();
			firstName = customerOfLoans.getFirstName();
			secondName = customerOfLoans.getSecondName();
			lastName = customerOfLoans.getLastName();
			secondLastName = customerOfLoans.getSecondLastName();
			
		}else if(null!=responseApiPerson) {
			firstName = responseApiPerson.getFirstName();
			secondName = responseApiPerson.getSecondName();
			lastName = responseApiPerson.getLastName();
			secondLastName = responseApiPerson.getSecondLastName();
		}
		
		StringBuilder sb = new StringBuilder(!StringUtils.isEmpty(firstName)? firstName + Constants.SPACE : Constants.EMPTY);
		sb.append(!StringUtils.isEmpty(secondName)? secondName + Constants.SPACE : Constants.EMPTY);
		sb.append(!StringUtils.isEmpty(lastName)? lastName + Constants.SPACE : Constants.EMPTY);
		sb.append(!StringUtils.isEmpty(secondLastName)? secondLastName + Constants.SPACE : Constants.EMPTY);
		return sb.toString().trim();
	}

	private void generateHashStep1(String userId, String customerType, String isCustomer) {
		try {
			Map<String, String> stepMap = new HashMap<>();
			stepMap.put(Constants.STEP1, userId);
			stepMap.put(CUSTOMER_TYPE, customerType);
			stepMap.put(IS_CUSTOMER, isCustomer);
			thirteenRedisDb.opsForHash().putAll(userId.concat(FLOW_REGISTER), stepMap);
		} catch (Exception e) {
			throw new RegisterCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	@Override
	public void savePhone(RegisterRequestDto registerRequestDto) {

		String phoneNumber = registerRequestDto.getPhoneNumber();
		String userId = DigestUtils.sha256Hex(registerRequestDto.getDni());
		validationFormat(phoneNumber, RegexType.REGEX_WRONG_PHONE_FORMAT.getRegex(),
				ErrorType.ERROR_WRONG_PHONE_FORMAT);
		String phoneNumberId = DigestUtils.sha256Hex(CODE_NUMBER_PHONE + phoneNumber);

		validateStep1(userId);

		validationUserByPhoneNumber(phoneNumberId, ErrorType.ERROR_EXISTING_PHONENUMBER);

		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(registerRequestDto.getMessageId(), userId,
				registerRequestDto.getSessionId());

		if (consumeApiFraudList.consumeApiFraudList(consumeApiRequestDto, registerRequestDto.getPhoneNumber())) {
			blockUserForCellphoneInBlackList(registerRequestDto);
			sendNotificationEmail(registerRequestDto);
			throw new RegisterCustomException(ErrorType.ERROR_PHONENUMBER_IS_IN_FRAUD_LIST);
		} else {
			generateHashStep2(userId, phoneNumberId);
		}
	}

	private void validateStep1(String userId) {

		Map<Object, Object> map = thirteenRedisDb.opsForHash().entries(userId.concat(FLOW_REGISTER));
		String step1 = (String) map.get(Constants.STEP1);
		if (null == step1 || !step1.equals(userId)) {
			throw new RegisterCustomException(ErrorType.INVALID_STEP_ONE_HASH);
		}
	}

	private void validationUserByPhoneNumber(String dato, ErrorType errorType) {
		List<User> users = userRepository.findByPhoneNumberId(dato);
		if (users != null && !users.isEmpty())
			throw new RegisterCustomException(errorType);
	}

	private void blockUserForCellphoneInBlackList(RegisterRequestDto registerRequestDto) {
		String userId = DigestUtils.sha256Hex(registerRequestDto.getDni());
		UserBlockForBlackList userBlockForBlackList = new UserBlockForBlackList();
		userBlockForBlackList.setCurrentDate(Util.getCurrentTimeDate());
		userBlockForBlackList.setUserId(userId);

		Generic generic = new Generic();
		StringBuilder sb = new StringBuilder(userId);
		sb.append(USERBLOCKINBLACKLIST);
		String identifier = DigestUtils.sha256Hex(sb.toString());
		generic.setIdentifier(identifier);
		generic.setUserBlock(userBlockForBlackList);
		genericRepository.save(generic);
	}
	
	@Async
	private void sendNotificationEmail(RegisterRequestDto registerRequestDto) {
		String[] dest = registerConfiguration.getDestinationUsersBlocks().split(",");
		for (String destination : dest) {
			try {
				log.info("Se va a enviar mail a: {}", destination);	
				registerRequestDto.setEmail(destination);
				registerRequestDto.setAttachment(Constants.EMPTY);			
				registerRequestDto.setSubjectEmail(setSubjectEmail(registerRequestDto));
				consumeEndpointNotificationEmail.consumeEndpointNotificationEmail(registerRequestDto);
				log.info("Se realizó el envio de mail a: {}", destination);
			} catch (Exception e) {
				log.info("Error en el envio de mail a: {}", destination);
			}
		}

	}
	
	private String setSubjectEmail(RegisterRequestDto registerRequestDto) {
		StringBuilder sb = new StringBuilder("[Maverick] ");
		sb.append(registerConfiguration.getSubjectBlockUser());
		sb.append(" ");
		sb.append(registerRequestDto.getDni());
		return sb.toString();
	}

	private void generateHashStep2(String userId, String phoneNumberId) {
		try {
			Map<String, String> stepMap = new HashMap<>();
			stepMap.put(PHONE_NUMBER, phoneNumberId);
			thirteenRedisDb.opsForHash().putAll(userId.concat(FLOW_REGISTER), stepMap);
		} catch (Exception e) {
			throw new RegisterCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	@Override
	public void validationPassword(RegisterRequestDto registerRequestDto) {
		String passwordDecrypted = registerRequestDto.getSecret().trim();
		validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_LENGTH.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_LENGTH);
		validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_HAS_NUMBER.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_NUMBER);
		validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_HAS_LETTER.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_LETTER);
		validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_HAS_SPECIAL_CHARACTERS.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_SPECIAL_CHARACTERS);
	}

	@Override
	public RegisterResponseDto createUser(RegisterRequestDto registerRequestDto) {
		RegisterResponseDto registerResponseDto = new RegisterResponseDto();
		Register register = new Register();

		String dni = registerRequestDto.getDni();
		String userId = DigestUtils.sha256Hex(dni);
		register.setIsCustomer(FALSE);
		validationPassword(registerRequestDto);
		validateStep1(userId);

		if (registerRequestDto.getTermsConditions()) {
			if (null == registerRequestDto.getIsCustomer()) {
				registerRequestDto.setIsCustomer(getIsCustomerOfRedis(userId));
				if (TRUE.equals(registerRequestDto.getIsCustomer())) {
					registerCustomer(registerRequestDto);
					register.setIsCustomer(TRUE);
				}

			} else if (TRUE.equals(registerRequestDto.getIsCustomer())) {
				registerCustomer(registerRequestDto);
				register.setIsCustomer(TRUE);

			} else {
				validateStep2(userId, DigestUtils.sha256Hex(CODE_NUMBER_PHONE + registerRequestDto.getPhoneNumber()));
			}
		} else {
			throw new RegisterCustomException(ErrorType.ERROR_WRONG_PASSWORD_ACCEPT_TERMS_CONDITIONS);
		}
		registerResponseDto.setRegister(register);
		return registerResponseDto;
	}

	private void registerCustomer(RegisterRequestDto registerRequestDto) {
		String dni = registerRequestDto.getDni();
		String userId = DigestUtils.sha256Hex(dni);

		User user = createUserFromDto(registerRequestDto);
		setTypeInUserOfRedis(userId, user);
		user.setType(user.getType().equals(BUC) ? BUC : null);
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(registerRequestDto.getMessageId(), userId,
				registerRequestDto.getSessionId());
		LoansAgreementResponseDto loansAgreementResponseDto = identityUtil.getResponseApiLoansAgreement(dni,
				consumeApiRequestDto);
		if (null != loansAgreementResponseDto && !loansAgreementResponseDto.getCredits().isEmpty()) {
			identityUtil.setUserWithLoans(user, loansAgreementResponseDto);
		}
		GetInformationPersonResponseDto responseApiPerson = identityUtil.getResponseApiPerson(dni,
				consumeApiRequestDto);
		if (null != responseApiPerson && responseApiPerson.getCustomerId() != null) {
			identityUtil.setUserWithApiPerson(user, responseApiPerson);
		}
		passwordHistoryServiceImpl.loadPasswordHistory(registerRequestDto);
		userRepository.insert(user);
		thirteenRedisDb.delete(userId.concat(FLOW_REGISTER));
	}

	private User createUserFromDto(RegisterRequestDto registerRequestDto) {
		User user = new User();
		BeanUtils.copyProperties(registerRequestDto, user);
		String dni = registerRequestDto.getDni();
		user.setUserId(DigestUtils.sha256Hex(dni));
		user.setDni(sensitiveData.encryptHsm(dni));
		user.setPassword(DigestUtils.sha256Hex(registerRequestDto.getSecret()));
		user.getDeviceDetails()
				.setDeviceId(registerRequestDto.getDeviceDetails().getDeviceId() != null
						? sensitiveData.encryptHsm(registerRequestDto.getDeviceDetails().getDeviceId())
						: null);
		user.setCurrentTimeDate(Calendar.getInstance().getTime().getTime());
		return user;
	}

	private void setTypeInUserOfRedis(String userId, User user) {

		try {
			Map<Object, Object> map = thirteenRedisDb.opsForHash().entries(userId.concat(FLOW_REGISTER));
			user.setType((String) map.get(CUSTOMER_TYPE));
		} catch (Exception e) {
			log.error("error al obtener el customerType de redis {}", e.getMessage());
		}
	}

	private String getIsCustomerOfRedis(String userId) {
		String isCustomer = null;
		try {
			Map<Object, Object> map = thirteenRedisDb.opsForHash().entries(userId.concat(FLOW_REGISTER));
			isCustomer = (String) map.get(IS_CUSTOMER);
		} catch (Exception e) {
			log.error("error al obtener el isCustomer of Redis {}", e.getMessage());
		}
		return isCustomer;
	}

	private void validateStep2(String userId, String phoneNumberId) {
		Map<Object, Object> map = thirteenRedisDb.opsForHash().entries(userId.concat(FLOW_REGISTER));
		String step2 = (String) map.get(PHONE_NUMBER);
		if (null == step2 || !step2.equals(phoneNumberId)) {
			throw new RegisterCustomException(ErrorType.INVALID_STEP_TWO_HASH);
		}
	}

	@Override
	public void createBucNoClientUser(RegisterRequestDto registerRequestDto) {
		if (registerRequestDto.getTermsConditions()) {
			User user = createUserFromDto(registerRequestDto);
			String phoneNumber = registerRequestDto.getPhoneNumber();
			user.setPhoneNumber(
					!StringUtils.isEmpty(phoneNumber) ? sensitiveData.encryptHsm(CODE_NUMBER_PHONE.concat(phoneNumber))
							: null);
			user.setPhoneNumberId(DigestUtils.sha256Hex(CODE_NUMBER_PHONE.concat(phoneNumber)));
			user.setDataProtection(registerRequestDto.getDataProtection());
			user.setTermsConditions(registerRequestDto.getTermsConditions());
			user.setStatus('F');
			user.setAttempts(0);
			user.setType(BUC);
			identityUtil.setNamesUserOfRedis(user);
			passwordHistoryServiceImpl.loadPasswordHistory(registerRequestDto);
			userRepository.insert(user);
		} else {
			throw new RegisterCustomException(ErrorType.ERROR_WRONG_PASSWORD_ACCEPT_TERMS_CONDITIONS);
		}
	}


}