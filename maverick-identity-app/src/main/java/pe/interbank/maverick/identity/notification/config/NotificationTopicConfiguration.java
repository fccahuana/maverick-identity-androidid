package pe.interbank.maverick.identity.notification.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.microsoft.azure.servicebus.ITopicClient;
import com.microsoft.azure.servicebus.TopicClient;
import com.microsoft.azure.servicebus.primitives.ConnectionStringBuilder;
import com.microsoft.azure.servicebus.primitives.ServiceBusException;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.azure.KeyVaultConfiguration;
import pe.interbank.maverick.commons.core.config.ServiceBusConfiguration;

@Getter
@Setter
@Configuration
public class NotificationTopicConfiguration {
private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTopicConfiguration.class);
	
	@Value("${identity.servicebus.topic.name}")
	String topicNotificationName;
	
	
	@Primary
	@Bean (name = "topicNotificationClient")
	public ITopicClient topicClient(KeyVaultConfiguration keyVaultConfiguration, ServiceBusConfiguration serviceBusConfiguration) throws InterruptedException {
		ITopicClient topicClient = null;
		
		try {
			topicClient = new TopicClient(new ConnectionStringBuilder(serviceBusConfiguration.getServiceBusSecretname(), topicNotificationName));

		} catch (InterruptedException e) {
			LOGGER.error(e.getMessage());
			Thread.currentThread().interrupt();
		} catch (ServiceBusException e) {
			LOGGER.error(e.getMessage());
		}
		return topicClient;
	}
}
