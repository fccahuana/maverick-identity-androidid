package pe.interbank.maverick.identity.redis.config.ten;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import lombok.Getter;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;

@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.redis.config.ten", redisTemplateRef = "stringRedisDb10")
public class OauthTokenAssiConfiguration {

	@Value("${identity.api.assi.tokenAssi.redis}")
	int redisOauthToken;
	
	@Bean("stringRedisDb10")
	StringRedisTemplate stringRedisDb10(RedisConfiguration redisConfiguration) {
		return new StringRedisTemplate(redisConfiguration.jedisConnectionFactory(redisOauthToken));
	}
}
