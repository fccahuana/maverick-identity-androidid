package pe.interbank.maverick.identity.campaign.service;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponseDto;

public interface ICampaignService {

	CampaignResponseDto getBucLead(String dni, ConsumeApiRequestDto consumeApiRequestDto);

}
