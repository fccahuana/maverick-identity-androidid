package pe.interbank.maverick.identity.refreshtoken.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.model.login.AccessToken;

import pe.interbank.maverick.identity.oauth.service.impl.TokenServiceImpl;
import pe.interbank.maverick.identity.refreshtoken.dto.RefreshTokenResponseDto;
import pe.interbank.maverick.identity.refreshtoken.dto.TokenDto;
import pe.interbank.maverick.identity.refreshtoken.service.IRefreshTokenService;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@Service
public class RefreshTokenServiceImpl implements IRefreshTokenService {
	
	@Autowired
	TokenServiceImpl tokenService;
	
	@Autowired
	UserRepository userRepository;	

	@Override
	public RefreshTokenResponseDto refreshToken(String  userId) {
		RefreshTokenResponseDto refreshTokenResponseDto = new RefreshTokenResponseDto();
		TokenDto tokenDto = new TokenDto();	
		User user = getUser(userId);
		AccessToken accessToken = tokenService.getRefreshToken(userId,user);
		tokenDto.setAccessToken(accessToken.getAccesToken());
		refreshTokenResponseDto.setToken(tokenDto);
		tokenService.updateTokenRedis(userId, accessToken.getAccesToken(), accessToken.getRefreshToken());
		return refreshTokenResponseDto;
	}
	
	private User getUser(String userId) {
		Optional<User> user = userRepository.findById(userId);
		if(user.isPresent()) {
			return user.get();
		}
		return null;
	}
	
	
}