package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.api.assi.exception.ApiAssiException;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.exception.OAuthCustomException;
import pe.interbank.maverick.identity.api.assi.repository.impl.OauthTokenRepositoryImpl;
import pe.interbank.maverick.identity.api.assi.model.token.OauthTokenResponse;
import pe.interbank.maverick.identity.core.util.ErrorType;


@Slf4j
@Component
public class ConsumeApiOauthTokenAssi {

	@Autowired
	private OauthTokenRepositoryImpl oauthTokenRepositoryImpl;

	@Resource(name = "stringRedisDb10")
	StringRedisTemplate stringRedisDb10;

	@Value("${identity.api.assi.tokenAssi.parameters}")
	String parameterOauthToken;
	
	public String getTokenAssi(ConsumeApiRequestDto consumeApiRequestDto) {

		String accessTokenAssi = null;
		String userId = consumeApiRequestDto.getUserId();
		accessTokenAssi = getTokenAssiOfRedis(userId);
		if (null == accessTokenAssi) {
			String jsonResponseOauthAssi = callServiceOauthTokenAssi(consumeApiRequestDto);
			OauthTokenResponse oauthTokenResponse = parseResponseOauthTokenAssi(jsonResponseOauthAssi);
			accessTokenAssi = getAccessTokenMoreBearer(oauthTokenResponse);
			setTokenInRedis(userId, oauthTokenResponse);
		}
		return accessTokenAssi;
	}
	
	private String getTokenAssiOfRedis(String userId) {
		String accessTokenAssi = null;
		Map<Object, Object> accessTokensAssi = stringRedisDb10.opsForHash().entries(userId);
		if (!accessTokensAssi.isEmpty()) {
			accessTokenAssi = String.valueOf(accessTokensAssi.get(userId));
		}
		return accessTokenAssi;
	}
	
	private String callServiceOauthTokenAssi(ConsumeApiRequestDto consumeApiRequestDto) {
		String jsonTokenAssi = null;
		try {

			jsonTokenAssi = oauthTokenRepositoryImpl.getTokenAssi(consumeApiRequestDto);

		} catch (ApiAssiException e) {
			log.error("Error in callServiceOauthTokenAssi: {}", e.getMessage());
			throw new OAuthCustomException(ErrorType.ERROR_GENERIC,e.getMessage());
		}
		return jsonTokenAssi;
	}
	private OauthTokenResponse parseResponseOauthTokenAssi(String jsonTokenAssi) {

		if (!StringUtils.isNotBlank(jsonTokenAssi.trim())) {
			throw new NullPointerException();
		}
		OauthTokenResponse oAuthTokenResponse = null;
		try {
			oAuthTokenResponse = new Gson().fromJson(jsonTokenAssi, OauthTokenResponse.class);
		} catch (ApiAssiException e) {
			log.error("Error in parseResponseOauthTokenAssi: {}", e.getError().getSystemMessage());
			throw new OAuthCustomException(ErrorType.ERROR_GENERIC,e.getError().getSystemMessage());
		}catch (Exception e) {
			log.error("Error in parseResponseOauthTokenAssi: {}", e.getMessage());
			throw new OAuthCustomException(ErrorType.ERROR_GENERIC);
		}
		return oAuthTokenResponse;
	}

	private String getAccessTokenMoreBearer(OauthTokenResponse oauthTokenResponse) {
		StringBuilder accesToken = new StringBuilder(WordUtils.capitalizeFully(oauthTokenResponse.getTokenType()));
		accesToken.append(" ");
		accesToken.append(oauthTokenResponse.getAccessToken());
		return accesToken.toString();
	}
	
	private void setTokenInRedis(String userId, OauthTokenResponse oauthTokenResponse) {
		try {
			Map<String, String> oauthToken = new HashMap<>();
			String accessTokenMoreBearer = getAccessTokenMoreBearer(oauthTokenResponse);
			oauthToken.put(userId, accessTokenMoreBearer);
			stringRedisDb10.opsForHash().putAll(userId, oauthToken);
			stringRedisDb10.expire(userId, oauthTokenResponse.getExpiresIn(), TimeUnit.SECONDS);
		} catch (Exception e) {
			log.error("Error in setTokenInRedis: {}", e.getMessage());
			throw new OAuthCustomException(ErrorType.ERROR_GENERIC);
		}
	}

}
