package pe.interbank.maverick.identity.exception.api;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;


@Getter
@Setter
public class ConsumeApiInformationPersonCustomException extends BaseException{

	private final ErrorDto error;
	
	private static final long serialVersionUID = 1L;
	
	public ConsumeApiInformationPersonCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
		error = super.getError();
	}
	
	public ConsumeApiInformationPersonCustomException(String code,String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		super(code, title, userMessage, systemMessage, httpStatus);
		error = super.getError();
	}
	
	public ConsumeApiInformationPersonCustomException(EsbBackendCommunicationException e) {
		super("01.06.26", "Error", "Ups, ocurrio un error inesperado.", "Ocurrió un error en el servicio de api person",
				HttpStatus.INTERNAL_SERVER_ERROR);
		ErrorType genericError = ErrorType.ERROR_GENERIC;
		ErrorType  errorType = null;
		ConsumeApiInformationPersonCustomException consumeApiInformationPersonCustomException = new ConsumeApiInformationPersonCustomException(genericError.getCode(), genericError.getTitle(),
				genericError.getUserMessage(),
				e.getEsbResponseMessage() != null ? e.getEsbResponseMessage() : e.getError().getSystemMessage(),
				genericError.getHttpStatus());
		if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN && Integer.parseInt(e.getEsbResponseCode())!=500) {
			errorType = ErrorType.ERROR_SERVICE_GET_PHONE_RM;
			consumeApiInformationPersonCustomException= new ConsumeApiInformationPersonCustomException(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage()
					,e.getEsbResponseMessage(), errorType.getHttpStatus());
		}else if(e.getError().getHttpStatus() == HttpStatus.FORBIDDEN && Integer.parseInt(e.getEsbResponseCode())==500) {
			errorType = ErrorType.ERROR_SERVICE_EQUIFAX;
			consumeApiInformationPersonCustomException= new ConsumeApiInformationPersonCustomException(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage()
					,errorType.getSystemMessage(), errorType.getHttpStatus());
		}
		error = consumeApiInformationPersonCustomException.getError();
	}
}
