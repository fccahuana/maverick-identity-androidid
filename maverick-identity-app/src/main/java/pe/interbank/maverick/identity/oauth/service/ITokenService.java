package pe.interbank.maverick.identity.oauth.service;

import pe.interbank.maverick.identity.oauth.dto.TokenResponseDto;

public interface ITokenService {
	TokenResponseDto refreshToken(String userId);
}