package pe.interbank.maverick.identity.register.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class Register implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String isCustomer;
	private String type;
	private String customerName;

}
