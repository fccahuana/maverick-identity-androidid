package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.identity.esb.api.model.getquestions.Question;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class GetQuestionResponseDto implements Serializable{
	
	private static final long serialVersionUID = -8720049738224363193L;
	private String operationId;
	private String reject;
	private String unanswered;
	private String disapproved;
	private List<Question> questions;
	private ErrorDto error = null;
}
