package pe.interbank.maverick.identity.register.service;

import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.dto.RegisterResponseDto;

public interface IRegisterService {
	
	RegisterResponseDto validationDni(RegisterRequestDto userRequestDto);
	void savePhone(RegisterRequestDto userRequestDto);
	void validationPassword(RegisterRequestDto userRequestDto);
	RegisterResponseDto createUser(RegisterRequestDto userRequestDto);
	void createBucNoClientUser(RegisterRequestDto registerRequestDto);
}
