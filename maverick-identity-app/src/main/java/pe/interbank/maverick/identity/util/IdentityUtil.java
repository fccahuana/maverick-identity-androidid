package pe.interbank.maverick.identity.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.microsoft.azure.servicebus.IMessage;
import com.microsoft.azure.servicebus.ITopicClient;
import com.microsoft.azure.servicebus.Message;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.catalogs.Catalogs;
import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.commons.crypto.service.CryptoAesService;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.api.ConsumeApiLoansAgreement;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponseDto;
import pe.interbank.maverick.identity.campaign.service.impl.CampaignServiceImpl;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.EmailResponse;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.PhoneResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CreditsResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CustomerResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.LoansAgreementResponseDto;
import pe.interbank.maverick.identity.exception.api.ConsumeApiCustomerLeadCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiDetailLeadCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiInformationPersonCustomException;
import pe.interbank.maverick.identity.exception.api.ConsumeApiLoansAgreementCustomException;
import pe.interbank.maverick.identity.exception.register.RegisterCustomException;
import pe.interbank.maverick.identity.exception.validation.IdentityValidationCustomException;
import pe.interbank.maverick.identity.register.dto.Customer;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@Slf4j
@Component
public class IdentityUtil {

	@Autowired
	UserRepository userRepository;

	@Autowired
	SensitiveData sensitiveData;

	@Autowired
	@Qualifier("topicNotificationClient")
	ITopicClient topicNotificationClient;

	@Autowired
	@Qualifier("identityValidationTokenOtpRedisDb")
	StringRedisTemplate identityValidationTokenOtpRedisDb;

	@Value("${identity.notification.sms}")
	String sms;

	@Value("${identity.validation.expiration.redis.token}")
	long identityValidationTokenExpirationTime;

	@Value("${identity.validation.bucNoClientsAttempts}")
	int allowedBucNoClientsAttempts;

	@Autowired
	ConsumeApiLoansAgreement consumeApiLoansAgreement;

	@Autowired
	CampaignServiceImpl campaignServiceImpl;

	@Autowired
	ConsumeApiInformationPerson consumeApiInformationPerson;

	@Autowired
	GenericRepository genericRepository;
	
	@Autowired
	@Qualifier("thirteenRedisDb")
	StringRedisTemplate thirteenRedisDb;
	
	@Autowired
	CryptoAesService cryptoAesService;

	static final String COMPLETE_CERO = "%010d";
	private static final String OTPCODE = "$otpcode$";
	private static final String DATE = "$date$";
	private static final String TIME = "$time$";
	private static final String EQUIFAX = "EQUIFAX";
	private static final String ATTEMPTS_REPLACE = "[attempts]";
	private static final String ID_CELLPHONE = "C";
	
	static final String FIRST_NAME = "FIRST_NAME";
	static final String SECOND_NAME = "SECOND_NAME";
	static final String LAST_NAME = "LAST_NAME";
	static final String SECOND_LAST_NAME = "SECOND_LAST_NAME";
	static final String FLOW_REGISTER = "_REGISTER";
	
	
	public void updateNames(GetInformationPersonResponseDto informationPersonResponseDto, String userId) {
		try {
			Optional<User> userFound = userRepository.findById(userId);
			if (userFound.isPresent()) {
				User user = userFound.get();
				String firstName = informationPersonResponseDto.getFirstName();
				user.setFirstName(!StringUtils.isEmpty(firstName) ? sensitiveData.encryptHsm(firstName) : null);
				String secondName = informationPersonResponseDto.getSecondName();
				user.setSecondName(!StringUtils.isEmpty(secondName) ? sensitiveData.encryptHsm(secondName) : null);
				String lastName = informationPersonResponseDto.getLastName();
				user.setLastName(!StringUtils.isEmpty(lastName) ? sensitiveData.encryptHsm(lastName) : null);
				String secondLastName = informationPersonResponseDto.getSecondLastName();
				user.setSecondLastName(!StringUtils.isEmpty(secondLastName) ? sensitiveData.encryptHsm(secondLastName): null);
				userRepository.save(user);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}

	public User setUserEncryptedNames(GetInformationPersonResponseDto informationPersonResponseDto, String userId) {
		User user = null;
		try {
			Optional<User> userFound = userRepository.findById(userId);
			if (userFound.isPresent()) {
				user = userFound.get();
				user.setFirstName(informationPersonResponseDto.getFirstName());
				user.setSecondName(informationPersonResponseDto.getSecondName());
				user.setLastName(informationPersonResponseDto.getLastName());
				user.setSecondLastName(informationPersonResponseDto.getSecondLastName());
				encryptNamesUser(user);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return user;
	}

	private void encryptNamesUser(User user) {
		String firstName = user.getFirstName();
		String secondName = user.getSecondName();
		String lastName = user.getLastName();
		String secondLastName = user.getSecondLastName();
		user.setFirstName(!StringUtils.isEmpty(firstName) ? sensitiveData.encryptHsm(firstName) : null);
		user.setSecondName(!StringUtils.isEmpty(secondName) ? sensitiveData.encryptHsm(secondName) : null);
		user.setLastName(!StringUtils.isEmpty(lastName) ? sensitiveData.encryptHsm(lastName) : null);
		user.setSecondLastName(!StringUtils.isEmpty(secondLastName) ? sensitiveData.encryptHsm(secondLastName) : null);
	}

	public String getPhoneNumberOfApiPerson(GetInformationPersonResponseDto informationPersonResponseDto) {
		String phoneNumberOfService = "";

		List<PhoneResponse> phones = informationPersonResponseDto.getTelephones();

		for (PhoneResponse phone : phones) {
			if (ID_CELLPHONE.equals(phone.getType())) {
				phoneNumberOfService = phone.getNumber();
				break;
			}
		}
		return phoneNumberOfService;
	}

	public void validateClientHaveCellPhone(GetInformationPersonResponseDto informationPersonResponseDto,
			ErrorType error) {
		boolean haveCellPhone = false;
		List<PhoneResponse> phones = informationPersonResponseDto.getTelephones();
		if (phones.isEmpty()) {
			throw new ConsumeApiInformationPersonCustomException(error);
		} else {
			for (PhoneResponse phone : phones) {
				if (ID_CELLPHONE.equals(phone.getType())) {
					haveCellPhone = true;
					break;
				}
			}
		}
		if (!haveCellPhone) {
			throw new ConsumeApiInformationPersonCustomException(error);
		}
	}

	public String getJsonSMS(String otpCode, User user, String messageId, String sessionId) {
		StringBuilder jsonSMS = new StringBuilder();
		if (!StringUtils.isBlank(otpCode)) {
			String smsBody = sms.replace(OTPCODE, otpCode);
			smsBody = smsBody.replace(DATE, Util.getDateForLong(new Date().getTime()));
			smsBody = smsBody.replace(TIME, Util.getHourForLongDate(new Date().getTime()));
			jsonSMS.append("{\"messageId\":\"").append(messageId);
			jsonSMS.append("\",\"userId\":\"").append(user.getUserId());
			jsonSMS.append("\",\"sessionId\":\"").append(sessionId);
			jsonSMS.append("\",\"customerId\":\"").append(user.getCustomerId());
			jsonSMS.append("\",\"destination\":\"").append(user.getPhoneNumber()).append("\",\"carrier\":\"")
					.append(user.getCarrier()).append("\",\"body\":\"").append(smsBody).append("\"}");
		} else {
			throw new IdentityValidationCustomException(ErrorType.ERROR_GENERIC);
		}
		return jsonSMS.toString();
	}

	public void sendMessage(String smsBody) {
		if (smsBody.trim().length() > 0) {
			String encryptedMessageInTopic = cryptoAesService.encryptAes(smsBody);
			IMessage message = new Message(encryptedMessageInTopic);
			message.setLabel(Constants.SMS_LABEL);
			topicNotificationClient.sendAsync(message).thenRunAsync(() -> {
			});
		}
	}

	public void saveTokenInRedis(String idTokenForRedis, GenerateKeyResponseDto generateKeyResponseDto) {
		if (null == generateKeyResponseDto)
			throw new NullPointerException();
		String tokenInRedis = DigestUtils.sha256Hex(generateKeyResponseDto.getValidToken().trim());
		Map<String, String> tokenMap = new HashMap<>();
		tokenMap.put(Constants.TOKEN, tokenInRedis);
		identityValidationTokenOtpRedisDb.opsForHash().putAll(idTokenForRedis, tokenMap);
		identityValidationTokenOtpRedisDb.expire(idTokenForRedis, identityValidationTokenExpirationTime,
				TimeUnit.SECONDS);
	}

	public void validateTokenInRedis(String idTokenForRedis, String verificationCode, ErrorType errorType) {
		Map<Object, Object> map = identityValidationTokenOtpRedisDb.opsForHash().entries(idTokenForRedis);
		String tokenInRedis = (String) map.get(Constants.TOKEN);
		if (null == tokenInRedis || !tokenInRedis.equals(DigestUtils.sha256Hex(verificationCode))) {
			throw new IdentityValidationCustomException(errorType);
		}
	}

	public String generateIdToken(String dni, String typeFlow) {
		StringBuilder tokenIdInRedis = new StringBuilder(dni);
		tokenIdInRedis.append(typeFlow);
		return DigestUtils.sha256Hex(tokenIdInRedis.toString());
	}

	public LoansAgreementResponseDto getResponseApiLoansAgreement(String dni,
			ConsumeApiRequestDto consumeApiRequestDto) {
		LoansAgreementResponseDto loansAgreementResponseDto = null;
		try {
			loansAgreementResponseDto = consumeApiLoansAgreement.consumeServiceLoansAgreement(dni,
					consumeApiRequestDto);
		} catch (ConsumeApiLoansAgreementCustomException e) {
			if (e.getError().getCode().equals(ErrorType.ERROR_ONLY_CLIENTS.getCode())) {
				loansAgreementResponseDto = new LoansAgreementResponseDto();
				loansAgreementResponseDto.setCredits(new ArrayList<CreditsResponse>());
			} else {
				throw e;
			}
		}
		return loansAgreementResponseDto;
	}

	public boolean customerHasCredit(LoansAgreementResponseDto loansAgreementResponseDto) {
		boolean customerHasCredit = true;

		if (loansAgreementResponseDto.getCredits().isEmpty()) {
			customerHasCredit = false;
		}

		return customerHasCredit;
	}

	public void validateCustomerBuc(String dni, ConsumeApiRequestDto consumeApiRequestDto) {
		try {
			CampaignResponseDto campaignResponseDto = campaignServiceImpl.getBucLead(dni, consumeApiRequestDto);
			if (CollectionUtils.isEmpty(campaignResponseDto.getLeads())) {
				throw new ConsumeApiDetailLeadCustomException(ErrorType.ERROR_ONLY_CLIENTS);
			}
		} catch (ConsumeApiDetailLeadCustomException | ConsumeApiCustomerLeadCustomException e) {
			if (e.getError().getCode().equals(ErrorType.NO_EXTENSION_CAMPAIGN.getCode())) {
				throw new ConsumeApiDetailLeadCustomException(ErrorType.ERROR_ONLY_CLIENTS);
			} else {
				throw e;
			}
		}

	}

	public GetInformationPersonResponseDto getResponseApiPerson(String dni, ConsumeApiRequestDto consumeApiRequestDto) {
		GetInformationPersonResponseDto responseApiPerson = null;
		try {
			responseApiPerson = consumeApiInformationPerson.consumeServiceApiInformationPerson(consumeApiRequestDto,
					dni);
		} catch (ConsumeApiInformationPersonCustomException e) {
			if (e.getError().getCode().equals(ErrorType.ERROR_SERVICE_GET_PHONE_RM.getCode())) {
				throw new ConsumeApiInformationPersonCustomException(ErrorType.ERROR_ONLY_CLIENTS);
			} else {
				throw e;
			}
		}

		return responseApiPerson;

	}

	public String getFamilyNameForCompanyId(int companyId) {
		String familyName = Constants.EMPTY;
		Catalogs catalog = getFamilyForCompanyId(companyId);
		if (null != catalog) {
			familyName = catalog.getFamily();
			if ("Otros".equals(catalog.getFamily())) {
				familyName = catalog.getOtherFamily();
			}
		}
		return familyName;
	}

	public String getPlaceNameForCompanyId(int companyId) {
		Catalogs catalog = getFamilyForCompanyId(companyId);
		String placeName = Constants.EMPTY;
		if (null != catalog) {
			placeName = catalog.getPlace();
			if ("NULL".equals(placeName)) {
				placeName = Constants.EMPTY;
			}
		}
		return placeName;
	}

	private Catalogs getFamilyForCompanyId(int companyId) {
		StringBuilder sb = new StringBuilder("familia");
		sb.append(String.format(COMPLETE_CERO, companyId));
		String identifier = DigestUtils.sha256Hex(sb.toString());
		Optional<Generic> generic = genericRepository.findById(identifier);
		Catalogs catalog = null;
		if (generic.isPresent()) {
			catalog = generic.get().getCatalogs();
		}
		return catalog;
	}

	public User getUser(String userId) {
		User user = null;
		Optional<User> userFound = userRepository.findById(userId);
		if (userFound.isPresent()) {
			user = userFound.get();
		}
		return user;
	}

	public String getEmailApiPerson(GetInformationPersonResponseDto informationPersonResponseDto) {
		String customerEmail = "";
		List<EmailResponse> emails = informationPersonResponseDto.getEmails();
		for (EmailResponse email : emails) {
			if ("EMAPER".equals(email.getSubtype())) {
				customerEmail = email.getEmail();
				break;
			}
		}
		return customerEmail;
	}

	public String getPhoneNumber(GetInformationPersonResponseDto informationPersonResponseDto, String userId) {
		String phoneNumber = null;
		if (null != informationPersonResponseDto.getCustomerId()) {
			this.validateClientHaveCellPhone(informationPersonResponseDto,
					ErrorType.ERROR_USER_NOT_PHONENUMBER_RECOVERY_PASSWORD);
			phoneNumber = this.getPhoneNumberOfApiPerson(informationPersonResponseDto);

		} else {
			phoneNumber = sensitiveData.decrypt(this.getUser(userId).getPhoneNumber()).substring(3);
		}
		return phoneNumber;
	}

	public void validateBucNoClients(String dni) {
		validateBucNoClientsAttempts(dni);
		validateBucNoClientsAllAttempts(dni);
	}

	public void validateBucNoClientsAllAttempts(String dni) {
		String identifierAllAttempts = DigestUtils.sha256Hex(EQUIFAX.concat(dni));
		Optional<Generic> genericOp = genericRepository.findById(identifierAllAttempts);
		if (genericOp.isPresent()) {
			Generic generic = genericOp.get();
			int attempts = Integer.parseInt(generic.getValidationAttempts().getAttempts());
			if (attempts >= allowedBucNoClientsAttempts) {
				throw new IdentityValidationCustomException(ErrorType.ERROR_INCORRECT_ALL_ATTEMPTS_VALID_QUESTIONS);
			}
		}
	}

	public void validateBucNoClientsAttempts(String dni) {
		String identifierAttempts = DigestUtils.sha256Hex(EQUIFAX.concat(dni));
		Optional<Generic> genericOp = genericRepository.findById(identifierAttempts);
		if (genericOp.isPresent()) {
			Generic generic = genericOp.get();
			int attempts = Integer.parseInt(generic.getValidationAttempts().getAttempts());
			if (attempts < allowedBucNoClientsAttempts) {
				ErrorType errorType = ErrorType.ERROR_INCORRECT_VALID_QUESTIONS;
				String message = errorType.getUserMessage().replace(ATTEMPTS_REPLACE,
						String.valueOf(allowedBucNoClientsAttempts - attempts));
				throw new IdentityValidationCustomException(errorType.getCode(), errorType.getTitle(), message,
						errorType.getSystemMessage(), errorType.getHttpStatus());
			}
		}
	}

	public void validationUserByPhoneNumber(String dato, ErrorType errorType) {
		List<User> users = userRepository.findByPhoneNumberId(dato);
		if (users != null && !users.isEmpty())
			throw new RegisterCustomException(errorType);
	}

	public void validationIsUser(String userId) {
		Optional<User> user = userRepository.findById(userId);
		if (user.isPresent())
			throw new RegisterCustomException(ErrorType.ERROR_EXISTING_USER);
	}

	public void setUserWithLoans(User user, LoansAgreementResponseDto loansAgreementResponseDto) {
		Customer customer = loadBusinessData(loansAgreementResponseDto);
		String institutionName = customer.getInstitutionName();
		String familyName = customer.getFamilyName();
		String placeName = customer.getPlaceName();
		String id = customer.getId();
		CustomerResponse customerOfLoans = loansAgreementResponseDto.getCustomer();

		user.setInstitutionName(StringUtils.isEmpty(institutionName) ? null : institutionName);
		user.setFamilyName(StringUtils.isEmpty(familyName) ? null : familyName);
		user.setPlaceName(StringUtils.isEmpty(placeName) ? null : placeName);
		user.setCustomerId(StringUtils.isEmpty(id) ? null : id);

		user.setCompanyId(String.format(COMPLETE_CERO, customer.getCompanyId()));
		user.setFirstName(customerOfLoans.getFirstName());
		user.setSecondName(customerOfLoans.getSecondName());
		user.setLastName(customerOfLoans.getLastName());
		user.setSecondLastName(customerOfLoans.getSecondLastName());
		encryptNamesUser(user);
	}

	private Customer loadBusinessData(LoansAgreementResponseDto loansAgreementResponseDto) {
		Customer customer = new Customer();
		CreditsResponse credit = loansAgreementResponseDto.getCredits().get(0);
		int companyId = credit.getCompanyId();
		customer.setInstitutionName(WordUtils.capitalizeFully(credit.getAgreementName()));
		customer.setFamilyName(WordUtils.capitalizeFully(getFamilyNameForCompanyId(companyId)));
		customer.setPlaceName(WordUtils.capitalizeFully(getPlaceNameForCompanyId(companyId)));
		customer.setId(loansAgreementResponseDto.getCustomer().getId());
		customer.setCompanyId(credit.getCompanyId());
		return customer;
	}

	public void setUserWithApiPerson(User user, GetInformationPersonResponseDto informationPersonResponseDto) {
		String id = String.format(COMPLETE_CERO, Integer.parseInt(informationPersonResponseDto.getCustomerId()));
		user.setCustomerId(Constants.EMPTY.equals(id) ? null : id);
		user.setFirstName(informationPersonResponseDto.getFirstName());
		user.setSecondName(informationPersonResponseDto.getSecondName());
		user.setLastName(informationPersonResponseDto.getLastName());
		user.setSecondLastName(informationPersonResponseDto.getSecondLastName());
		encryptNamesUser(user);
	}
	
	public void deleteKeyOfRedis(String key, StringRedisTemplate redisDb) {
		redisDb.delete(key);
	}
	
	public void setNamesUserOfRedis(User user) {
		try {
			Map<Object, Object> map = thirteenRedisDb.opsForHash().entries(user.getUserId().concat(FLOW_REGISTER));
			String firstName = (String) map.get(FIRST_NAME);
			String secondName = (String) map.get(SECOND_NAME);
			String lastName = (String) map.get(LAST_NAME);
			String secondLastName = (String) map.get(SECOND_LAST_NAME);
			user.setFirstName(getFieldOfRedis(firstName));
			user.setSecondName(getFieldOfRedis(secondName));
			user.setLastName(getFieldOfRedis(lastName));
			user.setSecondLastName(getFieldOfRedis(secondLastName));
		} catch (Exception e) {
			log.error("Error al obtener los nombres de redis - no cliente buc {}", e.getMessage());
		}
	}
	
	private String getFieldOfRedis(String field) {
		return !StringUtils.isEmpty(field) ? field : null;
	}
	
	public ConsumeApiRequestDto setRequestApi(String messageId, String userId, String sessionId) {
		ConsumeApiRequestDto consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId(messageId);
		consumeApiRequestDto.setUserId(userId);
		consumeApiRequestDto.setSessionId(sessionId);
		return consumeApiRequestDto;
	}

}