package pe.interbank.maverick.identity.recoverypassword.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;


@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.recoverypassword", redisTemplateRef = "stringRedisTemplateForgot")
public class RecoveryPasswordConfiguration {
	
	@Value("${identity.forgot.redis.db}")
	int forgotDb;
	
	@Bean("stringRedisTemplateForgot")
	StringRedisTemplate stringRedisTemplateForgot(RedisConfiguration redisConfiguration) {
		return new StringRedisTemplate(redisConfiguration.jedisConnectionFactory(forgotDb));
	}
}
