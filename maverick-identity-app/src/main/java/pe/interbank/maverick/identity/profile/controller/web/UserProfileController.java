package pe.interbank.maverick.identity.profile.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.profile.dto.UserProfileRequestDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileResponseDto;
import pe.interbank.maverick.identity.profile.service.impl.UserProfileServiceImpl;


@RequestMapping("/private")
@RefreshScope
@RestController
public class UserProfileController {

	@Autowired
	UserProfileServiceImpl userProfileService;
	


	@PostMapping(value = "/user/profile")
	public ResponseEntity<UserProfileResponseDto> getUserProfile(@RequestHeader("Authorization") String token, @RequestHeader("Firebase-token") String fcmtoken, 
			@RequestHeader("Fingerprint-Subscription-Key") String fingerPrint) {
		UserProfileRequestDto userProfileRequestDto = new UserProfileRequestDto();
		userProfileRequestDto.setFcmtoken(fcmtoken);
		userProfileRequestDto.setFingerPrint(fingerPrint);
		userProfileRequestDto.setUserId(Util.getUserIdFromJWT(token));
		UserProfileResponseDto userProfileResponseDto = userProfileService.getUserProfile(userProfileRequestDto);
		return  new ResponseEntity<>(userProfileResponseDto, HttpStatus.OK);	
	}
	
	@PostMapping(value = "/user/profile/update")
	public ResponseEntity<UserProfileResponseDto> updateUserProfile(@RequestHeader("Authorization") String token, @RequestHeader("Firebase-token") String fcmtoken, 
			@RequestHeader("Fingerprint-Subscription-Key") String fingerPrint, @RequestBody UserProfileRequestDto userProfileRequestDto) {
		userProfileRequestDto.setFcmtoken(fcmtoken);
		userProfileRequestDto.setFingerPrint(fingerPrint);
		userProfileRequestDto.setUserId(Util.getUserIdFromJWT(token));
		UserProfileResponseDto userProfileResponseDto = userProfileService.updateUserProfile(userProfileRequestDto);
		return  new ResponseEntity<>(userProfileResponseDto, HttpStatus.OK);	
	}

}
