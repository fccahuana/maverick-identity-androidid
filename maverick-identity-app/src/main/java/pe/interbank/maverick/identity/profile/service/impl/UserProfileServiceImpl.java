package pe.interbank.maverick.identity.profile.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Optional;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.exception.profile.UserProfileCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.impl.PasswordHistoryServiceImpl;
import pe.interbank.maverick.identity.profile.dto.ProfileDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileRequestDto;
import pe.interbank.maverick.identity.profile.dto.UserProfileResponseDto;
import pe.interbank.maverick.identity.profile.service.UserProfileService;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;

@Service
public class UserProfileServiceImpl implements UserProfileService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	SensitiveData sensitiveData;
	
	@Autowired
	PasswordHistoryServiceImpl passwordHistoryService;
	
	@Autowired
	IdentityUtil identityUtil;
	
	@Override
	public UserProfileResponseDto getUserProfile(UserProfileRequestDto userProfileRequestDto) {

		UserProfileResponseDto userProfileResponseDto =new UserProfileResponseDto();
		userProfileResponseDto.setProfile(new ProfileDto());
		Optional<User> userOp = userRepository.findById(userProfileRequestDto.getUserId());
		if (userOp.isPresent()) {
			User user = userOp.get();
			setCustomerNames(user, userProfileResponseDto);
			setOfuscateFields(user, userProfileResponseDto);
			userProfileResponseDto.getProfile().setDataProtection(user.isDataProtection());
			setFingerPrintFlag(user, userProfileRequestDto, userProfileResponseDto);
		}
		
		return userProfileResponseDto;
	}
	
	private void setCustomerNames (User user, UserProfileResponseDto userProfileResponseDto) {
		String firstName = user.getFirstName();
		String secondName = user.getSecondName();
		String lastName = user.getLastName();
		String secondLastName = user.getSecondLastName();
		StringBuilder sb = new StringBuilder(firstName != null ? sensitiveData.decrypt(firstName) + Constants.SPACE : Constants.EMPTY);
		sb.append(secondName != null ? sensitiveData.decrypt(secondName) + Constants.SPACE : Constants.EMPTY);
		sb.append(lastName != null ? sensitiveData.decrypt(lastName) + Constants.SPACE : Constants.EMPTY);
		sb.append(secondLastName != null ? sensitiveData.decrypt(secondLastName) : Constants.EMPTY);
		String fullName = sb.toString().trim();
		userProfileResponseDto.getProfile().setCustomerName(fullName);
	}
	
	private void setOfuscateFields(User user, UserProfileResponseDto userProfileResponseDto) {
		String email = user.getEmail();
		if (!StringUtils.isEmpty(email)) {
			userProfileResponseDto.getProfile().setEmail(Util.ofuscateFieldMail(sensitiveData.decrypt(email),
					'•', 3));
		}
		String phoneNumber = user.getPhoneNumber();
		if (!StringUtils.isEmpty(phoneNumber)) {
			userProfileResponseDto.getProfile().setPhoneNumber(Util.ofuscateFieldValidation(sensitiveData.decrypt(phoneNumber).substring(3), 
					'•', 2, 3));
		}
	}
	
	private void setFingerPrintFlag(User user, UserProfileRequestDto userProfileRequestDto, 
			UserProfileResponseDto userProfileResponseDto) {
		String fingerHash = Util.getFingerPrintHash(userProfileRequestDto.getFcmtoken(), userProfileRequestDto.getUserId(),
				userProfileRequestDto.getFingerPrint());
		userProfileResponseDto.getProfile().setEnableFingerPrint(false);
		if(!StringUtils.isEmpty(user.getFingerPrintId())  && 
				MessageDigest.isEqual(user.getFingerPrintId().getBytes(StandardCharsets.UTF_8), fingerHash.getBytes(StandardCharsets.UTF_8))) {
			userProfileResponseDto.getProfile().setEnableFingerPrint(true);
		} 
	}

	@Override
	public UserProfileResponseDto updateUserProfile(UserProfileRequestDto userProfileRequestDto) {
		UserProfileResponseDto userProfileResponseDto = new UserProfileResponseDto();
		userProfileResponseDto.setProfile(new ProfileDto());
		Optional<User> userOp = userRepository.findById(userProfileRequestDto.getUserId());
		if(userOp.isPresent()) {
			User user = userOp.get();
			String oldPassword = userProfileRequestDto.getOldPassword();
			String newPassword = userProfileRequestDto.getNewPassword();
			String email = userProfileRequestDto.getNewEmail();
			if(userProfileRequestDto.getDataProtection() != null) {
			user.setDataProtection(userProfileRequestDto.getDataProtection());
			}
			userProfileResponseDto.getProfile().setDataProtection(userProfileRequestDto.getDataProtection());
			userProfileResponseDto.getProfile().setEnableFingerPrint(userProfileRequestDto.getEnableFingerPrint()); 
			if(!StringUtils.isEmpty(oldPassword) && !StringUtils.isEmpty(newPassword)) {
				validateOldPassword(oldPassword, user);
				savePasswordHistory(newPassword, userProfileRequestDto.getUserId());
				updatePasswordProfileData(user, newPassword);
			}
			if (!StringUtils.isEmpty(email)) {
				userProfileResponseDto.getProfile().setUpdatedEmail(Util.ofuscateFieldMail(email,'•', 3));
				userProfileResponseDto.getProfile().setEmail(email);
				updateEmailProfileData(user, email);
			}
			validateFingerPrint(userProfileRequestDto, user);
			
		}
		return userProfileResponseDto;
	}
	
	private void validateFingerPrint(UserProfileRequestDto userProfileRequestDto, User user) {
		Boolean enableFingerPrint = userProfileRequestDto.getEnableFingerPrint();
		if(enableFingerPrint != null) {
			if (enableFingerPrint.booleanValue()) {
				enableFingerPrint(userProfileRequestDto, user);
			} else {
				disableFingerPrint(user);
			}
		}
	}
	
	private void enableFingerPrint(UserProfileRequestDto userProfileRequestDto, User user) {
		user.setFingerPrintId(Util.getFingerPrintHash(userProfileRequestDto.getFcmtoken(), userProfileRequestDto.getUserId(),
				userProfileRequestDto.getFingerPrint()));
		user.setTermsConditionsFingerPrint(true);
		userRepository.save(user);
	}
	
	private void disableFingerPrint(User user) {
		user.setFingerPrintId(null);
		userRepository.save(user);
	}

	private void updateEmailProfileData (User user, String email) {
		user.setEmail(sensitiveData.encryptHsm(email));
		updateProfileData(user);
	}
	
	private void updatePasswordProfileData (User user, String newPassword) {
		user.setPassword(DigestUtils.sha256Hex(newPassword));
		updateProfileData(user);
	}
	
	private void updateProfileData(User user) {
		userRepository.save(user);
	}
	
	private void validateOldPassword (String oldPassword, User user) {
		String passwordHash = DigestUtils.sha256Hex(oldPassword);
		if(! MessageDigest.isEqual(passwordHash.getBytes(StandardCharsets.UTF_8), user.getPassword().getBytes(StandardCharsets.UTF_8))) {
			throw new UserProfileCustomException(ErrorType.ERROR_INCORRECT_CURRENT_PASSWORD);
		}
	}

	private void savePasswordHistory(String newPassword, String userId ) {
		passwordHistoryService.savePasswordHistoryByRecoveryPassword(newPassword, userId);
	}

}
