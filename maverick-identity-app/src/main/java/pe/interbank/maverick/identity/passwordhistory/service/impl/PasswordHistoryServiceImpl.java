package pe.interbank.maverick.identity.passwordhistory.service.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.core.model.passwordhistory.PasswordHistory;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;
import pe.interbank.maverick.identity.passwordhistory.service.IPasswordHistoryService;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.repository.passwordhistory.PasswordHistoryRepository;

@Service
public class PasswordHistoryServiceImpl implements IPasswordHistoryService {

	@Autowired
	PasswordHistoryRepository passwordHistoryRepository;

	@Autowired
	private SensitiveData sensitiveData;

	@Autowired
	@Qualifier("stringRedisTemplateStep")
	protected StringRedisTemplate redisStepTemplate;

	@Override
	public void loadPasswordHistory(RegisterRequestDto userRequestDto) {
		PasswordHistory passwordHistory = new PasswordHistory();
		passwordHistory.setPassword(sensitiveData.encryptHsm(userRequestDto.getSecret()));
		passwordHistory.setUserId(DigestUtils.sha256Hex(userRequestDto.getDni()));
		passwordHistory.setCurrentTimeDate(Calendar.getInstance().getTime().getTime());
		long passwords = passwordHistoryRepository.countByUserId(passwordHistory.getUserId());
		if (passwords > 0) {
			throw new DuplicateKeyException("Password exist");
		}
		passwordHistoryRepository.insert(passwordHistory);
	}

	@Override
	public void savePasswordHistoryByRecoveryPassword(String newPassword, String userId) {
		PasswordHistory objPasswordHistory = new PasswordHistory();
		objPasswordHistory.setUserId(userId);
		Sort sort = new Sort(Sort.Direction.DESC, "currentTimeDate");
		List<PasswordHistory> passwordsHistory = passwordHistoryRepository.findFirst5ByUserId(userId, sort);
		for (PasswordHistory passwordHistory : passwordsHistory) {
			String passwordDecrypted = sensitiveData.decrypt(passwordHistory.getPassword());
			if (newPassword.equals(passwordDecrypted)) {
				throw new RecoveryPasswordCustomException(ErrorType.ERROR_PASSWORD_USED_BEFORE);
			}
		}
		objPasswordHistory.setPassword(sensitiveData.encryptHsm(newPassword));
		objPasswordHistory.setCurrentTimeDate(Calendar.getInstance().getTime().getTime());
		passwordHistoryRepository.insert(objPasswordHistory);

	}

}
