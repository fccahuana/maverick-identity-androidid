package pe.interbank.maverick.identity.register.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.dto.RegisterResponseDto;
import pe.interbank.maverick.identity.register.service.impl.RegisterServiceImpl;

@RequestMapping("/public/register")
@RefreshScope
@RestController
public class RegisterController {

	@Autowired
	RegisterServiceImpl userService;

	@PostMapping(value = "/validation/dni")
	public ResponseEntity<RegisterResponseDto> validationDni(@RequestHeader("X-MESSAGE-ID") String messageId, 
			@RequestHeader("Session-Id") String sessionId, @RequestBody RegisterRequestDto userRequestDto) {
		userRequestDto.setMessageId(messageId);
		userRequestDto.setSessionId(sessionId);		
		return  new ResponseEntity<>(userService.validationDni(userRequestDto),HttpStatus.OK);			
	}
	
	@PostMapping(value = "/phone")
	public ResponseEntity<RegisterResponseDto> savePhone(@RequestHeader("X-MESSAGE-ID") String messageId, 
			@RequestHeader("Session-Id") String sessionId, @RequestBody RegisterRequestDto registerRequestDto) {
		registerRequestDto.setMessageId(messageId);
		registerRequestDto.setSessionId(sessionId);
		userService.savePhone(registerRequestDto);
		return  new ResponseEntity<>(new RegisterResponseDto(),HttpStatus.OK);			
	}
	
	@PostMapping(value = "/user")
	public ResponseEntity<RegisterResponseDto> createUser(@RequestHeader("X-MESSAGE-ID") String messageId,@RequestHeader("Session-Id") String sessionId,  @RequestBody RegisterRequestDto userRequestDto) {
		userRequestDto.setMessageId(messageId);
		userRequestDto.setSessionId(sessionId);
		return new ResponseEntity<>(userService.createUser(userRequestDto),HttpStatus.OK);			
	}

}
