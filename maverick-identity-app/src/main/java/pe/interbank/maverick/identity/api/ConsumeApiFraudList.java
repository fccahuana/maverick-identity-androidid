package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.repository.impl.FraudListRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiFraudListCustomException;

@Component
public class ConsumeApiFraudList {

	@Autowired
	FraudListRepositoryImpl fraudListRepositoryImpl;

	public boolean consumeApiFraudList(ConsumeApiRequestDto consumeApiRequestDto, String dni) {
		return callServiceFraudList(consumeApiRequestDto, dni);

	}

	private boolean callServiceFraudList(ConsumeApiRequestDto consumeApiRequestDto, String phonNumber) {

		boolean phoneNumberIsInFraud = true;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("phoneNumber", phonNumber);
			fraudListRepositoryImpl.validatePhoneInFraudList(consumeApiRequestDto,
					new CurrentSession(), parameter);
		} catch (EsbBackendCommunicationException e) {
			if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN) {
				phoneNumberIsInFraud = false;
			} else {
				throw new ConsumeApiFraudListCustomException(ErrorType.ERROR_GENERIC, e.getEsbResponseMessage());
			}

		}

		return phoneNumberIsInFraud;
	}

}
