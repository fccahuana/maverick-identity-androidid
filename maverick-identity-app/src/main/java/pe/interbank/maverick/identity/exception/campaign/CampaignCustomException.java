package pe.interbank.maverick.identity.exception.campaign;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.identity.core.util.ErrorType;

@Getter
@Setter
public class CampaignCustomException extends BaseException{

	private static final long serialVersionUID = 1L;
	
	public CampaignCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
	}
	
	public CampaignCustomException(ErrorType errorType, String systemMessage) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), systemMessage, errorType.getHttpStatus());
	}
	
}
