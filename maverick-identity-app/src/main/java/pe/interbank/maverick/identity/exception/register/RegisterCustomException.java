package pe.interbank.maverick.identity.exception.register;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.identity.core.util.ErrorType;

@Getter
@Setter
public class RegisterCustomException extends BaseException{

	private final ErrorDto error;
	
	private static final long serialVersionUID = 1L;
	
	public RegisterCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
		error = super.getError();
	}
}
