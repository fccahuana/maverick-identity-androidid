package pe.interbank.maverick.identity.api;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestionResponse;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestions;
import pe.interbank.maverick.identity.esb.api.repository.ValidationQuestionsRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiValidationQuestionsCustomException;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;


@Component
public class ConsumeApiValidationQuestions {

	@Autowired
	ValidationQuestionsRepository validationQuestionsRepository;

	public ValidationQuestionResponse consumeServiceValidationQuestions(ConsumeApiRequestDto consumeApiRequestDto, ValidationQuestionRequestDto validationQuestionRequestDto) {
		String jsonResponseValidationQuestions = callServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
		return parserValidationQuestionsResponse(jsonResponseValidationQuestions);
	}

	private String callServiceValidationQuestions(ConsumeApiRequestDto consumeApiRequestDto, ValidationQuestionRequestDto validationQuestionRequestDto) {
		String jsonValidationQuestions = null;
		try {
			ValidationQuestions validationQuestions = new ValidationQuestions();
			validationQuestions.setModel(validationQuestionRequestDto.getModel());
			validationQuestions.setOperationId(validationQuestionRequestDto.getOperationId());
			validationQuestions.setQuestion(validationQuestionRequestDto.getQuestion());
			jsonValidationQuestions = validationQuestionsRepository.validationAnswers(consumeApiRequestDto, new CurrentSession(), validationQuestions);
		} catch (EsbBackendCommunicationException e) {
			ErrorType genericError = ErrorType.ERROR_GENERIC;
			if (e.getError().getHttpStatus() == HttpStatus.FORBIDDEN) {
			ErrorType errorValidationQuestions = ErrorType.ERROR_VALIDATION_QUESTIONS;
				throw new ConsumeApiValidationQuestionsCustomException(errorValidationQuestions.getCode(), errorValidationQuestions.getTitle(),
						errorValidationQuestions.getUserMessage(),
						e.getEsbResponseMessage() != null ? e.getEsbResponseMessage() : e.getError().getSystemMessage(),
								errorValidationQuestions.getHttpStatus());
			} else {
				throw new ConsumeApiValidationQuestionsCustomException(genericError.getCode(), genericError.getTitle(),
						genericError.getUserMessage(),
						e.getEsbResponseMessage() != null ? e.getEsbResponseMessage() : e.getError().getSystemMessage(),
						genericError.getHttpStatus());
			}
		}

		return jsonValidationQuestions;
	}

	private ValidationQuestionResponse parserValidationQuestionsResponse(String jsonValidationQuestions) {

		if (!StringUtils.isNotBlank(jsonValidationQuestions.trim())) {
			throw new NullPointerException();
		}
		ValidationQuestionResponse validationResponseDto;
		try {
			validationResponseDto = new Gson().fromJson(jsonValidationQuestions, ValidationQuestionResponse.class);
		} catch (Exception e) {
			throw new ConsumeApiValidationQuestionsCustomException(ErrorType.ERROR_GENERIC);
		}
		return validationResponseDto;
	}

}
