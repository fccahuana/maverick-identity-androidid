package pe.interbank.maverick.identity.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Configuration
public class IdentityConfiguration {

	@Value("${identity.send.message.sendMethod}")
	int sendMethod;

	@Value("${identity.send.message.channel}")
	String channel;

	@Value("${identity.send.message.operationKey}")
	String operationKey;

	@Value("${identity.send.message.operationValue}")
	String operationValue;

	@Value("${identity.send.message.destinationKey}")
	String destinationKey;

	@Value("${identity.send.message.destinationValue}")
	String destinationValue;


}
