package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfirmCarrierRequestDto  implements Serializable{

	private static final long serialVersionUID = 1L;
	@NotNull
	private String dni;
	@NotNull
	private String carrier;
	private String messageId;
	private String sessionId;
}
