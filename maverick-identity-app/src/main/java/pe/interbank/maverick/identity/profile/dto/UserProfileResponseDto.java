package pe.interbank.maverick.identity.profile.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.ErrorDto;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class UserProfileResponseDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private ErrorDto error = null;
	private ProfileDto profile;
}
