package pe.interbank.maverick.identity.validation.service;

import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationCodeRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationPhoneRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationResponseDto;

public interface IdentityValidationService {
	ValidationResponseDto getPhone(ValidationPhoneRequestDto validationPhoneRequestDto);
	ValidationResponseDto confirmCarrier(ConfirmCarrierRequestDto confirmCarrierRequestDto) ;
	ValidationResponseDto validateCode(ValidationCodeRequestDto validationCodeRequestDto, String sessionId, String messageId);
	GetQuestionResponseDto getquestions(GetQuestionRequestDto getQuestionRequestDto);
	ValidationQuestionResponseDto validationQuestions (ValidationQuestionRequestDto validationQuestionRequestDto);
}
