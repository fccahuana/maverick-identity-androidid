package pe.interbank.maverick.identity.validation.token.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;
import lombok.Getter;

@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.validation.token", redisTemplateRef = "identityValidationTokenOtpRedisDb")
public class IdentityValidationTokenOtpConfiguration {
	
	@Value("${identity.validation.redis.tokenOtpDb}")
	int tokenOtpDb;
	
	@Bean("identityValidationTokenOtpRedisDb")
	StringRedisTemplate identityValidationTokenOtpRedisDb(RedisConfiguration genericRedisConfiguration) {
		return new StringRedisTemplate(genericRedisConfiguration.jedisConnectionFactory(tokenOtpDb));
	}
}
