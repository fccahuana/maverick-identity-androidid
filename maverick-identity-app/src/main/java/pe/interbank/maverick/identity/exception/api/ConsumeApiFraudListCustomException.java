package pe.interbank.maverick.identity.exception.api;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.BaseException;
import pe.interbank.maverick.commons.core.exception.ErrorDto;
import pe.interbank.maverick.identity.core.util.ErrorType;



@Getter
@Setter
public class ConsumeApiFraudListCustomException extends BaseException{

	private final ErrorDto error;
	
	private static final long serialVersionUID = 1L;
	
	public ConsumeApiFraudListCustomException(ErrorType errorType) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), errorType.getSystemMessage(), errorType.getHttpStatus());
		error = super.getError();
	}
	
	public ConsumeApiFraudListCustomException(String code,String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		super(code, title, userMessage, systemMessage, httpStatus);
		error = super.getError();
	}
	
	public ConsumeApiFraudListCustomException(ErrorType errorType, String systemMessage) {
		super(errorType.getCode(), errorType.getTitle(), errorType.getUserMessage(), systemMessage, errorType.getHttpStatus());
		error = super.getError();
	}
	
}
