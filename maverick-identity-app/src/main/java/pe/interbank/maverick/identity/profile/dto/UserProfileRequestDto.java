package pe.interbank.maverick.identity.profile.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class UserProfileRequestDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String oldPassword;
	private String newPassword;
	private String newEmail;
	private Boolean dataProtection; 
	private Boolean enableFingerPrint;
	private String fingerPrint;
	private String fcmtoken;
	private String userId;
}
