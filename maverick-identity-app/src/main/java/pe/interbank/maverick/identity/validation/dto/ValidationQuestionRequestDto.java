package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.model.user.DeviceDetails;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.Answer;

@Getter
@Setter
public class ValidationQuestionRequestDto implements Serializable {
	private static final long serialVersionUID = -7647894307373391565L;
	private String messageId;
	private String sessionId;
	private String id;
	private String phoneNumber;
	private String dni;
	private String secret;
	private Boolean dataProtection;
	private Boolean termsConditions;
	private DeviceDetails deviceDetails;
	private String carrier;
	private String status;
	private String operationId;
	private String model;
	private List<Answer> question;
}
