package pe.interbank.maverick.identity.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyDto;
import pe.interbank.maverick.identity.core.model.generatekey.ParameterKeyDto;
import pe.interbank.maverick.identity.core.util.Util;


@Setter
@Getter
@Configuration
public class GenerateKeyDtoBase {

	@Autowired
	IdentityConfiguration identityConfiguration;
	
	public GenerateKeyDto initGenerateKeyDtoBase() {
		GenerateKeyDto generateKeyDto = new GenerateKeyDto();
		ParameterKeyDto parameterKeyOperationDto = new ParameterKeyDto();
		ParameterKeyDto parameterKeyDestinationDto = new ParameterKeyDto();
		List<ParameterKeyDto> parameters = new ArrayList<>();
		generateKeyDto.setSendMethod(identityConfiguration.getSendMethod());
		generateKeyDto.setChannel(identityConfiguration.getChannel());
		generateKeyDto.setMessageId(Util.generateSessionId());
		parameterKeyOperationDto.setKey(identityConfiguration.getOperationKey());
		parameterKeyOperationDto.setValue(identityConfiguration.getOperationValue());
		parameterKeyDestinationDto.setKey(identityConfiguration.getDestinationKey());
		parameterKeyDestinationDto.setValue(identityConfiguration.getDestinationValue().concat(":"));
		parameters.add(parameterKeyOperationDto);
		parameters.add(parameterKeyDestinationDto);
		generateKeyDto.setParameters(parameters);	
		return generateKeyDto;
	}
}
