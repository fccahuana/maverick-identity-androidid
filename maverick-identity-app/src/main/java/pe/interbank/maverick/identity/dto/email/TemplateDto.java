package pe.interbank.maverick.identity.dto.email;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TemplateDto {

	private String name;
	private Map<String, String> parameters;
}
