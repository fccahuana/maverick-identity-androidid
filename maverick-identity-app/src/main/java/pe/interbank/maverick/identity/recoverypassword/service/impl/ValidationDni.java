package pe.interbank.maverick.identity.recoverypassword.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.RegexType;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;


@Component
public class ValidationDni {

	@Autowired
	RequiredValidations requiredValidations;

	public void validateDni(RecoveryPasswordRequestDto recoveryPasswordRequestDto) {		
		String dni = recoveryPasswordRequestDto.getDni();
		requiredValidations.validationFieldNull(dni);
		requiredValidations.validationFormat(dni, RegexType.REGEX_WRONG_DNI_FORMAT.getRegex(),ErrorType.ERROR_WRONG_DNI_FORMAT_FORGOT_PASSWORD);
	}

}
