package pe.interbank.maverick.identity.validation.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.generic.Generic;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.commons.core.model.validation.ValidationAttempts;
import pe.interbank.maverick.commons.core.util.SensitiveData;
import pe.interbank.maverick.identity.api.ConsumeApiGenerateKey;
import pe.interbank.maverick.identity.api.ConsumeApiGetQuestions;
import pe.interbank.maverick.identity.api.ConsumeApiInformationPerson;
import pe.interbank.maverick.identity.api.ConsumeApiOauthTokenAssi;
import pe.interbank.maverick.identity.api.ConsumeApiValidationQuestions;
import pe.interbank.maverick.identity.api.ConsumeApiVisionCustomer;
import pe.interbank.maverick.identity.api.ConsumeApiVisionNoCustomer;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdReponse;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;
import pe.interbank.maverick.identity.config.GenerateKeyDtoBase;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getquestions.QuestionsResponseDto;
import pe.interbank.maverick.identity.esb.api.model.validationquestions.ValidationQuestionResponse;
import pe.interbank.maverick.identity.esb.api.repository.GetInformationPersonRepository;
import pe.interbank.maverick.identity.exception.validation.IdentityValidationCustomException;
import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;
import pe.interbank.maverick.identity.register.service.impl.RegisterServiceImpl;
import pe.interbank.maverick.identity.repository.generic.GenericRepository;
import pe.interbank.maverick.identity.repository.register.UserRepository;
import pe.interbank.maverick.identity.util.IdentityUtil;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.GetQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationCodeRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationPhoneRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionRequestDto;
import pe.interbank.maverick.identity.validation.dto.ValidationQuestionResponseDto;
import pe.interbank.maverick.identity.validation.dto.ValidationResponse;
import pe.interbank.maverick.identity.validation.dto.ValidationResponseDto;
import pe.interbank.maverick.identity.validation.service.IdentityValidationService;
import rx.Observable;
import rx.schedulers.Schedulers;

@Slf4j
@Service
public class IdentityValidationServiceImpl implements IdentityValidationService {

	@Autowired
	GenerateKeyDtoBase generateKeyDtoBase;

	@Autowired
	ConsumeApiInformationPerson consumeApiInformationPerson;

	@Autowired
	ConsumeApiGenerateKey consumeApiGenerateKey;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ConsumeApiOauthTokenAssi consumeApiOauthTokenAssi;
	
	@Autowired
	ConsumeApiVisionCustomer consumeApiVisionCustomer;
	
	@Autowired
	ConsumeApiVisionNoCustomer consumeApiVisionNoCustomer;

	@Autowired
	IdentityUtil identityUtil;

	@Autowired
	@Qualifier("secureIdentityValidationRedisDb")
	StringRedisTemplate secureIdentityValidationRedisDb;
	
	@Autowired
	@Qualifier("identityValidationTokenOtpRedisDb")
	StringRedisTemplate identityValidationTokenOtpRedisDb;

	@Autowired
	@Qualifier("identityValidationAttemptsRedisDb")
	StringRedisTemplate identityValidationAttemptsRedisDb;

	@Autowired
	@Qualifier("threeRedisDb")
	StringRedisTemplate threeRedisDb;

	@Autowired
	@Qualifier("thirteenRedisDb")
	StringRedisTemplate thirteenRedisDb;

	@Autowired
	SensitiveData sensitiveData;

	@Autowired
	GetInformationPersonRepository getInformationPersonRepository;

	@Autowired
	ConsumeApiGetQuestions consumeApiGetQuestions;

	@Autowired
	ConsumeApiValidationQuestions consumeApiValidationQuestions;

	@Autowired
	GenericRepository genericRepository;

	@Autowired
	RegisterServiceImpl registerService;

	@Value("${identity.validation.attempts}")
	int allowedAttempts;

	@Value("${identity.validation.bucNoClientsAttempts}")
	int allowedBucNoClientsAttempts;

	@Value("${identity.validation.expiration.redis.attempts}")
	long identityValidationAttempsExpirationTime;

	@Value("${identity.validation.expiration.redis.steps}")
	long identityValidationStepExpirationTime;

	@Value("${identity.validation.expiration.redis.questions}")
	long identityValidationQuestionsExpirationTime;

	@Value("${identity.validation.approved}")
	String approved;
	
	@Value("${identity.ldpd.aceptanceCode}")
	String acceptanceCode;
	
	@Value("${identity.ldpd.terminalType}")
	String terminalType;
	
	@Value("${identity.ldpd.terminalTypeEquifax}")
	String terminalTypeEquifax;
	
	@Value("${identity.ldpd.identityDocumentType}")
	String identityDocumentType;
	
	private static final String EQUIFAX = "EQUIFAX";
	static final String FLOW_REGISTER = "_REGISTER";
	static final String COMPLETE_CERO = "%014d";
	
	@Override
	public ValidationResponseDto getPhone(ValidationPhoneRequestDto validationPhoneRequestDto) {

		ValidationResponseDto validationResponseDto = new ValidationResponseDto();
		ValidationResponse response = new ValidationResponse();
		String dni = validationPhoneRequestDto.getDni();
		String userId = DigestUtils.sha256Hex(dni);
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(validationPhoneRequestDto.getMessageId(), userId,
				validationPhoneRequestDto.getSessionId());
		GetInformationPersonResponseDto getInformationPersonResponseDto = consumeApiInformationPerson
				.consumeServiceApiInformationPerson(consumeApiRequestDto, dni);


		Observable.just(getInformationPersonResponseDto).subscribeOn(Schedulers.newThread())
				.subscribe(x -> identityUtil.updateNames(x, userId));

		identityUtil.validateClientHaveCellPhone(getInformationPersonResponseDto,
				ErrorType.ERROR_SERVICE_CLIENT_DOES_HAVE_CELL_PHONE);

		String phoneNumber = identityUtil.getPhoneNumberOfApiPerson(getInformationPersonResponseDto);
		String email = identityUtil.getEmailApiPerson(getInformationPersonResponseDto);
		String obfuscated = Util.ofuscateFieldValidation(phoneNumber, '•', 2, 3);
		response.setPhoneNumber(obfuscated);
		generateHashStep1(userId, phoneNumber, email);
		validationResponseDto.setValidation(response);
		return validationResponseDto;
	}

	private void generateHashStep1(String userId, String phoneNumber, String email) {
		try {
			Map<String, String> stepMap = new HashMap<>();
			stepMap.put(Constants.STEP1, userId);
			stepMap.put(Constants.PHONE, phoneNumber);
			stepMap.put(Constants.EMAIL, email);
			secureIdentityValidationRedisDb.opsForHash().putAll(userId, stepMap);
		} catch (Exception e) {
			throw new IdentityValidationCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	@Override
	public ValidationResponseDto confirmCarrier(ConfirmCarrierRequestDto confirmCarrierRequestDto) {
		String dni = confirmCarrierRequestDto.getDni().trim();
		String userId = DigestUtils.sha256Hex(dni);
		String idTokenForRedis = identityUtil.generateIdToken(dni, Constants.FLOW_IDENTITY_VALIDATION);
		validatePassStep1(userId);
		String phoneNumber = getPhoneNumberOfRedis(userId);
		validateAttempts(userId);
		String carrier = confirmCarrierRequestDto.getCarrier().toUpperCase();
		User userFound = identityUtil.getUser(userId);
		userFound.setDni(dni);
		userFound.setCarrier(carrier);
		userFound.setPhoneNumber(phoneNumber);
		GenerateKeyResponseDto generateKeyResponseDto = consumeApiGenerateKey.consumeServiceGenerateKey(userFound);
		String token = generateKeyResponseDto.getToken();
		String smsBody = identityUtil.getJsonSMS(generateKeyResponseDto.getValidToken(), userFound,
				confirmCarrierRequestDto.getMessageId(), confirmCarrierRequestDto.getSessionId());
		CompletableFuture.runAsync(() -> identityUtil.sendMessage(smsBody));
		identityUtil.saveTokenInRedis(idTokenForRedis, generateKeyResponseDto);
		updateAttemptsInRedis(userId);
		generateHashStep2(dni, carrier, userId, token);
		return getConfirmCarrierResponseDto(token);
	}

	private void validatePassStep1(String userId) {
		Map<Object, Object> map = secureIdentityValidationRedisDb.opsForHash().entries(userId);
		String step1 = (String) map.get(Constants.STEP1);

		if (null == step1 || !step1.equals(userId)) {
			throw new IdentityValidationCustomException(ErrorType.INVALID_STEP_ONE_HASH);
		}
	}

	private String getPhoneNumberOfRedis(String userId) {

		Map<Object, Object> map = secureIdentityValidationRedisDb.opsForHash().entries(userId);
		String phoneNumberOfRedis = (String) map.get(Constants.PHONE);
		if (null == phoneNumberOfRedis) {
			throw new IdentityValidationCustomException(ErrorType.INVALID_PHONE_NUMBER);
		}
		return phoneNumberOfRedis;
	}

	private String getEmailFromRedis(String userId) {
		Map<Object, Object> map = secureIdentityValidationRedisDb.opsForHash().entries(userId);
		return (String) map.get(Constants.EMAIL);
	}
	
	private void updateRegisteredUser(String userId, String phoneNumber, String carrier, String email) {
		Optional<User> userFound = userRepository.findById(userId);
		if (userFound.isPresent()) {
			User user = userFound.get();
			user.setStatus('F');
			user.setCarrier(CarrierType.valueOf(carrier).getName());
			user.setPhoneNumber(sensitiveData.encryptHsm("+51".concat(phoneNumber)));
			user.setPhoneNumberId(DigestUtils.sha256Hex("+51".concat(phoneNumber)));
			user.setEmail(!StringUtils.isEmpty(email) ? sensitiveData.encryptHsm(email): null);
			userRepository.save(user);
		}
	}

	private void validateAttempts(String userId) {
		int attempts = 1;
		Map<Object, Object> attemptsRedix = identityValidationAttemptsRedisDb.opsForHash().entries(userId);
		if (!attemptsRedix.isEmpty()) {
			attempts = Integer.parseInt(String.valueOf(attemptsRedix.get(userId))) + 1;
			if (attempts > allowedAttempts)
				throw new IdentityValidationCustomException(ErrorType.ERROR_MAX_ATTEMPS_IDENTITY_VALIDATION);
		}
	}

	private void updateAttemptsInRedis(String userId) {
		int attempts = 1;
		Map<String, String> attempsIdentityValidationInRedis = new HashMap<>();
		Map<Object, Object> attemptsRedix = identityValidationAttemptsRedisDb.opsForHash().entries(userId);
		if (!attemptsRedix.isEmpty()) {
			attempts = Integer.parseInt(String.valueOf(attemptsRedix.get(userId))) + 1;
			if (allowedAttempts >= attempts) {
				attempsIdentityValidationInRedis.put(userId, String.valueOf(attempts));
				identityValidationAttemptsRedisDb.opsForHash().putAll(userId, attempsIdentityValidationInRedis);
			}
		} else {
			attempsIdentityValidationInRedis.put(userId, String.valueOf(attempts));
			identityValidationAttemptsRedisDb.opsForHash().putAll(userId, attempsIdentityValidationInRedis);
			identityValidationAttemptsRedisDb.expire(userId,
					identityValidationAttempsExpirationTime - Util.getSecondsCurrentTime(), TimeUnit.SECONDS);
		}

	}

	private void generateHashStep2(String dni, String carrier, String userId, String token) {

		if (Constants.EMPTY.equals(token.trim()))
			throw new NullPointerException();

		StringBuilder sb = new StringBuilder(dni);
		sb.append(token);

		String hashStep2Redis = DigestUtils.sha256Hex(sb.toString());
		Map<String, String> stepMap = new HashMap<>();
		stepMap.put(Constants.CARRIER, carrier.toUpperCase());
		stepMap.put(Constants.STEP2, hashStep2Redis);
		secureIdentityValidationRedisDb.opsForHash().putAll(userId, stepMap);
		secureIdentityValidationRedisDb.expire(userId, identityValidationStepExpirationTime, TimeUnit.SECONDS);
	}

	private ValidationResponseDto getConfirmCarrierResponseDto(String hashToken) {
		ValidationResponseDto validationResponseDto = new ValidationResponseDto();
		ValidationResponse validationResponse = new ValidationResponse();
		validationResponse.setHashToken(hashToken);
		validationResponseDto.setValidation(validationResponse);
		return validationResponseDto;
	}

	@Override
	public ValidationResponseDto validateCode(ValidationCodeRequestDto validationCodeRequestDto, String sessionId, String messageId) {
		String dni = validationCodeRequestDto.getDni();
		String userId = DigestUtils.sha256Hex(dni);
		String idTokenForRedis = identityUtil.generateIdToken(dni, Constants.FLOW_IDENTITY_VALIDATION);
		ValidationResponseDto validationResponseDto = new ValidationResponseDto();
		String verificationCode = validationCodeRequestDto.getVerificationCode().trim();
		
		validatePassStep1(userId);
		validatePassStep2(validationCodeRequestDto, userId);
		identityUtil.validateTokenInRedis(idTokenForRedis, verificationCode,
				ErrorType.ERROR_VALIDATION_CODE_IDENTITY_VALIDATION);
		String carrier = getCarrierFromRedis(userId);
		String phoneNumber = getPhoneNumberOfRedis(userId);
		String email = getEmailFromRedis(userId);

		updateRegisteredUser(userId, phoneNumber, carrier, email);
		CompletableFuture.runAsync(() -> registerLdpd(identityUtil.setRequestApi(messageId, userId, sessionId), phoneNumber));
		identityUtil.deleteKeyOfRedis(userId, secureIdentityValidationRedisDb);
		identityUtil.deleteKeyOfRedis(idTokenForRedis,identityValidationTokenOtpRedisDb);
		return validationResponseDto;
	}

	private void validatePassStep2(ValidationCodeRequestDto validationCodeRequestDto, String userId) {
		StringBuilder sb = new StringBuilder(validationCodeRequestDto.getDni());
		sb.append(validationCodeRequestDto.getHashToken());
		String step2 = DigestUtils.sha256Hex(sb.toString());
		Map<Object, Object> map = secureIdentityValidationRedisDb.opsForHash().entries(userId);
		String step2Redis = (String) map.get(Constants.STEP2);
		if (null == step2Redis || !step2Redis.equals(step2)) {
			throw new IdentityValidationCustomException(ErrorType.INVALID_STEP_TWO_HASH);
		}
	}
	
	private void registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, String phoneNumber) {
		Optional<User> userFound = userRepository.findById(consumeApiRequestDto.getUserId());
		if (userFound.isPresent() && !userFound.get().isDataProtection()) { 
			return;
		}
		String customerId = String.format(COMPLETE_CERO, Integer.parseInt(userFound.get().getCustomerId()));
		log.info("------------responseVisionCustomer Start REGISTER: CU " +  customerId);
		String oauthTokenAssi = consumeApiOauthTokenAssi.getTokenAssi(consumeApiRequestDto);
		consumeApiRequestDto.setToken(oauthTokenAssi);
		RegisterLdpdRequest registerLdpdRequest = new RegisterLdpdRequest();
		registerLdpdRequest.setCellPhoneNumber(phoneNumber);
		registerLdpdRequest.setAcceptanceCode(acceptanceCode);
		registerLdpdRequest.setTerminalType(terminalType);
		RegisterLdpdReponse responseVisionCustomer = consumeApiVisionCustomer.registerLdpd(consumeApiRequestDto, registerLdpdRequest, customerId);
		log.info("------------responseVisionCustomer REGISTER: CU " +  customerId + " Response " + responseVisionCustomer.getMensaje());
	}

	private String getCarrierFromRedis(String userId) {

		Map<Object, Object> map = secureIdentityValidationRedisDb.opsForHash().entries(userId);
		String carrierFromRedis = (String) map.get(Constants.CARRIER);
		if (null == carrierFromRedis) {
			throw new IdentityValidationCustomException(ErrorType.INVALID_CARRIER);
		}
		return carrierFromRedis;
	}

	@Override
	public GetQuestionResponseDto getquestions(GetQuestionRequestDto getQuestionRequestDto) {
		String dni = getQuestionRequestDto.getDni();
		identityUtil.validateBucNoClientsAllAttempts(dni);
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(getQuestionRequestDto.getMessageId(), null,
				getQuestionRequestDto.getSessionId());
		QuestionsResponseDto questionsResponseDto = consumeApiGetQuestions.consumeServiceGetQuestions(dni,
				consumeApiRequestDto);
		updateAttemptsToGeneric(dni);
		generateHashValidationQuestionStep1(dni);
		return createGetQuestionResponseDto(questionsResponseDto);
	}

	private String updateAttemptsToGeneric(String dni) {
		String identifier = DigestUtils.sha256Hex(EQUIFAX.concat(dni));
		Optional<Generic> genericOp = genericRepository.findById(identifier);
		String currentAttempts = null;
		Generic generic = null;
		if (genericOp.isPresent()) {
			generic = genericOp.get();
			int attempts = Integer.parseInt(generic.getValidationAttempts().getAttempts()) + 1;
			generic.getValidationAttempts().setAttempts(String.valueOf(attempts));
		} else {
			generic = new Generic();
			generic.setValidationAttempts(new ValidationAttempts());
			generic.setIdentifier(identifier);
			generic.getValidationAttempts().setIdentifier(DigestUtils.sha256Hex(dni));
			generic.getValidationAttempts().setAttempts("1");
			generic.getValidationAttempts().setType("equifax");
		}
		genericRepository.save(generic);
		currentAttempts = generic.getValidationAttempts().getAttempts();
		return currentAttempts;
	}

	private void generateHashValidationQuestionStep1(String dni) {
		if (Constants.EMPTY.equals(dni.trim()))
			throw new NullPointerException();
		String hashStep1Redis = DigestUtils.sha256Hex(dni);
		Map<String, String> stepMap = new HashMap<>();
		stepMap.put(Constants.STEP1, hashStep1Redis);
		threeRedisDb.opsForHash().putAll(hashStep1Redis, stepMap);
		threeRedisDb.expire(hashStep1Redis, identityValidationQuestionsExpirationTime, TimeUnit.SECONDS);
	}

	private GetQuestionResponseDto createGetQuestionResponseDto(QuestionsResponseDto questionsResponseDto) {
		GetQuestionResponseDto getQuestionResponseDto = new GetQuestionResponseDto();
		getQuestionResponseDto.setDisapproved(questionsResponseDto.getDisapproved());
		getQuestionResponseDto.setOperationId(questionsResponseDto.getOperationId());
		getQuestionResponseDto.setReject(questionsResponseDto.getReject());
		getQuestionResponseDto.setUnanswered(questionsResponseDto.getUnanswered());
		getQuestionResponseDto.setQuestions(questionsResponseDto.getQuestions());
		return getQuestionResponseDto;
	}

	@Override
	public ValidationQuestionResponseDto validationQuestions(
			ValidationQuestionRequestDto validationQuestionRequestDto) {
		String dni = validationQuestionRequestDto.getDni();
		ValidationQuestionResponseDto validationQuestionResponseDto = new ValidationQuestionResponseDto();
		ConsumeApiRequestDto consumeApiRequestDto = Util.setRequestApi(validationQuestionRequestDto.getMessageId(),
				null, validationQuestionRequestDto.getSessionId());
		validateHashValidationQuestionStep1(dni);
		identityUtil.validationIsUser(DigestUtils.sha256Hex(dni));
		identityUtil.validationUserByPhoneNumber(
				DigestUtils.sha256Hex("+51".concat(validationQuestionRequestDto.getPhoneNumber())),
				ErrorType.ERROR_EXISTING_PHONENUMBER);
		ValidationQuestionResponse validationQuestionResponse = consumeApiValidationQuestions
				.consumeServiceValidationQuestions(consumeApiRequestDto, validationQuestionRequestDto);
		if (validationQuestionResponse != null
				&& validationQuestionResponse.getResult().getDescription().equals(approved)) {
			RegisterRequestDto registerRequestDto = new RegisterRequestDto();
			BeanUtils.copyProperties(validationQuestionRequestDto, registerRequestDto);
			registerService.createBucNoClientUser(registerRequestDto);
			deleteRedisValidationQuestions(dni);
			CompletableFuture.runAsync(() -> registerLdpdNoClient(consumeApiRequestDto, dni, validationQuestionRequestDto.getPhoneNumber()));
		} else {
			identityUtil.validateBucNoClients(dni);
		}
		return validationQuestionResponseDto;
	}
	

	private void registerLdpdNoClient(ConsumeApiRequestDto consumeApiRequestDto, String dni, String phoneNumber) {
		log.info("------------responseVisionCustomer Start REGISTER: CU " +  dni);
		String userId = DigestUtils.sha256Hex(dni);
		consumeApiRequestDto.setUserId(userId);
		Optional<User> userFound = userRepository.findById(userId);
		if (userFound.isPresent() && !userFound.get().isDataProtection()) { 
			log.info("------------responseVisionCustomer Finish REGISTER: DNI " +  dni + " No acepto data protection");
			return;
		}
		String oauthTokenAssi = consumeApiOauthTokenAssi.getTokenAssi(consumeApiRequestDto);
		consumeApiRequestDto.setToken(oauthTokenAssi);
		RegisterLdpdRequest registerLdpdRequest = new RegisterLdpdRequest();
		registerLdpdRequest.setCellPhoneNumber(phoneNumber);
		registerLdpdRequest.setAcceptanceCode(acceptanceCode);
		registerLdpdRequest.setTerminalType(terminalTypeEquifax);
		registerLdpdRequest.setIdentityDocumentNumber(dni);
		registerLdpdRequest.setIdentityDocumentType(identityDocumentType);
		RegisterLdpdReponse responseVisionCustomer = consumeApiVisionNoCustomer.registerLdpd(consumeApiRequestDto, registerLdpdRequest);
		log.info("------------responseVisionCustomer REGISTER: CU " +  dni + " Response " + responseVisionCustomer.getMensaje());
		
	}

	private void validateHashValidationQuestionStep1(String dni) {
		String step1 = DigestUtils.sha256Hex(dni);
		Map<Object, Object> map = threeRedisDb.opsForHash().entries(step1);
		String step1Redis = (String) map.get(Constants.STEP1);
		if (null == step1Redis || !step1Redis.equals(step1)) {
			identityUtil.validateBucNoClients(dni);
		}
	}

	private void deleteRedisValidationQuestions(String dni) {
		String userId = DigestUtils.sha256Hex(dni);
		threeRedisDb.delete(userId);
		thirteenRedisDb.delete(userId.concat(FLOW_REGISTER));
	}

}
