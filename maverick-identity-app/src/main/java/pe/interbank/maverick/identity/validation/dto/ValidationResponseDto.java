package pe.interbank.maverick.identity.validation.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.exception.ErrorDto;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class ValidationResponseDto implements Serializable{
	
	private static final long serialVersionUID = -8720049738224363193L;
	private ValidationResponse validation;
	private ErrorDto error = null;
}
