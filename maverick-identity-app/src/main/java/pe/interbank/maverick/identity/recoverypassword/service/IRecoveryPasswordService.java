package pe.interbank.maverick.identity.recoverypassword.service;

import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.RecoveryPasswordResponseDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidateTokenSMSRequestDto;
import pe.interbank.maverick.identity.validation.dto.ConfirmCarrierRequestDto;

public interface IRecoveryPasswordService {
	
	RecoveryPasswordResponseDto validateScreenDni(RecoveryPasswordRequestDto userRequestDto);
	RecoveryPasswordResponseDto confirmScreenCarrier(ConfirmCarrierRequestDto confirmCarrierRequestDto);
	void validateScreenValidationCode(ValidateTokenSMSRequestDto validateTokenSMSRequestDto);
	void validateScreenNewPassword(ValidatePasswordRequestDto validatePasswordRequestDto);
}
