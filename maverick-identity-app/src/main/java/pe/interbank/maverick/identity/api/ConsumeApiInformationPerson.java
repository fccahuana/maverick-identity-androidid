package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.GetInformationPersonResponseDto;
import pe.interbank.maverick.identity.esb.api.model.getinformationperson.PhoneResponse;
import pe.interbank.maverick.identity.esb.api.repository.GetInformationPersonRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiInformationPersonCustomException;

@Component
public class ConsumeApiInformationPerson {

	@Autowired
	GetInformationPersonRepository getInformationPersonRepository;

	public GetInformationPersonResponseDto consumeServiceApiInformationPerson(ConsumeApiRequestDto consumeApiRequestDto,
			String dni) {
		String jsonInformationPerson = callServiceGetInformationPerson(consumeApiRequestDto, dni);
		return parserInformationPerson(jsonInformationPerson);
	}

	private String callServiceGetInformationPerson(ConsumeApiRequestDto consumeApiRequestDto, String dni) {

		String jsonCustomerInformation = null;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("documentId", dni);
			jsonCustomerInformation = getInformationPersonRepository.getInformationPerson(consumeApiRequestDto,
					new CurrentSession(), parameter);
		} catch (EsbBackendCommunicationException e) {
			throw new ConsumeApiInformationPersonCustomException(e);
		}

		return jsonCustomerInformation;
	}

	private GetInformationPersonResponseDto parserInformationPerson(String jsonInformationPerson) {

		if (!StringUtils.isNotBlank(jsonInformationPerson.trim())) {
			throw new NullPointerException();
		}
		GetInformationPersonResponseDto getInformationPersonResponseDto;
		try {
			getInformationPersonResponseDto = new Gson().fromJson(jsonInformationPerson,
					GetInformationPersonResponseDto.class);
		} catch (Exception e) {
			throw new ConsumeApiInformationPersonCustomException(ErrorType.ERROR_GENERIC);
		}
		return getInformationPersonResponseDto;
	}

	public String getPhoneNumber(GetInformationPersonResponseDto informationPersonResponseDto) {
		String phoneNumberOfService = "";
		List<PhoneResponse> phones = informationPersonResponseDto.getTelephones();
		for (PhoneResponse phone : phones) {
			if ("C".equals(phone.getType())) {
				phoneNumberOfService = phone.getNumber();
				break;
			}
		}
		return phoneNumberOfService;
	}

}
