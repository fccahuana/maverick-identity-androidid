package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import lombok.extern.slf4j.Slf4j;
import pe.interbank.maverick.commons.api.assi.exception.ApiAssiException;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdReponse;
import pe.interbank.maverick.identity.api.assi.repository.impl.VisionCustomerRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiVisionCustomerException;
import pe.interbank.maverick.identity.core.util.ErrorType;

@Slf4j
@Component
public class ConsumeApiVisionCustomer {

	@Autowired
	private VisionCustomerRepositoryImpl visionCustomerRepositoryImpl; 
	
	public RegisterLdpdReponse registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest, String customerId) {
		String response = callRegisterLdpd(consumeApiRequestDto, registerLdpdRequest, customerId);
		return parseResponseApiVisionCliente(response);
	}
	
	private String callRegisterLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest, String customerId) {
		String jsonRegisterLdpd = null;
		Map<String, String> parameter = new HashMap<>();
		parameter.put("customerId", customerId);
		try {
			jsonRegisterLdpd = visionCustomerRepositoryImpl.registerLdpd(consumeApiRequestDto, registerLdpdRequest, parameter);
		} catch (ApiAssiException e) {
			log.error("Error in callRegisterLdpd: {}", e.getError().getSystemMessage());
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC,e.getError().getSystemMessage());
		}
		return jsonRegisterLdpd;
	}
	
	private RegisterLdpdReponse parseResponseApiVisionCliente(String jsonRegisterLdpd) {
		if (!StringUtils.isNotBlank(jsonRegisterLdpd.trim())) {
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC, "Response Blank");
		}
		RegisterLdpdReponse registerLdpdResponse = null;
		try {
			registerLdpdResponse = new Gson().fromJson(jsonRegisterLdpd, RegisterLdpdReponse.class);
		} catch (Exception e) {
			log.error("Error in parseResponseApiAccountCreation: {}", e.getMessage());
			throw new ConsumeApiVisionCustomerException(ErrorType.ERROR_GENERIC,e.getMessage());
		}
		return registerLdpdResponse;
	}
}
