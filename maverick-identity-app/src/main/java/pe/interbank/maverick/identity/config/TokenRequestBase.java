package pe.interbank.maverick.identity.config;

import org.springframework.http.HttpHeaders;
import java.nio.charset.StandardCharsets;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.oauth.dto.TokenRequestDto;

@Getter
@Setter
@Configuration
public class TokenRequestBase {

	@Value("${identity.instance.uri}")
	String tokenUri;

	@Value("${identity.oauthClientDetails.clientId}")
	String clientId;
	
	@Value("${identity.oauthClientDetails.clientSecret}")
	String clientSecret;

	public TokenRequestDto initTokenRequestDto(String grantType, User user) {
		TokenRequestDto tokenRequestDto = new TokenRequestDto();
		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("client_id", clientId);
		body.add(Constants.GRANT_TYPE, grantType);
		body.add("customerId", user.getCustomerId());
		//body.add("family", user.getFamilyName());
		tokenRequestDto.setBody(body);
		String plainCreds = clientId + ":" + clientSecret;
		byte[] plainCredsBytes = plainCreds.getBytes(StandardCharsets.UTF_8);
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes, StandardCharsets.UTF_8);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Basic " + base64Creds);
		headers.add("Accept", "application/x-www-form-urlencoded, text/plain, */*");
		headers.add("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
		tokenRequestDto.setHttpHeaders(headers);
		String uriAuth = tokenUri + "/oauth/token";
		tokenRequestDto.setUriAuth(uriAuth);
		return tokenRequestDto;
	}
}
