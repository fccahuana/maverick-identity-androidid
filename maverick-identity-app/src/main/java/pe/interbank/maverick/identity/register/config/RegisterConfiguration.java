package pe.interbank.maverick.identity.register.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;

@RefreshScope
@Getter
@Setter
@Configuration
public class RegisterConfiguration {

	@Value("${identity.register.redis.db}")
	int thirteenDb;
	
	@Value("${identity.consume.endpoint.sendEmail}")
	String endpointSendEmail;

	@Value("${identity.notificationMail.message.from}")
	String mailFrom;

	@Value("${identity.notificationMail.message.templateName}")
	String mailTemplate;

	@Value("${identity.notificationMail.message.SRV}")
	String sftpSRV;
	
	@Value("${identity.notificationMail.message.destinationUsersBlocks}")
	String destinationUsersBlocks;
	
	@Value("${identity.notificationMail.message.subjectBlockUser}")
	String subjectBlockUser;
	
	@Value("${identity.register.redis.expiration}")
	long expirationRedisRegister;
	
	@Value("${main.rsa.publickey}")
	String publickey;
	

}
