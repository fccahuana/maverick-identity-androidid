package pe.interbank.maverick.identity.oauth.service.impl;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@Service
public class UserDetailsServiceImpl  implements UserDetailsService{

	@Autowired
	UserRepository userRepository;
	
	private static final String KEY_ROLE_USER="USER";
	
	 @Autowired
	 private BCryptPasswordEncoder passwordEncoder;
	 
	  @Override
	    public UserDetails loadUserByUsername(String username){
		  pe.interbank.maverick.commons.core.model.user.User  user = null;
	    		Optional<pe.interbank.maverick.commons.core.model.user.User> userById = userRepository.findById(username);
	    		if (!userById.isPresent()) {
	    			throw new UsernameNotFoundException(String.format("User %s does not exist!", username));
	    		}else {
	    			user = userById.get();
	    		}	    		   		
	        return toUserDetails(user);
	    }

	    private UserDetails toUserDetails(pe.interbank.maverick.commons.core.model.user.User userObject) {

	        return User.withUsername(userObject.getUserId())
	                   .password(passwordEncoder.encode(userObject.getPassword()))
	                   .roles(KEY_ROLE_USER).build();
	    }

	
}