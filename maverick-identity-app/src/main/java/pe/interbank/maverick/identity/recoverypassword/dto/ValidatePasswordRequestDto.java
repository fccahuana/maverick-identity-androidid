package pe.interbank.maverick.identity.recoverypassword.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidatePasswordRequestDto implements Serializable{

	private static final long serialVersionUID = 1L;
	private String secret;
	private String dni;
    private String verificationCode;
    private String id;
}
