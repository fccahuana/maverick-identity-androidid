package pe.interbank.maverick.identity.validation.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;
import lombok.Getter;

@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.validation", redisTemplateRef = "identityValidationAttemptsRedisDb")
public class IdentityValidationConfiguration {
	
	@Value("${identity.validation.redis.db}")
	int validationDb;
	
	@Bean("identityValidationAttemptsRedisDb")
	StringRedisTemplate identityValidationAttemptsRedisDb(RedisConfiguration genericRedisConfiguration) {
		return new StringRedisTemplate(genericRedisConfiguration.jedisConnectionFactory(validationDb));
	}
}
