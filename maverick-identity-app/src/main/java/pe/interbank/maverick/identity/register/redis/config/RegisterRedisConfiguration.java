package pe.interbank.maverick.identity.register.redis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import lombok.Getter;
import pe.interbank.maverick.commons.core.config.RedisConfiguration;
import pe.interbank.maverick.identity.register.config.RegisterConfiguration;

@Getter
@Configuration
@EnableRedisRepositories(basePackages = "pe.interbank.maverick.identity.register.redis", redisTemplateRef = "thirteenRedisDb")
public class RegisterRedisConfiguration {
	
	@Bean("thirteenRedisDb")
	StringRedisTemplate thirteenRedisDb(RedisConfiguration genericRedisConfiguration, RegisterConfiguration registerConfiguration) {
		return new StringRedisTemplate(genericRedisConfiguration.jedisConnectionFactory(registerConfiguration.getThirteenDb()));
	}
}
