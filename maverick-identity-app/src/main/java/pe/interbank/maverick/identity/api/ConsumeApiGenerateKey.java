package pe.interbank.maverick.identity.api;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import pe.interbank.maverick.identity.config.GenerateKeyDtoBase;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyDto;
import pe.interbank.maverick.identity.core.model.generatekey.GenerateKeyResponseDto;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.util.CarrierType;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.exception.api.ConsumeApiGenerateKeyCustomException;


@Component
public class ConsumeApiGenerateKey {

	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("${identity.generateKey.endpoint}")
	String uri;
	
	@Autowired
	GenerateKeyDtoBase generateKeyDtoBase;
	
	public GenerateKeyResponseDto consumeServiceGenerateKey(User userFound) {
		GenerateKeyDto generateKeyDto = loadGenerateKeyDtoByJson(userFound);
		String jsonResponseGenerateKey = callServiceGenerateKey(generateKeyDto);
		return parserResponseGenerateKey(jsonResponseGenerateKey);
	}
	
	private GenerateKeyDto loadGenerateKeyDtoByJson(User userFoundDecrypted) {
		String dni = userFoundDecrypted.getDni();
		GenerateKeyDto generateKeyDto = generateKeyDtoBase.initGenerateKeyDtoBase();
		generateKeyDto.setUserId(dni);
		generateKeyDto.setId(dni);
		generateKeyDto.setPhoneNumber(userFoundDecrypted.getPhoneNumber());
		generateKeyDto.setCarrier(CarrierType.valueOf(userFoundDecrypted.getCarrier()).getId());
		return generateKeyDto;
	}
	
	private String callServiceGenerateKey(GenerateKeyDto generateKeyDto) {
		String response= null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Type", "application/json");
			ObjectMapper mapper = new ObjectMapper();
			String jsonBody = mapper.writeValueAsString(generateKeyDto);
			HttpEntity<?> httpEntity = new HttpEntity<>(jsonBody, headers);
			restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
			ResponseEntity<String> responseEntity = restTemplate.exchange(uri,HttpMethod.POST, httpEntity, String.class);
			response = responseEntity.getBody();
			} catch (RuntimeException | JsonProcessingException e) {
				throw new ConsumeApiGenerateKeyCustomException(ErrorType.ERROR_GENERIC);
			} 
		return response;
	}
	
	private GenerateKeyResponseDto parserResponseGenerateKey(String token) {		
		if(!StringUtils.isNotBlank(token.trim())) {
			throw new NullPointerException();
		}
		GenerateKeyResponseDto generateKeyResponseDto = null;
		try {
			generateKeyResponseDto = new Gson().fromJson(token, GenerateKeyResponseDto.class);
		}catch(Exception e) {
			throw new ConsumeApiGenerateKeyCustomException(ErrorType.ERROR_GENERIC);
		}
		return generateKeyResponseDto;
	}
	
	

	
}
