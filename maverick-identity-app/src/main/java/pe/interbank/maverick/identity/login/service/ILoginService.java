package pe.interbank.maverick.identity.login.service;

import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.login.dto.LoginResponseDto;

public interface ILoginService {
	
	LoginResponseDto loginAndAccesToken(LoginRequestDto loginRequestDto, String fcmToken);
	
	LoginResponseDto logout(String token);
	
}