package pe.interbank.maverick.identity.register.dto;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.core.model.user.DeviceDetails;

@Getter
@Setter
public class RegisterRequestDto {

	private String id;
	private String phoneNumber;
	private String dni;
	private String secret;
	private Boolean dataProtection;
	private Boolean termsConditions;
	private DeviceDetails deviceDetails;
	private String carrier;
	private String status;
	private String messageId;
	private String sessionId;
	private String isCustomer;
	private String email;
	private String attachment;
	private String subjectEmail;
	private String customerName;
}
