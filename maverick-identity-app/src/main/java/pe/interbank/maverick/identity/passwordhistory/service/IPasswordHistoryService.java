package pe.interbank.maverick.identity.passwordhistory.service;

import pe.interbank.maverick.identity.register.dto.RegisterRequestDto;

public interface IPasswordHistoryService {

	void loadPasswordHistory(RegisterRequestDto userRequestDto);
	void savePasswordHistoryByRecoveryPassword(String newPassword, String userId);
}
