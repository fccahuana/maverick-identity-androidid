package pe.interbank.maverick.identity.oauth.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface IUserDetailsService {
	UserDetails loadUserByUsername(String username);
}