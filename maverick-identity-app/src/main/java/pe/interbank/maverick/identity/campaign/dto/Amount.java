package pe.interbank.maverick.identity.campaign.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@JsonInclude(Include.NON_EMPTY)
public class Amount implements Serializable{

	private static final long serialVersionUID = 1L;
    private String value;

}
