package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.leads.LeadsResponseDto;
import pe.interbank.maverick.identity.esb.api.repository.impl.CustomerLeadsRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiCustomerLeadCustomException;


@Component
public class ConsumeApiCustomerLeads {

	@Autowired
	CustomerLeadsRepositoryImpl customerLeadsRepositoryImpl;

	public LeadsResponseDto consumeApiCustomerLeads(String dni, ConsumeApiRequestDto consumeApiRequestDto) {
		String jsonResponseApiLeads = callApiLeads(dni, consumeApiRequestDto);
		return parserLeadsResponseDto(jsonResponseApiLeads);
	}

	private String callApiLeads(String dni, ConsumeApiRequestDto consumeApiRequestDto) {

		String jsonApiCustomerLeads = null;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("documentId", dni);
			jsonApiCustomerLeads = customerLeadsRepositoryImpl.getLeads(consumeApiRequestDto, new CurrentSession(),
					parameter);
		} catch (EsbBackendCommunicationException e) {
			throw new ConsumeApiCustomerLeadCustomException(e);
		} catch (Exception e) {
			throw new ConsumeApiCustomerLeadCustomException(ErrorType.ERROR_SERVICE_CAMPAIGN,
					ErrorType.ERROR_SERVICE_CAMPAIGN.getSystemMessage());
		}

		return jsonApiCustomerLeads;
	}

	private LeadsResponseDto parserLeadsResponseDto(String jsonResponseApiLeads) {

		if (!StringUtils.isNotBlank(jsonResponseApiLeads.trim())) {
			throw new NullPointerException();
		}
		LeadsResponseDto leadsResponseDto;
		try {
			leadsResponseDto = new Gson().fromJson(jsonResponseApiLeads, LeadsResponseDto.class);
		} catch (Exception e) {
			throw new ConsumeApiCustomerLeadCustomException(ErrorType.ERROR_SERVICE_CAMPAIGN);
		}
		return leadsResponseDto;
	}

}
