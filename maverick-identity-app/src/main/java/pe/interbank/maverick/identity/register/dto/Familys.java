package pe.interbank.maverick.identity.register.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Familys {

	private int companyId;
	private String family;
	private String place;
	private String agreementName;
	private String otherFamily;
}
