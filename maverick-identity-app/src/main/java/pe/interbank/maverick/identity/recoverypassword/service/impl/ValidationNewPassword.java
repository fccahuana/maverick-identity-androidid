package pe.interbank.maverick.identity.recoverypassword.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.RegexType;
import pe.interbank.maverick.identity.recoverypassword.dto.ValidatePasswordRequestDto;

@Service
public class ValidationNewPassword {

	@Autowired
	RequiredValidations requiredValidations;


	public void validationNewPassword(ValidatePasswordRequestDto validatePasswordRequestDto) {
		String passwordDecrypted = validatePasswordRequestDto.getSecret();
		requiredValidations.validationFieldNull(passwordDecrypted);
		validationFormatPassword(passwordDecrypted);
	}

	private void validationFormatPassword(String passwordDecrypted) {
		requiredValidations.validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_LENGTH.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_LENGTH);
		requiredValidations.validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_HAS_NUMBER.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_NUMBER);
		requiredValidations.validationFormat(passwordDecrypted, RegexType.REGEX_WRONG_PASSWORD_HAS_LETTER.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_LETTER);
		requiredValidations.validationFormat(passwordDecrypted,
				RegexType.REGEX_WRONG_PASSWORD_HAS_SPECIAL_CHARACTERS.getRegex(),
				ErrorType.ERROR_WRONG_PASSWORD_HAS_SPECIAL_CHARACTERS);
	}

}
