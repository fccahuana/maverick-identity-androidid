package pe.interbank.maverick.identity.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.CreditsResponse;
import pe.interbank.maverick.identity.esb.api.model.loansagreement.LoansAgreementResponseDto;
import pe.interbank.maverick.identity.esb.api.repository.LoansAgreementRepository;
import pe.interbank.maverick.identity.exception.api.ConsumeApiLoansAgreementCustomException;

@Component
public class ConsumeApiLoansAgreement {

	@Autowired
	LoansAgreementRepository loansAgreementRepository;

	public LoansAgreementResponseDto consumeServiceLoansAgreement(String dni,
			ConsumeApiRequestDto consumeApiRequestDto) {
		String jsonResponseLoansAgreement = callServiceLoansAgreement(dni, consumeApiRequestDto);
		return getTheHighestActiveCredit(parserLoansAgreement(jsonResponseLoansAgreement));
	}

	private String callServiceLoansAgreement(String dni, ConsumeApiRequestDto consumeApiRequestDto) {

		String jsonCustomerInformation = null;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("documentId", dni);
			jsonCustomerInformation = loansAgreementRepository.getLoansAgreement(consumeApiRequestDto,
					new CurrentSession(), parameter);
		} catch (EsbBackendCommunicationException e) {
			throw new ConsumeApiLoansAgreementCustomException(e);
		}

		return jsonCustomerInformation;
	}

	private LoansAgreementResponseDto parserLoansAgreement(String jsonResponseLoansAgreement) {

		if (!StringUtils.isNotBlank(jsonResponseLoansAgreement.trim())) {
			throw new NullPointerException();
		}
		LoansAgreementResponseDto loansAgreementResponseDto;
		try {
			loansAgreementResponseDto = new Gson().fromJson(jsonResponseLoansAgreement,
					LoansAgreementResponseDto.class);
		} catch (Exception e) {
			throw new ConsumeApiLoansAgreementCustomException(ErrorType.ERROR_GENERIC);
		}
		return loansAgreementResponseDto;
	}

	private LoansAgreementResponseDto getTheHighestActiveCredit(LoansAgreementResponseDto loansAgreementResponseDto) {
		List<CreditsResponse> credits = loansAgreementResponseDto.getCredits();
		List<CreditsResponse> maximumCredit = new ArrayList<>();
		int positionOfTheHighestCredit = -1;
		if(loansAgreementResponseDto.getCredits()!=null) {
			double theHighestCredit = 0;
			int totalCredits = credits.size();
			int currentCreditNumber = 0;
			Integer creditNumber = 0;
			for (int index = 0; index < totalCredits; index++) {
				CreditsResponse credit = credits.get(index);
				Double amount = Double.parseDouble(credit.getDisbursement().getAmount());
				String nextDueDate = credit.getInstallment().getNextDueDate();
				creditNumber = Integer.valueOf(credit.getId());
				if (!Constants.EMPTY.equals(nextDueDate) && !Constants.CREDIT_CANCELED.equals(nextDueDate)
						&& !Constants.CREDIT_DISBURSED.equals(nextDueDate)) {
					if (amount > theHighestCredit) {
						positionOfTheHighestCredit = index;
						theHighestCredit = amount;
					} else if (amount == theHighestCredit && creditNumber > currentCreditNumber) {
						positionOfTheHighestCredit = index;
						currentCreditNumber = creditNumber;
					}
				}

			}
		}
		

		if (positionOfTheHighestCredit > -1) {
			maximumCredit.add(credits.get(positionOfTheHighestCredit));
		}

		loansAgreementResponseDto.setCredits(maximumCredit);
		return loansAgreementResponseDto;
	}
}
