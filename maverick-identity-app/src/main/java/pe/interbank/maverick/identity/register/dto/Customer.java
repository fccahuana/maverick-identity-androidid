package pe.interbank.maverick.identity.register.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class Customer implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String id;
	private String firstName;
	private String secondName;
	private String lastName;
	private String secondLastName;
	private String placeName;
	private String familyName;
	private String institutionName;	
	private int companyId;
}
