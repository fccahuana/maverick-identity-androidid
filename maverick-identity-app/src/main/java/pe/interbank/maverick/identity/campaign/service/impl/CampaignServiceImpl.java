package pe.interbank.maverick.identity.campaign.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.ConsumeApiCustomerLeads;
import pe.interbank.maverick.identity.api.ConsumeApiLead;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponse;
import pe.interbank.maverick.identity.campaign.dto.CampaignResponseDto;
import pe.interbank.maverick.identity.campaign.service.ICampaignService;
import pe.interbank.maverick.identity.esb.api.model.lead.Additional;
import pe.interbank.maverick.identity.esb.api.model.lead.DetailLead;
import pe.interbank.maverick.identity.esb.api.model.lead.DetailLeadsResponseDto;
import pe.interbank.maverick.identity.esb.api.model.leads.LeadsResponseDto;
import pe.interbank.maverick.identity.repository.register.UserRepository;


@Service
public class CampaignServiceImpl implements ICampaignService {

	@Autowired
	ConsumeApiCustomerLeads consumeApiCustomerLeads;

	@Autowired
	ConsumeApiLead consumeApiLead;

	@Autowired
	UserRepository userRepository;

	@Value("${identity.campaign.buc}")
	String campaignBuc;

	@Override
	public CampaignResponseDto getBucLead(String dni, ConsumeApiRequestDto consumeApiRequestDto) {
		CampaignResponseDto campaignResponseDto = new CampaignResponseDto();
		LeadsResponseDto leadsResponseDto = consumeApiCustomerLeads.consumeApiCustomerLeads(dni, consumeApiRequestDto);
		DetailLead detailLead = getDetailLead(leadsResponseDto, consumeApiRequestDto);
		List<CampaignResponse> campaignsResponse = setParseDetailLeadsResponseDtoToCampaignResponseDto(detailLead);
		campaignResponseDto.setLeads(campaignsResponse);
		return campaignResponseDto;
	}
	
	private DetailLead getDetailLead(LeadsResponseDto leadsResponseDto, ConsumeApiRequestDto consumeApiRequestDto) {
		String leadId = null;
		Boolean leadIdfound = false;
		DetailLead detailLead = null;
		for (int i = 0; i < leadsResponseDto.getLeads().size(); i++) {
			leadId = leadsResponseDto.getLeads().get(i).getId();
			DetailLeadsResponseDto detailLeadsResponseDto = consumeApiLead.consumeApiLeads(leadId, consumeApiRequestDto);
			List<DetailLead> detailCampaigns = detailLeadsResponseDto.getLeads();
			for (int j = 0; j < detailCampaigns.size(); j++) {
				detailLead = detailCampaigns.get(j);
				Additional additional = detailLead.getAdditionals().get(0);
				if (null != additional && null != additional.getItem15()
						&& additional.getItem15().equals(campaignBuc)) {
					leadIdfound = true;
					break;
				}
			}
			if (Boolean.TRUE.equals(leadIdfound)) {
				break;
			} else {
				detailLead = null;
			}
		}
		return detailLead;
	}

	private List<CampaignResponse> setParseDetailLeadsResponseDtoToCampaignResponseDto(DetailLead detailLead) {
		List<CampaignResponse> campaignsResponse = new ArrayList<>();
		if (detailLead != null && detailLead.getAdditionals() != null &&
				detailLead.getAdditionals().get(0).getItem15().equals(campaignBuc)) {
			CampaignResponse campaignResponse = new CampaignResponse();
			campaignResponse.setCampaignType(campaignBuc);
			campaignsResponse.add(campaignResponse);
		}
		return campaignsResponse;
	}

}
