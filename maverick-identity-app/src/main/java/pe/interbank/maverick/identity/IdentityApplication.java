package pe.interbank.maverick.identity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

/*
 * @author Abilio Caceres <acacerec@everis.com>
 * 
 */
@EnableAsync(proxyTargetClass = true)
@EnableAutoConfiguration
@EnableDiscoveryClient  
@SpringBootApplication
@ComponentScan({"pe.interbank.maverick"})
public class IdentityApplication   {
	
	public static void main(String[] args) {
		SpringApplication.run(IdentityApplication.class, args);
    }
		@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}	

}
