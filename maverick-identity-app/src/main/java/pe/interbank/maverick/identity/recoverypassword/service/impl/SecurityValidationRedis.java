package pe.interbank.maverick.identity.recoverypassword.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.core.util.Util;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;

@Component
public class SecurityValidationRedis {

	@Autowired
	@Qualifier("stringRedisTemplateForgot")
	StringRedisTemplate redisTemplateAttempts;

	@Autowired
	@Qualifier("stringRedisTemplateStep")
	StringRedisTemplate redisStepTemplate;

	@Value("${identity.forgot.attempts}")
	int allowedAttempts;

	@Value("${identity.forgot.expiration.redis.attempts}")
	long forgotAttempsExpirationTime;

	@Value("${identity.forgot.expiration.redis.steps}")
	long forgotStepExpirationTime;

	private static final String STEP1 = "STEP1";
	private static final String STEP3 = "STEP3";

	public void validateAttempsRecoveryPassword(String userId) {
		int attemps = 1;
		Map<Object, Object> attempsRedix = redisTemplateAttempts.opsForHash().entries(userId);
		if (!attempsRedix.isEmpty()) {
			attemps = Integer.parseInt(String.valueOf(attempsRedix.get(userId))) + 1;
			if (attemps > allowedAttempts)
				throw new RecoveryPasswordCustomException(ErrorType.ERROR_MAX_ATTEMPS_FORGOT_PASSWORD);
		}
	}

	public void updateAttemptsInRedis(String userId) {
		int attempts = 1;
		Map<String, String> attempsIdentityValidationInRedis = new HashMap<>();
		Map<Object, Object> attemptsRedix = redisTemplateAttempts.opsForHash().entries(userId);
		if (!attemptsRedix.isEmpty()) {
			attempts = Integer.parseInt(String.valueOf(attemptsRedix.get(userId))) + 1;
			if (allowedAttempts >= attempts) {
				attempsIdentityValidationInRedis.put(userId, String.valueOf(attempts));
				redisTemplateAttempts.opsForHash().putAll(userId, attempsIdentityValidationInRedis);
			}
		} else {
			attempsIdentityValidationInRedis.put(userId, String.valueOf(attempts));
			redisTemplateAttempts.opsForHash().putAll(userId, attempsIdentityValidationInRedis);
			redisTemplateAttempts.expire(userId, forgotAttempsExpirationTime - Util.getSecondsCurrentTime(), TimeUnit.SECONDS);
		}

	}

	public void generateHashStep1(String userId, String phonNumber) {
		try {
			Map<String, String> stepMap = new HashMap<>();
			stepMap.put(STEP1, userId);
			stepMap.put(Constants.PHONE, phonNumber);
			redisStepTemplate.opsForHash().putAll(userId, stepMap);
		} catch (Exception e) {
			throw new RecoveryPasswordCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	public void validateHashStep1(String dni) {
		String userId = DigestUtils.sha256Hex(dni);
		Map<Object, Object> map = redisStepTemplate.opsForHash().entries(userId);
		String step1 = (String) map.get(STEP1);
		if (null == step1 || !step1.equals(userId)) {
			throw new RecoveryPasswordCustomException(ErrorType.INVALID_STEP_ONE_HASH);
		}
	}
	
	public void generateHashStep2(String dni, String carrier) {
		try {
		String userId = DigestUtils.sha256Hex(dni);
		Map<String, String> stepMap = new HashMap<>();
		stepMap.put(Constants.CARRIER, carrier.toUpperCase());
		redisStepTemplate.opsForHash().putAll(userId, stepMap);
		} catch (Exception e) {
			throw new RecoveryPasswordCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	public void generateHashStep3(String dni, String verificationCode) {
		try {
		String userId = DigestUtils.sha256Hex(dni);
		StringBuilder sb = new StringBuilder(dni);
		sb.append(verificationCode);
		String stept3 = DigestUtils.sha256Hex(sb.toString());
		Map<String, String> stepMap = new HashMap<>();
		stepMap.put(STEP3, stept3);
		redisStepTemplate.opsForHash().putAll(userId, stepMap);
		redisStepTemplate.expire(userId, forgotStepExpirationTime, TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new RecoveryPasswordCustomException(ErrorType.ERROR_GENERIC);
		}
	}

	public void validateHashStep3(String dni, String verificationCode) {
		String userId = DigestUtils.sha256Hex(dni);
		StringBuilder sb = new StringBuilder(dni);
		sb.append(verificationCode);
		String stept3 = DigestUtils.sha256Hex(sb.toString());
		Map<Object, Object> map = redisStepTemplate.opsForHash().entries(userId);
		String stept3Redis = (String) map.get(STEP3);
		if (null == stept3Redis || !stept3Redis.equals(stept3)) {
			throw new RecoveryPasswordCustomException(ErrorType.INVALID_STEP_THREE_HASH);
		}
	}
	
	public void deleteStepsOfRedis(String userId) {
		redisStepTemplate.delete(userId);
	}
	
	public String getPhoneNumberOfRedis(String userId) {

		Map<Object, Object> map = redisStepTemplate.opsForHash().entries(userId);
		String phoneNumberOfRedis = (String) map.get(Constants.PHONE);
		if (null == phoneNumberOfRedis) {
			throw new RecoveryPasswordCustomException(ErrorType.INVALID_PHONE_NUMBER);
		}
		return phoneNumberOfRedis;
	}

}
