package pe.interbank.maverick.identity.oauth.security;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import pe.interbank.maverick.commons.core.util.UtilGenericToken;
import pe.interbank.maverick.identity.core.util.Constants;

/**
 * 
 * @author Abilio Caceres <acacerec@everis.com>
 * Customizar token y organizacion para el que se genera.
 *
 */
public class CustomTokenEnhancer implements TokenEnhancer {

	@Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("organization", authentication.getName() + randomAlphabetic(4));
        @SuppressWarnings("unchecked")
		Map<String, String> parameter =(Map<String,String>) authentication.getUserAuthentication().getDetails();
        if(null!=parameter) {
        	additionalInfo.put(Constants.CUSTOMER_ID, parameter.get(Constants.CUSTOMER_ID));
        	additionalInfo.put(Constants.FAMILY, parameter.get(Constants.FAMILY));
        }else {
        	additionalInfo.put(Constants.CUSTOMER_ID, UtilGenericToken.getClaim(accessToken.getRefreshToken().getValue(), Constants.CUSTOMER_ID));
        	additionalInfo.put(Constants.FAMILY, UtilGenericToken.getClaim(accessToken.getRefreshToken().getValue(), Constants.FAMILY));
        }
        
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
