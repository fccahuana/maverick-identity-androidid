package pe.interbank.maverick.identity.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.commons.core.model.CurrentSession;
import pe.interbank.maverick.commons.esb.api.exception.EsbBackendCommunicationException;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.esb.api.model.lead.DetailLeadsResponseDto;
import pe.interbank.maverick.identity.esb.api.repository.impl.LeadRepositoryImpl;
import pe.interbank.maverick.identity.exception.api.ConsumeApiDetailLeadCustomException;


@Component
public class ConsumeApiLead {

	@Autowired
	LeadRepositoryImpl leadRepositoryImpl;

	public DetailLeadsResponseDto consumeApiLeads(String leadId, ConsumeApiRequestDto consumeApiRequestDto) {
		String jsonResponseApiLead = callApiLead(leadId, consumeApiRequestDto);
		return parseLeadResponseDto(jsonResponseApiLead);
	}

	private String callApiLead(String leadId, ConsumeApiRequestDto consumeApiRequestDto) {

		String jsonApiLeads = null;

		try {
			Map<String, String> parameter = new HashMap<>();
			parameter.put("leadId", leadId);
			jsonApiLeads = leadRepositoryImpl.getDetailLead(consumeApiRequestDto, new CurrentSession(), parameter);
		} catch (EsbBackendCommunicationException e) {
			throw new ConsumeApiDetailLeadCustomException(e);
		} catch (Exception e) {
			throw new ConsumeApiDetailLeadCustomException(ErrorType.ERROR_SERVICE_CAMPAIGN,
					ErrorType.ERROR_SERVICE_CAMPAIGN.getSystemMessage());
		}

		return jsonApiLeads;
	}

	private DetailLeadsResponseDto parseLeadResponseDto(String jsonResponseApiLead) {

		if (!StringUtils.isNotBlank(jsonResponseApiLead.trim())) {
			throw new NullPointerException();
		}
		DetailLeadsResponseDto detailLeadsResponseDto;
		try {
			detailLeadsResponseDto = new Gson().fromJson(jsonResponseApiLead, DetailLeadsResponseDto.class);
		} catch (Exception e) {
			throw new ConsumeApiDetailLeadCustomException(ErrorType.ERROR_SERVICE_CAMPAIGN);
		}
		return detailLeadsResponseDto;
	}

}
