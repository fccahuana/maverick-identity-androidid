package pe.interbank.maverick.identity.profile.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_EMPTY)
public class ProfileDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String customerName;
	private String phoneNumber;
	private String email;
	private String updatedEmail;
	private Boolean dataProtection;
	private Boolean enableFingerPrint;
	
}
