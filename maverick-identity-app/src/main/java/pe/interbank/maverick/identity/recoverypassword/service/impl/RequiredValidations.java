package pe.interbank.maverick.identity.recoverypassword.service.impl;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.exception.recoverypassword.RecoveryPasswordCustomException;
import pe.interbank.maverick.identity.repository.register.UserRepository;

@Component
public class RequiredValidations {

	@Autowired
	UserRepository userRepository;
	
	public void validationFieldNull(String field) {
		if(Constants.EMPTY.equals(field)){
			throw new NullPointerException();
		}
	}
	
	public void validationFormat(String dato, String regex, ErrorType errorType) {
		Pattern patterValidation = Pattern.compile(regex);
		Matcher matcherValidation = patterValidation.matcher(dato);
		if (!matcherValidation.matches())
			throw new RecoveryPasswordCustomException(errorType);
	}
	
	public void validateRegisteredUser(String userId) {		
		Optional<User> userById = userRepository.findById(userId);
		if (!userById.isPresent()) {
			throw new RecoveryPasswordCustomException(ErrorType.ERROR_UNREGISTERED_USER_FORGOT_PASSWORD);
		}
	}
}
