package pe.interbank.maverick.identity.oauth.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;

import pe.interbank.maverick.commons.core.model.user.User;
import pe.interbank.maverick.identity.config.TokenRequestBase;
import pe.interbank.maverick.identity.core.model.login.AccessToken;
import pe.interbank.maverick.identity.core.util.Constants;
import pe.interbank.maverick.identity.core.util.ErrorType;
import pe.interbank.maverick.identity.login.dto.LoginRequestDto;
import pe.interbank.maverick.identity.oauth.dto.TokenRequestDto;
import pe.interbank.maverick.identity.oauth.exception.TokenCustomException;

@Component
public class TokenServiceImpl  {

	@Autowired
	TokenRequestBase tokenRequestBase;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	@Qualifier("stringRedisTemplateLogin")
	StringRedisTemplate stringRedisTemplateLogin;

	@Value("${identity.token.expiration}")
	long tokenExpirationTime;

	Gson gson = new Gson();

	public AccessToken getRefreshToken(String userId, User user) {
		TokenRequestDto tokenRequestDto = tokenRequestBase.initTokenRequestDto(Constants.REFRESHTOKEN, user);
		MultiValueMap<String, String> body = tokenRequestDto.getBody();
		body.add(Constants.REFRESHTOKEN, getRefreshTokenOfRedis(userId));
		HttpEntity<?> httpEntity = new HttpEntity<>(body, tokenRequestDto.getHttpHeaders());
		ResponseEntity<String> responseOauth = restTemplate.exchange(tokenRequestDto.getUriAuth(), HttpMethod.POST,
				httpEntity, String.class);
		return gson.fromJson(responseOauth.getBody(), AccessToken.class);
	}
	
	private String getRefreshTokenOfRedis(String userId) {
		Map<Object,Object> map = stringRedisTemplateLogin.opsForHash().entries(userId);
		String hashRefreshToken = (String)map.get(Constants.REFRESH_TOKEN);
		if(null == hashRefreshToken) {
			throw new TokenCustomException(ErrorType.ERROR_GENERIC);
		}
		return hashRefreshToken;
	}
	
	public void updateTokenRedis(String userId, String accessToken, String refreshToken) {
		Map<String, String> tokenCache = new HashMap<>();
		tokenCache.put(Constants.ACCESS_TOKEN, accessToken);
		tokenCache.put(Constants.REFRESH_TOKEN, refreshToken);
		stringRedisTemplateLogin.opsForHash().putAll(userId,tokenCache);
		stringRedisTemplateLogin.expire(userId, tokenExpirationTime, TimeUnit.MINUTES);
	}
	
	public AccessToken getAccessToken(LoginRequestDto loginRequestDto, User user) {
		TokenRequestDto tokenRequestDto = tokenRequestBase.initTokenRequestDto("password", user);
 		MultiValueMap<String, String> body = tokenRequestDto.getBody();
 		body.add(Constants.USERNAME, loginRequestDto.getUsername());
 		body.add("password", user.getPassword());
 		HttpEntity<?> httpEntity = new HttpEntity<>(body, tokenRequestDto.getHttpHeaders());
 		ResponseEntity<String> responseOauth = restTemplate.exchange(tokenRequestDto.getUriAuth(), HttpMethod.POST,	httpEntity, String.class);
 		return gson.fromJson(responseOauth.getBody(), AccessToken.class);
 	}
	
	
}