package pe.interbank.maverick.identity.api.assi.databind.ldpd;

import com.fasterxml.jackson.databind.JsonNode;

import pe.interbank.maverick.commons.api.assi.databind.ApiAssiObjectDeserializer;
import pe.interbank.maverick.identity.api.assi.exception.LoansAgreementApiAssiException;

public class RegisterLdpdBodyDeSerializer extends ApiAssiObjectDeserializer<String> {
	private static final long serialVersionUID = -2629542383935766593L;
	
	public RegisterLdpdBodyDeSerializer() {
		super(String.class);
	}
	
	@Override
	protected String deserialize(JsonNode jsonNode) {
		if(jsonNode.isMissingNode())
			throw new LoansAgreementApiAssiException();
		return jsonNode.toString();
	}
}
