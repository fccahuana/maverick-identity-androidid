package pe.interbank.maverick.identity.api.assi.repository;

import java.util.Map;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;

public interface VisionCustomerRepository {

	String registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdServiceRequest,  Map<String, String> bean);
}
