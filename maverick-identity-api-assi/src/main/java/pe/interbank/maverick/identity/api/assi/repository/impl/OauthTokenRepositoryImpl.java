package pe.interbank.maverick.identity.api.assi.repository.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.api.assi.ApiAssiClient;
import pe.interbank.maverick.commons.api.assi.ApiAssiClientBuilder;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.ApiAssiProperties;
import pe.interbank.maverick.identity.api.assi.databind.token.OauthTokenServiceBodyDeserializer;
import pe.interbank.maverick.identity.api.assi.databind.token.OauthTokenServiceBodySerializer;
import pe.interbank.maverick.identity.api.assi.repository.OauthTokenRepository;
import pe.interbank.maverick.identity.api.assi.model.token.OauthTokenRequest;

@Repository
public class OauthTokenRepositoryImpl implements OauthTokenRepository {
	
	@Value("${identity.api.assi.tokenAssi.parameters}")
	String parameterOauthToken;
	
	private ApiAssiClient<OauthTokenRequest, String> apiAssiClient;
	
	public OauthTokenRepositoryImpl(ApiAssiProperties apiAssiProperties, RestTemplateBuilder restTemplateBuilder) {
		apiAssiProperties.getAuthentication().setBasicUser("Basic ".concat(apiAssiProperties.getAuthentication().getBasicUser()));
		apiAssiClient = new ApiAssiClientBuilder<>(OauthTokenRequest.class, String.class).properties(apiAssiProperties)
				.restTemplateBuilder(restTemplateBuilder)
				.serializer(new OauthTokenServiceBodySerializer(apiAssiProperties))
				.deserializer(new OauthTokenServiceBodyDeserializer())
				.endpoint(apiAssiProperties.getTokenAssi().getEndpoint())
				.methodType(apiAssiProperties.getTokenAssi().getMethodType())
				.subscriptionKey(apiAssiProperties.getTokenAssi().getSubscriptionKey())
				.contentType(apiAssiProperties.getTokenAssi().getContentType()).buildForObjectOutput();
	}
	
	@Override
	public String getTokenAssi(ConsumeApiRequestDto consumeApiRequestDto) {
		OauthTokenRequest oAuthTokenRequest = new OauthTokenRequest();
		oAuthTokenRequest.setBody(parameterOauthToken);
		return apiAssiClient.execute(consumeApiRequestDto,oAuthTokenRequest, null);
	}

}
