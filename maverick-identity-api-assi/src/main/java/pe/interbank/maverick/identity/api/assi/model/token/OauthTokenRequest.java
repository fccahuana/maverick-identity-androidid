package pe.interbank.maverick.identity.api.assi.model.token;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OauthTokenRequest {

	private String body;
}
