package pe.interbank.maverick.identity.api.assi.exception;

import org.springframework.http.HttpStatus;

import pe.interbank.maverick.commons.api.assi.exception.ApiAssiException;

public class LoansAgreementApiAssiException extends ApiAssiException{
	private static final long serialVersionUID = 1805024473996646394L;

	public LoansAgreementApiAssiException() {
		super(HttpStatus.INTERNAL_SERVER_ERROR, "03.01.09", "Unexpected error");
	}
}
