package pe.interbank.maverick.identity.api.assi;

import java.io.Serializable;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.commons.api.assi.config.AbstractApiAssiProperties;

@Getter
@Setter
@ConfigurationProperties("identity.api.assi")
public class ApiAssiProperties extends AbstractApiAssiProperties implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private OauthTokenService tokenAssi;
	private VisionCustomer clients;
	private VisionNoCustomer noClients;
	
	public static class OauthTokenService extends Service {
		private static final long serialVersionUID = 7099246928423935204L;
	}
	
	public static class VisionCustomer extends Service {
		private static final long serialVersionUID = -3498771065943590066L;
	}
	
	public static class VisionNoCustomer extends Service {
		private static final long serialVersionUID = -5371928597516312539L;
	}
}
