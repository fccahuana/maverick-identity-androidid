package pe.interbank.maverick.identity.api.assi.repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;

public interface OauthTokenRepository {
	String getTokenAssi(ConsumeApiRequestDto consumeApiRequestDto);
}
