package pe.interbank.maverick.identity.api.assi.repository.impl;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.api.assi.ApiAssiClient;
import pe.interbank.maverick.commons.api.assi.ApiAssiClientBuilder;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.ApiAssiProperties;
import pe.interbank.maverick.identity.api.assi.databind.ldpd.RegisterLdpdBodyDeSerializer;
import pe.interbank.maverick.identity.api.assi.databind.ldpd.RegisterLdpdBodySerializer;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;
import pe.interbank.maverick.identity.api.assi.repository.VisionNoCustomerRepository;

@Repository
public class VisionNoCustomerRepositoryImpl implements VisionNoCustomerRepository {

	private ApiAssiClient<RegisterLdpdRequest, String> apiNoCustomerVision;
	
	public VisionNoCustomerRepositoryImpl(ApiAssiProperties apiAssiProperties, RestTemplateBuilder restemplateBuilder) {
		apiNoCustomerVision = new ApiAssiClientBuilder<>(RegisterLdpdRequest.class, String.class)
				.properties(apiAssiProperties)
				.restTemplateBuilder(restemplateBuilder)
				.serializer(new RegisterLdpdBodySerializer(apiAssiProperties))
				.deserializer(new RegisterLdpdBodyDeSerializer())
				.endpoint(apiAssiProperties.getNoClients().getEndpoint())
				.methodType(apiAssiProperties.getNoClients().getMethodType())
				.subscriptionKey(apiAssiProperties.getNoClients().getSubscriptionKey())
				.contentType(apiAssiProperties.getNoClients().getContentType())
				.buildForObjectOutput();
			
	}
	
	@Override
	public String registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest) {
		return apiNoCustomerVision.execute(consumeApiRequestDto, registerLdpdRequest, null);
	}

	
}
