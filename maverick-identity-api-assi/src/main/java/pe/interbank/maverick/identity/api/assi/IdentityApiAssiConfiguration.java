package pe.interbank.maverick.identity.api.assi;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({ ApiAssiProperties.class})
public class IdentityApiAssiConfiguration {

}
