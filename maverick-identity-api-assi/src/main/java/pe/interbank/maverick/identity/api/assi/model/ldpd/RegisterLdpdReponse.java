package pe.interbank.maverick.identity.api.assi.model.ldpd;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterLdpdReponse {

	@SerializedName("codRespuesta")
	private String codRespuesta;
	
	@SerializedName("mensaje")
	private String mensaje;
}
