package pe.interbank.maverick.identity.api.assi.databind.token;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;

import pe.interbank.maverick.commons.api.assi.databind.ApiAssiSerializer;
import pe.interbank.maverick.identity.api.assi.ApiAssiProperties;
import pe.interbank.maverick.identity.api.assi.model.token.OauthTokenRequest;

public class OauthTokenServiceBodySerializer extends ApiAssiSerializer<OauthTokenRequest>{
	private static final long serialVersionUID = -2811631616860012473L;
	
	@SuppressWarnings("unused")
	private ApiAssiProperties esbApiProperties;

	public OauthTokenServiceBodySerializer(ApiAssiProperties esbApiProperties) {
		this.esbApiProperties = esbApiProperties;
	}

	@Override
	public void serialize(OauthTokenRequest bean, JsonGenerator gen) throws IOException {
		if (StringUtils.isNoneEmpty(bean.getBody()))
			serializeBody(bean, gen);
		
	}
	
	private void serializeBody(OauthTokenRequest bean, JsonGenerator gen) throws IOException {
		gen.writeRawValue(bean.getBody());
	}
}
