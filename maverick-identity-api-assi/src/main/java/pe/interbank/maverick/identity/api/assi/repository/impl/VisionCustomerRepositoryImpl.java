package pe.interbank.maverick.identity.api.assi.repository.impl;

import java.util.Map;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.web.util.UriComponentsBuilder;

import pe.interbank.maverick.commons.api.assi.ApiAssiClient;
import pe.interbank.maverick.commons.api.assi.ApiAssiClientBuilder;
import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.ApiAssiProperties;
import pe.interbank.maverick.identity.api.assi.databind.ldpd.RegisterLdpdBodyDeSerializer;
import pe.interbank.maverick.identity.api.assi.databind.ldpd.RegisterLdpdBodySerializer;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;
import pe.interbank.maverick.identity.api.assi.repository.VisionCustomerRepository;

@Repository
public class VisionCustomerRepositoryImpl implements VisionCustomerRepository {
	
	private ApiAssiClient<RegisterLdpdRequest, String> apiCustomerVision;
	private String endPointOriginal;
	
	public VisionCustomerRepositoryImpl(ApiAssiProperties apiAssiProperties, RestTemplateBuilder restemplateBuilder) {
		apiCustomerVision = new ApiAssiClientBuilder<>(RegisterLdpdRequest.class, String.class)
				.properties(apiAssiProperties)
				.restTemplateBuilder(restemplateBuilder)
				.serializer(new RegisterLdpdBodySerializer(apiAssiProperties))
				.deserializer(new RegisterLdpdBodyDeSerializer())
				.endpoint(apiAssiProperties.getClients().getEndpoint())
				.methodType(apiAssiProperties.getClients().getMethodType())
				.subscriptionKey(apiAssiProperties.getClients().getSubscriptionKey())
				.contentType(apiAssiProperties.getClients().getContentType())
				.buildForObjectOutput();
		endPointOriginal = apiAssiProperties.getClients().getEndpoint();				
	}
	
	@Override
	public String registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdServiceRequest,  Map<String, String> bean) {
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(endPointOriginal);
		apiCustomerVision.setEndpoint(builder.buildAndExpand(bean).toUriString());
		return apiCustomerVision.execute(consumeApiRequestDto, registerLdpdServiceRequest, null);
	}
}
