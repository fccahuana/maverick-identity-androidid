package pe.interbank.maverick.identity.api.assi.databind.token;

import com.fasterxml.jackson.databind.JsonNode;

import pe.interbank.maverick.commons.api.assi.databind.ApiAssiObjectDeserializer;
import pe.interbank.maverick.identity.api.assi.exception.LoansAgreementApiAssiException;

/**
 * 
 * @author victor alejandro saico justo :)
 *
 */
public class OauthTokenServiceBodyDeserializer extends ApiAssiObjectDeserializer<String> {
	private static final long serialVersionUID = -2629542383935766593L;

	public OauthTokenServiceBodyDeserializer() {
		super(String.class);
	}


	@Override
	protected String deserialize(JsonNode jsonNode) {
		if(jsonNode.isMissingNode())
			throw new LoansAgreementApiAssiException();
		return  jsonNode.toString();
	}
}
