package pe.interbank.maverick.identity.api.assi.model.token;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OauthTokenResponse {

	@SerializedName("access_token")
	private String accessToken;
	
	@SerializedName("token_type")
	private String tokenType;
	
	@SerializedName("expires_in")
	private int expiresIn;
	
	private String scope;
	
	private String jti;
}
