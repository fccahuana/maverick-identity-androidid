package pe.interbank.maverick.identity.api.assi.repository;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;

public interface VisionNoCustomerRepository {

	String registerLdpd(ConsumeApiRequestDto consumeApiRequestDto, RegisterLdpdRequest registerLdpdRequest);
}
