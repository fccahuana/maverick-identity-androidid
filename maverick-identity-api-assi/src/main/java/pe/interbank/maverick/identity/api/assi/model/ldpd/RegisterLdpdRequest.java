package pe.interbank.maverick.identity.api.assi.model.ldpd;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RegisterLdpdRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String acceptanceCode;
	private String cellPhoneNumber;
	private String terminalType;
	private String identityDocumentNumber;
	private String identityDocumentType;
	
}
