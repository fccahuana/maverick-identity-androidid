package pe.interbank.maverick.identity.api.assi.databind.ldpd;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;

import pe.interbank.maverick.commons.api.assi.databind.ApiAssiSerializer;
import pe.interbank.maverick.identity.api.assi.ApiAssiProperties;
import pe.interbank.maverick.identity.api.assi.model.ldpd.RegisterLdpdRequest;


public class RegisterLdpdBodySerializer extends ApiAssiSerializer<RegisterLdpdRequest> {
	private static final long serialVersionUID = -2811631616860012473L;
	
	@SuppressWarnings("unused")
	private ApiAssiProperties esbApiProperties;
	
	public RegisterLdpdBodySerializer(ApiAssiProperties esbApiProperties) {
		this.esbApiProperties = esbApiProperties;
	}
	
	@Override
	public void serialize(RegisterLdpdRequest bean, JsonGenerator json) throws IOException {
		json.writeStartObject();
		serializeBody(bean, json);
		json.writeEndObject();
	}
	
	private void serializeBody(RegisterLdpdRequest bean, JsonGenerator json) throws IOException {
		json.writeStringField("acceptanceCode", bean.getAcceptanceCode());
		json.writeStringField("cellPhoneNumber", bean.getCellPhoneNumber());
		json.writeStringField("terminalType", bean.getTerminalType());
		if (bean.getIdentityDocumentNumber() != null && !bean.getIdentityDocumentNumber().isEmpty())
			json.writeStringField("identityDocumentNumber", bean.getIdentityDocumentNumber());
		if (bean.getIdentityDocumentType() != null && !bean.getIdentityDocumentType().isEmpty())
			json.writeStringField("identityDocumentType", bean.getIdentityDocumentType());
	}
}
