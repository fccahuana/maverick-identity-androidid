package pe.interbank.maverick.identity.repository.generic;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.model.generic.Generic;


@Repository
public interface GenericRepository extends MongoRepository<Generic, String> {
}
