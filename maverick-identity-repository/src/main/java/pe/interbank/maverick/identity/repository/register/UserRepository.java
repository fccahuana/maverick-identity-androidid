package pe.interbank.maverick.identity.repository.register;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.commons.core.model.user.User;


@Repository
public interface UserRepository extends MongoRepository<User, String>{
	List<User> findByPhoneNumberId(String phoneNumberId);
	User findByUserIdAndPassword(String userId, String password);
	User findByUserId(String userId);
	List<User> findByFcmToken(String fcmToken);
}
