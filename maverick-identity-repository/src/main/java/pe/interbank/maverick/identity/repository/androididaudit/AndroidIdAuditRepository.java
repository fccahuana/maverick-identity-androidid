package pe.interbank.maverick.identity.repository.androididaudit;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.identity.core.model.androididaudit.AndroidIdAudit;


@Repository
public interface AndroidIdAuditRepository extends MongoRepository<AndroidIdAudit, String>{
	AndroidIdAudit findByAndroidId(String androidId);
}
