package pe.interbank.maverick.identity.repository.androididwhite;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.identity.core.model.androididwhite.AndroidIdWhite;


@Repository
public interface AndroidIdWhiteRepository extends MongoRepository<AndroidIdWhite, String>{
	AndroidIdWhite findByAndroidId(String androidId);
}
