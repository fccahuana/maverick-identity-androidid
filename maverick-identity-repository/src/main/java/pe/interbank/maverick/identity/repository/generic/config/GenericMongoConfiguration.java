package pe.interbank.maverick.identity.repository.generic.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.MongoConfiguration;

@Configuration
@Getter
@EnableMongoRepositories(basePackages = "pe.interbank.maverick.identity.repository.generic", mongoTemplateRef = "genericMongoTemplate")
public class GenericMongoConfiguration {

	@Value("${identity.mongodb.collection.generic}")
	String dbCollectionGeneric;

	@Bean
	public MongoTemplate genericMongoTemplate(MongoConfiguration mongoConfiguration) throws Exception {
		return mongoConfiguration.customMongoTemplate();
	}

}
