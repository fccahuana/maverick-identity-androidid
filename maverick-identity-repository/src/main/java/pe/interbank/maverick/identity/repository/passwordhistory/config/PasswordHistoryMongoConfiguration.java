package pe.interbank.maverick.identity.repository.passwordhistory.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.MongoConfiguration;

@Configuration
@EnableMongoRepositories(basePackages = "pe.interbank.maverick.identity.repository.passwordhistory", mongoTemplateRef = "passwordHistoryMongoTemplate")
@Getter
public class PasswordHistoryMongoConfiguration {

	@Value("${identity.mongodb.collection.passwordHistory}")
	String dbCollectionPasswordHistory;

	@Bean(name = "passwordHistoryMongoTemplate")
	public MongoTemplate passwordHistoryMongoTemplate(MongoConfiguration mongoConfiguration) throws Exception {
		return mongoConfiguration.customMongoTemplate();
	}

}
