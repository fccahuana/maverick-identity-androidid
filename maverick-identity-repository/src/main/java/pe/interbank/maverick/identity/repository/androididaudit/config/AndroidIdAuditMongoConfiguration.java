package pe.interbank.maverick.identity.repository.androididaudit.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.MongoConfiguration;

@Configuration
@EnableMongoRepositories(basePackages = "pe.interbank.maverick.identity.repository.androididaudit", mongoTemplateRef = "androidIdAuditMongoTemplate")
@Getter
public class AndroidIdAuditMongoConfiguration {

	@Value("${identity.mongodb.collection.androidIdAudit}")
	String dbCollectionAndroidIdAudit;
	       
	@Bean(name = "androidIdAuditMongoTemplate")
	public MongoTemplate androidIdAuditMongoTemplate(MongoConfiguration mongoConfiguration) throws Exception {
		return mongoConfiguration.customMongoTemplate();
	}

}
