package pe.interbank.maverick.identity.repository.androididwhite.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import lombok.Getter;
import pe.interbank.maverick.commons.core.config.MongoConfiguration;

@Configuration
@EnableMongoRepositories(basePackages = "pe.interbank.maverick.identity.repository.androididwhite", mongoTemplateRef = "androidIdWhiteMongoTemplate")
@Getter
public class AndroidIdWhiteMongoConfiguration {

	@Value("${identity.mongodb.collection.androidIdWhite}")
	String dbCollectionAndroidIdWhite;
	       
	@Bean(name = "androidIdWhiteMongoTemplate")
	public MongoTemplate androidIdWhiteMongoTemplate(MongoConfiguration mongoConfiguration) throws Exception {
		return mongoConfiguration.customMongoTemplate();
	}

}
