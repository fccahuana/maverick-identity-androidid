package pe.interbank.maverick.identity.repository.passwordhistory;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.interbank.maverick.identity.core.model.passwordhistory.PasswordHistory;

@Repository
public interface PasswordHistoryRepository extends MongoRepository<PasswordHistory, String> {
	
	List<PasswordHistory> findByUserId(String userId);
	long countByUserId (String userId);
	List<PasswordHistory> findFirst5ByUserId(String userId, Sort sort);
	
}
