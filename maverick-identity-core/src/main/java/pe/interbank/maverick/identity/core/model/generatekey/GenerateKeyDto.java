package pe.interbank.maverick.identity.core.model.generatekey;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenerateKeyDto {

	@JsonProperty("usuario_id")
	private String userId;
	private String id;
	private String email;
	@JsonProperty("phone_number")
	private String phoneNumber;
	@JsonProperty("phone_operator")
	private String carrier;
	@JsonProperty("send_method")
	private int sendMethod;
	private String channel;
	@JsonProperty("message_id")
	private String messageId;
	private List<ParameterKeyDto>parameters;

}
