package pe.interbank.maverick.identity.core.model.generatekey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenerateKeyResponseDto {

	private String token;
	private String sendKey;
	private String message;
	private String validToken;
}
