package pe.interbank.maverick.identity.core.model.androididwhite;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AndroidId {

	private List<LoginUser> users;
	private boolean blocked;
	
	public AndroidId() {
		this.users = new ArrayList<LoginUser>();
	}
}