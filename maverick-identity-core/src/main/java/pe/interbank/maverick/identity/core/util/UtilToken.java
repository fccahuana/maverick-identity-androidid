package pe.interbank.maverick.identity.core.util;

import java.io.IOException;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class UtilToken {
	
	private static final Logger logger = LoggerFactory.getLogger(UtilToken.class);
	
	public static String getUserIdFromJWT(String authorizationToken) {
        JsonNode jsonNode;
        try {
            Jwt jwt = JwtHelper.decode(authorizationToken.replaceFirst(Constants.PREFIX_TOKEN, ""));
            jsonNode = new ObjectMapper().readTree(jwt.getClaims());
            return jsonNode.get(Constants.TAG_USER_NAME).asText();
        } catch (IOException e) {
        	logger.error(e.getMessage());
        }
        return "";
    }
	
	public String getClaim(String authorizationToken, String claim) {
        JsonNode jsonNode;
        try {
            Jwt jwt = JwtHelper.decode(authorizationToken.replaceFirst("Bearer ", ""));
            jsonNode = new ObjectMapper().readTree(jwt.getClaims());
            return jsonNode.get(claim).asText();
        } catch (IOException e) {
        	logger.error("Error getting claim {}", claim, e);
        }
        return "";
    }

}
