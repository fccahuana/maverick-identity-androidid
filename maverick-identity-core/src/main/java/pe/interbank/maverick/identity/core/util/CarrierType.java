package pe.interbank.maverick.identity.core.util;

import lombok.Getter;

@Getter
public enum CarrierType {
	MOVISTAR("MOVISTAR","M","Movistar"),
	CLARO("CLARO","C","Claro"),
	ENTEL("ENTEL","E","Entel"),
	BITEL("BITEL","B","Bitel");
	
	private String name;
	private String id;
	private String formatName;
	
	CarrierType(String name, String id, String formatName) {
		this.name = name;
		this.id = id;
		this.formatName = formatName;
	}
	
}
