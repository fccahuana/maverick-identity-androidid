package pe.interbank.maverick.identity.core.model.passwordhistory;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Document(collection = "#{@passwordHistoryMongoConfiguration.getDbCollectionPasswordHistory()}")
public class PasswordHistory {
	
	
	private String userId;
	private String password;
	private long currentTimeDate;
}
