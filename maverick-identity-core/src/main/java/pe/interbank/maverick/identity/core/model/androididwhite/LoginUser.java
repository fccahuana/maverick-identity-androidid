package pe.interbank.maverick.identity.core.model.androididwhite;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class LoginUser {

	private String userId;
	private String lastLoginTimeDate;
	
}
