package pe.interbank.maverick.identity.core.model.generatekey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParameterKeyDto  {

	/**
	 * 
	 */
	private String key;
	private String value;
}
