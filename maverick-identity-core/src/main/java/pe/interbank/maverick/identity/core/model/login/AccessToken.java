package pe.interbank.maverick.identity.core.model.login;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AccessToken {

	@SerializedName("access_token")
	private String accesToken;
	@SerializedName("token_type")
	private String typeToken;
	@SerializedName("refresh_token")
	private String refreshToken;
	@SerializedName("expires_in")
	private int expiresIn;
	private String scope;
	private String organization;
}
