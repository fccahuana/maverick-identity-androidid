package pe.interbank.maverick.identity.core.model.androididaudit;

import lombok.Getter;
import lombok.Setter;
import pe.interbank.maverick.identity.core.model.androididwhite.LoginUser;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "#{@androidIdAuditMongoConfiguration.getDbCollectionAndroidIdAudit()}")
public class AndroidIdAudit {

	private String androidId;
	private List<LoginUser> users;
	private String blockedTimeDate;
	
}