package pe.interbank.maverick.identity.core.util;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ErrorType {
	
	ERROR_GENERIC(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Error interno",HttpStatus.INTERNAL_SERVER_ERROR),
	INVALID_REQUEST(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Request no válido",HttpStatus.BAD_REQUEST),
	NULLPOINTER_REQUEST(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR, "java.lang.NullPointerException", HttpStatus.INTERNAL_SERVER_ERROR),
	INVALID_STEP_ONE_HASH(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Hash generado en el paso 1 es invalido",HttpStatus.FORBIDDEN),
	INVALID_STEP_TWO_HASH(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Hash generado en el paso 2 es invalido",HttpStatus.FORBIDDEN),
	INVALID_STEP_THREE_HASH(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Hash generado en el paso 3 es invalido",HttpStatus.FORBIDDEN),
	INVALID_PHONE_NUMBER(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"No se pudo obtener el teléfono",HttpStatus.FORBIDDEN),
	INVALID_CARRIER(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"No se pudo obtener el carrier",HttpStatus.FORBIDDEN),
	ERROR_DUPLICATE_REGISTER_MONGO(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"El _id ya se encuentra insertado en la colección.",HttpStatus.CONFLICT),
	CANNOT_GENERATE_QUESTIONS(ErrorConstants.CODE_GENERIC_ERROR,ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"No se pudo generar preguntas",HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_EXISTING_USER("01.06.06",ErrorConstants.USER_REGISTERED,"El número de DNI ingresado ya se encuentra registrado. Inicia sesión para ingresar o inténtalo con un número de DNI diferente.","El DNI ingresado ya se encuentra registrado en la colección.",HttpStatus.CONFLICT),
	ERROR_ONLY_CLIENTS(ErrorConstants.CODE_01_06_07,ErrorConstants.TITLE_01_06_07,ErrorConstants.USER_MESSAGE_01_06_07,"El cliente no tiene un credito vigente.",HttpStatus.FORBIDDEN),
	ERROR_WRONG_PHONE_FORMAT("01.06.10","Celular Inválido","Por favor ingresa un número de celular válido.","El celular ingresado no hace match con el regex definido.",HttpStatus.BAD_REQUEST),
	ERROR_WRONG_DNI_FORMAT("01.06.08",ErrorConstants.INVALID_DNI,"Por favor ingresa un número de DNI válido.","El DNI ingresado no hace match con el regex definido.",HttpStatus.BAD_REQUEST),
	ERROR_WRONG_PASSWORD_LENGTH("01.06.11",ErrorConstants.INVALID_PASS,"La contraseña debe tener al menos 8.",ErrorConstants.INVALID_REGEX,HttpStatus.BAD_REQUEST),
	ERROR_WRONG_PASSWORD_HAS_NUMBER("01.06.12",ErrorConstants.INVALID_PASS,"La contraseña debe tener al menos 1 número.",ErrorConstants.INVALID_REGEX,HttpStatus.BAD_REQUEST),
	ERROR_WRONG_PASSWORD_HAS_LETTER("01.06.13",ErrorConstants.INVALID_PASS,"La contraseña debe tener al menos 1 letra.",ErrorConstants.INVALID_REGEX,HttpStatus.BAD_REQUEST),
	ERROR_WRONG_PASSWORD_HAS_SPECIAL_CHARACTERS("01.06.14",ErrorConstants.INVALID_PASS,"La contraseña no debe contener caracteres especiales.",ErrorConstants.INVALID_REGEX,HttpStatus.BAD_REQUEST),
	ERROR_WRONG_PASSWORD_ACCEPT_TERMS_CONDITIONS("01.06.15",ErrorConstants.USER_REGISTERED,"Por favor acepta los términos y condiciones.","El check de aceptar términos y condiciones es falso.",HttpStatus.FORBIDDEN),
	
	ERROR_UNREGISTERED_USER("01.06.01",ErrorConstants.USER_NOT_REGISTERED,"El número de DNI ingresado no se encuentra registrado. Si eres cliente actual de Crédito por Convenio puedes crear una cuenta ahora.","El DNI no existe en la colección.",HttpStatus.FORBIDDEN),
	ERROR_NOT_ACTIVE_ACCOUNT("01.06.02","No se ha podido validar tu identidad","Por el momento no podrás ingresar a la aplicación de Crédito por Convenio.","El campo status de la colección es E para el DNI ingresado.",HttpStatus.FORBIDDEN),
	ERROR_BLOCKED_ACCOUNT("01.06.03","Cuenta bloqueada","Has superado el límite de intentos. Para acceder a tu cuenta, puedes restablecer tu contraseña.","El campo attemps de la colección es mayor a lo permitido.",HttpStatus.FORBIDDEN),
	ERROR_NOT_ANSWER_QUESTIONS("01.06.04","Usuario no paso el flujo de validación de identidad","Redirigir a pantalla de validación de identidad.","El campo status de la colección es R para el DNI ingresado.",HttpStatus.FORBIDDEN),
	ERROR_INCORRECT_PASSWORD_OR_USER("01.06.05","Número de DNI o contraseña incorrecta","El número de DNI y contraseña no coinciden. Por favor inténtalo nuevamente.","Número de DNI o contraseña ingresada no corresponde con lo registrado en la colección.",HttpStatus.UNAUTHORIZED),
	
	ERROR_MAX_ATTEMPS_IDENTITY_VALIDATION("01.06.16","Has alcanzado el número máximo de intentos","Puedes intentarlo nuevamente mañana.","La cantidad de intentos ha superado la cantidad permitida.",HttpStatus.FORBIDDEN),
	ERROR_VALIDATION_CODE_IDENTITY_VALIDATION("01.06.17","El código ingresado es incorrecto o ha expirado","Te sugerimos: Copiar el código que aparece en el mensaje de texto (SMS) y permanecer en el App de Crédito por Convenios hasta culminar tu registro.","El código es inválido o ha transcurrido mucho tiempo desde su solicitud",HttpStatus.FORBIDDEN),
	ERROR_SERVICE_GET_PHONE_RM("01.06.18",ErrorConstants.ERROR,ErrorConstants.GENERIC_ERROR,"Error al invocar el servicio RM",HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_SERVICE_CLIENT_DOES_HAVE_CELL_PHONE("01.06.27","No tienes ningún celular registrado","Para continuar con la validación de identidad actualiza tus datos llamando al (01) 311-9006.","El cliente no tiene un celular registrado en RM",HttpStatus.FORBIDDEN),
	
	ERROR_UNREGISTERED_USER_FORGOT_PASSWORD("01.06.19",ErrorConstants.USER_NOT_REGISTERED,"El número de DNI ingresado no se encuentra registrado. Si eres cliente actual de Crédito por Convenio puedes crear una cuenta ahora.","El DNI no existe en la colección.",HttpStatus.FORBIDDEN),
	ERROR_MAX_ATTEMPS_FORGOT_PASSWORD("01.06.20","Has alcanzado el número máximo de intentos","Puedes intentarlo nuevamente mañana.","La cantidad de intentos ha superado la cantidad permitida.",HttpStatus.FORBIDDEN),
	ERROR_WRONG_DNI_FORMAT_FORGOT_PASSWORD("01.06.21",ErrorConstants.INVALID_DNI,"Por favor ingresa un número de DNI válido.","El DNI ingresado no hace match con el regex definido.",HttpStatus.BAD_REQUEST),
	ERROR_VALIDATION_CODE("01.06.22","El código ingresado es incorrecto o ha expirado","Te sugerimos: Copiar el código que aparece en el mensaje de texto (SMS) y permanecer en el App de Crédito por Convenios hasta culminar el cambio de contraseña.","El código es inválido o ha transcurrido mucho tiempo desde su solicitud",HttpStatus.FORBIDDEN),
	ERROR_PASSWORD_USED_BEFORE("01.06.23","Has utilizado esta contraseña anteriormente","Por tu seguridad, ingresa una contraseña que no hayas utilizado antes.","La contraseña ha sido recientemente usada",HttpStatus.FORBIDDEN),
	ERROR_USER_NOT_PHONENUMBER_RECOVERY_PASSWORD("01.06.28","No tienes ningún celular registrado","Para continuar, actualiza tus datos llamando al (01) 311-9006.","El cliente no tiene un celular registrado en RM",HttpStatus.FORBIDDEN),
	NO_EXTENSION_CAMPAIGN("01.06.25", ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"El cliente no cuenta con una campaña.", HttpStatus.FORBIDDEN),
	ERROR_SERVICE_CAMPAIGN("01.06.26", ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"Ocurrió un error en el servicio de leads", HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_GET_QUESTIONS("01.06.35",ErrorConstants.ERROR,"Ocurrió un error al mostrar preguntas de validación","No se pudo obtener preguntas del servicio de Equifax",HttpStatus.FORBIDDEN),
	ERROR_VALIDATION_QUESTIONS("01.06.36",ErrorConstants.ERROR,"Ocurrió un error en el servicio de validación de identidad","Ocurrió un error en el servicio de validacion de equifax",HttpStatus.FORBIDDEN),
	ERROR_INCORRECT_VALID_QUESTIONS("01.06.37","Una o más preguntas son incorrectas","Te queda [attempts] intento para poder validar tu identidad","Falló al momento de validar preguntas en servicio Equifax",
			HttpStatus.FORBIDDEN),
	ERROR_INCORRECT_ALL_ATTEMPTS_VALID_QUESTIONS("01.06.38","No hemos podido validar tu identidad","Por el momento no podrás ingresar al app. Inténtalo nuevamente el siguiente mes",
			"Falló todos los intentos al validar preguntas en servicio de Equifax",HttpStatus.FORBIDDEN),
	ERROR_EXISTING_PHONENUMBER("01.06.39","Celular registrado","El número de celular ingresado ya se encuentra registrado. Intenta ingresar un número de celular diferente.","El celular ingresado ya existe en la coleccion.",HttpStatus.FORBIDDEN),
	ERROR_INCORRECT_CURRENT_PASSWORD("01.06.40","La constraseña actual es incorrecta","La constraseña actual es incorrecta","La contraseña actual no coincide con el registrado en coleccion",HttpStatus.FORBIDDEN),
	ERROR_INCORRECT_LOGIN_FINGERPRINT("01.06.41","Ingreso con huella digital incorrecto","Al parecer tu huella digital no fue detectada correctamente. Por favor, inténtalo nuevamente.","El fingerprintId no coincide con el registrado en la coleccion",HttpStatus.FORBIDDEN),
	ERROR_PHONENUMBER_IS_IN_FRAUD_LIST("01.06.49","Este número de celular no puede ser registrado","Para mayor información puedes comunicarte al (01) 311-9006.","El celular ingresado se encuentra en fraud list.",HttpStatus.FORBIDDEN),
	ERROR_SERVICE_EQUIFAX(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"Ocurrió un error en el servicio de Equifax", HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_SAVE_NAMES_IN_REDIS(ErrorConstants.CODE_GENERIC_ERROR, ErrorConstants.ERROR, ErrorConstants.GENERIC_ERROR,
			"Ocurrió un error al guardar los nombres en redis", HttpStatus.INTERNAL_SERVER_ERROR),
	ERROR_DNI_IN_FRAUD(ErrorConstants.CODE_01_06_07,ErrorConstants.TITLE_01_06_07,ErrorConstants.USER_MESSAGE_01_06_07,"El dni del cliente se encuentra bloqueado en la coleccion.",HttpStatus.FORBIDDEN);
	
	private String code;
	private String title;
	private String userMessage;
	private String systemMessage;
	private HttpStatus httpStatus;
	
	ErrorType(String code,String title, String userMessage, String systemMessage, HttpStatus httpStatus) {
		this.code = code;
		this.title = title;
		this.userMessage = userMessage;
		this.systemMessage = systemMessage;
		this.httpStatus=httpStatus;
	}
	
	private static class ErrorConstants {
        public static final String ERROR                  = "Error";
        public static final String GENERIC_ERROR          = "Ups, ocurrio un error inesperado.";
        public static final String INVALID_PASS           = "Contraseña Inválida";
        public static final String INVALID_DNI            = "DNI Inválido";
        public static final String USER_NOT_REGISTERED    = "Usuario no registrado";
        public static final String CODE_GENERIC_ERROR     = "01.06.99";
        public static final String INVALID_REGEX          = "La contraseña ingresada no hace match con el Regex definido.";
		public static final String USER_REGISTERED 		  = "Usuario registrado";
        public static final String CODE_01_06_07          = "01.06.07";
        public static final String TITLE_01_06_07         = "Por el momento no podrás registrarte";
        public static final String USER_MESSAGE_01_06_07  = "Esta aplicación es solo para clientes con un Crédito por Convenio vigente o una campaña pre aprobada.";
        
	}
	
}