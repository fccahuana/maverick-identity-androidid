package pe.interbank.maverick.identity.core.util;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.interbank.maverick.commons.core.dto.ConsumeApiRequestDto;


public class Util {

	static DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd/MM");
	static DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");
	static final DateTimeFormatter dateTimeFormatter2 = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	
	private static final Logger logger = LoggerFactory.getLogger(Util.class);

	private Util() {

	}

	public static int getSecondsCurrentTime() {

		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(Constants.ZONE));
		return localDateTime.getHour() * 3600 + localDateTime.getMinute() * 60 + localDateTime.getSecond();
	}

	public static String ofuscateField(String cadena, Integer nroCaracteresLibres, Character caracterMascara,
			boolean libresAlFinal) {
		Integer cantidad = cadena.length() - nroCaracteresLibres;
		if (cantidad < 0) {
			cantidad = cadena.length();
		}
		String ofuscador = StringUtils.repeat(caracterMascara.toString(), cantidad);
		if (libresAlFinal) {
			return StringUtils.overlay(cadena, ofuscador, 0, cantidad);
		} else {
			return StringUtils.overlay(cadena, ofuscador, nroCaracteresLibres, cadena.length());
		}
	}

	public static String ofuscateFieldValidation(String cadena, Character caracterMascara,
			Integer nroCaracteresLibresInicio, Integer nroCaracteresLibresFinal) {
		Integer cantidad = cadena.length() - (nroCaracteresLibresInicio + nroCaracteresLibresFinal);
		String ofuscador = StringUtils.repeat(caracterMascara.toString(), cantidad);
		StringBuilder sb = new StringBuilder(cadena.substring(0, nroCaracteresLibresInicio));
		sb.append(ofuscador).append(cadena.substring(cadena.length() - nroCaracteresLibresFinal));
		return sb.toString();
	}
	
	public static String ofuscateFieldMail (String cadena, Character caracterMascara, Integer nroCaracteresLibresInicio) {
		int arrobaIndex = cadena.indexOf('@');
		int nroCaracteresLibresFinal = cadena.length() - arrobaIndex;
		return ofuscateFieldValidation(cadena, caracterMascara, nroCaracteresLibresInicio, nroCaracteresLibresFinal);
	}

	public static String generateSessionId() {
		String uuid = UUID.randomUUID().toString();
		String time = String.valueOf(Calendar.getInstance().getTime().getTime());
		return uuid.concat("-" + time);
	}

	public static long getLongCurrentDate() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(Constants.ZONE));
		ZonedDateTime zdt = localDateTime.atZone(ZoneId.of(Constants.ZONE));
		return zdt.toInstant().toEpochMilli();
	}

	public static String getDateForLong(long millisecondsSinceEpoch) {
		Instant instant = Instant.ofEpochMilli(millisecondsSinceEpoch);
		ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.of(Constants.ZONE));
		return formatterDate.format(zdt);
	}

	public static String getHourForLongDate(long millisecondsSinceEpoch) {
		Instant instant = Instant.ofEpochMilli(millisecondsSinceEpoch);
		ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.of(Constants.ZONE));
		return formatterTime.format(zdt);
	}
	
	public static String getMessageId() {
		String uuid = UUID.randomUUID().toString();
		String time = String.valueOf(Calendar.getInstance().getTime().getTime());
		return uuid.concat("_" + time);
	}
	
	public static ConsumeApiRequestDto setRequestApi(String messageId, String userId, String sessionId) {
		ConsumeApiRequestDto consumeApiRequestDto = new ConsumeApiRequestDto();
		consumeApiRequestDto.setMessageId(messageId);
		consumeApiRequestDto.setUserId(userId);
		consumeApiRequestDto.setSessionId(sessionId);
		return consumeApiRequestDto;
	}
	
	public static String getUserIdFromJWT(String authorizationToken) {
		JsonNode jsonNode;
		try {
			Jwt jwt = JwtHelper.decode(authorizationToken.replaceFirst(Constants.PREFIX_TOKEN, ""));
			jsonNode = new ObjectMapper().readTree(jwt.getClaims());
			return jsonNode.get(Constants.TAG_USER_NAME).asText();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
		return "";
	}
	
	public static String getFingerPrintHash(String fcmtoken, String userId, String fingerPrint) {
		StringBuilder fingerKey = new StringBuilder();
		fingerKey.append(fcmtoken).append(userId).append(fingerPrint);
		return DigestUtils.sha256Hex(fingerKey.toString());
	}
	
	public static String getCurrentTimeDate() {
		LocalDateTime localDateTime = LocalDateTime.now(ZoneId.of(Constants.ZONE));
		return localDateTime.format(dateTimeFormatter2);
	}

}
