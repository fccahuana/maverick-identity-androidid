package pe.interbank.maverick.identity.core.util;

public final class Constants {

	public static final String EMPTY                             = "";
	public static final String SPACE                                               = " ";
	public static final String STEP1				             = "STEP1";
	public static final String STEP2				             = "STEP2";
	public static final String PHONE				             = "PHONE";
	public static final String EMAIL				             = "EMAIL";
	public static final String CARRIER				             = "CARRIER";
	public static final String PREFIX_TOKEN                      = "Bearer ";
    public static final String TAG_USER_NAME                     = "user_name";
    public static final String ACCESS_TOKEN                      = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN                     = "REFRESH_TOKEN";
    public static final String ZONE                              = "America/Lima";
    public static final String GRANT_TYPE                        = "grant_type";
    public static final String USERNAME                          = "username";
    public static final String TRUE                              = "true";
    public static final Integer VISITS                           = 3;
    public static final String TOKEN                             = "TOKEN";
    public static final String FLOW_IDENTITY_VALIDATION          = "IDENTITY_VALIDATION";
    public static final String FLOW_RECOVERY_PASS                = "FLOW_RECOVERY_PASSWORD";
    public static final String SMS_LABEL                         = "sms";
    public static final String PROVIDER_CODE_NOT_CREDITS         = "0009";
    public static final String PROVIDER_CODE_NOT_DETAILS_CREDIT  = "0010";
    public static final String CREDIT_CANCELED                   = "Cancelado";
    public static final String CREDIT_DISBURSED                  = "SinDesembolso";
    public static final String REFRESHTOKEN                      = "refresh_token";
    public static final String CUSTOMER_ID                       = "customerId";
    public static final String FAMILY                            = "family";
	private Constants() {

	}


}
