package pe.interbank.maverick.identity.core.model.androididwhite;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Document(collection = "#{@androidIdWhiteMongoConfiguration.getDbCollectionAndroidIdWhite()}")
public class AndroidIdWhite {
	
	@Id
	private String androidId;
	private String lastLoginTimeDate;
}
